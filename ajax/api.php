<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?

//if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    \Bitrix\Main\Loader::includeModule("rdn");
    $request = $_REQUEST;
    $return['success'] = false;
    switch ($request['action']){
        case 'company_info':
            if(!empty($request['inn'])){
                $isset_company = \rdn\Helper::getCompanyByInnKpp($request['inn'],$request['kpp']);
                if($isset_company) {
					$validComp=\rdn\Helper::CheckCompanyAttr($isset_company);
                    $return['success'] = true;
                    $return['isset_company'] = $isset_company;
					$return['need_address'] = !$validComp['is_ok'];;
                }
                else{
                    $company = \rdn\Api::company_info($request['inn'],$request['kpp']);
                    if(!empty($company)){
						$validComp=\rdn\Helper::CheckCompanyAttr($company);
                    	$return['success'] = true;
                        $return['data'] = $company[0];
                        $return['isset_company'] = false;
						$return['need_address'] = !$validComp['is_ok'];
                    }
                }
            }

        break;
        case 'add_company':

            if($request['isset_company'] == 'Y'){
                $return['success'] = true;
            }
            else{
                $data = $request['data'];
                $company = \rdn\Api::add_company($data);

                if(count($company) > 3){
                    $return['success'] = true;
                    $return['data'] = $company;
                }
                elseif(!empty($company)){
                    $return['error'] = $company;
                }
            }
        break;

        case 'bind_company':
            $return = \rdn\Helper::addUserToCompany($request['id']);
        break;
        case 'unbind_company':
            $return = \rdn\Helper::removeUserFromCompany($request['id']);
            break;
        case 'editor_request':
            $return = \rdn\Helper::editorUserRequest($request['id'],$request['user']);
            break;

        case 'site_company_info':
            $data = \rdn\Helper::getCompanyById($request['id']);
            if(!empty($data)){
                $return['success'] = true;
                $return['data'] = $data;
            }
        break;
		case 'set_done':///ajax/api.php?action=set_done&id=123
			$return = \rdn\Helper::setDone($request['id']);
		break;
		case 'send_phone':///ajax/api.php?action=set_done&id=123
			$return = \rdn\Helper::sendPhone($request['phone']);
		break;
		case 'send_sms':///ajax/api.php?action=set_done&id=123
			$return = \rdn\Helper::sendSms($request['phone']);
		break;

    }
    echo json_encode($return);
//}