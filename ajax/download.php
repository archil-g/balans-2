<?php
/**
 * Created by PhpStorm.
 * User: starikov
 * Date: 03.02.2020
 * Time: 14:45
 */
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule("rdn");
//var_dump($_REQUEST);
if(!$USER->IsAuthorized()) {
	LocalRedirect("/cabinet/");
}
$userId = $USER->GetID();
$arFilter = [
	'IBLOCK_ID' => '56',
	'CREATED_BY' => (int) $userId,
	'ID'=>(int) $_REQUEST['id']
];

$rs = \CIBlockElement::GetList(['SORT' => 'asc'], $arFilter, false, false, ["ID", "PREVIEW_TEXT", "DETAIL_TEXT",'DATE_CREATE','PROPERTY_CONTRACT_NUMBER','PROPERTY_REP_PERIOD','PROPERTY_PRODUCT','PROPERTY_OFFER','PROPERTY_PRODUCT.NAME','PROPERTY_OFFER.NAME']);
$order = $rs->Fetch();
//var_dump($order);
$order=json_decode($order['PREVIEW_TEXT'],1);
$contractId=$order[0]['contract_id'];
if(!empty($contractId)){
	$type=preg_replace('/[^a-z]/','',$_REQUEST['doc']);
	$ret = getDoc('https://api.balans2.ru/php/bitrix/doc?id=' . (int)$contractId.'&type='.$type);
	if($ret['info']['http_code']=='200' && filesize($ret['file'])){
		//COption::SetOptionString('webprostor.smtp', 's1_PRIORITY', '1');
		COption::SetOptionString('webprostor.smtp', 's1_PRIORITY', '');
		custom_mail('internet@oviont.ru', 'ОК получения документа id: '.$contractId,json_encode($ret,JSON_UNESCAPED_UNICODE));
		header('Content-Disposition: attachment; filename="' . md5(mt_rand(99, 9999) . time()) . '.pdf"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize($ret['file']));
		header('Content-Type: application/pdf');
		readfile($ret['file']);
	}else{
		COption::SetOptionString('webprostor.smtp', 's1_PRIORITY', '1');
		$json=json_decode(file_get_contents($ret['file']),1);
		if(json_last_error()){
			custom_mail('internet@oviont.ru', 'Ошибка получения документа id: '.$contractId,json_encode($ret,JSON_UNESCAPED_UNICODE));
			?>
			Ошибка получения ответа от сервера документов, попробуйте позже.
			<?php
		}else{
			if($json['error']){
				echo $json['error'];
			}
			custom_mail('internet@oviont.ru', 'Ошибка получения документа id: '.$contractId,json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		COption::SetOptionString('webprostor.smtp', 's1_PRIORITY', '');
	}
}else{?>
	Документы не найдены или у Вас нет доступа.
<?php }

function getDoc($url) {
	$tmpfname = tempnam('/tmp', "doc_");
	$handle = fopen($tmpfname, "w");
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
	curl_setopt($ch, CURLOPT_FAILONERROR, 0);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_USERAGENT, 'User-Agent: Mozilla/5.0 (compatible; MSIE 6.0; Widows NT)');
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	// Нужно пробросить порт и запустить fiddler curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8889');
	$urlInfo=parse_url($url);
	if(isset($urlInfo['port']))curl_setopt ($ch, CURLOPT_PORT , $urlInfo['port']);
	curl_setopt($ch, CURLOPT_FILE, $handle);
	curl_setopt($ch, CURLOPT_TIMEOUT, 15); // times out after 4s
	$result = curl_exec($ch); // run the whole process
	$header=curl_getinfo($ch);
	curl_close($ch);
	fclose($handle);
	return ['file'=>$tmpfname,'info'=>$header];
}
// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>