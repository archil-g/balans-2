<?

// добавляем тип для инфоблока
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CUserTypeFileCustom", "GetUserTypeDescription_"));
// добавляем тип для главного модуля
AddEventHandler("main", "OnUserTypeBuildList", array("CUserTypeFileCustom", "GetUserTypeDescription_"));

class CUserTypeFileCustom
{
    static $n = 0;
    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => "filemy",
            "USER_TYPE" => "filemy",
            "CLASS_NAME" => "CUserTypeFileCustom",
            "DESCRIPTION" => "Название описание картинка",
            "BASE_TYPE" => "file",
        );
    }

    function GetUserTypeDescription_()
    {
        return array(
            "DESCRIPTION" => "Название описание картинка",
            "PROPERTY_TYPE" => "N",
            "USER_TYPE" => "filemy",
            "CLASS_NAME" => "CUserTypeFileCustom",
            "BASE_TYPE" => "file",
            "GetAdminListViewHTML" => array("CUserTypeFileCustom","GetAdminListViewHTML"),
            "GetPropertyFieldHtml" => array("CUserTypeFileCustom","GetEditFormHTML"),
           // "PrepareSettings" => array("CUserTypeFileCustom","PrepareSettings"),

            'ConvertToDB' => array('CUserTypeFileCustom', 'OnBeforeSave'),
            'ConvertFromDB' => array('CUserTypeFileCustom', 'ConvertFromDB'),
        );
    }


    function GetDBColumnType($arUserField)
    {
        global $DB;
        switch(strtolower($DB->type))
        {
            case "mysql":
                return "int(18)";
            case "oracle":
                return "number(18)";
            case "mssql":
                return "int";
        }
    }

    function PrepareSettings($arUserField)
    {
        $size = intval($arUserField["SETTINGS"]["SIZE"]);
        $ar = array();

        if(is_array($arUserField["SETTINGS"]["EXTENSIONS"]))
            $ext = $arUserField["SETTINGS"]["EXTENSIONS"];
        else
            $ext = explode(",", $arUserField["SETTINGS"]["EXTENSIONS"]);

        foreach($ext as $k=>$v)
        {
            if ($v === true)
                $v = trim($k);
            else
                $v = trim($v);
            if(strlen($v) > 0)
                $ar[$v] = true;
        }

        return array(
            "SIZE" =>  ($size <= 1? 20: ($size > 255? 225: $size)),
            "LIST_WIDTH" => intval($arUserField["SETTINGS"]["LIST_WIDTH"]),
            "LIST_HEIGHT" => intval($arUserField["SETTINGS"]["LIST_HEIGHT"]),
            "MAX_SHOW_SIZE" => intval($arUserField["SETTINGS"]["MAX_SHOW_SIZE"]),
            "MAX_ALLOWED_SIZE" => intval($arUserField["SETTINGS"]["MAX_ALLOWED_SIZE"]),
            "EXTENSIONS" => $ar
        );
    }

    function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
    {
        $result = '';

        if($bVarsFromForm)
            $value = intval($GLOBALS[$arHtmlControl["NAME"]]["SIZE"]);
        elseif(is_array($arUserField))
            $value = intval($arUserField["SETTINGS"]["SIZE"]);
        else
            $value = 20;
        $result .= '
		<tr>
			<td>'.GetMessage("USER_TYPE_FILE_SIZE").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[SIZE]" size="20"  maxlength="20" value="'.$value.'">
			</td>
		</tr>
		';
        if($bVarsFromForm)
            $width = intval($GLOBALS[$arHtmlControl["NAME"]]["LIST_WIDTH"]);
        elseif(is_array($arUserField))
            $width = intval($arUserField["SETTINGS"]["LIST_WIDTH"]);
        else
            $width = 200;
        if($bVarsFromForm)
            $height = intval($GLOBALS[$arHtmlControl["NAME"]]["LIST_HEIGHT"]);
        elseif(is_array($arUserField))
            $height = intval($arUserField["SETTINGS"]["LIST_HEIGHT"]);
        else
            $height = 200;
        $result .= '
		<tr>
			<td>'.GetMessage("USER_TYPE_FILE_WIDTH_AND_HEIGHT").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[LIST_WIDTH]" size="7"  maxlength="20" value="'.$width.'">
				&nbsp;x&nbsp;
				<input type="text" name="'.$arHtmlControl["NAME"].'[LIST_HEIGHT]" size="7"  maxlength="20" value="'.$height.'">
			</td>
		</tr>
		';
        if($bVarsFromForm)
            $value = intval($GLOBALS[$arHtmlControl["NAME"]]["MAX_SHOW_SIZE"]);
        elseif(is_array($arUserField))
            $value = intval($arUserField["SETTINGS"]["MAX_SHOW_SIZE"]);
        else
            $value = 0;
        $result .= '
		<tr>
			<td>'.GetMessage("USER_TYPE_FILE_MAX_SHOW_SIZE").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[MAX_SHOW_SIZE]" size="20"  maxlength="20" value="'.$value.'">
			</td>
		</tr>
		';
        if($bVarsFromForm)
            $value = intval($GLOBALS[$arHtmlControl["NAME"]]["MAX_ALLOWED_SIZE"]);
        elseif(is_array($arUserField))
            $value = intval($arUserField["SETTINGS"]["MAX_ALLOWED_SIZE"]);
        else
            $value = 0;
        $result .= '
		<tr>
			<td>'.GetMessage("USER_TYPE_FILE_MAX_ALLOWED_SIZE").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[MAX_ALLOWED_SIZE]" size="20"  maxlength="20" value="'.$value.'">
			</td>
		</tr>
		';
        if($bVarsFromForm)
        {
            $value = htmlspecialcharsbx($GLOBALS[$arHtmlControl["NAME"]]["EXTENSIONS"]);
            $result .= '
			<tr>
				<td>'.GetMessage("USER_TYPE_FILE_EXTENSIONS").':</td>
				<td>
					<input type="text" size="20" name="'.$arHtmlControl["NAME"].'[EXTENSIONS]" value="'.$value.'">
				</td>
			</tr>
			';
        }
        else
        {
            if(is_array($arUserField))
                $arExt = $arUserField["SETTINGS"]["EXTENSIONS"];
            else
                $arExt = "";
            $value = array();
            if(is_array($arExt))
                foreach($arExt as $ext=>$flag)
                    $value[] = htmlspecialcharsbx($ext);
            $result .= '
			<tr>
				<td>'.GetMessage("USER_TYPE_FILE_EXTENSIONS").':</td>
				<td>
					<input type="text" size="20" name="'.$arHtmlControl["NAME"].'[EXTENSIONS]" value="'.implode(", ", $value).'">
				</td>
			</tr>
			';
        }
        return $result;
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
            \CJSCore::Init(array('jquery'));
            $arHtmlControl["NAME"] = "PROP[" . $arUserField['ID'] . "]";
            CModule::IncludeModule("fileman");

            $arHtmlControl["VALIGN"] = "middle";
            $arHtmlControl["ROWCLASS"] = "adm-detail-file-row";

            if (($p = strpos($arHtmlControl["NAME"], "[")) > 0)
                $strOldIdName = substr($arHtmlControl["NAME"], 0, $p) . "_old_id" . substr($arHtmlControl["NAME"], $p);
            else
                $strOldIdName = $arHtmlControl["NAME"] . "_old_id";


            //if(is_array($val))
            $inputName[$arHtmlControl["NAME"] . "[n" . self::$n . "][IMAGE]"] = $arHtmlControl['VALUE']['IMAGE'];
            //else
            //     $inputName[$arHtmlControl["NAME"]."[".0."]"] = $val;


            $name = $arHtmlControl["NAME"] . "[n" . self::$n . "][TITLE]";
            $desc = $arHtmlControl["NAME"] . "[n" . self::$n . "][DESC]";
            $file = $arHtmlControl["NAME"] . "[n" . self::$n . "][IMAGE]";

        if (CModule::IncludeModule("fileman")) {
            $id = preg_replace("/[^a-z0-9]/i", '', $desc);
            ob_start();
            echo '<input type="hidden" name="' . $desc . '[TYPE]" value="html">';
            $LHE = new CLightHTMLEditor;
            $LHE->Show(array(
                'id' => $id,
                'width' => '100%',
                'height' => '150px',
                'inputName' => $desc,
                'content' => htmlspecialchars_decode($arHtmlControl['VALUE']['DESC']),
                'bUseFileDialogs' => false,
                'bFloatingToolbar' => false,
                'bArisingToolbar' => false,
                'toolbarConfig' => array(
                    'Bold', 'Italic', 'Underline', 'RemoveFormat',
                    'CreateLink', 'DeleteLink', 'Image', 'Video',
                    'ForeColor',
                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
                    'InsertOrderedList', 'InsertUnorderedList',
                    'StyleList', 'HeaderList',
                    'FontList', 'FontSizeList','Source',
                ),
            ));
            $html_editor = ob_get_contents();
            ob_end_clean();
        } else {
            $html_editor = '<textarea style="width:100%;height:50px;" name="' . $desc . '">' . $arHtmlControl['VALUE']['DESC'] . '</textarea>';
        }



            $html = '<div style="width: 100%;display: inline-block;"><br><label>Название</label><br><input style="width:100%;" type="text" value="' . $arHtmlControl['VALUE']['TITLE'] . '" name="' . $name . '"><br>';
            $html .= '<br><label>Описание</label><br>'.$html_editor.'<br><br>';
            $html .= '</div></div>';


        self::$n++;
        $rand =  uniqid();
       // $i = self::$n;
          for($i=1;$i<10;$i++){
                $html .= CMedialib::ShowDialogScript(array(
                    "event" => "OpenMedialibDialogbx_file_prop_7__n".$i."__IMAGE__".$rand,
                    "arResultDest" => array(
                        "FUNCTION_NAME" => "SetValueFromMedialibbx_file_prop_7__n".$i."__IMAGE__".$rand,
                    )
                ));
           }

      /*  $html .="
        <script>
        var i = ".self::$n.";
        var rand = Math.round(Math.random()*1000000);
        $(function(){
            alert(rand);
        });
        </script>
        ";*/



            if (class_exists('\Bitrix\Main\UI\FileInput', true)) {
                $historyId = 0;
                return '<div style="position: relative;border: 2px solid #bbbbbb;padding: 27px;margin-bottom: 15px;"><div style="position: absolute;top: 0px;left: 0;padding: 5px;background: #d8f9de;font-weight: bold;"></div>'.\Bitrix\Main\UI\FileInput::createInstance((
                        array(
                            "name" => $file,
                            "id" => $file . "_" . $rand,
                            "description" => "N",
                            "allowUpload" => "F",
                            "maxCount" => 1,
                            "allowUploadExt" => ''
                        ) + ($historyId > 0 ? array(
                            "delete" => false,
                            "edit" => false
                        ) : array(
                            "upload" => true,
                            "medialib" => true,
                            "fileDialog" => true,
                            "cloud" => true
                        ))
                    ))->show($inputName) . $html;
            }






    }

    function GetEditFormHTMLMulty($arUserField, $arHtmlControl)
    {
        $arHtmlControl["ROWCLASS"] = "adm-detail-file-row";

        CModule::IncludeModule("fileman");

        $values = array();
        $fieldName = substr($arHtmlControl["NAME"], 0, -2);
        $result = "";
        foreach ($arHtmlControl["VALUE"] as $key => $fileId)
        {
            $result .= '<input type="hidden" name="'.$fieldName.'_old_id['.$key.']" value="'.$fileId.'">';
            $values[$fieldName."[".$key."]"] = $fileId;
        }

        return CFileInput::ShowMultiple($values, $fieldName."[n#IND#]", array(
                "IMAGE" => "Y",
                "PATH" => "Y",
                "FILE_SIZE" => "Y",
                "DIMENSIONS" => "Y",
                "IMAGE_POPUP" => "Y",
                "MAX_SIZE" => array("W" => 200, "H"=>200)
            ),
                false,
                array(
                    'upload' => $arUserField["EDIT_IN_LIST"] == "Y",
                    'medialib' => false,
                    'file_dialog' => false,
                    'cloud' => false,
                    'del' => true,
                    'description' => false
                )
            ).$result;
    }

    function GetFilterHTML($arUserField, $arHtmlControl)
    {
        return '&nbsp;';
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        return CFile::ShowFile($arHtmlControl["VALUE"], $arUserField["SETTINGS"]["MAX_SHOW_SIZE"], $arUserField["SETTINGS"]["LIST_WIDTH"], $arUserField["SETTINGS"]["LIST_HEIGHT"], true);
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl)
    {
        //TODO edit mode
        return CFile::ShowFile($arHtmlControl["VALUE"], $arUserField["SETTINGS"]["MAX_SHOW_SIZE"], $arUserField["SETTINGS"]["LIST_WIDTH"], $arUserField["SETTINGS"]["LIST_HEIGHT"], true);
    }

    function GetAdminListEditHTMLMulty($arUserField, $arHtmlControl)
    {
        //TODO edit mode
        $result = "&nbsp;";
        foreach($arHtmlControl["VALUE"] as $value)
        {
            $result .= CFile::ShowFile($value, $arUserField["SETTINGS"]["MAX_SHOW_SIZE"], $arUserField["SETTINGS"]["LIST_WIDTH"], $arUserField["SETTINGS"]["LIST_HEIGHT"], true)."<br>";
        }
        return $result;
    }

    function CheckFields($arUserField, $value)
    {
        $aMsg = array();
        if($arUserField["SETTINGS"]["MAX_ALLOWED_SIZE"]>0 && $value["size"]>$arUserField["SETTINGS"]["MAX_ALLOWED_SIZE"])
        {
            $aMsg[] = array(
                "id" => $arUserField["FIELD_NAME"],
                "text" => GetMessage("USER_TYPE_FILE_MAX_SIZE_ERROR",
                    array(
                        "#FIELD_NAME#"=>$arUserField["EDIT_FORM_LABEL"],
                        "#MAX_ALLOWED_SIZE#"=>$arUserField["SETTINGS"]["MAX_ALLOWED_SIZE"]
                    )
                ),
            );
        }

        //Extention check
        if(is_array($arUserField["SETTINGS"]["EXTENSIONS"]) && count($arUserField["SETTINGS"]["EXTENSIONS"]))
        {
            foreach($arUserField["SETTINGS"]["EXTENSIONS"] as $ext => $tmp_val)
                $arUserField["SETTINGS"]["EXTENSIONS"][$ext] = $ext;
            $error = CFile::CheckFile($value, 0, false, implode(",", $arUserField["SETTINGS"]["EXTENSIONS"]));
        }
        else
        {
            $error = "";
        }

        if (strlen($error))
        {
            $aMsg[] = array(
                "id" => $arUserField["FIELD_NAME"],
                "text" => $error,
            );
        }

        //For user without edit php permissions
        //we allow only pictures upload
        global $USER;
        if(!is_object($USER) || !$USER->IsAdmin())
        {
            if(HasScriptExtension($value["name"]))
            {
                $aMsg[] = array(
                    "id" => $arUserField["FIELD_NAME"],
                    "text" => GetMessage("FILE_BAD_TYPE")." (".$value["name"].").",
                );
            }
        }

        return $aMsg;
    }

    function OnBeforeSave($arUserField, $value)
    {


        //deb('-------------OnBeforeSave--------------');


        if(isset($_REQUEST['PROP_del'][$arUserField['ID']])){
            foreach($_REQUEST['PROP_del'][$arUserField['ID']] as $k => $p){
                if($_REQUEST['PROP_del'][$arUserField['ID']][$k]['IMAGE'] == 'Y'){
                    $id_file = $_REQUEST['PROP'][$arUserField['ID']][$k]['IMAGE'];
                    if($value['VALUE']['IMAGE'] == $id_file){
                        return  false;
                        $res = CFile::Delete($id_file);
                        $_REQUEST['PROP_del'][$arUserField['ID']][$k]['IMAGE'] = 'N';
                        $value['VALUE']['IMAGE'] = '';
                        //$value['VALUE'] = serialize($value['VALUE']);
                        //return $value;
                    }
                }

            }

        }


        //если файл был выбран из библиотеки
        if(!(int)$value['VALUE']['IMAGE'] > 0 && !is_array($value['VALUE']['IMAGE']) && $value['VALUE']['IMAGE'] != ''){
           // deb($value);die();
            $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$value['VALUE']['IMAGE']);
            $id =  CFile::SaveFile($arFile, "uf");
            if($id > 0){
                $value['VALUE']['IMAGE'] = $id;
            }else{
                $value['VALUE']['IMAGE'] = '';
            }
        }else
        if(is_array($value['VALUE']['IMAGE']))
        {

            $file = $value['VALUE']['IMAGE'];
            //Protect from user manipulation
            if(isset($file["old_id"]) && $file["old_id"] > 0)
            {
                if(is_array($arUserField["VALUE"]))
                {
                    if(!in_array($file["old_id"], $arUserField["VALUE"]))
                        unset($file["old_id"]);
                }
                else
                {
                    if($arUserField["VALUE"] != $file["old_id"])
                        unset($file["old_id"]);
                }
            }

            if($file["del"] && $file["old_id"])
            {
                CFile::Delete($file["old_id"]);
                $file["old_id"] = false;
            }


            if($file["error"])
            {
                return $file["old_id"];
            }
            else
            {
                if($file["old_id"])
                {
                    CFile::Delete($file["old_id"]);
                }
                $file['tmp_name'] = $_SERVER["DOCUMENT_ROOT"].$file['tmp_name'];
                $file["MODULE_ID"] = "main";

                $file=Array(
                    "name" => $value['VALUE']["IMAGE"]["name"],
                    "size" => $value['VALUE']["IMAGE"]["size"],
                    "tmp_name" => $_SERVER["DOCUMENT_ROOT"]."/upload/tmp".$value['VALUE']["IMAGE"]["tmp_name"],
                    "type" => $value['VALUE']["IMAGE"]["type"],
                    "MODULE_ID" => "iblock"
                );


                $id =  CFile::SaveFile($file, "uf");
                $value['VALUE']['IMAGE'] = $id;
            }
        }

        if($value['VALUE']['IMAGE'] != '' || $value['VALUE']['TITLE'] != '' || $value['VALUE']['DESC'] != ''){
            $value['VALUE'] = serialize($value['VALUE']);
            return $value;
        }else{
            $value['VALUE'] = serialize($value['VALUE']);
            return  false;
        }


    }
    function ConvertFromDB($arProperty, $value)
    {

        $value['VALUE'] = unserialize($value['VALUE']);
       // deb('--------------------ConvertFromDB------------');
       // deb($value);
        if($value['VALUE']['IMAGE'] != '' || $value['VALUE']['TITLE'] != '' || $value['VALUE']['DESC'] != ''){
            return $value;
        }else{
            return false;
        }
    }

    function OnSearchIndex($arUserField)
    {
        static $max_file_size = null;
        $res = '';

        if(is_array($arUserField["VALUE"]))
            $val = $arUserField["VALUE"];
        else
            $val = array($arUserField["VALUE"]);

        $val = array_filter($val, "strlen");
        if(count($val))
        {
            $val = array_map(array("CUserTypeFile", "__GetFileContent"), $val);
            $res = implode("\r\n", $val);
        }

        return $res;
    }

    function __GetFileContent($FILE_ID)
    {
        static $max_file_size = null;

        $arFile = CFile::MakeFileArray($FILE_ID);
        if($arFile && $arFile["tmp_name"])
        {
            if(!isset($max_file_size))
                $max_file_size = COption::GetOptionInt("search", "max_file_size", 0)*1024;

            if($max_file_size > 0 && $arFile["size"] > $max_file_size)
                return "";

            $arrFile = false;
            foreach(GetModuleEvents("search", "OnSearchGetFileContent", true) as $arEvent)
            {
                if($arrFile = ExecuteModuleEventEx($arEvent, array($arFile["tmp_name"])))
                    break;
            }

            if(is_array($arrFile))
                return $arrFile["CONTENT"];
        }

        return "";
    }


}
?>
