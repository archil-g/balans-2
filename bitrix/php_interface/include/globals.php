<?php

use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock as HL;
use \Bitrix\Main\Entity;

CModule::IncludeModule('highloadblock');
/*
 * Механизм определения значений глобального массива, хранящего ID основных инфоблоков
 * */

$GLOBALS["ASPRO_IBLOCKS"] = [];

Loader::IncludeModule('iblock');

$iblockList = CIBlock::GetList(
    [],
    [
        'SITE_ID' => SITE_ID,
        'ACTIVE' => 'Y',
    ],
    true
);
while ($arIblock = $iblockList->Fetch()) {
    $GLOBALS["ASPRO_IBLOCKS"][$arIblock['CODE']] = $arIblock['ID'];
}

$rsHlblock = HL\HighloadBlockTable::getList(['select' => ['ID', 'NAME']]);
while ($arHlblock = $rsHlblock->fetch()) {
    $GLOBALS["ASPRO_HLBLOCKS"][$arHlblock['NAME']] = $arHlblock['ID'];
}

global $aURI;
$aURI = Array();
$pureURI = $_SERVER["REQUEST_URI"];
if (substr_count($pureURI, "?")) {
    $pos = strpos($pureURI, "?");
    $pureURI = substr($pureURI, 0, $pos);
    $pureURI = substr($pureURI, 1);
}
$aURI = explode("/", $pureURI);