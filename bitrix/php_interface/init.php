<?php

define('HELP_IBLOCK_TYPES', [
        'download_link',
        'questions',
        'files',
	//'videos',
        'articles',
        'instructions',
        'changes'
    ]
);

define('SERVICES_IBLOCK_ID',32);
define('ACTIONS_IBLOCK_ID',33);

function getRootSection($filter = [])
{
    CModule::IncludeModule('iblock');
    if(!empty($filter)){
        return \CIBlockSection::GetList([],$filter,true,["UF_*"])->GetNext();
    }

    return false;
}

function hadSectionArea($iblockID, $sectionID){
    CModule::IncludeModule('iblock');
    $dbResult = CIBlockSection::GetList(
        array(),
        array("IBLOCK_ID" => $iblockID, "UF_PO" => $sectionID)
    );
    $section = [];
    while($item = $dbResult->GetNext()){
        $section[] = $item["ID"];
    }

    if(!empty($section))
        return $section;

    return false;
}

function dump($data, $allUsers = false, $die = false)
{
    global $USER;

    if ($allUsers || $USER->IsAdmin()) {
        echo '<pre style="padding: 10px; background: #fff; color: #000; border: 1px solid #777; border-radius: 4px; text-align: left;">';
        print_r($data);
        echo '</pre>';
    }

    if ($die) {
        die();
    }
}

if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/globals.php"))
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/globals.php");

if (file_exists($_SERVER['DOCUMENT_ROOT']."/bitrix/php_interface/classes/UserFields.php")) {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/php_interface/classes/UserFields.php");
}

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementAddHandler");

function OnBeforeIBlockElementAddHandler(&$arFields)
{
    if(in_array($arFields['IBLOCK_ID'],array(56))){
        CModule::IncludeModule("rdn");



    }
}

use Bitrix\Main\EventManager;

$eventManager = EventManager::getInstance();

$eventManager->addEventHandlerCompatible("main", "OnAfterUserLogin", function(&$arFields) {
    if(!empty($_SESSION[SITE_ID][0]['BASKET_ITEMS'])) {
//        if(!empty($_SESSION[SITE_ID][$arFields['USER_ID']]['BASKET_ITEMS'])) {
//            $_SESSION[SITE_ID][$arFields['USER_ID']]['BASKET_ITEMS'] = array_merge($_SESSION[SITE_ID][$arFields['USER_ID']]['BASKET_ITEMS'],$_SESSION[SITE_ID][0]['BASKET_ITEMS']);
//        }
//        else {
            $_SESSION[SITE_ID][$arFields['USER_ID']]['BASKET_ITEMS'] = $_SESSION[SITE_ID][0]['BASKET_ITEMS'];
//        }
    }
});
$eventManager->addEventHandlerCompatible("main", "OnAfterUserAuthorize", function($arUser) {
    if(!empty($_SESSION[SITE_ID][0]['BASKET_ITEMS'])) {
//        if(!empty($_SESSION[SITE_ID][$arUser['ID']]['BASKET_ITEMS'])) {
//            $_SESSION[SITE_ID][$arUser['ID']]['BASKET_ITEMS'] = array_merge($_SESSION[SITE_ID][$arUser['ID']]['BASKET_ITEMS'],$_SESSION[SITE_ID][0]['BASKET_ITEMS']);
//        }
//        else {
            $_SESSION[SITE_ID][$arUser['ID']]['BASKET_ITEMS'] = $_SESSION[SITE_ID][0]['BASKET_ITEMS'];
//        }
    }
});
//https://dev.1c-bitrix.ru/community/webdev/user/2854/blog/2193/
//https://dev.1c-bitrix.ru/community/blogs/oracle/183.php
//https://dev.1c-bitrix.ru/api_help/iblock/classes/user_properties/index.php
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CIBlockInnProperty", "GetUserTypeDescription"));
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CIBlockKppProperty", "GetUserTypeDescription"));
class CIBlockInnProperty
{
	public function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE"        => "S", #-----один из стандартных типов
			"USER_TYPE"            => "INN", #-----идентификатор типа свойства
			"DESCRIPTION"          => "ИНН компании",
			"GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
			"CheckFields" => array(__CLASS__, "CheckFields"),
			//"ConvertToDB" => array(__CLASS__, "ConvertToDB"), #-----функция конвертирования данных перед сохранением в базу данных
			//"ConvertFromDB" => array(__CLASS__, "ConvertFromDB"), #-----функция конвертации после извлечения значения из базы данных и перед показом в форме редактирования элемента инфоблока
		);
	}
	// Функция валидатор значений свойства
	// вызвается в $GLOBALS['USER_FIELD_MANAGER']->CheckField() при добавлении/изменении
	// @param array $value значение для проверки на валидность
	// @return array массив массивов ("id","text") ошибок
	function CheckFields($arUserField, $value) {
		$aMsg = array();
		if(!empty($value['VALUE']) && !CIBlockInnProperty::checkInn($value['VALUE'])){
			$aMsg[]='Ошибка формата ИНН';
		}
		return $aMsg;
	}

	/*--------- вывод поля свойства на странице редактирования ---------*/
	public function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		return '<input type="text" name="'.$strHTMLControlName["VALUE"].'" value="'.$value['VALUE'].'">';
	}

	public static function checkInn($n)
	{
		if (!is_scalar($n)) {
			//triggertrigger_error('Scalar type expected, ' . gettype($n) . ' given ', E_USER_WARNING);
			return false;
		}
		$n = strval($n);
		if (!in_array(strlen($n), array(10, 12)) || !ctype_digit($n)) {
			return false;
		}

		if (strlen($n) == 10) {
			$sum = 0;
			foreach (array(2, 4, 10, 3, 5, 9, 4, 6, 8) as $i => $weight) {
				$sum += $weight * substr($n, $i, 1);
			}
			return $sum % 11 % 10 == substr($n, 9, 1);
		}
		#для 12 знаков:
		$sum1 = $sum2 = 0;
		foreach (array(7, 2, 4, 10, 3, 5, 9, 4, 6, 8) as $i => $weight) {
			$sum1 += $weight * substr($n, $i, 1);
		}
		foreach (array(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8) as $i => $weight) {
			$sum2 += $weight * substr($n, $i, 1);
		}
		return ($sum1 % 11 % 10) . ($sum2 % 11 % 10) == substr($n, 10, 2);
	}
}

class CIBlockKppProperty
{
	public function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE"        => "S", #-----один из стандартных типов
			"USER_TYPE"            => "KPP", #-----идентификатор типа свойства
			"DESCRIPTION"          => "КПП компании",
			"GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
			"CheckFields" => array(__CLASS__, "CheckFields"),
			//"ConvertToDB" => array(__CLASS__, "ConvertToDB"), #-----функция конвертирования данных перед сохранением в базу данных
			//"ConvertFromDB" => array(__CLASS__, "ConvertFromDB"), #-----функция конвертации после извлечения значения из базы данных и перед показом в форме редактирования элемента инфоблока
		);
	}
	// Функция валидатор значений свойства
	// вызвается в $GLOBALS['USER_FIELD_MANAGER']->CheckField() при добавлении/изменении
	// @param array $value значение для проверки на валидность
	// @return array массив массивов ("id","text") ошибок
	function CheckFields($arUserField, $value) {
		$aMsg = array();
		if(!empty($value['VALUE']) && !CIBlockInnProperty::checkKpp($value['VALUE'])){
			$aMsg[]='Ошибка формата ИНН';
		}
		return $aMsg;
	}

	/*--------- вывод поля свойства на странице редактирования ---------*/
	public function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		return '<input type="text" name="'.$strHTMLControlName["VALUE"].'" value="'.$value['VALUE'].'">';
	}

	public static function checkKpp($n){
		return  preg_match('/^[0-9]{4}[0-9A-Z]{2}[0-9]{3}$/',$n);
	}
}