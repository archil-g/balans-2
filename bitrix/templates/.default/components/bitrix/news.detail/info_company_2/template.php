<?
if ( !defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true )die();
$this->setFrameMode( true );
/*if($arParams["ADDITIONAL_SECTION_NAME"]){
    $APPLICATION->AddChainItem($arParams["ADDITIONAL_SECTION_NAME"]);
}*/
?>
<?if($arResult):?>
<div class="main_info greyline top_line">
	<div class="maxwidth-theme">
		
		<?
				$bMainAction = (isset($arResult['DISPLAY_PROPERTIES']['MAIN_ACTION_PICTURE1']) && $arResult['DISPLAY_PROPERTIES']['MAIN_ACTION_PICTURE1']['VALUE'] ? true : false);
				?>
		<?
				$bMainAction2 = (isset($arResult['DISPLAY_PROPERTIES']['MAIN_ACTION_PICTURE2']) && $arResult['DISPLAY_PROPERTIES']['MAIN_ACTION_PICTURE2']['VALUE'] ? true : false);
				?>


		<?
				$bOrderButton = (isset($arResult['DISPLAY_PROPERTIES']['ORDER_FORM']) && $arResult['DISPLAY_PROPERTIES']['ORDER_FORM']['VALUE_XML_ID'] == 'Y' ? true : false);
				$bCallbackButton = (isset($arResult['DISPLAY_PROPERTIES']['CALLBACK_FORM']) && $arResult['DISPLAY_PROPERTIES']['CALLBACK_FORM']['VALUE_XML_ID'] == 'Y' ? true : false);
				?>
		<?if($bOrderButton || $bCallbackButton):?>

		<div class="row">
			<div class="col-md-12">

				<div align="center" class="<?=(!$bMainAction ? ' wti' : '')?>">
					<?if(isset($arResult['DISPLAY_PROPERTIES']['INFO_BLOCK_NAME']) && $arResult['DISPLAY_PROPERTIES']['INFO_BLOCK_NAME']['VALUE']):?>
					<div class="title">
						<h2>
							<?=$arResult['DISPLAY_PROPERTIES']['INFO_BLOCK_NAME']['VALUE'];?>
						</h2>
					</div>
					<?endif;?>

				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-5" align="center">


				<?if($bOrderButton):?>
				<div class="button">

					<?if($bMainAction):?>
					<?
					$arImage = CFile::ResizeImageGet($arResult['DISPLAY_PROPERTIES']['MAIN_ACTION_PICTURE1']['VALUE'], array('width' => 250, 'height' => 275), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
					?>
					<img class="img-responsive" src="<?=$arImage['src'];?>" alt="<?=$arResult['NAME'];?>" title="<?=$arResult['NAME'];?>">
					<?endif;?>



					<span class="order_button btn btn-default animate-load" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_question");?>" data-name="question">
						<?=($arParams['ORDER_BUTTON_TITLE'] ? $arParams['ORDER_BUTTON_TITLE'] : GetMessage('ORDER_BUTTON_TITLE'));?>
					</span>
				</div>
				<?endif;?>





			</div>



			<div class="col-md-2"></div>

			<div class="col-md-5" align="center">



				<?if($bCallbackButton):?>


				<div class="button">


					<?if($bMainAction2):?>
					<?
					$arImage2 = CFile::ResizeImageGet($arResult['DISPLAY_PROPERTIES']['MAIN_ACTION_PICTURE2']['VALUE'], array('width' => 250, 'height' => 275), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
					?>
					<img class="img-responsive" src="<?=$arImage2['src'];?>" alt="<?=$arResult['NAME'];?>" title="<?=$arResult['NAME'];?>">
					<?endif;?>


					<span class="callback-block btn btn-default btn-transparent animate-load" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_callback");?>" data-name="callback">
						<?=($arParams['CALLBACK_BUTTON_TITLE'] ? $arParams['CALLBACK_BUTTON_TITLE'] : GetMessage('CALLBACK_BUTTON_TITLE'));?>
					</span>
				</div>
				<?endif;?>

			</div>

		</div>

	</div>
</div>


<?endif;?><?endif;?>