<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');
?>
<div class="col-md-9 col-sm-12 col-xs-12 content-md">
    <div class="item-views documents_list type_2">

        <div class="group-content">
            <div class="tab-pane">
                <div class="text_before_items">
                    <?=$arResult["~DETAIL_TEXT"]?>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
