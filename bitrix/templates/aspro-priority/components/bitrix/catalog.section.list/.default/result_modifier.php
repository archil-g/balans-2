<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

$arViewModeList = array('LIST', 'LINE', 'TEXT', 'TILE');

$arDefaultParams = array(
    'VIEW_MODE' => 'LIST',
    'SHOW_PARENT_NAME' => 'Y',
    'HIDE_SECTION_NAME' => 'N'
);
$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
    $arParams['VIEW_MODE'] = 'LIST';
if ('N' != $arParams['SHOW_PARENT_NAME'])
    $arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
    $arParams['HIDE_SECTION_NAME'] = 'N';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT']) {
    if ('LIST' != $arParams['VIEW_MODE']) {
        $boolClear = false;
        $arNewSections = array();
        foreach ($arResult['SECTIONS'] as &$arOneSection) {
            if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL']) {
                $boolClear = true;
                continue;
            }
            $arNewSections[] = $arOneSection;
        }
        unset($arOneSection);
        if ($boolClear) {
            $arResult['SECTIONS'] = $arNewSections;
            $arResult['SECTIONS_COUNT'] = count($arNewSections);
        }
        unset($arNewSections);
    }
}

if (0 < $arResult['SECTIONS_COUNT']) {
    $boolPicture = false;
    $boolDescr = false;
    $arSelect = array('ID');
    $arMap = array();
    if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE']) {
        reset($arResult['SECTIONS']);
        $arCurrent = current($arResult['SECTIONS']);
        if (!isset($arCurrent['PICTURE'])) {
            $boolPicture = true;
            $arSelect[] = 'PICTURE';
        }
        if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent)) {
            $boolDescr = true;
            $arSelect[] = 'DESCRIPTION';
            $arSelect[] = 'DESCRIPTION_TYPE';
        }
    }

    if($arParams["SELECTED_SECTION"] == $arResult["SECTION"]["ID"]){
        $arrFilter = [
            "IBLOCK_ID" => $arResult["SECTION"]["IBLOCK_ID"],
            "SECTION_ID" => $arResult["SECTION"]["ID"],
            "INCLUDE_SUBSECTIONS" => "Y"
        ];
        if(!empty($arParams['ELEMENT_FILTER'])){
            $arrFilter = array_merge($arrFilter,$arParams['ELEMENT_FILTER']);
        }

        $questions = CIBlockElement::GetList(
            [],
            $arrFilter,
            false,
            false,
            [
                "ID",
                "IBLOCK_ID",
                "NAME",
                "PREVIEW_TEXT",
                "DETAIL_TEXT",
                "DETAIL_PAGE_URL"
            ]
        );
        if ($questions->SelectedRowsCount() > 0) {
            while ($question = $questions->GetNext()) {
                $arResult["QUESTIONS"][] = $question;
            }
        }
    }else{
        foreach ($arResult['SECTIONS'] as $arSection) {
            $filterSections["IDS"][] = $arSection["ID"];
            if (stripos($APPLICATION->GetCurDir(), $arSection["CODE"]) > 0) {
                $arrFilter = [
                    "IBLOCK_ID" => $arSection["IBLOCK_ID"],
                    "SECTION_ID" => $arSection["ID"],
                    "INCLUDE_SUBSECTIONS" => "Y"
                ];
                if(!empty($arParams['ELEMENT_FILTER'])){
                    $arrFilter = array_merge($arrFilter,$arParams['ELEMENT_FILTER']);
                }

                $questions = CIBlockElement::GetList(
                    [],
                    $arrFilter,
                    false,
                    false,
                    [
                        "ID",
                        "IBLOCK_ID",
                        "NAME",
                        "PREVIEW_TEXT",
                        "DETAIL_TEXT",
                        "DETAIL_PAGE_URL"
                    ]
                );
                if ($questions->SelectedRowsCount() > 0) {
                    while ($question = $questions->GetNext()) {
                        $arResult["QUESTIONS"][] = $question;
                    }
                }

            }
        }

        foreach ($filterSections["IDS"] as $sectionID) {
            $sectionsInfo = CIBlockElement::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"]["faq"], "SECTION_ID" => $sectionID], false, false, ["ID", "IBLOCK_ID", "NAME"]);
            $filterSections["INFO"][$sectionID]["COUNT"] = $sectionsInfo->SelectedRowsCount();
        }

        foreach ($arResult['SECTIONS'] as &$arSection) {
            $arSection["ELEMENT_COUNT"] = $filterSections["INFO"][$arSection["ID"]]["COUNT"];
        }
    }


    if ($boolPicture || $boolDescr) {
        foreach ($arResult['SECTIONS'] as $key => $arSection) {
            $arMap[$arSection['ID']] = $key;
        }
        $rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
        while ($arSection = $rsSections->GetNext()) {
            if (!isset($arMap[$arSection['ID']]))
                continue;
            $key = $arMap[$arSection['ID']];
            if ($boolPicture) {
                $arSection['PICTURE'] = intval($arSection['PICTURE']);
                $arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
                $arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
                $arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
            }
            if ($boolDescr) {
                $arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
                $arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
                $arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
                $arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
            }
        }
    }

    if ($arParams["PAGE_TYPE"] == "product") {
        foreach ($arResult["SECTIONS"] as &$section) {
            $section["SECTION_PAGE_URL"] = $arParams["SECTION_URL"] . $section["CODE"] . '/';
        }
    }
}
?>