<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

$arViewModeList = array('LIST', 'LINE', 'TEXT', 'TILE');

$arDefaultParams = array(
    'VIEW_MODE' => 'LIST',
    'SHOW_PARENT_NAME' => 'Y',
    'HIDE_SECTION_NAME' => 'N'
);
$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
    $arParams['VIEW_MODE'] = 'LIST';
if ('N' != $arParams['SHOW_PARENT_NAME'])
    $arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
    $arParams['HIDE_SECTION_NAME'] = 'N';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT']) {
    if ('LIST' != $arParams['VIEW_MODE']) {
        $boolClear = false;
        $arNewSections = array();
        foreach ($arResult['SECTIONS'] as &$arOneSection) {
            if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL']) {
                $boolClear = true;
                continue;
            }
            $arNewSections[] = $arOneSection;
        }
        unset($arOneSection);
        if ($boolClear) {
            $arResult['SECTIONS'] = $arNewSections;
            $arResult['SECTIONS_COUNT'] = count($arNewSections);
        }
        unset($arNewSections);
    }
}
if (0 < $arResult['SECTIONS_COUNT']) {
    $boolPicture = false;
    $boolDescr = false;
    $arSelect = array('ID');
    $arMap = array();
    if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE']) {
        reset($arResult['SECTIONS']);
        $arCurrent = current($arResult['SECTIONS']);
        if (!isset($arCurrent['PICTURE'])) {
            $boolPicture = true;
            $arSelect[] = 'PICTURE';
        }
        if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent)) {
            $boolDescr = true;
            $arSelect[] = 'DESCRIPTION';
            $arSelect[] = 'DESCRIPTION_TYPE';
        }
    }
    foreach ($arResult['SECTIONS'] as $arSection) {
        $filterSections["IDS"][] = $arSection["ID"];
        if (stripos($APPLICATION->GetCurDir(), $arSection["CODE"]) > 0) {
            $questions = CIBlockElement::GetList(
                [],
                [
                    "IBLOCK_ID" => $arSection["IBLOCK_ID"],
                    "SECTION_ID" => $arSection["ID"]
                ],
                false,
                false,
                [
                    "ID",
                    "IBLOCK_ID",
                    "NAME",
                    "PREVIEW_TEXT",
                    "DETAIL_TEXT",
                    "DETAIL_PAGE_URL",
                    "PROPERTY_*"
                ]
            );
            //dump($questions->SelectedRowsCount());
            if ($questions->SelectedRowsCount() > 0) {
                while ($question = $questions->GetNextElement()) {
                    $properties = $question->GetProperties();
                    $fields = $question->GetFields();
                    //dump($properties);
                    if (sizeof($properties["DOCUMENTS"]["VALUE"]) == 1) {
                        $fileInfo = CFile::GetFileArray($properties["DOCUMENTS"]["VALUE"][0]);
                        switch ($fileInfo["CONTENT_TYPE"]) {
                            case "application/vnd.ms-excel":
                                $properties["FILE_TYPE"] = "xls";
                                break;
                            case "image/png":
                                $properties["FILE_TYPE"] = "jpg";
                                break;
                        }
                    }

                    $properties["FILE_SIZE"] = round($fileInfo["FILE_SIZE"] / 1000, 1);
                    $properties["FILE_SRC"] = $fileInfo["SRC"];
                    $properties["NAME"] = $fields["NAME"];
                    $arResult["QUESTIONS"][] = $properties;
                }
            }

        }
    }

    foreach ($filterSections["IDS"] as $sectionID) {
        $sectionsInfo = CIBlockElement::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"]["faq"], "SECTION_ID" => $sectionID], false, false, ["ID", "IBLOCK_ID", "NAME"]);
        $filterSections["INFO"][$sectionID]["COUNT"] = $sectionsInfo->SelectedRowsCount();
    }

    foreach ($arResult['SECTIONS'] as &$arSection) {
        $arSection["ELEMENT_COUNT"] = $filterSections["INFO"][$arSection["ID"]]["COUNT"];
    }
} else {
    $questions = CIBlockElement::GetList(
        [],
        [
            "IBLOCK_ID" => $arResult["SECTION"]["IBLOCK_ID"],
            "SECTION_ID" => $arResult["SECTION"]["ID"]
        ],
        false,
        false,
        [
            "ID",
            "IBLOCK_ID",
            "NAME",
            "PREVIEW_TEXT",
            "DETAIL_TEXT",
            "DETAIL_PAGE_URL",
            "PROPERTY_*"
        ]
    );


}

if ($questions && $questions->SelectedRowsCount() > 0) {
    while ($question = $questions->GetNextElement()) {
        $properties = $question->GetProperties();
        $fields = $question->GetFields();
        //dump($properties);
        if (sizeof($properties["DOCUMENTS"]["VALUE"]) == 1) {
            $fileInfo = CFile::GetFileArray($properties["DOCUMENTS"]["VALUE"][0]);
            switch ($fileInfo["CONTENT_TYPE"]) {
                case "application/vnd.ms-excel":
                    $properties["FILE_TYPE"] = "xls";
                    break;
                case "image/png":
                    $properties["FILE_TYPE"] = "jpg";
                    break;
            }
        }

        $properties["FILE_SIZE"] = round($fileInfo["FILE_SIZE"] / 1000, 1);
        $properties["FILE_SRC"] = $fileInfo["SRC"];
        $properties["NAME"] = $fields["NAME"];
        $arResult["QUESTIONS"][] = $properties;
    }
}
?>