<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>

<div class="col-md-9 col-sm-12 col-xs-12 content-md">
    <div class="item-views faq_list">

        <div class="tabs">
            <ul class="nav nav-tabs font_upper_md">
                <?
                $themeFlag = false;
                $themeDescription = $subThemeDescription = "";
                foreach ($arResult['SECTIONS'] as $arSection) {
                    if ($arSection["DEPTH_LEVEL"] < 3) {
                        $themeDescription = $arSection["DESCRIPTION"];
                        $themeFlag = true;
                        continue;
                    }
                    if (stripos($APPLICATION->GetCurDir(), $arSection["CODE"]) !== false)
                        $subThemeDescription = $arSection["DESCRIPTION"];
                    ?>
                    <li class="shadow border <?
                    if (stripos($APPLICATION->GetCurDir(), $arSection["CODE"]) !== false) echo "active" ?>">
                        <a href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?></a>
                    </li>
                    <?
                }
                ?>
            </ul>
            <?=$arResult["SECTION"]["DESCRIPTION"]?>
            <div class="item-views documents_list type_2 margin_top_50">
                <div class="group-content">
                    <div class="tab-pane">
                        <div class="docs-block">
                            <div class="docs_wrap row margin0">
                                <?
                                foreach ($arResult["QUESTIONS"] as $question) {
                                    ?>
                                    <div class="item pull-left">
                                        <div class="blocks border shadow clearfix <?= $question["FILE_TYPE"] ?>">
                                            <div class="inner-wrapper">
                                                <div class="title">
                                                    <a href="<?= $question["FILE_SRC"] ?>" class="dark-color text <?
                                                    if ($question["FILE_TYPE"] == "jpg") echo "fancybox"; ?>"
                                                       target="_blank" <?
                                                    if ($question["FILE_TYPE"] != "jpg") echo "download" ?>><?= $question["NAME"] ?></a>
                                                </div>
                                                <div class="filesize font_xs">
                                                    <?
                                                    if (!empty($question["FILE_SIZE"])){
                                                    ?>
                                                    <?= $question["FILE_SIZE"] ?> Кб
                                                </div>
                                                <?
                                                } ?>
                                                <?
                                                if (!empty($question["FILE_SRC"])) {
                                                    ?>
                                                    <a href="<?= $question["FILE_SRC"] ?>" class="arrow_link <?
                                                    if ($question["FILE_TYPE"] == "jpg") echo "fancybox"; ?>" <?
                                                    if ($question["FILE_TYPE"] != "jpg") echo "download" ?>></a>
                                                    <?
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>