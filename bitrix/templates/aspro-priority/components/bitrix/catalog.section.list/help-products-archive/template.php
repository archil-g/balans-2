<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>


<div class="item-views services-items type_5 icons type_3_within" id="<?=$arResult["SECTION"]["CODE"]?>">
    <h2><?=$arResult["SECTION"]["NAME"]?></h2>
    <div class="items clearfix" style="margin-bottom: 50px">
        <?
        foreach ($arResult['SECTIONS'] as &$arSection) {
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

            if (false === $arSection['PICTURE'])
                $arSection['PICTURE'] = array(
                    'SRC' => $arCurView['EMPTY_IMG'],
                    'ALT' => (
                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                        ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                        : $arSection["NAME"]
                    ),
                    'TITLE' => (
                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                        ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                        : $arSection["NAME"]
                    )
                );
            ?>
            <div class="item shadow border left wbg program_item_wrapper">
                <div class="wrap">
                    <div class="body-info">
                        <div style="float:left; padding-right: 10px">
                            <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><img
                                        src="<?= $arSection['PICTURE']["SRC"] ?>">
                            </a>
                        </div>
                        <a class="dark-color"
                           style="display: inline-block; max-width: 60%; overflow-wrap: break-word;"
                           href="<?=$arSection["SECTION_PAGE_URL"]?>"><?= $arSection["NAME"] ?></a>
                    </div>
                </div>
            </div>
            <?
        }
        ?>
    </div>
</div>
