<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="item-views services-items type_5 icons type_3_within" id="<?=$arResult["SECTION"]["CODE"]?>">
	<?=$arParams['TITLE']?'<h2>'.$arParams['TITLE'].'</h2>':(!empty($arResult["SECTION"]["NAME"])?'<h2>'.$arResult["SECTION"]["NAME"].'</h2>':'')?>
    <div class="items clearfix" style="margin-bottom: 50px">
        <?
        foreach ($arResult['SECTIONS'] as &$arSection) {
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

            if (false === $arSection['PICTURE'])
                $arSection['PICTURE'] = array(
                    'SRC' => $arCurView['EMPTY_IMG'],
                    'ALT' => (
                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                        ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                        : $arSection["NAME"]
                    ),
                    'TITLE' => (
                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                        ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                        : $arSection["NAME"]
                    )
                );
            ?>
	
		<?if ($arSection["NAME"] == "Статьи на тему") {
			?>
			   <div class="item shadow border left wbg program_item_wrapper" style="width: 33%">
                <div class="wrap custom">
                    <div class="body-info">                     
                        <a class="dark-color"
                           style="overflow-wrap: break-word;"
                           href="/help/<?=$arParams["IBLOCK_CODE"]?>/<?=$arSection["CODE"]?>/"><?/*=$arSection["NAME"]*/?>ВСЕ СТАТЬИ</a>
                    </div>
                </div>
            </div>
			
		<?}else{?>
		
			 <div class="item shadow border left wbg program_item_wrapper" style="width: 33%">
                <div class="wrap custom">
                    <div class="body-info">
                        <div>
                           <a href="/software/<?=$arSection["CODE"]?>/<?=$arResult["UF_PRODUCT_FAQ"][$arSection["UF_PRODUCT_FAQ"]]?>/<?=$arParams["IBLOCK_CODE"]?>/"><img
                                        src="<?= $arSection['PICTURE']["SRC"] ?>">
                            </a>
                        </div>
                        <a class="dark-color"
                           style="overflow-wrap: break-word;"
                           href="/software/<?=$arSection["CODE"]?>/<?=$arResult["UF_PRODUCT_FAQ"][$arSection["UF_PRODUCT_FAQ"]]?>/<?=$arParams["IBLOCK_CODE"]?>/"><?=$arSection["NAME"]?></a>
                    </div>
                </div>
            </div>
			
		
		<?}
        }
        ?>
    </div>
</div>
