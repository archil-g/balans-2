<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
    'LIST' => array(
        'CONT' => 'bx_sitemap',
        'TITLE' => 'bx_sitemap_title',
        'LIST' => 'bx_sitemap_ul',
    ),
    'LINE' => array(
        'CONT' => 'bx_catalog_line',
        'TITLE' => 'bx_catalog_line_category_title',
        'LIST' => 'bx_catalog_line_ul',
        'EMPTY_IMG' => $this->GetFolder() . '/images/line-empty.png'
    ),
    'TEXT' => array(
        'CONT' => 'bx_catalog_text',
        'TITLE' => 'bx_catalog_text_category_title',
        'LIST' => 'bx_catalog_text_ul'
    ),
    'TILE' => array(
        'CONT' => 'bx_catalog_tile',
        'TITLE' => 'bx_catalog_tile_category_title',
        'LIST' => 'bx_catalog_tile_ul',
        'EMPTY_IMG' => $this->GetFolder() . '/images/tile-empty.png'
    )
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>
<?//echo '<pre>';print_r($arParams["SECTION_URL"]);echo '</pre>';?>

<div class="item-views faq_list">
    <div class="tabs">
        <ul class="nav nav-tabs font_upper_md">
            <?foreach ($arResult['SECTIONS'] as $arSection) {?>

                <?if ((stripos($APPLICATION->GetCurDir(), $arSection["CODE"]) !== false
                        || $arResult["SELECTED_THEME"] == $arSection["CODE"])
                    && !$selectedSection
                ):?>
                    <li class="shadow border active">
                    <?$selectedSection = $arSection;?>
                <?else:?>
                    <li class="shadow border">
                <?endif;?>
                    <a href="<?= $arParams["SECTION_URL"].$arSection["CODE"] ?>/"><?= $arSection["NAME"] ?></a>
                </li>
            <?}?>
        </ul>
        <div class="tab-content center">
            <?if($selectedSection):?>
            <h2><?=($selectedSection["NAME"])?></h2>
            <?endif;?>
            <div class="items">
                <?
                foreach ($arResult["QUESTIONS"] as $k => $question) {
                    ?>
                    <div class='request__wrapper' id="wrapper<?=($k+1)?>">
                        <div class="request__number<?=($k%2==0)?"":"-right";?>"><?=($k+1)?></div>
                        <div class="request__text">
                            <h3><?=$question["NAME"]?><br><br></h3>
                            <p><?=$question["PREVIEW_TEXT"]?></p>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
    </div>
</div>