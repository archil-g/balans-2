<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?use \Bitrix\Main\Localization\Loc;?>

<?if($arResult['SECTIONS']):?>
    <?if($arParams["TITLE_P"]):?><h4><?=$arParams["TITLE_P"]?></h4><?endif;?>
    <div class="stockblock">
        <div class="item-views sales linked sections">
            <?foreach($arResult['SECTIONS'] as $arItem):?>
                <?
                // edit/add/delete buttons for edit mode
                $arSectionButtons = CIBlock::GetPanelButtons($arItem['IBLOCK_ID'], 0, $arItem['ID'], array('SESSID' => false, 'CATALOG' => true));
                $this->AddEditAction($arItem['ID'], $arSectionButtons['edit']['edit_section']['ACTION_URL'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'SECTION_EDIT'));
                $this->AddDeleteAction($arItem['ID'], $arSectionButtons['edit']['delete_section']['ACTION_URL'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'SECTION_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                // preview picture
                $bImage = strlen($arItem['~PICTURE']);
                $arSectionImage = ($bImage ? CFile::ResizeImageGet($arItem['~PICTURE'], array('width' => 538, 'height' => 538), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true) : array());
                $imageSectionSrc = ($bImage ? $arSectionImage['src'] : '');
                ?>
                <div class="items row" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
                <div class="col-md-12">
                    <div class="item_wrap shadow border">
                        <div class="item wdate clearfix">
                            <div class="image">
                                <a href="<?=$arItem['SECTION_PAGE_URL']?>">
                                    <img src="<?=$imageSectionSrc?>" alt="<?=( $arItem['PICTURE']['ALT'] ? $arItem['PICTURE']['ALT'] : $arItem['NAME']);?>" title="<?=( $arItem['PICTURE']['TITLE'] ? $arItem['PICTURE']['TITLE'] : $arItem['NAME']);?>" class="img-responsive" />
                                </a>
                            </div>
                            <div class="info">
                                <div class="title">
                                    <a href="<?=$arItem['SECTION_PAGE_URL']?>" class="dark-color"><?=$arItem['NAME']?></a>
                                </div>
                                <div class="previewtext">
                                    <?=$arItem['DESCRIPTION']?>
                                    <a class="arrow_open link" href="<?=$arItem['SECTION_PAGE_URL']?>"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?endforeach;?>
        </div>
    </div>
<?endif;?>