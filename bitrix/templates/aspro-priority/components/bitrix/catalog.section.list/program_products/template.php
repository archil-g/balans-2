<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="maxwidth-theme">
    <div class="item-views services-items type_5 icons type_3_within hidden-lg">
        <div class="items clearfix">
            <?

            foreach ($arResult['SECTIONS'] as &$arSection) {
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                if (false === $arSection['PICTURE'])
                    $arSection['PICTURE'] = array(
                        'SRC' => $arCurView['EMPTY_IMG'],
                        'ALT' => (
                        '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                            ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                            : $arSection["NAME"]
                        ),
                        'TITLE' => (
                        '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                            ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                            : $arSection["NAME"]
                        )
                    );
                ?>
                <div class="item shadow border left wbg program_item_wrapper"
                     id="<?= $this->GetEditAreaId($arSection['ID']); ?>">
                    <div class="wrap wrap-brick-software">
                        <div class="body-info">
                            <?
                            if ($arSection['UF_ICON']) { ?>
                                <div class="pp-announce-img">
                                    <a class="pp-announce-img-link" href="#<?= $arSection["CODE"] ?>"><img
                                                src="<?=\CFile::GetPath($arSection['UF_ICON'])?>">
                                    </a>
                                </div>
                            <?
                            } ?>
                            <a class="dark-color ancor-link-smooth <?= !empty($arSection['UF_ICON']) ? "max-w-72" : "max-w-85" ?>"
                               style=" padding-left: 29px; display: inline-block; overflow-wrap: break-word;"
                               href="#<?= $arSection["CODE"] ?>"><?= $arSection["NAME"] ?>
                            </a>
                        </div>
                    </div>
                </div>
                <?
            }
            ?>
        </div>
    </div>

    <div class="row" style="margin-top: 50px">

        <div class="col-md-9 col-sm-12 col-xs-12 content-md">

            <div class="item-views within type_2_within type_3_within services-items">
                <div class="items clearfix">
                    <?

                    foreach ($arResult['SECTIONS'] as &$arSection) {
                        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                        if (false === $arSection['PICTURE'])
                            $arSection['PICTURE'] = array(
                                'SRC' => $arCurView['EMPTY_IMG'],
                                'ALT' => (
                                '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                    ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                    : $arSection["NAME"]
                                ),
                                'TITLE' => (
                                '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                    ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                    : $arSection["NAME"]
                                )
                            );
                        ?>

                        <div class="item" id="<?= $arSection["CODE"] ?>" style="padding-left: 0px; padding-right: 0px;">
                            <div class="row">
                                <? if (!empty($arSection['UF_ICON'])) { ?>
                                    <div class="col-xs-3 col-md-3 col-lg-1 vcenter pp-announce-img" style="padding: 0;">
                                        <a class="pp-announce-img-link" href="<?= $arSection["SECTION_PAGE_URL"] ?>">
                                            <img src="<?=\CFile::GetPath($arSection['UF_ICON'])?>" class="img-responsive">
                                        </a>
                                    </div>
                                    <?
                                } ?>
                                <div class="col-xs-8 col-md-7 col-lg-9 vcenter">
                                    <div class="title" style="font-size: 23px; font-weight: 500;">
                                        <a href="<?= $arSection["SECTION_PAGE_URL"] ?>"
                                           class="dark-color"><?= $arSection["NAME"] ?></a>
                                    </div>


                                </div>
                            </div>
                            <div class="previewtext preview-text-program-product">
                                <p><?= $arSection["DESCRIPTION"] ?></p>
                            </div>
                            <a href="<?= $arSection["SECTION_PAGE_URL"] ?>">
                                <div class="btn btn-transparent">
                                    Перейти на страницу программного продукта
                                </div>
                            </a>
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 hidden-xs hidden-sm">
        <div class="fixed_block_fix"></div>
        <div class="ask_a_question_wrapper">
            <div class="ask_a_question">
                <? if ($arParams["UF_SHOW_PROMO"]): ?>
                        <div class="promoblock">
                            <? $APPLICATION->IncludeComponent(
                                'bitrix:main.include',
                                '',
                                Array(
                                    'AREA_FILE_SHOW' => 'file',
                                    'PATH' => SITE_DIR . 'include/promoblock.php',
                                    'EDIT_TEMPLATE' => '',
                                    'TYPE' => 435
                                )
                            ); ?>
                        </div>
                <? endif; ?>
                <? if ($arParams["SHOW_USEFUL_LINKS"]): ?>
                    <? $APPLICATION->IncludeComponent(
                        'bitrix:main.include',
                        '',
                        Array(
                            'AREA_FILE_SHOW' => 'file',
                            'PATH' => SITE_DIR . 'include/useful_links.php',
                            'EDIT_TEMPLATE' => '',
                            'TYPE' => 432
                        )
                    ); ?>
                <? endif; ?>
            </div>
        </div>
        </div>
    </div>
</div>
