<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="catalog_sections" id="<?=$arResult["SECTION"]["CODE"]?>">
    <h2><?=$arResult["SECTION"]["NAME"]?></h2>
    <div class="item-views services-items type_5 icons type_2_within">
        <div class="items flexbox">
            <?
            foreach ($arResult['SECTIONS'] as $arSection) {
                ?>
                <div class="item shadow border left wbg" style="width: 33%">
                    <div class="wrap">
                        <div class="body-info">
                            <div class="title">
                               <a class="dark-color"
                                   href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?
            }
			
            if($arResult["ARCHIVE"] == "Y" && empty($arParams["IS_ARCHIVE"])){
                ?>
                <div class="item shadow border left wbg" style="width: 33%">
                    <div class="wrap">
                        <div class="body-info">
                            <div class="title">
                               <a class="dark-color"
                                   href="<?= "/help/".$arParams["IBLOCK_CODE"]."/".$arParams["ARCHIVE_URL"]."/"?>">Архив</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?
            }
            ?>
        </div>
    </div>
</div>
