<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
    'LIST' => array(
        'CONT' => 'bx_sitemap',
        'TITLE' => 'bx_sitemap_title',
        'LIST' => 'bx_sitemap_ul',
    ),
    'LINE' => array(
        'CONT' => 'bx_catalog_line',
        'TITLE' => 'bx_catalog_line_category_title',
        'LIST' => 'bx_catalog_line_ul',
        'EMPTY_IMG' => $this->GetFolder() . '/images/line-empty.png'
    ),
    'TEXT' => array(
        'CONT' => 'bx_catalog_text',
        'TITLE' => 'bx_catalog_text_category_title',
        'LIST' => 'bx_catalog_text_ul'
    ),
    'TILE' => array(
        'CONT' => 'bx_catalog_tile',
        'TITLE' => 'bx_catalog_tile_category_title',
        'LIST' => 'bx_catalog_tile_ul',
        'EMPTY_IMG' => $this->GetFolder() . '/images/tile-empty.png'
    )
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>

<div class="col-md-9 col-sm-12 col-xs-12 content-md">
    <div class="item-views faq_list">

        <div class="tabs">
            <ul class="nav nav-tabs font_upper_md">
                <?
                foreach ($arResult['SECTIONS'] as $arSection) {
                    if (stripos($APPLICATION->GetCurDir(), $arSection["CODE"]) !== false){
                        $sectionDescription = $arSection["DESCRIPTION"];
                    }
                    ?>
                    <li class="shadow border <?
                    if (stripos($APPLICATION->GetCurDir(), $arSection["CODE"]) !== false) echo "active" ?>">
                        <a href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?></a>
                    </li>
                    <?
                }
                ?>
            </ul>
            <div class="text_before_items">
                <?=$sectionDescription?>
            </div>
            <div class="tab-content">
                <div id="bx_651765591_430" class="tab-pane active">


                    <div class="title-tab-heading border shadow visible-xs">Общие вопросы<span
                                class="arrow_open"></span></div>
                    <div class="items">
                        <?
                        foreach ($arResult["QUESTIONS"] as $question) {
                            ?>
                            <div class="accordion-type-1">
                                <div class="item border shadow wti">
                                    <div class="accordion-head accordion-close" data-toggle="collapse"
                                         href="#accordion_<?= $question["ID"] ?>">
                                        <span class="arrow_open pull-right"></span>
                                        <?= $question["NAME"] ?>
                                    </div>
                                    <div id="accordion_<?= $question["ID"] ?>" class="panel-collapse collapse">
                                        <div class="accordion-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="text">

                                                        <div class="previewtext">
                                                            <div>
                                                                <?= $question["~PREVIEW_TEXT"] ?>
                                                            </div>
                                                        </div>
                                                        <?
                                                        if ($question["DETAIL_TEXT"] != ""){
                                                            ?>
                                                            <div>
                                                                <a class="btn btn-default btn-sm"
                                                                   href="<?= $question["DETAIL_PAGE_URL"] ?>">
                                                                    Подробнее
                                                                </a>
                                                            </div>
                                                            <?
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>