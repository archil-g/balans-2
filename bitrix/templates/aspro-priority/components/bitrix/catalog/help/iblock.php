<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$tabs = CIBlockSection::GetList(
        ["SORT" => "ASC"],
        [
            "IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]],
            "CODE" => ["products", "themes"]
        ],
        false,
        ["ID", "IBLOCK_ID", "NAME", "CODE"]
);
?>
<div class="maxwidth-theme">
        <div class="sections head-block top controls clearfix">
            <?
            while ($tab = $tabs->GetNext()){
                ?>
                <div class="item-link shadow border">
                    <div class="title font_upper_md">
                        <a href="#<?=$tab["CODE"]?>">
                            <span class="btn-inline black"><?=$tab["NAME"]?></span>
                        </a>
                    </div>
                </div>
                <?
            }
            ?>
        </div>

            <?
            $sectionForFilter = CIBlockSection::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]], "CODE" => "products"], false, ["ID", "IBLOCK_ID"]);
            $sectionForFilter = $sectionForFilter->GetNext();
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "help-products",
                Array(
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COUNT_ELEMENTS" => "Y",
                    "FILTER_NAME" => "",
                    "IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]],
                    "IBLOCK_TYPE" => "",
                    "SECTION_CODE" => "",
                    "SECTION_FIELDS" => array("NAME", "PICTURE", "UF_*"),
                    "SECTION_ID" => $sectionForFilter["ID"],
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array("", ""),
                    "SHOW_PARENT_NAME" => "Y",
                    "TOP_DEPTH" => "2",
                    "VIEW_MODE" => "LINE",
                    "IBLOCK_CODE" => $arResult["VARIABLES"]["IBLOCK_CODE"]
                )
            );?>
        <div class="text_before_items" style="margin-bottom: 50px;">
            <p>
На нашем сайте представлена продукция для бизнеса и частных лиц. Предлагаем вам системы для промышленной
                автоматизации, системы защиты онлайн-платежей, металлоконструкции, станки и производственные агрегаты.
Комплектующие привозим из Японии и Германии, собираем в России. Металлоконструкции произведены в
                Уральском регионе России.
            </p>
            <p>
У нас самые выгодные условия для покупки автомобилей марок BMW, Porsche, Audi и Mercedes. Наши
                консультанты подробно проконсультируют вас по техническим параметрам представленных моделей и подберут
                подходящий автомобиль, ориентируясь на ваши ожидания. Юридическим лицам предлагаем услугу лизинга,
                благодаря которой вы сможете ускорить амортизацию и снизить налог на имущество в три раза.
            </p>
        </div>
        <?
        global $arrFilter;
        $sectionForFilter = CIBlockSection::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]], "CODE" => "themes"], false, ["ID", "IBLOCK_ID"]);
        $sectionForFilter = $sectionForFilter->GetNext();

        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "themes",
            Array(
                "ADD_SECTIONS_CHAIN" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "COUNT_ELEMENTS" => "Y",
                "FILTER_NAME" => "arrFilter",
                "IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]],
                "IBLOCK_CODE" => $arResult["VARIABLES"]["IBLOCK_CODE"],
                "IBLOCK_TYPE" => "questions",
                "SECTION_CODE" => "",
                "SECTION_FIELDS" => array("", ""),
                "SECTION_ID" => $sectionForFilter["ID"],
                "SECTION_URL" => "#SECTION_CODE#/",
                "SECTION_USER_FIELDS" => array("", ""),
                "SHOW_PARENT_NAME" => "Y",
                "TOP_DEPTH" => "5",
                "VIEW_MODE" => "LINE",
                "ARCHIVE_URL" => "archive"
            )
        );
?>
</div>