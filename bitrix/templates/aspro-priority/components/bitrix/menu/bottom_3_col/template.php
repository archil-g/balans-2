<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$this->setFrameMode(true);
$colmd = 12;
$colsm = 12;
?>
<?if($arResult):?>
	<?
	if(!function_exists("ShowSubItems2")){
		function ShowSubItems_2_col($arItem){
			?>
			<?if($arItem["CHILD"]):?>
            <?
                if(count($arItem["CHILD"]) > 1){
                    $child = array_chunk($arItem["CHILD"],round(count($arItem["CHILD"])/3));
                }
                else{
                    $child = [$arItem["CHILD"]];
                }
                ?>
				<?$noMoreSubMenuOnThisDepth = false;
				$count = count($arItem["CHILD"]);?>
                <div class="row">
                    <?foreach ($child as $ch_items):?>
                        <div class="col-md-4 col-xs-12">
                            <div class="wrap">
                                <?foreach($ch_items as $arSubItem):?>
                                    <?
                                    if($arSubItem['PARAMS']['TYPE'] == 'PRODUCT'){
                                        $bProductItem = true;
                                        continue;
                                    }
                                    ?>
                                    <?$bLink = strlen($arSubItem['LINK']);?>
                                    <div class="item<?=($arSubItem["SELECTED"] ? " active" : "")?>">
                                        <div class="title">
                                            <?if($bLink):?>
                                                <a href="<?=$arSubItem['LINK']?>"><?=$arSubItem['TEXT']?></a>
                                            <?else:?>
                                                <span><?=$arSubItem['TEXT']?></span>
                                            <?endif;?>
                                        </div>
                                    </div>
                                    <?$noMoreSubMenuOnThisDepth |= CPriority::isChildsSelected($arSubItem["CHILD"]);?>
                                <?endforeach;?>
                            </div>
                        </div>
                    <?endforeach;?>
                </div>

			<?endif;?>
			<?
		}
	}
	?>
	<div class="bottom-menu">
		<div class="items">
			<?foreach($arResult as $i => $arItem):?>
					<?$bLink = strlen($arItem['LINK']);?>
					<div class="item<?=($arItem["SELECTED"] ? " active" : "")?>">
						<div class="title">
							<?if($bLink):?>
								<a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
							<?else:?>
								<span><?=$arItem['TEXT']?></span>
							<?endif;?>
						</div>
					</div>
					<?ShowSubItems_2_col($arItem);?>
			<?endforeach;?>
		</div>
	</div>
<?endif;?>