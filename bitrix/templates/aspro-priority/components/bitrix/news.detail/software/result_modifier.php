<?
if ($arResult["IBLOCK_SECTION_ID"] > 0) {
    $res = \CIBlockElement::GetList(
        ["sort" => "desc"],
        [
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "IBLOCK_SECTION_ID" => $arResult["IBLOCK_SECTION_ID"],
            "ACTIVE" => "Y"
        ],
        false,
        false
    );
    while($elems = $res->GetNextElement()){
        $arFields = $elems->GetFields();
        $arProps = $elems->GetProperties();

       $arResult["DESCR"][] = $arProps["custom_preview"]["VALUE"];

    }
}
?>