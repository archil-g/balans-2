<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?
global $isMenu, $arTheme;
use \Bitrix\Main\Localization\Loc;

$bOrderViewBasket = $arParams['ORDER_VIEW'];
$basketURL = (isset($arTheme['URL_BASKET_SECTION']) && strlen(trim($arTheme['URL_BASKET_SECTION']['VALUE'])) ? $arTheme['URL_BASKET_SECTION']['VALUE'] : SITE_DIR.'cart/');
$dataItem = ($bOrderViewBasket ? CPriority::getDataItem($arResult) : false);
$bFormQuestion = (isset($arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']) && $arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']['VALUE'] == 'Y');
$catalogLinkedTemplate = (isset($arTheme['ELEMENTS_TABLE_TYPE_VIEW']) && $arTheme['ELEMENTS_TABLE_TYPE_VIEW']['VALUE'] == 'catalog_table_2' ? 'catalog_linked_2' : 'catalog_linked');

/*set array props for component_epilog*/
$templateData = array(
	'DOCUMENTS' => $arResult['DISPLAY_PROPERTIES']['DOCUMENTS']['VALUE'],
	'LINK_SALE' => $arResult['DISPLAY_PROPERTIES']['LINK_SALE']['VALUE'],
	'LINK_FAQ' => $arResult['DISPLAY_PROPERTIES']['LINK_FAQ']['VALUE'],
	'LINK_PROJECTS' => $arResult['DISPLAY_PROPERTIES']['LINK_PROJECTS']['VALUE'],
	'LINK_SERVICES' => $arResult['DISPLAY_PROPERTIES']['LINK_SERVICES']['VALUE'],
	'LINK_GOODS' => $arResult['DISPLAY_PROPERTIES']['LINK_GOODS']['VALUE'],
	'LINK_PARTNERS' => $arResult['DISPLAY_PROPERTIES']['LINK_PARTNERS']['VALUE'],
	'LINK_SERTIFICATES' => $arResult['DISPLAY_PROPERTIES']['LINK_SERTIFICATES']['VALUE'],
	'LINK_VACANCYS' => $arResult['DISPLAY_PROPERTIES']['LINK_VACANCYS']['VALUE'],
	'LINK_STAFF' => $arResult['DISPLAY_PROPERTIES']['LINK_STAFF']['VALUE'],
	'LINK_REVIEWS' => $arResult['DISPLAY_PROPERTIES']['LINK_REVIEWS']['VALUE'],
	'BRAND_ITEM' => $arResult['BRAND_ITEM'],
	'GALLERY_BIG' => $arResult['GALLERY_BIG'],
	'VIDEO' => $arResult['VIDEO'],
	'VIDEO_IFRAME' => $arResult['VIDEO_IFRAME'],
	'DETAIL_TEXT' => $arResult['FIELDS']['DETAIL_TEXT'],
	'ORDER' => $bOrderViewBasket,
	'FORM_QUESTION' => $arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']['VALUE'],
	'CATALOG_LINKED_TEMPLATE' => $catalogLinkedTemplate,
);
if(isset($arResult['PROPERTIES']['BNR_TOP']) && $arResult['PROPERTIES']['BNR_TOP']['VALUE_XML_ID'] == 'YES')
	$templateData['SECTION_BNR_CONTENT'] = true;
?>

<?// shot top banners start?>
<?$bShowTopBanner = false;//(isset($arResult['SECTION_BNR_CONTENT'] ) && $arResult['SECTION_BNR_CONTENT'] == true);?>
<?if($bShowTopBanner):?>
	<?$this->SetViewTarget("section_bnr_content");?>
		<?CPriority::ShowTopDetailBanner($arResult, $arParams);?>
	<?$this->EndViewTarget();?>
<?endif;?>
<?$bShowFormQuestion = ($arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']['VALUE_XML_ID'] == 'YES');?>
<??>
<?// shot top banners end?>
<?if(\Bitrix\Main\Loader::includeModule('subscribe')):?>
<?if($arResult['PREVIEW_TEXT']) $SHOW_ABOUT = true;?>
    <?ob_start();?>
    <div class="ask_a_question">
        <?if ($arParams["UF_SHOW_PROMO"]) {?>
            <div class="text promoblock media-promo" style="display: block">
                <?$APPLICATION->IncludeComponent(
                    'bitrix:main.include',
                    '',
                    Array(
                        'AREA_FILE_SHOW' => 'file',
                        'PATH' => SITE_DIR.'include/promoblock.php',
                        'EDIT_TEMPLATE' => '',
                        'TYPE' => $arParams["USER_PROMO_TYPE"],
                        'DIR_TYPE' => $arParams["USER_PROMO_DIR_TYPE"],
                    )
                );?>
            </div>
        <?}?>
        <?if ($arParams["SHOW_USEFUL_LINKS"]) {?>
            <?$APPLICATION->IncludeComponent(
                'bitrix:main.include',
                '',
                Array(
                    'AREA_FILE_SHOW' => 'file',
                    'PATH' => SITE_DIR.'include/useful_links.php',
                    'EDIT_TEMPLATE' => '',
                    'TYPE' => $arParams["USER_LINKS_TYPE"],
                    'DIR_TYPE' => $arParams["USER_LINKS_DIR_TYPE"],
                )
            );?>
        <?}?>
        <div class="ask_a_question_custom border shadow subscribe">
            <div class="inner">
                <div class="text-block">
                    <?$APPLICATION->IncludeComponent(
                        'bitrix:main.include',
                        '',
                        Array(
                            'AREA_FILE_SHOW' => 'file',
                            'PATH' => SITE_DIR.'include/ask_question_product.php',
                            'EDIT_TEMPLATE' => ''
                        )
                    );?>
                </div>
            </div>
            <div class="outer">
                <span class="font_upper animate-load" title="Написать сообщение" data-event="jqm" data-param-id="20" data-name="question"><?=GetMessage('S_ASK_QUESTION')?></span>
            </div>
        </div>
    </div>
    <?$sFormQuestion = ob_get_contents();
    ob_end_clean();?>
<?else:?>
	<?ob_start();?>
		<div class="ask_a_question border shadow">
			<div class="inner">
				<div class="text-block">
					<?$APPLICATION->IncludeComponent(
						 'bitrix:main.include',
						 '',
						 Array(
							  'AREA_FILE_SHOW' => 'file',
							  'PATH' => SITE_DIR.'include/ask_question.php',
							  'EDIT_TEMPLATE' => ''
						 )
					);?>
				</div>
			</div>
			<div class="outer">
				<span class="font_upper animate-load" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_question");?>" data-autoload-need_product="<?=CPriority::formatJsName($arResult['NAME'])?>" data-name="question"><?=(strlen($arParams['S_ASK_QUESTION']) ? $arParams['S_ASK_QUESTION'] : Loc::getMessage('S_ASK_QUESTION'))?></span>
			</div>
		</div>

	<?$sFormQuestion = ob_get_contents();
	ob_end_clean();?>
<?endif;?>
<div class="row">
	<div class="col-md-9">
		<?// element name?>
		<?if($arParams['DISPLAY_NAME'] != 'N' && strlen($arResult['NAME'])):?>
			<h2><?=$arResult['NAME']?></h2>
		<?endif;?>
        <?if(!empty($arResult["DESCR"])):?>
			<div class="content about-product-content">
                <?
                $akk = 0;
                $akkCount = 1;
                ?>
				<?foreach ($arResult["DESCR"] as $descItem):?>

                    <?if($descItem[0]["IMAGE"] && !$descItem[0]["DESC"] && $descItem[0]["TITLE"]):?>
                        <div class="organisation__icons-wrapper">
                            <?foreach ($descItem as $dt):?>
                            <div class="organisation__icon-item">
                                <div class="pp-organization-img">
                                <img width="70" src="<?=\CFile::GetPath($dt["IMAGE"])?>" alt="">
                                </div>
                                <div class="pp-organization-text">
                                <p><?=$dt["TITLE"]?></p>
                                </div>
                            </div>
                            <?endforeach;?>
                        </div>
                    <?elseif (!$descItem[0]["IMAGE"] && $descItem[0]["DESC"] && $descItem[0]["TITLE"]):?>
                        <div class="accordion-type-<?=$akkCount?>">
                            <?foreach ($descItem as $dt):?>
                                <div class="item border shadow">
                                    <div class="accordion-head accordion-close collapsed" data-toggle="collapse" data-parent="#accordion<?=$akk?>" href="#accordion<?=$akk?>">
                                        <span class="arrow_open pull-right"></span>
                                        <?=$dt["TITLE"]?>
                                    </div>
                                    <div id="accordion<?=$akk?>" class="panel-collapse collapse" style="height: 0px;">
                                        <div class="accordion-body">
                                            <div class="row">
                                                <div class="col-md-12"><?=htmlspecialchars_decode($dt["DESC"])?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?$akk++;?>
                            <?endforeach;?>
                        </div>
                        <?$akkCount++?>
                    <?else:?>
                        <?foreach ($descItem as $dt):?>
                            <?if($dt["TITLE"]):?>
                                <h4><?=$dt["TITLE"]?></h4>
                            <?endif;?>
                            <?if($dt["DESC"]):?>
                                <p style="margin:10px;" ><?=htmlspecialchars_decode($dt["DESC"])?></p>
                            <?endif;?>
                        <?endforeach;?>
                    <?endif;?>
                <?endforeach;?>
			</div>
		<?endif;?>
	</div>
	<div class="col-md-3 scrollable-block">
		<div class="fixed_block_fix"></div>
		<div class="ask_a_question_wrapper">
			<?=$sFormQuestion;?>
		</div>
	</div>
</div>