<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?if($arResult['ITEMS']):?>
    <div class="items-for-list">
            <?foreach ($arResult['ITEMS'] as $key => $arItem):
            ?>
            <div class="item_block <?= ($key%4 < 2) ? "greylineAS" : ""?>">
                <div class="maxwidth-theme">
                    <div class="itemAS clearfix <?if($arItem["PREVIEW_PICTURE"]["SRC"]){?> pic-<?= $key%2 == 0 ? "left" : "right"?><?}?>">
                        <?if($arItem["PREVIEW_PICTURE"]["SRC"]){?>
                        <div class="image">
                            <img alt class="img-responsive" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                        </div>
                        <?}?>
                        <div class="text">
                            <h3><?=$arItem["NAME"]?></h3>
                            <?=$arItem['PREVIEW_TEXT']?>
                        </div>
                    </div>
                </div>
            </div>
            <?endforeach;?>
        </div>
<?endif;?>
<br /><br /><br /><br />
