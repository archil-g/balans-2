<?
$creatersIds = [];
foreach($arResult['ITEMS'] as &$arItem){
    $creatersIds[$arItem['ID']] = $arItem['CREATED_BY'];
    if($arParams['CUR_USER'] != $arItem['CREATED_BY'] && !empty($arItem['DISPLAY_PROPERTIES']['USERS_EDITORS']['VALUE'])) {
        $filter = Array("ID" => implode('|',$arItem['DISPLAY_PROPERTIES']['USERS_EDITORS']['VALUE']));
        $arItem['DISPLAY_PROPERTIES']['USERS_EDITORS']['VALUE'] = [];
        $rsUsers = CUser::GetList(($by = "NAME"), ($order = "asc"), $filter,["FIELDS"=>['ID', 'NAME', 'LAST_NAME']]);
        while ($arUser = $rsUsers->Fetch()) {
            $arItem['DISPLAY_PROPERTIES']['USERS_EDITORS']['VALUE'][] = $arUser['NAME'].' '.$arUser['LAST_NAME'].' ('.$arUser['NAME'].')';
        }
    }
}
if(!empty($creatersIds)) {
    $filter = Array("ID" => implode('|',$creatersIds));
    $rsUsers = CUser::GetList(($by = "NAME"), ($order = "asc"), $filter,["FIELDS"=>['ID', 'EMAIL', 'LAST_NAME']]);
    while ($arUser = $rsUsers->Fetch()) {
        $arResult['CREATED_EMAILS'][array_search($arUser['ID'],$creatersIds)] = $arUser['EMAIL'];
    }
}
?>