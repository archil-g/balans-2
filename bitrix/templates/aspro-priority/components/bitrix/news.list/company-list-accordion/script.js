$(document).ready(function(){
	if($('.table .row.sid').length)
	{
		$('.table .row.sid').each(function(){
			$(this).find('.item:visible .image').sliceHeight({lineheight: -3});
			$(this).find('.item:visible .properties').sliceHeight();
			$(this).find('.item:visible .text').sliceHeight();
		})
	}
	if($('.table.item-views .tabs a').length)
	{
		$('.table.item-views .tabs a').first().addClass('heightsliced');
		$('.table.item-views .tabs a').on('click', function() {
			if(!$(this).hasClass('heightsliced')){
				$('.table.item-views .tab-pane.active').find('.item .image').sliceHeight({lineheight: -3});
				$('.table.item-views .tab-pane.active').find('.item .properties').sliceHeight();
				$('.table.item-views .tab-pane.active').find('.item .text').sliceHeight();
				$(this).addClass('heightsliced');
			}
		});
	}
    $('.js-unbind-btn').on('click',function (e) {
        e.preventDefault();
        var _this = $(this);
        var wrap = $(this).closest('.js-company-item');
        $.ajax({
            url: '/ajax/api.php',
            dataType : 'json',
            data: {"action": "unbind_company",'id':_this.data('id')},
            beforeSend: function(){
                wrap.addClass('loadings');
            },
            success: function(data){
                if(data.success){
                    wrap.slideDown(300).remove();
                }
            },
            complete: function () {
                wrap.removeClass('loadings');
            }
        });
    });
    $('.js-editor-request').on('click',function (e) {
        e.preventDefault();
        var _this = $(this);
        var wrap = $(this).closest('.js-company-item');
        $.ajax({
            url: '/ajax/api.php',
            dataType : 'json',
            data: {"action": "editor_request",'id':_this.data('id'),'user':_this.data('user')},
            beforeSend: function(){
                wrap.addClass('loadings');
            },
            success: function(data){
                if(data.success){
                    pushNotifications('successJsButtonSuccess', "Запрос отправлен");
                    _this.remove();
                }
            },
            complete: function () {
                wrap.removeClass('loadings');
            }
        });
    })
})