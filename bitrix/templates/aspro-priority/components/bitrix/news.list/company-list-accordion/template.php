<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>

<div class="item-views">
<?if($arResult['ITEMS']):?>
<div class="items">
    <?foreach($arResult['ITEMS'] as $i => $arItem):?>
        <?ob_start();?>
        <?// element display properties?>
        <?if($arItem['DISPLAY_PROPERTIES']):?>
            <table class="table">
                <?foreach($arItem['DISPLAY_PROPERTIES'] as $PCODE => $arProperty):?>
                    <?if(in_array($PCODE, array('PERIOD', 'TITLE_BUTTON', 'LINK_BUTTON'))) continue;?>
                    <?if($PCODE == "USERS_EDITORS" && $arParams['CUR_USER'] != $arItem['CREATED_BY']) continue;?>
                    <tr>
                        <td>
                            <?=$arProperty['NAME']?>:
                        </td>
                        <td>
                            <?if(is_array($arProperty['DISPLAY_VALUE'])):?>
                                <?$val = implode('&nbsp;/&nbsp;', $arProperty['DISPLAY_VALUE']);?>
                            <?else:?>
                                <?$val = $arProperty['DISPLAY_VALUE'];?>
                            <?endif;?>
                            <?=$val?>
                        </td>
                    </tr>
                <?endforeach;?>
            </table>
        <?endif;?>
        <?if($arParams['CUR_USER'] == $arItem['CREATED_BY'] || in_array($arParams['CUR_USER'],$arItem['PROPERTIES']['USERS_EDITORS']['VALUE'])):?>
            <a class="btn btn-default pull-right mr10" href="/cabinet/company/form/?company_id=<?=$arItem['ID']?>">Редактировать</a>
        <?elseif(!empty($arResult['CREATED_EMAILS'][$arItem['ID']])):?>
<!--        <p>Запросить доступ на редактирование можно, написав письмо создателю компании --><?//=$arResult['CREATED_EMAILS'][$arItem['ID']]?><!--</p>-->
            <a class="btn btn-default pull-right mr10 js-editor-request" href="" data-user="<?=$USER->getId()?>" data-id="<?=$arItem['ID']?>">Запросить доступ на редактирование</a>
        <?endif?>
        <a class="btn btn-default pull-right js-unbind-btn mr10" data-id="<?=$arItem['ID']?>" href="">Открепить</a>
        <?$textPart = ob_get_clean();?>
        <div class="accordion-type-1 js-company-item">
            <div class="item border shadow">
                <div class="accordion-head accordion-close" data-toggle="collapse" href="#accordion<?=$arItem['ID']?>">
                    <span><b><?=$arItem['NAME']?></b> <span class="small"><?=$arItem['DISPLAY_PROPERTIES']['INN']['VALUE']?> / <?=$arItem['DISPLAY_PROPERTIES']['KPP']['VALUE']?></span>
                        <i class="fa fa-angle-down" style="top: 26px"></i></span>

                </div>
                <div id="accordion<?=$arItem['ID']?>" class="panel-collapse collapse">
                    <div class="accordion-body">
                        <div class="row">
                           <div class="col-md-12"><div class="text"><?=$textPart?></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?endforeach;?>
</div>
<div class="clearfix mt20"></div>
	<?// bottom pagination?>
	<?if($arParams['DISPLAY_BOTTOM_PAGER']):?>
		<?=$arResult['NAV_STRING']?>
	<?endif;?>
<?else:?>
<p>Нет компаний</p>
<?endif;?>
</div>
