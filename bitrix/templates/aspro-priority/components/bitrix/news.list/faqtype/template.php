<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<div class="item-views faq_list">
    <div class="tabs">
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="items">
                    <? foreach ($arResult["ITEMS"] as $arItem): ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>

                        <div class="accordion-type-1">
                            <div class="item border shadow wti">
                                <div class="accordion-head $arItem-close" data-toggle="collapse"
                                     href="#accordion_<?= $arItem["ID"] ?>">
                                    <span class="arrow_open pull-right"></span>
                                    <?= $arItem["NAME"] ?>
                                </div>
                                <div id="accordion_<?= $arItem["ID"] ?>" class="panel-collapse collapse">
                                    <div class="accordion-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="text">
                                                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['VALUE'])):?>
                                                    <?if(!is_array($arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['VALUE'])){
                                                        $documents = [$arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['VALUE']];
                                                        }else{
                                                            $documents = $arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['VALUE'];
                                                        }
                                                        ?>
                                                    <div class="row docs-block">
                                                        <?foreach($documents as $docID):?>
                                                            <?$arItem = CPriority::get_file_info($docID);?>
                                                            <div class="col-md-4">
                                                                <?
                                                                $fileName = substr($arItem['ORIGINAL_NAME'], 0, strrpos($arItem['ORIGINAL_NAME'], '.'));
                                                                $fileTitle = (strlen($arItem['DESCRIPTION']) ? $arItem['DESCRIPTION'] : $fileName);

                                                                ?>
                                                                <div class="blocks clearfix <?=$arItem["TYPE"];?>">
                                                                    <div class="inner-wrapper">
                                                                        <a href="<?=$arItem['SRC']?>" class="dark-color text" download="<?=$fileName?>"><?=$fileTitle?></a>
                                                                        <div class="filesize font_xs"><?=CPriority::filesize_format($arItem['FILE_SIZE']);?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?endforeach;?>
                                                    </div>
                                                    <?endif?>
                                                    <div class="previewtext">
                                                        <?/*if(!empty($arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['FILE_VALUE'])){
                                                            if(is_array($arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['DISPLAY_VALUE'])){
                                                                $i = 0;
                                                                foreach ($arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['FILE_VALUE'] as $doc){
                                                                    $i++;
                                                                    ?><a class="btn btn-default btn-transparent mr10" href="<?=$doc['SRC']?>" download="<?=$doc['ORIGINAL_NAME']?>"><?=$doc['DESCRIPTION']?$doc['DESCRIPTION']:'Загрузить '.$i?></a> <?
                                                                }
                                                            }else{
                                                                ?><a class="btn btn-default btn-transparent mr10" href="<?=$arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['FILE_VALUE']['SRC']?>" download="<?=$arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['FILE_VALUE']['ORIGINAL_NAME']?>"><?=$arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['FILE_VALUE']['DESCRIPTION']?$arItem['DISPLAY_PROPERTIES']['DOCUMENTS']['FILE_VALUE']['DESCRIPTION']:'Загрузить'?></a> <?
                                                            }
                                                        }*/?>
                                                        <div>
                                                            <?= $arItem["~PREVIEW_TEXT"] ?>
                                                        </div>
                                                    </div>
                                                    <?
                                                    if ($arItem["DETAIL_TEXT"] != "") {
                                                        ?>
                                                        <div>
                                                            <a class="btn btn-default btn-sm"
                                                               href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                                                Подробнее
                                                            </a>
                                                        </div>
                                                        <?
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
                <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?><div class="pagination_nav"><?= $arResult["NAV_STRING"] ?></div><? endif; ?>
            </div>
        </div>
    </div>
</div>
