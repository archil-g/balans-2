<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

foreach ($arResult["ITEMS"] as &$item){
    if (is_array($item["DISPLAY_PROPERTIES"]["DOCUMENTS"]["VALUE"])) {
        if (sizeof($item["DISPLAY_PROPERTIES"]["DOCUMENTS"]["VALUE"]) == 1) {
            switch ($item["DISPLAY_PROPERTIES"]["DOCUMENTS"]["FILE_VALUE"]["CONTENT_TYPE"]) {
                case "application/vnd.ms-excel":
                    $item["FILE_TYPE"] = "xls";
                    break;
                case "image/png":
                    $item["FILE_TYPE"] = "jpg";
                    break;
            }
            $item["FILE_SIZE"] = round($item["DISPLAY_PROPERTIES"]["DOCUMENTS"]["FILE_VALUE"]["FILE_SIZE"] / 1000, 1);
            $item["FILE_SRC"] = $item["DISPLAY_PROPERTIES"]["DOCUMENTS"]["FILE_VALUE"]["SRC"];
        }
    }
}

foreach ($arResult['ITEMS'] as &$arItem) {
    $arItem["DETAIL_PAGE_URL"] = $arParams["ELEMENT_URL"] . $arItem["CODE"] . "/";
}

?>