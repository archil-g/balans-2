<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (sizeof($arResult["ITEMS"]) == 0)
    exit;
?>
<div class="tab-pane">
    <?if($arParams["HIDE_SECTION_INFO"] != "Y"):?>
    <h3><?= $arParams["SECTION_NAME"] ?></h3>
    <div class="text_before_items">
        <?= $arParams["SECTION_DESCRIPTION"] ?>
    </div>
    <?endif;?>
    <div class="docs-block">
        <div class="docs_wrap row margin0">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                ?>
                <div class="item pull-left" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <div class="blocks border shadow clearfix <?=$arItem["FILE_TYPE"]?>">
                        <div class="inner-wrapper">
                            <div class="title">
                                <a href="<? if(isset($arItem["FILE_SRC"])) echo $arItem["FILE_SRC"]; else echo $arItem["DETAIL_PAGE_URL"];?>"
                                   class="dark-color text <?
                                   if ($arItem["FILE_TYPE"] == "jpg" && sizeof($arItem["DISPLAY_PROPERTIES"]["DOCUMENTS"]["VALUE"])==1) echo "fancybox"; ?>"
                                   target="_blank" <?
                                if ($arItem["FILE_TYPE"] != "jpg" && sizeof($arItem["DISPLAY_PROPERTIES"]["DOCUMENTS"]["VALUE"])==1) echo "download" ?> target="_blank"><?=$arItem["NAME"]?></a>
                            </div>
                            <div class="filesize font_xs">
                                <?
                                if (!empty($arItem["FILE_SIZE"])){
                                ?>
                                <?= $arItem["FILE_SIZE"] ?> Кб
                                <?}?>
                            </div>
                            <?
                            if(sizeof($arItem["DISPLAY_PROPERTIES"]["DOCUMENTS"]["VALUE"])==1){
                                ?>
                                <a href="<? if(isset($arItem["FILE_SRC"])) echo $arItem["FILE_SRC"]; else echo $arItem["DETAIL_PAGE_URL"];?>" class="arrow_link <?if($arItem["FILE_TYPE"] == "jpg") echo "fancybox";?>"></a>
                                <?
                            }
                            ?>
                            <div style="margin-top: 25px;">
                                <?=$arItem["~PREVIEW_TEXT"];?>
                            </div>
                            <?
                            if ($arItem["DETAIL_TEXT"] != "") {
                                ?>
                                <div>
                                    <a class="btn btn-default btn-sm"
                                       href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                        Подробнее
                                    </a>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
