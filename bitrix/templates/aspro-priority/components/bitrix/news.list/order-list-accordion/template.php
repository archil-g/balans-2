<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?//dump($arResult['ITEMS']); die();?>
<?if($arResult['ITEMS']):?>

    <?foreach($arResult['ITEMS'] as $i => $arItem):?>
        <?ob_start();?>
        <?$prop_codes = ["REP_PERIOD", 'COMPANY', 'COMPANY_PAY'];?>
        <?if($arItem['DISPLAY_PROPERTIES']):?>
            <table class="table">
                <?foreach($arItem['DISPLAY_PROPERTIES']['OFFER']['DISPLAY_VALUE'] as $key => $offer):?>
                <tr><td colspan="2"><b><?=is_array($arItem['DISPLAY_PROPERTIES']['PRODUCT']['DISPLAY_VALUE'])?strip_tags($arItem['DISPLAY_PROPERTIES']['PRODUCT']['DISPLAY_VALUE'][$key]):strip_tags($arItem['DISPLAY_PROPERTIES']['PRODUCT']['DISPLAY_VALUE'])?>: <?=strip_tags($offer)?></b></td></tr>
                    <?foreach ($prop_codes as $code):?>
                        <?if(is_array($arItem['DISPLAY_PROPERTIES'][$code]['DISPLAY_VALUE']) && !empty($arItem['DISPLAY_PROPERTIES'][$code]['DISPLAY_VALUE'][$key])):?>
                            <tr>
                                <td>
                                    <?=$arItem['DISPLAY_PROPERTIES'][$code]['NAME']?>:
                                </td>
                                <td>
                                    <?=strip_tags($arItem['DISPLAY_PROPERTIES'][$code]['DISPLAY_VALUE'][$key])?>
                                </td>
                            </tr>
                        <?elseif(!empty($arItem['DISPLAY_PROPERTIES'][$code]['DISPLAY_VALUE'])):?>
                            <tr>
                                <td>
                                    <?=$arItem['DISPLAY_PROPERTIES'][$code]['NAME']?>:
                                </td>
                                <td>
                                    <?=strip_tags($arItem['DISPLAY_PROPERTIES'][$code]['DISPLAY_VALUE'])?>
                                </td>
                            </tr>
                        <?endif?>
                    <?endforeach;?>
                <?endforeach;?>
                <?$prop_codes_dop = ["DOP_INN", "DOP_KPP", "DOP_CODE"];?>
                <?foreach ($prop_codes_dop as $code):?>
                    <?if(!empty($arItem['DISPLAY_PROPERTIES'][$code]['DISPLAY_VALUE'][$key])):?>
                        <tr>
                            <td>
                                <?=$arItem['DISPLAY_PROPERTIES'][$code]['NAME']?>:
                            </td>
                            <td>
                                <?=strip_tags($arItem['DISPLAY_PROPERTIES'][$code]['DISPLAY_VALUE'][$key])?>
                            </td>
                        </tr>
                    <?endif?>
                <?endforeach;?>

            </table>
        <?endif;?>
        <p class="summ"><div class="pull-right"><b><?=$arItem['DISPLAY_PROPERTIES']['SUMM']['DISPLAY_VALUE']?> руб</b></div> </p>
        <?$textPart = ob_get_clean();?>
        <div class="accordion-type-1">
            <div class="item border shadow">
                <div class="accordion-head accordion-close" data-toggle="collapse" href="#accordion<?=$arItem['ID']?>">
                    <span><b>#<?=$arItem['ID']?> <?=$arItem['NAME']?></b> <span class="label label-info"><?=$arItem['DISPLAY_PROPERTIES']['STATUS']['DISPLAY_VALUE']?></span>
                        <i class="fa fa-angle-down"></i></span>
                </div>
                <div id="accordion<?=$arItem['ID']?>" class="panel-collapse collapse">
                    <div class="accordion-body">
                        <div class="row">
                           <div class="col-md-12"><div class="text"><?=$textPart?></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?endforeach;?>
	<?// bottom pagination?>
	<?if($arParams['DISPLAY_BOTTOM_PAGER']):?>
		<?=$arResult['NAV_STRING']?>
	<?endif;?>
<?else:?>
<p>Нет компаний</p>
<?endif;?>