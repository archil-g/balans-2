<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<? use \Bitrix\Main\Localization\Loc;

//dump($arResult['ITEMS']);
if ($arResult['ITEMS']) {
    ?>
    <div class="item-views tarifs type_3 tarifs_scroll linked upper-margin">
        <div class="flexslider unstyled row front dark-nav view-control navigation-vcenter"
             data-plugin-options='{"useCSS": false, "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "counts": [3, 2, 1], "itemMargin": 0}'>
            <ul class="slides">
                <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                    <li class="item border shadow" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <a class="link-not-colored" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <div class="image">
                            <div class="wrap">
                                <?
                                $arImage = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width' => 568, 'height' => 10000), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
                                ?>
                                <img class="img-responsive" src="<?= $arImage['src']; ?>" alt="<?= $arItem['NAME']; ?>"
                                     title="<?= $arItem['NAME']; ?>">
                            </div>
                        </div>
                        <div class="wrap">
                            <div class="body-wrap">
                                <div class="body-info">
                                    <h3>
                                        <?= $arItem['NAME'] ?>
                                    </h3>
                                    <?= $arItem['PREVIEW_TEXT'] ?>
                                </div>
                            </div>
                        </div>
                        </a>
                    </li>
                <? }; ?>
            </ul>
        </div>
    </div>
<? } ?>