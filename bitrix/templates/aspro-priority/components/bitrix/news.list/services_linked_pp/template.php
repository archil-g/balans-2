<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<div class="drag_block services">
    <div class="wraps">
        <h4><?=$arParams["TITLE"]?></h4>
        <div class="row">
            <div class="col-md-12">
                <div class="item-views linked sections services">
                    <div class="items row">
                        <?foreach($arResult['ITEMS'] as $arItem):?>
                        <?
                        // edit/add/delete buttons for edit mode
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div class="col-md-12 col-sm-12" id="">
                            <div class="item_wrap border shadow">
                                <div class="item clearfix" id="bx_3218110189_1577">
                                    <div class="image">
                                        <div class="wrap">
                                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=( $arItem['PICTURE']['ALT'] ? $arItem['PICTURE']['ALT'] : $arItem['NAME']);?>" title="<?=( $arItem['PICTURE']['TITLE'] ? $arItem['PICTURE']['TITLE'] : $arItem['NAME']);?>" class="img-responsive" />
                                            </a>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <div class="title">
                                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="dark-color"><?=$arItem['NAME']?></a>
                                        </div>
                                        <div class="previewtext"><p><?=$arItem['PREVIEW_TEXT']?></p></div>
                                    </div>
                                    <a class="arrow_open link" href="<?=$arItem['DETAIL_PAGE_URL']?>"></a>
                                </div>
                            </div>
                        </div>
                        <?endforeach;?>
                    </div>
                    <?=$arResult["NAV_STRING"]?>
                </div>
            </div>
        </div>
    </div>
</div>
