<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<? use \Bitrix\Main\Localization\Loc;
;?>
<div class="maxwidth-theme">
    <div class="item-views services-items type_5 icons type_3_within oportunities">
        <? if ($arResult['SECTIONS']): ?>
            <? foreach ($arResult['SECTIONS'] as $arItem): ?>
                <? if (empty($arItem["CHILD"])) continue; ?>
                <h2><?= $arItem["NAME"] ?></h2>
                <div class="items clearfix" style="margin-bottom: 50px">
                    <? foreach ($arItem['CHILD'] as $element): ?>
                        <? //echo "<pre>";print_r($element);echo "</pre>";?>
                        <div class="item shadow border left wbg program_item_wrapper" id="bx_1847241719_57"
                             style="height: 153px;">
                            <div class="wrap">
                                <div class="body-info opp-brick-announce-container">
                                    <div class="opp-brick-announce-img">
                                        <a class="opp-brick-announce-img-link" href="#<?= $element["ID"] ?>"><img
                                                    src="<?= $element["PROPERTY_ICON_VALUE"] ?>"></a>
                                    </div>
                                    <a class="dark-color opp-brick-announce-link"
                                       href="#<?= $element["ID"] ?>"><?= $element["NAME"] ?></a>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            <? endforeach; ?>
        <? endif; ?>
    </div>
</div>
<div class="maxwidth-theme oportunities">
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12 content-md">
            <? $count = 0; ?>
            <? foreach ($arResult['SECTIONS'] as $arItem): ?>
                <? if (empty($arItem["CHILD"])) continue; ?>
                <? foreach ($arItem['CHILD'] as $element): ?>
                    <?
                    if ($count % 2 === 0) {
                        $even = true;
                        $class1 = 'col-md-4 opp-announce-img';
                        $class2 = 'col-md-8 text-right no-right-padding opp-announce-text';
                    } else {
                        $even = false;
                        $class1 = 'col-md-8 text-left no-right-padding opp-announce-text';
                        $class2 = 'col-md-4 opp-announce-img';
                    }
                    ?>
                    <div class="row row-opportunities-item" id="<?= $element["ID"] ?>">
                        <div class="<?= $class1 ?>">
                            <? if ($even): ?>
                                <img class="media-object" src="<?= \CFile::GetPath($element["PREVIEW_PICTURE"]) ?>"
                                     alt="<?= $element["NAME"] ?>">
                            <? else: ?>
                                <h4 class="media-heading"><?= $element["NAME"] ?></h4>
                                <span><?= $element["PREVIEW_TEXT"] ?></span><br>
                                <? if ($element["DETAIL_TEXT"] != ""): ?><a
                                    href="<?= $arParams["SEF_FOLDER"] . $element["CODE"] . "/" ?>" >
                                        Подробнее</a><? endif ?>
                            <? endif; ?>

                        </div>
                        <div class="<?= $class2 ?>">
                            <? if ($even): ?>
                                <h4 class="media-heading"><?= $element["NAME"] ?></h4>
                                <span><?= $element["PREVIEW_TEXT"] ?></span><br>
                                <? if ($element["DETAIL_TEXT"] != ""): ?><a
                                    href="<?= $arParams["SEF_FOLDER"] . $element["CODE"] . "/" ?>" >
                                        Подробнее</a><? endif ?>
                            <? else: ?>
                                <img class="media-object" src="<?= \CFile::GetPath($element["PREVIEW_PICTURE"]) ?>"
                                     alt="<?= $element["NAME"] ?>">
                            <? endif; ?>
                        </div>

                    </div>
                    <? $count++; ?>
                <? endforeach; ?>
            <? endforeach; ?>
        </div>
		<?include('section_detail.php');?>
        <div class="col-md-3 col-sm-3 hidden-xs hidden-sm">
            <div class="fixed_block_fix"></div>
            <div class="ask_a_question_wrapper">
                <div class="ask_a_question">
                    <? if ($arParams["UF_SHOW_PROMO"]): ?>
                        <div class="promoblock">
                            <? $APPLICATION->IncludeComponent(
                                'bitrix:main.include',
                                '',
                                Array(
                                    'AREA_FILE_SHOW' => 'file',
                                    'PATH' => SITE_DIR . 'include/promoblock.php',
                                    'EDIT_TEMPLATE' => '',
                                    'TYPE' => 436
                                )
                            ); ?>
                        </div>
                    <? endif; ?>
                    <? if ($arParams["SHOW_USEFUL_LINKS"]): ?>
                        <? $APPLICATION->IncludeComponent(
                            'bitrix:main.include',
                            '',
                            Array(
                                'AREA_FILE_SHOW' => 'file',
                                'PATH' => SITE_DIR . 'include/useful_links.php',
                                'EDIT_TEMPLATE' => '',
                                'TYPE' => 433
                            )
                        ); ?>
                    <? endif; ?>
					
					<div class="ask_a_question_custom border shadow subscribe">
                        <div class="inner">
                            <div class="text-block">
                                <?$APPLICATION->IncludeComponent(
                                    'bitrix:main.include',
                                    '',
                                    Array(
                                        'AREA_FILE_SHOW' => 'file',
                                        'PATH' => SITE_DIR.'include/ask_question_product.php',
                                        'EDIT_TEMPLATE' => '',
										'S_ASK_QUESTION' => 'Задать вопрос',
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="outer">
							<span class="font_upper animate-load" title="Написать сообщение" data-event="jqm" data-param-id="20" data-name="question">Задать вопрос<?/*=GetMessage('S_ASK_QUESTION')*/?></span>
                        </div>
                    </div>
					
					
					
                </div>
            </div>
        </div>
    </div>

</div>
