<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<? use \Bitrix\Main\Localization\Loc; ?>
<div class="col-md-9" style="margin-bottom: 50px;">
    <div class="item-views services-items type_5 icons type_3_within oportunities">
        <?
        //dump($arResult);
        if (!empty($arResult['ITEMS'])):?>
            <h4 class="title-h2">Возможности</h4>
            <? foreach ($arResult['ITEMS'] as $element): ?>
                <?if($i == 3) break?>
                <div class="items clearfix opportunity-element">
                        <div class="wrap">
                            <div class="opportunity-main">
                                <div class="image-main hidden-xs">
									<a href="<?//= $element["DETAIL_PAGE_URL"]?><?= "/software/" . $arParams["SECTION_CODE"] . "/opportunities/" ?>#<?= $element["ID"] ?>"><img width="70"
                                                src="<?= $element["PROPERTIES"]["ICON"]["VALUE"] ?>"></a>
                                </div>
                                <div class="text-main">
                                <a class="link-to-detail" href="<?= "/software/" . $arParams["SECTION_CODE"] . "/opportunities/" ?>#<?= $element["ID"] ?>"><?= $element["NAME"] ?></a>
                                <div><?= $element["PROPERTIES"]["SUBTITLE"]["~VALUE"]?></div>
                                </div>
                            </div>
                        </div>
                </div>
                <?$i++;?>
            <? endforeach; ?>
            <a class="btn btn-default" href="<?= "/software/" . $arParams["SECTION_CODE"] . "/opportunities/" ?>">Подробнее</a>
        <? endif; ?>
    </div>
</div>
