<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
$frame = $this->createFrame()->begin();
$frame->setAnimation(true);
global $USER;
$userID = CUser::GetID();
$userID = ($userID > 0 ? $userID : 0);
global $arTheme;
$arParams["COUNT_IN_LINE"] = intval($arParams["COUNT_IN_LINE"]);
$arParams["COUNT_IN_LINE"] = (($arParams["COUNT_IN_LINE"] > 0 && $arParams["COUNT_IN_LINE"] < 12) ? $arParams["COUNT_IN_LINE"] : 3);
$colmd = floor(12 / $arParams['COUNT_IN_LINE']);
$colsm = floor(12 / round($arParams['COUNT_IN_LINE'] / 2));
$bShowImage = in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE']);
$bOrderViewBasket = $arParams['ORDER_VIEW'];
$basketURL = (strlen(trim($arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']['URL_BASKET_SECTION']['VALUE'])) ? trim($arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']['URL_BASKET_SECTION']['VALUE']) : '');
?>
<?
$bHasSection = false;
if(isset($arResult['SECTION_CURRENT']) && $arResult['SECTION_CURRENT'])
	$bHasSection = true;
if($bHasSection)
{
	// edit/add/delete buttons for edit mode
	$arSectionButtons = CIBlock::GetPanelButtons($arParams['IBLOCK_ID'], 0, $arResult['SECTION_CURRENT']['ID'], array('SESSID' => false, 'CATALOG' => true));
	$this->AddEditAction($arResult['SECTION_CURRENT']['ID'], $arSectionButtons['edit']['edit_section']['ACTION_URL'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'SECTION_EDIT'));
	$this->AddDeleteAction($arResult['SECTION_CURRENT']['ID'], $arSectionButtons['edit']['delete_section']['ACTION_URL'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'SECTION_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="section" id="<?=$this->GetEditAreaId($arResult['SECTION_CURRENT']['ID'])?>">
	<?
}?>
<div class="catalog item-views table" data-slice="Y">
	<?if($arResult["ITEMS"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?>
			<div class="pagination_nav">
				<?=$arResult["NAV_STRING"]?>
			</div>
		<?endif;?>
		<div class="items flexbox clearfix">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				// edit/add/delete buttons for edit mode
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				// use detail link?
				$bDetailLink = $arParams['SHOW_DETAIL_LINK'] != 'N' && (!strlen($arItem['DETAIL_TEXT']) ? ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] !== 'Y' && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] != 1) : true);
				// preview image
				if($bShowImage){
					$bImage = $arItem['PICTURE'];
					$imageSrc = $arItem['PICTURE'];
					$imageDetailSrc = false;
				}
				// use order button?

				$bOrderButton = $arParams['SHOW_DETAYL_BUTTON'] != 'Y';
				$dataItem = ($bOrderViewBasket ? CPriority::getDataItem($arItem) : false);
				?>

				<div class="item-wrap col-md-4 col-sm-4 col-xs-6">
					<div data-slice-block="Y" data-slice-params='{"classNull" : ".footer-button"}' class="item<?=($bShowImage ? '' : ' wti')?>" id="<?=$this->GetEditAreaId($arItem['ID'])?>"<?=($bOrderViewBasket ? ' data-item="'.$dataItem.'"' : '')?>>
						<div class="inner-wrap">
							<?if($bShowImage):?>
								<div class="image">
									<div class="wrap">
										<?if($arItem['PROPERTIES']['HIT']['VALUE']):?>
											<div class="stickers">
												<div class="stickers-wrapper">
													<?foreach($arItem['PROPERTIES']['HIT']['VALUE_XML_ID'] as $key => $class):?>
														<div class="sticker_<?=strtolower($class);?>"><?=$arItem['PROPERTIES']['HIT']['VALUE'][$key]?></div>
													<?endforeach;?>
												</div>
											</div>
										<?endif;?>
										<?if($bDetailLink):?><a href="<?=$arItem['DETAIL_PAGE_URL']?>">
										<?elseif($imageDetailSrc):?><a href="<?=$imageDetailSrc?>" alt="<?=($bImage ? $arItem['PREVIEW_PICTURE']['ALT'] : $arItem['NAME'])?>" title="<?=($bImage ? $arItem['PREVIEW_PICTURE']['TITLE'] : $arItem['NAME'])?>" class="img-inside fancybox">
										<?endif;?>
											<img class="img-responsive" src="<?=$imageSrc?>" alt="<?=($bImage ? $arItem['PREVIEW_PICTURE']['ALT'] : $arItem['NAME'])?>" title="<?=($bImage ? $arItem['PREVIEW_PICTURE']['TITLE'] : $arItem['NAME'])?>" />
										<?if($bDetailLink):?></a>
										<?elseif($imageDetailSrc):?><span class="zoom"><i class="fa fa-16 fa-white-shadowed fa-search"></i></span></a>
										<?endif;?>
									</div>
								</div>
							<?endif;?>

							<div class="text">
								<div class="cont">
									<?// element name?>
									<?if(strlen($arItem['FIELDS']['NAME'])):?>
										<div class="title">
											<?if($bDetailLink):?><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="dark-color"><?endif;?>
												<?=$arItem['NAME']?>
											<?if($bDetailLink):?></a><?endif;?>
										</div>
									<?endif;?>

									<?/*
									<?// element section name?>
									<?if(strlen($arItem['SECTION_NAME'])):?>
										<div class="section_name"><?=$arItem['SECTION_NAME']?></div>
									<?endif;?>
									*/?>

									<?// element status?>
									<?if(strlen($arItem['DISPLAY_PROPERTIES']['STATUS']['VALUE'])):?>
										<span class="status-icon <?=$arItem['DISPLAY_PROPERTIES']['STATUS']['VALUE_XML_ID']?>"><?=$arItem['DISPLAY_PROPERTIES']['STATUS']['VALUE']?></span>
									<?endif;?>

									<?// element article?>
									<?/*if(strlen($arItem['DISPLAY_PROPERTIES']['ARTIC']['VALUE'])):?>
										<span class="article"><?=GetMessage('S_ARTICLE')?>&nbsp;<span><?=$arItem['DISPLAY_PROPERTIES']['ARTIC']['VALUE']?></span></span>
									<?endif;*/?>
									
									<?if(isset($arItem['DISPLAY_PROPERTIES']['DELIVERY']) && strlen($arItem['DISPLAY_PROPERTIES']['DELIVERY']['DISPLAY_VALUE'])):?>
										<div class="delivery pull-right">
											<span class="icon"><?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/info.svg');?></span>
											<div class="tooltip"><span><?=$arItem['DISPLAY_PROPERTIES']['DELIVERY']['DISPLAY_VALUE'];?></span></div>
										</div>
									<?endif;?>

									<?/*
									<?// element preview text?>
									<?if(strlen($arItem['FIELDS']['PREVIEW_TEXT'])):?>
										<div class="description">
											<?if($arItem['PREVIEW_TEXT_TYPE'] == 'text'):?>
												<p><?=$arItem['FIELDS']['PREVIEW_TEXT']?></p>
											<?else:?>
												<?=$arItem['FIELDS']['PREVIEW_TEXT']?>
											<?endif;?>
										</div>
									<?endif;?>
									*/?>
								</div>

								<div class="row foot">
									<div class="col-md-12 col-sm-12 col-xs-12 clearfix slice_price">
										<?// element price?>
										<?if(!empty($arItem['PRICES'])):?>
											<div class="price<?=($bOrderViewBasket ? '  inline' : '')?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
												<div class="price_new">
													<span class="price_val"><?=CPriority::FormatPriceShema($arItem['PRICES']['PRICE'])?></span>
												</div>
												<?if(!empty($arItem['PRICES']['OLD_PRICE'])):?>
													<div class="price_old">
														<span class="price_val"><?=$arItem['PRICES']['OLD_PRICE']?></span>
													</div>
												<?endif;?>
											</div>
										<?endif;?>
									</div>
                                    <?
                                    $critNabor = [];
                                    foreach ($arItem['OPTIONS'] as $v) {
                                        $critNabor[$v['CRIT']] = $v['VAL'];
                                    }
                                    $dataItem = array(
                                        "IBLOCK_ID" => $arItem['IBLOCK_ID'],
                                        "ID" => $arItem['ID'],
                                        "NAME" => $arParams["SECTION_NAME"] . ': ' . $arItem['NAME'],
                                        "DETAIL_PAGE_URL" => $arItem['DETAIL_PAGE_URL'],
                                        "PREVIEW_PICTURE" => $arItem['PREVIEW_PICTURE']['ID'] ? $arItem['PREVIEW_PICTURE']['ID'] : $arParams["SECTION_PICTURE"],
                                        "DETAIL_PICTURE" => $arItem['DETAIL_PICTURE']['ID'] ? $arItem['DETAIL_PICTURE']['ID'] : $arParams["SECTION_PICTURE"],
                                        "PROPERTY_FILTER_PRICE_VALUE" => $arItem['PRICES']['PRICE'] ? $arItem['PRICES']['PRICE'] : '',
                                        "PROPERTY_PRICE_VALUE" => $arItem['PRICES']['PRICE'] ? $arItem['PRICES']['PRICE'] : '',
                                        "PROPERTY_PRICEOLD_VALUE" => $arItem['PRICES']['OLD_PRICE'] ? $arItem['PRICES']['OLD_PRICE'] : '',
                                        "PROPERTY_ARTICLE_VALUE" => $arItem['PROPERTIES']['ARTIC']['VALUE'],
                                        "PROPERTY_STATUS_VALUE" => '',
                                        "SECTION_ID" => $arParams['SECTION_ID'],
                                        "CRIT" => $critNabor,
                                        "IS_CODE" => $arItem['PROPERTIES']['IS_CODE']['VALUE'],
                                    ); ?>
									<div class="col-md-12 col-sm-12 col-xs-12" data-id="<?= $arItem['ID'] ?>"
                                         data-item="<?= htmlspecialchars(json_encode($dataItem)) ?>">

										<div class="footer-button buy_block" >
                                            <?if($arParams['SHOW_DETAYL_BUTTON'] == 'Y'):?>
                                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>"
                                                   class="btn btn-default"><span><?=(strlen($arParams['TO_ALL']) ? $arParams['TO_ALL'] : GetMessage('TO_ALL'))?></span></a>
                                            <?else:?>
                                                <span class="btn btn-default to_cart animate-load btn-transparent"
                                                      data-quantity="1"><span>В корзину</span></span>
                                                <a href="/cart/"
                                                   class="btn btn-default in_cart"><span>В корзине</span></a>
                                            <?endif?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?endforeach;?>
		</div>

		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<div class="pagination_nav">		
				<?=$arResult["NAV_STRING"]?>
			</div>
		<?endif;?>
	<?endif;?>
</div>
<?if($bHasSection):?>
	</div>
<?endif;?>
<?$frame->end();?>