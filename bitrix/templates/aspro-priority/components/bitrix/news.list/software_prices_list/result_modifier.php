<?
\Bitrix\Main\Loader::includeModule("rdn");
// get section names elements
$critList = \rdn\Helper::getCritByOpt($arParams['ALL_CRIT']);
foreach($arResult['ITEMS'] as $k => $arItem){

    $bImage = empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID']) ? $arParams['SECTION_PICTURE'] : $arItem['FIELDS']['PREVIEW_PICTURE']['ID'];
    $arImage = ($bImage ? CFile::ResizeImageGet($bImage, array('width' => 160, 'height' => 160), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true) : array());
    $imageSrc = ($bImage ? $arImage['src'] : SITE_TEMPLATE_PATH.'/images/svg/noimage_product.svg');
    $arResult['ITEMS'][$k]['PICTURE'] = $imageSrc;

    $prices = \rdn\Helper::getOfferPrices($arItem['ID']);
    $optionArticles = \rdn\Helper::parseOptFromArticle($arItem['PROPERTIES']['ARTIC']['VALUE']);
    $arResult['ITEMS'][$k]['PRICES'] = $prices;
	$arSectionsIDs[] = $arItem['IBLOCK_SECTION_ID'];

	if(!empty($optionArticles)){
	    foreach ($optionArticles as $opt){
	        if(!empty($critList[$opt]))
            $arResult['ITEMS'][$k]['OPTIONS'][] = $critList[$opt];
        }
    }
   /* echo "<pre>";
    print_r($arResult['ITEMS'][$k]['OPTIONS']); die();*/
}

?>