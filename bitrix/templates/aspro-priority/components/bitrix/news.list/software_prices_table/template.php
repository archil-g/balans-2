<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
$frame = $this->createFrame()->begin();
$frame->setAnimation(true);
global $USER;
$userID = CUser::GetID();
$userID = ($userID > 0 ? $userID : 0);
global $arTheme;
$arParams["COUNT_IN_LINE"] = intval($arParams["COUNT_IN_LINE"]);
$arParams["COUNT_IN_LINE"] = (($arParams["COUNT_IN_LINE"] > 0 && $arParams["COUNT_IN_LINE"] < 12) ? $arParams["COUNT_IN_LINE"] : 3);
$colmd = floor(12 / $arParams['COUNT_IN_LINE']);
$colsm = floor(12 / round($arParams['COUNT_IN_LINE'] / 2));
$bShowImage = in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE']);
$bOrderViewBasket = $arParams['ORDER_VIEW'];
$basketURL = (strlen(trim($arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']['URL_BASKET_SECTION']['VALUE'])) ? trim($arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']['URL_BASKET_SECTION']['VALUE']) : '');
?>

    <div class="catalog item-views list image-top table-list" data-slice="Y">
        <? if ($arResult["ITEMS"]): ?>
            <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
                <div class="pagination_nav">
                    <?= $arResult["NAV_STRING"] ?>
                </div>
            <? endif; ?>
            <div class="row items" itemscope itemtype="http://schema.org/ItemList">
                <? foreach ($arResult["ITEMS"] as $arItem): ?>

                    <?
// edit/add/delete buttons for edit mode
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
// use detail link?
                    $bDetailLink = $arParams['SHOW_DETAIL_LINK'] != 'N' && (!strlen($arItem['DETAIL_TEXT']) ? ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] !== 'Y' && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] != 1) : true);
// preview image
                    if($bShowImage){
                        $bImage = $arItem['PICTURE'];
                        $imageSrc = $arItem['PICTURE'];
                        $imageDetailSrc = false;
                    }
// use order button?
                    $bOrderButton = ($arItem["DISPLAY_PROPERTIES"]["FORM_ORDER"]["VALUE_XML_ID"] == "YES");
                    $dataItem = ($bOrderViewBasket ? CPriority::getDataItem($arItem) : false);
                    ?>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="item border shadow" itemprop="itemListElement" itemscope=""
                             itemtype="http://schema.org/Product">
                            <div class="image-wrapper">
                                <div class="image">
                                    <? if ($arItem['PROPERTIES']['HIT']['VALUE']): ?>
                                        <div class="stickers">
                                            <div class="stickers-wrapper">
                                                <? foreach ($arItem['PROPERTIES']['HIT']['VALUE_XML_ID'] as $key => $class): ?>
                                                    <div class="sticker_<?= strtolower($class); ?>"><?= $arItem['PROPERTIES']['HIT']['VALUE'][$key] ?></div>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($bDetailLink): ?><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                        <? elseif ($imageDetailSrc): ?><a href="<?= $imageDetailSrc ?>"
                                                                          alt="<?= ($bImage ? $arItem['PREVIEW_PICTURE']['ALT'] : $arItem['NAME']) ?>"
                                                                          title="<?= ($bImage ? $arItem['PREVIEW_PICTURE']['TITLE'] : $arItem['NAME']) ?>"
                                                                          class="img-inside fancybox">
                                            <? endif; ?>
                                            <img class="img-responsive" src="<?= $imageSrc ?>"
                                                 alt="<?= ($bImage ? $arItem['PREVIEW_PICTURE']['ALT'] : $arItem['NAME']) ?>"
                                                 title="<?= ($bImage ? $arItem['PREVIEW_PICTURE']['TITLE'] : $arItem['NAME']) ?>"/>
                                            <? if ($bDetailLink): ?></a>
                                        <? elseif ($imageDetailSrc): ?><span class="zoom"><i
                                                    class="fa fa-16 fa-white-shadowed fa-search"></i></span></a>
                                <? endif; ?>
                                </div>
                            </div>
                            <div class="text">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="cont">

                                            <div class="title">
                                                <? if ($bDetailLink): ?><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                                                                           class="dark-color"><? endif; ?>
                                                    <?= $arItem['NAME'] ?>
                                                    <? if ($bDetailLink): ?></a><? endif; ?>
                                            </div>
                                            <? if (strlen($arItem['PREVIEW_TEXT'])): ?>
                                                <div class="previewtext font_xs" itemprop="description">
                                                    <? if ($arItem['PREVIEW_TEXT_TYPE'] == 'text'): ?>
                                                        <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                                                    <? else: ?>
                                                        <?= $arItem['PREVIEW_TEXT'] ?>
                                                    <? endif; ?>
                                                </div>
                                            <? endif; ?>

                                            <?/*?><div class="props_list">
                                                <? if (!empty($arItem['OPTIONS'])): $i = 0; ?>
                                                    <? foreach ($arItem['OPTIONS'] as $option): $i++; ?>
                                                        <? if ($i == 4): ?>
                                                            <div class="hidden-block">
                                                            <div class="wrap">
                                                        <? endif ?>
                                                        <div class="prop font_xs">
                                                            <span class="title-prop"><?= $option['CRIT'] ?></span>
                                                            <span class="separator">—</span>
                                                            <span class="value"><?= $option['VAL'] ?></span>
                                                        </div>


                                                    <? endforeach ?>
                                                    <? if ($i > 3): ?>
                                                        </div>
                                                        </div>
                                                        <div class="rolldown font_upper"><span data-open_text="Свернуть"
                                                                                               data-close_text="Развернуть"><span
                                                                        class="text">Развернуть</span><svg width="8"
                                                                                                           height="5"
                                                                                                           viewBox="0 0 8 5">
	<path d="M510.692,193.293a1,1,0,0,0-1.415,0L507,195.6l-2.305-2.305a1,1,0,1,0-1.414,1.415l3.016,3.016a1,1,0,0,0,1.414,0l2.985-3.016A1,1,0,0,0,510.692,193.293Z"
          transform="translate(-503 -193)"></path>
</svg></span></div>
                                                    <? endif ?>
                                                <? endif ?>
                                            </div><?*/?>
                                        </div>
                                    </div>
                                    <?
                                    $critNabor = [];
                                    foreach ($arItem['OPTIONS'] as $v) {
                                        $critNabor[$v['CRIT']] = $v['VAL'];
                                    }
                                    $dataItem = array(
                                        "IBLOCK_ID" => $arItem['IBLOCK_ID'],
                                        "ID" => $arItem['ID'],
                                        "NAME" => $arParams["SECTION_NAME"] . ': ' . $arItem['NAME'],
                                        "DETAIL_PAGE_URL" => $arItem['DETAIL_PAGE_URL'],
                                        "PREVIEW_PICTURE" => $arItem['PREVIEW_PICTURE']['ID'] ? $arItem['PREVIEW_PICTURE']['ID'] : $arParams["SECTION_PICTURE"],
                                        "DETAIL_PICTURE" => $arItem['DETAIL_PICTURE']['ID'] ? $arItem['DETAIL_PICTURE']['ID'] : $arParams["SECTION_PICTURE"],
                                        "PROPERTY_FILTER_PRICE_VALUE" => $arItem['PRICES']['PRICE'] ? $arItem['PRICES']['PRICE'] : '',
                                        "PROPERTY_PRICE_VALUE" => $arItem['PRICES']['PRICE'] ? $arItem['PRICES']['PRICE'] : '',
                                        "PROPERTY_PRICEOLD_VALUE" => $arItem['PRICES']['OLD_PRICE'] ? $arItem['PRICES']['OLD_PRICE'] : '',
                                        "PROPERTY_ARTICLE_VALUE" => $arItem['PROPERTIES']['ARTIC']['VALUE'],
                                        "IS_CODE" => $arItem['PROPERTIES']['IS_CODE']['VALUE'],
                                        "PROPERTY_STATUS_VALUE" => '',
                                        "SECTION_ID" => $arParams['SECTION_ID'],
                                        "CRIT" => $critNabor,
                                    ); ?>
                                    <div class="col-md-4 col-sm-4 col-xs-12" data-id="<?= $arItem['ID'] ?>"
                                         data-item="<?= htmlspecialchars(json_encode($dataItem)) ?>">
                                        <div class="foot">

                                            <? if (!empty($arItem['PRICES'])): ?>
                                                <div class="price<?= ($bOrderViewBasket ? '  inline' : '') ?>"
                                                     itemprop="offers" itemscope
                                                     itemtype="http://schema.org/Offer">
                                                    <div class="price_new">
                                                        <span class="price_val"><?= CPriority::FormatPriceShema($arItem['PRICES']['PRICE']) ?></span>
                                                    </div>
                                                    <? if (!empty($arItem['PRICES']['OLD_PRICE'])): ?>
                                                        <div class="price_old">
                                                            <span class="price_val"><?= $arItem['PRICES']['OLD_PRICE'] ?></span>
                                                        </div>
                                                    <? endif; ?>
                                                </div>
                                            <? endif; ?>

                                            <div class="buy_block clearfix" >
                                                <div class="buttons">
                                                    <?if($arParams['SHOW_DETAYL_BUTTON'] == 'Y'):?>
                                                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>"
                                                           class="btn btn-default btn-sm"><span>Подробнее</span></a>
                                                    <?else:?>
                                                        <span class="btn btn-default btn-sm to_cart animate-load btn-transparent"
                                                              data-quantity="1"><span>В корзину</span></span>
                                                        <a href="/cart/"
                                                           class="btn btn-default in_cart btn-sm"><span>В корзине</span></a>
                                                    <?endif?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>

            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                <div class="pagination_nav">
                    <?= $arResult["NAV_STRING"] ?>
                </div>
            <? endif; ?>
        <? endif; ?>
    </div>
<? $frame->end(); ?>