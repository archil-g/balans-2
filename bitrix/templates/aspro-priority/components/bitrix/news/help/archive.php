<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$tabs = CIBlockSection::GetList(
    ["SORT" => "ASC"],
    [
        "IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]],
        "CODE" => ["products", "themes"]
    ],
    false,
    ["ID", "IBLOCK_ID", "NAME", "CODE"]
);
?>
<div class="maxwidth-theme">
    <?
    $sectionsIDs = [];
    $elements = CIBlockElement::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]], "ACTIVE" => "Y", "PROPERTY_ARCHIVE_VALUE" => "Y"], false, false, ["ID", "IBLOCK_ID", "IBLOCK_SECTION_ID"]);
    while ($element = $elements->GetNext()){
        $navChain = CIBlockSection::GetNavChain(false, $element["IBLOCK_SECTION_ID"]);
        while ($chain = $navChain->GetNext()){
            if($chain["DEPTH_LEVEL"] == 2){
                $sectionsIDs[] = $chain["ID"];
                break;
            }
        }
    }
    $sectionForFilter = CIBlockSection::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]], "CODE" => "products", "PROPERTY_ARCHIVE_VALUE" => "Y"], false, ["ID", "IBLOCK_ID"]);
    $sectionForFilter = $sectionForFilter->GetNext();
    global $arrFilter;
    $arrFilter["ID"] = $sectionsIDs;
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "help-products-archive",
        Array(
            "ADD_SECTIONS_CHAIN" => "N",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "COUNT_ELEMENTS" => "Y",
            "FILTER_NAME" => "arrFilter",
            "IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]],
            "IBLOCK_TYPE" => "",
            "SECTION_CODE" => "",
            "SECTION_FIELDS" => array("NAME", "PICTURE", "UF_*"),
            "SECTION_ID" => $sectionForFilter["ID"],
            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["archive_theme"],
            "SECTION_USER_FIELDS" => array("", ""),
            "SHOW_PARENT_NAME" => "Y",
            "TOP_DEPTH" => "2",
            "VIEW_MODE" => "LINE",
            "IBLOCK_CODE" => $arResult["VARIABLES"]["IBLOCK_CODE"]
        )
    );?>

</div>