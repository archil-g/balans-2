<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);
$this->addExternalCss("/bitrix/css/main/bootstrap.css");

$arParams["IBLOCK_ID"] = $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]];
$sectionInfo = CIBlockSection::GetList([], ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arResult["VARIABLES"]["ACTIVE_SUBTHEME"]], false, ["IBLOCK_ID", "ID", "IBLOCK_SECTION_ID", "DEPTH_LEVEL", "IBLOCK_TYPE"]);
$sectionInfo = $sectionInfo->Fetch();
$arResult["VARIABLES"]["SECTION_ID"] = $sectionInfo["ID"];
$arResult["VARIABLES"]["IBLOCK_SECTION_ID"] = $sectionInfo["IBLOCK_SECTION_ID"];
$elements = CIBlockElement::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]], "ACTIVE" => "Y", "PROPERTY_ARCHIVE_VALUE" => "Y"], false, false, ["ID", "IBLOCK_ID", "IBLOCK_SECTION_ID"]);
while ($element = $elements->GetNext()) {
    $navChain = CIBlockSection::GetNavChain(false, $element["IBLOCK_SECTION_ID"]);
    while ($chain = $navChain->GetNext()) {
        if ($chain["DEPTH_LEVEL"] == 4 && $chain["IBLOCK_SECTION_ID"] == $arResult["VARIABLES"]["IBLOCK_SECTION_ID"]) {
            $sectionsIDs[] = $chain["ID"];
            break;
        }
    }
}

$iblockInfo = CIBlock::GetByID($arParams["IBLOCK_ID"]);
$iblockInfo = $iblockInfo->fetch();
$iblockTypeID = $iblockInfo["IBLOCK_TYPE_ID"];
$iblockName = $iblockInfo["NAME"];
$template = ".default";

switch ($iblockInfo["IBLOCK_TYPE_ID"]) {
    case "files":
        $template = "files";
        break;
    case "videos":
        $template = "videos";
        break;
    case "articles":
        $template = "videos";
}

if ($template == "files") {
    ?>
    <div class="item-views documents_list type_2">
        <div class="group-content">
            <?
            $subSections = $sectionInfo = CIBlockSection::GetList([], ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"]], false, ["IBLOCK_ID", "ID", "IBLOCK_SECTION_ID", "DESCRIPTION", "NAME"]);
            while ($subSection = $subSections->GetNext()) {
                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "files",
                    Array(
                        "SECTION_NAME" => $subSection["NAME"],
                        "SECTION_DESCRIPTION" => $subSection["~DESCRIPTION"],
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array("", ""),
                        "FILTER_NAME" => "archiveFilter",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "51",
                        "IBLOCK_TYPE" => "files",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "99",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => $subSection["ID"],
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array("DOCUMENTS", ""),
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N"
                    )
                );
            } ?>
        </div>
    </div>
    <?
} else {
    ?>
    <div class="<?//= ($isSidebar ? "col-md-9 col-sm-8" : "col-xs-12") ?>">
        <div class="row">
                <?

                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",
                    $template,
                    array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $arResult["VARIABLES"]["IBLOCK_SECTION_ID"],
                        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                        "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
                        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["theme"],
                        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                        "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
                        "ADD_SECTIONS_CHAIN" => "N",
                        "IBLOCK_NAME" => $iblockName,
                        "ELEMENT_FILTER" => ['!PROPERTY_ARCHIVE' => false],
                        ""
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );

                if($template == "videos" || $template == "articles"){
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "video",
                        Array(
                            "SECTION_NAME" => $subSection["NAME"],
                            "SECTION_DESCRIPTION" => $subSection["~DESCRIPTION"],
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "Y",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array("NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT"),
                            "FILTER_NAME" => "archiveFilter",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => $sectionInfo["IBLOCK_ID"],
                            "IBLOCK_TYPE" => $arResult["VARIABLES"]["SECTION_CODE"],
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "N",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "99",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => $sectionInfo["ID"],
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array("DOCUMENTS", ""),
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "DESC",
                            "SORT_ORDER2" => "ASC",
                            "STRICT_SECTION_CHECK" => "N"
                        )
                    );
                }
                ?>

            <?

            ?>
        </div>
    </div>
    <?
}
?>
