<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$APPLICATION->AddChainItem("Архив");
$this->setFrameMode(true);
$this->addExternalCss("/bitrix/css/main/bootstrap.css");

global $arrFilter;
$sectionForFilter = CIBlockSection::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]], "CODE" => $arResult["VARIABLES"]["SECTION_CODE"]], false, ["ID", "IBLOCK_ID"]);
$sectionForFilter = $sectionForFilter->GetNext();

$elements = CIBlockElement::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]], "ACTIVE" => "Y", "PROPERTY_ARCHIVE_VALUE" => "Y"], false, false, ["ID", "IBLOCK_ID", "IBLOCK_SECTION_ID"]);
while ($element = $elements->GetNext()) {
    $navChain = CIBlockSection::GetNavChain(false, $element["IBLOCK_SECTION_ID"]);
    while ($chain = $navChain->GetNext()) {
        if ($chain["DEPTH_LEVEL"] == 3 && $chain["IBLOCK_SECTION_ID"] == $sectionForFilter["ID"]) {
            $sectionsIDs[] = $chain["ID"];
            break;
        }
    }
}

$arrFilter["ID"] = $sectionsIDs;

$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "themes",
    Array(
        "ADD_SECTIONS_CHAIN" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COUNT_ELEMENTS" => "Y",
        "FILTER_NAME" => "arrFilter",
        "IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]],
        "IBLOCK_CODE" => $arResult["VARIABLES"]["IBLOCK_CODE"],
        "IBLOCK_TYPE" => "questions",
        "SECTION_CODE" => "",
        "SECTION_FIELDS" => array("", ""),
        "SECTION_ID" => $sectionForFilter["ID"],
        "SECTION_URL" => "#SECTION_CODE#/",
        "SECTION_USER_FIELDS" => array("", ""),
        "SHOW_PARENT_NAME" => "Y",
        "TOP_DEPTH" => "5",
        "VIEW_MODE" => "LINE",
        "ARCHIVE_URL" => "archive",
        "IS_ARCHIVE" => "Y"
    )
);