<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$tabs = CIBlockSection::GetList(
        ["SORT" => "ASC"],
        [
            "IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]],
			"CODE" => ["products", /*"themes"*/],
			'ACTIVE '=>'Y'/*,'DEPTH_LEVEL'=>1*/
        ],
        false,
        ["ID", "IBLOCK_ID", "NAME", "CODE"]
);

$iblockInfo = CIBlock::GetByID($GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]]);
$iblockInfo = $iblockInfo->Fetch();
$APPLICATION->SetTitle($iblockInfo["NAME"]);
$APPLICATION->AddChainItem($iblockInfo["NAME"]);
?>
<div class="maxwidth-theme">
	<?//var_dump($tabs->SelectedRowsCount()); ?>
    <?if($tabs->SelectedRowsCount()>1):?>
        <div class="sections head-block top controls clearfix">
            <?
            while ($tab = $tabs->GetNext()){
                ?>
                <div class="item-link shadow border">
                    <div class="title font_upper_md">
                        <a href="#<?=$tab["CODE"]?>">
                            <span class="btn-inline black"><?=$tab["NAME"]?></span>
                        </a>
                    </div>
                </div>
                <?
            }
            ?>
        </div>
    <?endif;?>

            <?
            $sectionForFilter = CIBlockSection::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]], "CODE" => "products", 'ACTIVE' => 'Y'], false, ["ID", "IBLOCK_ID"]);
            $sectionForFilter = $sectionForFilter->GetNext();
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "help-products",
                Array(
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COUNT_ELEMENTS" => "Y",
                    "FILTER_NAME" => "",
                    "IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]],
                    "IBLOCK_TYPE" => "",
                    "SECTION_CODE" => "",
                    "SECTION_FIELDS" => array("NAME", "PICTURE", "UF_*"),
                    "SECTION_ID" => $sectionForFilter["ID"],
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array("", ""),
                    "SHOW_PARENT_NAME" => "Y",
                    "TOP_DEPTH" => "2",
                    "VIEW_MODE" => "LINE",
                    "TITLE" => $arResult["VARIABLES"]["IBLOCK_CODE"] == 'download_link'?'Программные продукты':'',
                    "IBLOCK_CODE" => $arResult["VARIABLES"]["IBLOCK_CODE"]
                )
            );?>
       <div class="text_before_items" style="margin-bottom: 50px;"><?=$iblockInfo["DESCRIPTION"]?></div>
            <?//=(!empty($iblockInfo["DESCRIPTION"]))?' <div class="text_before_items" style="margin-bottom: 50px;">'.$iblockInfo["DESCRIPTION"].'</div>':''?>
      
        <?
        global $arrFilter;
        $sectionForFilter = CIBlockSection::GetList([], ["IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]], "CODE" => "themes"], false, ["ID", "IBLOCK_ID"]);
        $sectionForFilter = $sectionForFilter->GetNext();
        if($sectionForFilter){
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "themes",
                Array(
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COUNT_ELEMENTS" => "Y",
                    "FILTER_NAME" => "arrFilter",
                    "IBLOCK_ID" => $GLOBALS["ASPRO_IBLOCKS"][$arResult["VARIABLES"]["IBLOCK_CODE"]],
                    "IBLOCK_CODE" => $arResult["VARIABLES"]["IBLOCK_CODE"],
                    "IBLOCK_TYPE" => "questions",
                    "SECTION_CODE" => "",
                    "SECTION_FIELDS" => array("", ""),
                    "SECTION_ID" => $sectionForFilter["ID"],
                    "SECTION_URL" => "#SECTION_CODE#/",
                    "SECTION_USER_FIELDS" => array("", ""),
                    "SHOW_PARENT_NAME" => "Y",
                    "TOP_DEPTH" => "5",
                    "VIEW_MODE" => "LINE",
                    "ARCHIVE_URL" => "archive"
                )
            );
        }


        ?>
</div>