<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$iblocksForMenu = CIBlock::GetList(["SORT" => "ASC"], ["TYPE" => HELP_IBLOCK_TYPES]);
?>
<?//страница поддрежка где иконки главной?>
<div class="item-views services-items type_3 front icons">
    <div class="items flexbox"><?
    while ($iblock = $iblocksForMenu->GetNext()) {?>
        <?$arSectionImage = $iblock['PICTURE'] ? CFile::ResizeImageGet($iblock['PICTURE'], array('width' => 75, 'height' => 75), BX_RESIZE_IMAGE_PROPORTIONAL) : array();?>
        <div class="item shadow border">
            <div class="wrap" style="text-align: center">
                <?if(!empty($arSectionImage)):?>
                <div class="opp-brick-announce-img">
                        <div class="opp-brick-announce-img-link"><img src="<?=$arSectionImage['src']?>"
                                                                      alt="<?=$iblock["NAME"]?>"
                                                                      title="<?=$iblock["NAME"]?>"
                                                                      class="img-responsive"/></div>
                </div>
<?endif;?>
                <div class="body-info" style="margin-top:10px;">
                    <div class="title">
                        <a class="dark-color" href="/help/<?=$iblock["CODE"]?>/"><?=$iblock["NAME"]?></a>
                    </div>
                </div>
                <a href="/help/<?=$iblock["CODE"]?>/"></a>
            </div>
        </div>
    <?
    }
    ?>
    </div>
</div>
