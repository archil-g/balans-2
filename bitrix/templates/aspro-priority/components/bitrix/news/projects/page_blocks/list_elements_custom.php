<?global $arTheme, $arRegion;?>
<div class="maxwidth-theme">
    <div class="mixitup-container">
        <?$arFilter = array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1);
        $arSelect = array('ID', 'SORT', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL','PICTURE');
        $arParentSections = CCache::CIBLockSection_GetList(array('SORT' => 'ASC', 'ID' => 'ASC', 'CACHE' => array('TAG' => CCache::GetIBlockCacheTag($arParams['IBLOCK_ID']), 'MULTI' => 'Y')), $arFilter, false, $arSelect);
        if($arTheme['SHOW_SECTIONS_REGION']['VALUE'] == 'Y' && $arTheme['USE_REGIONALITY']['DEPENDENT_PARAMS']['REGIONALITY_FILTER_ITEM']['VALUE'] == 'Y' && $arRegion){
            $arParentSectionsElements = CCache::CIBLockElement_GetList(array('SORT' => 'ASC', 'ID' => 'ASC', 'CACHE' => array('TAG' => CCache::GetIBlockCacheTag($arParams['IBLOCK_ID']), 'MULTI' => 'Y', 'GROUP' => 'IBLOCK_SECTION_ID')), array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'ACTIVE_DATE', 'PROPERTY_LINK_REGION' => $GLOBALS[$arParams['FILTER_NAME']]['PROPERTY_LINK_REGION']), false, false, array('ID', 'IBLOCK_SECTION_ID'));
            foreach($arParentSections as $key => $arParentSection){
                if(!$arParentSectionsElements[$arParentSection['ID']]){
                    unset($arParentSections[$key]);
                }
            }
        }
        if($arParentSections)
        {?>
            <div class="item-views news-items projects type_4 linked">
                <div class="items row">
                    <?foreach($arParentSections as $arParentItem):?>
                        <?
                        $imageSrc = '';
                        $bImage = true;
                        if(!empty($arParentItem['PICTURE'])) {
                            $file = CFile::ResizeImageGet($arParentItem['PICTURE'], array('width'=>420, 'height'=>280), BX_RESIZE_IMAGE_PROPORTIONAL);
                            if(!empty($file['src'])){
                                $imageSrc = $file['src'];
                            }
                        }
                        if(empty($imageSrc)){
                            $imageSrc = '/bitrix/templates/aspro-priority/images/noimage_sections.png';
                        }
                        ?>
                        <div class="item light clearfix<?=(!$bImage ? ' wti' : '')?> col-md-4 col-sm-4 col-xs-6" data-ref="mixitup-target">
                            <div class="wrap">
                                <?if($imageSrc):?>
                                    <div class="image<?=($bImage ? "" : " wti" );?>">
                                        <div class="wrap" style="background:url(<?=$imageSrc?>) top center / cover no-repeat;"></div>
                                    </div>
                                <?endif;?>
                                <div class="body-info">
                                    <div class="title"><?=$arParentItem['NAME'];?></div>

                                </div>
                                <a href="<?=$arParentItem['SECTION_PAGE_URL']?>"></a>
                            </div>
                        </div>
                    <?endforeach;?>
                </div>
            </div>
        <?}?>
    </div>
</div>