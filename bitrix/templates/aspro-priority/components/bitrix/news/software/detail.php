<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?$IBLOCK = CIBlock::GetList(Array(),Array("CODE"=> $arResult["VARIABLES"]["IB_CODE"]), true)->Fetch();?>
<?$type=false;?>
<?include('section_detail.php');?>
<?
if(!empty($SECTION['UF_OFFERS'])){
    $GLOBALS[$arParams["FILTER_NAME"]] = $filter;
    $GLOBALS[$arParams["FILTER_NAME"]]['ID'] = $SECTION['UF_OFFERS'];

    $pricesCodesToArticles = \rdn\Helper::getPricesCodesToArticles($SECTION['UF_OFFERS']);
}
//dump($GLOBALS[$arParams["FILTER_NAME"]]);

?>
<div class="maxwidth-theme ">
    <div class="row">
        <div class="col-md-9">

            <h3>Другие редакции продукта</h3>
            <? include_once(__DIR__ . "/include_sort.php"); ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                $display,
                Array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "COUNT_IN_LINE" => "3",
                    "ALL_CRIT" => $SECTION['UF_CRIT'],
                    "DETAIL_URL" => $SECTION["SECTION_PAGE_URL"]."prices/#ELEMENT_CODE#/",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array("NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_TEXT", "DETAIL_PICTURE", ""),
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => $arParams['PRICES_IBLOCK_ID'],
                    "IBLOCK_TYPE" => "aspro_priority_catalog",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "6",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array("PRICE", "PRICEOLD", "ARTICLE", "FORM_ORDER", "DELIVERY", "STATUS", "SHIRINAZAXVATA", "SHIRINAZAXVAT", "RANGE_MEASURE", "WORK_TEMP", "DIMENSIONS", "MASS", "MAX_SPEED", "MODEL_ENGINE", "MAX_POWER_VT", "VOLUME_ENGINE", "SEATS", "COUNTRY", "WORK_PRESSURE", "BENDING_ANGLE", "ENGINE_POWER", "WORK_SPEED", "GUARANTEE", "COMMUNIC_PORT", "INNER_MEMORY", "PRESS_POWER", "MAXIMUM_PRESSURE", "MAX_SIZE_ZAG", "BENDING_SIZE", "MAX_MASS_ZAG", "POWER_LS", "V_DVIGATELJA", "RAZGON", "PROIZVODITEKNOST", "BRAND", "SUPPLIED", ""),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SHOW_DETAIL_LINK" => "Y",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "S_ASK_QUESTION" => "",
                    "S_ORDER_PRODUCT" => "",
                    "T_CHARACTERISTICS" => "",
                    "T_DOCS" => "",
                    "T_GALLERY" => "",
                    "T_PROJECTS" => "",
                    "SECTION_ID" => $SECTION['ID'],
                    "SECTION_NAME" => $SECTION['NAME'],
                    "SECTION_PICTURE" => $SECTION["PICTURE"],

                )
            );?>
        </div>
        <div class="col-md-3 scrollable-block">
            <div class="fixed_block_fix"></div>
            <div class="ask_a_question_wrapper">
                <div class="ask_a_question">
                    <div class="text promoblock media-promo" style="display: block">
                        <?$APPLICATION->IncludeComponent(
                            'bitrix:main.include',
                            '',
                            Array(
                                'AREA_FILE_SHOW' => 'file',
                                'PATH' => SITE_DIR.'include/promoblock.php',
                                'EDIT_TEMPLATE' => '',
                                'TYPE' => $SECTION['ID'],
                                'DIR_TYPE' => 437,
                            )
                        );?>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        'bitrix:main.include',
                        '',
                        Array(
                            'AREA_FILE_SHOW' => 'file',
                            'PATH' => SITE_DIR.'include/useful_links.php',
                            'EDIT_TEMPLATE' => '',
                            'TYPE' => $SECTION['ID'],
                            'DIR_TYPE' => 434
                        )
                    );?>
                    <div class="ask_a_question_custom border shadow subscribe">
                        <div class="inner">
                            <div class="text-block">
                                <?$APPLICATION->IncludeComponent(
                                    'bitrix:main.include',
                                    '',
                                    Array(
                                        'AREA_FILE_SHOW' => 'file',
                                        'PATH' => SITE_DIR.'include/ask_question_product.php',
                                        'EDIT_TEMPLATE' => ''
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="outer">
                            <span class="font_upper animate-load" title="Написать сообщение" data-event="jqm" data-param-id="20" data-name="question"><?=GetMessage('S_ASK_QUESTION')?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?if($SHOW_OPPORTUNITIES){
        $opSection["ID"] = hadSectionArea(38, $SECTION["ID"])[0];
        $APPLICATION->IncludeComponent("bitrix:news.list", "software_oportunities_on_main", Array(
                "SECTION_CODE" => $SECTION["CODE"],
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "Y",
                "IBLOCK_TYPE" => "media",
                "IBLOCK_ID" => "38",
                "NEWS_COUNT" => "3",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "opportunitiesFilter",
                "FIELD_CODE" => Array("ID", "DETAIL_TEXT"),
                "PROPERTY_CODE" => Array("DESCRIPTION", "DETAIL_TEXT"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_META_DESCRIPTION" => "Y",
                "SET_LAST_MODIFIED" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                "PARENT_SECTION" => $opSection["ID"],
                "SEF_FOLDER" => $arParams["SEF_FOLDER"] . $arResult["VARIABLES"]["SECTION_CODE"] . "/opportunities/",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "Y",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "Y",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "PAGER_BASE_LINK_ENABLE" => "Y",
                "SET_STATUS_404" => "Y",
                "SHOW_404" => "Y",
                "MESSAGE_404" => "",
                "PAGER_BASE_LINK" => "",
                "PAGER_PARAMS_NAME" => "arrPager",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                'UF_SHOW_PROMO' => $SECTION["UF_SHOW_PROMO"],
                'SHOW_USEFUL_LINKS' => $SECTION["UF_SHOW_USF_LINKS"],
                'USER_LINKS_TYPE' => $SECTION['ID'],
                'USER_LINKS_DIR_TYPE' => 433,
                'USER_PROMO_TYPE' => $SECTION['ID'],
                'USER_PROMO_DIR_TYPE' => 436
            )
        );
    }

    if (!empty($SECTION["UF_SERVICES"])) { ?>
        <div class="col-md-9">
            <? global $servicesActions;
            $servicesActions = ["ID" => $SECTION["UF_SERVICES"]];
            $APPLICATION->IncludeComponent("bitrix:news.list", "services_linked_pp", Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "Y",
                    "IBLOCK_TYPE" => "aspro_priority_catalog",
                    "IBLOCK_ID" => SERVICES_IBLOCK_ID,
                    "NEWS_COUNT" => "2",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "servicesActions",
                    "FIELD_CODE" => Array("ID", "DETAIL_PICTURE"),
                    "PROPERTY_CODE" => Array("DESCRIPTION"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_LAST_MODIFIED" => "Y",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "Y",
                    "PAGER_TEMPLATE" => "main2",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "Y",
                    "SET_STATUS_404" => "Y",
                    "SHOW_404" => "Y",
                    "MESSAGE_404" => "",
                    "PAGER_BASE_LINK" => "",
                    "PAGER_PARAMS_NAME" => "arrPager",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "TITLE" => "Дополнительные услуги"
                )
            ); ?>
        </div>
        <div class="col-md-9 upper-margin">
            <?
            $APPLICATION->IncludeComponent("bitrix:news.list", "services_linked_pp", Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "Y",
                    "IBLOCK_TYPE" => "aspro_priority_catalog",
                    "IBLOCK_ID" => ACTIONS_IBLOCK_ID,
                    "NEWS_COUNT" => "2",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",//"actionsActions",
                    "FIELD_CODE" => Array("ID", "DETAIL_PICTURE"),
                    "PROPERTY_CODE" => Array("DESCRIPTION"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_LAST_MODIFIED" => "Y",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "Y",
                    "PAGER_TEMPLATE" => "main2",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "Y",
                    "SET_STATUS_404" => "Y",
                    "SHOW_404" => "Y",
                    "MESSAGE_404" => "",
                    "PAGER_BASE_LINK" => "",
                    "PAGER_PARAMS_NAME" => "arrPager",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "TITLE" => "Полезные новости"
                )
            ); ?>
        </div>
    <? } ?>

    <div class="col-md-9 drag-block <? if (!empty($SECTION["UF_SERVICES"]) || $element["ID"] > 0) {
        echo "upper-margin";
    } ?>"  data-class="PARTNERS_INDEX_drag" data-order="13">
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "recommended",
            array(
                "IBLOCK_TYPE" => "aspro_priority_content",
                "IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_priority_content"]["aspro_priority_partners"][0],
                "NEWS_COUNT" => "20",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arRegionLink",
                "FIELD_CODE" => array(
                    0 => "NAME",
                    1 => "PREVIEW_PICTURE",
                    2 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "100000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ITEM_IN_BLOCK" => "6",
                "SHOW_DETAIL_LINK" => "Y",
                "AJAX_OPTION_ADDITIONAL" => "",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "COMPONENT_TEMPLATE" => "partners_front_1",
                "SET_LAST_MODIFIED" => "N",
                "TITLE" => "Нас рекомендуют",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => "",
                "STRICT_SECTION_CHECK" => "N",
                "SHOW_ALL_TITLE" => ""
            ),
            false
        ); ?>
    </div>
    <div class="col-md-9">
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "projects-slider",
            array(
                "IBLOCK_TYPE" => "aspro_priority_content",
                "IBLOCK_ID" => "13",
                "NEWS_COUNT" => "20",
                "SORT_BY1" => "PROPERTY_PROGRAM_PRODUCTS",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "ASC",
                "USE_FILTER" => "Y",
                "FILTER_NAME" => "arSliderFilter",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array(
                    0 => "PROGRAM_PRODUCTS",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "100000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ITEM_IN_BLOCK" => "6",
                "SHOW_DETAIL_LINK" => "Y",
                "AJAX_OPTION_ADDITIONAL" => "",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "COMPONENT_TEMPLATE" => "",
                "SET_LAST_MODIFIED" => "N",
                "TITLE" => "",
                "SHOW_404" => "N",
                "MESSAGE_404" => "",
                "STRICT_SECTION_CHECK" => "N",
                "SHOW_ALL_TITLE" => ""
            ),
            false
        ); ?>
        <div class="clearfix"></div>
    </div>

</div>
<div class="clearfix mb60"></div>
<script>
    $(function () {
        var url = '<?=$SECTION["SECTION_PAGE_URL"]?>prices/';
        var pricesCodesToArticles = <?=json_encode($pricesCodesToArticles)?>;
        $('.js-option-tab').on('click',function () {
            var keyArr = [];
            if($(this).data('art')) {
                keyArr.push($(this).data('art'));
            }
            $('.js-option-select').each(function () {
                if($(this).val()) {
                    keyArr.push(parseInt($(this).val()));
                }
            });
            keyArr.sort(compareNumeric);
            var key = keyArr.join('');
            if(pricesCodesToArticles[key]) {
                document.location.href = url + pricesCodesToArticles[key] + '/';
            } else {
                pushNotifications('successJsButtonError', "По такому набору критериев не найдено предложение");
            }

        });
        $('.js-option-select').on('change',function () {
            var keyArr = [];
            if($('.js-option-tab.active').length && $('.js-option-tab.active').data('art')){
                keyArr.push($('.js-option-tab.active').data('art'));
            }

            $('.js-option-select').each(function () {
                if($(this).val()) {
                    keyArr.push(parseInt($(this).val()));
                }

            });
            keyArr.sort(compareNumeric);
            var key = keyArr.join('');
            if(pricesCodesToArticles[key]) {
                document.location.href = url + pricesCodesToArticles[key] + '/';
            } else{
                pushNotifications('successJsButtonError', "По такому набору критериев не найдено предложение");
                $(this).find('[data-cup-option]').prop('selected', true);
            }
        })
    })
    function compareNumeric(a, b) {
        if (a > b) return 1;
        if (a == b) return 0;
        if (a < b) return -1;
    }
</script>