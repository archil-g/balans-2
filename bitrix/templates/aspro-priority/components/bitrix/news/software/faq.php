<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?
global $APPLICATION;
$arRefferer = explode("/",$_SERVER["HTTP_REFERER"]);
$type = "questions_detail";
$arResult["VARIABLES"]["SECTION_CODE"] = $arRefferer[4];



$SECTION = CIBlockSection::GetList(
    ["SORT" => "ASC"],
    [
        "IBLOCK_ID" => 37,
        "CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
    ],
    false,
    ["UF_PRODUCT_FAQ"])->fetch();


$elementDetail = \CIBlockElement::GetList(
    [],
    [
        "IBLOCK_TYPE" => $arResult["VARIABLES"]["IB_TYPE"],
        "CODE" => $arResult["VARIABLES"]["EL_CODE"]
    ],
    false,
    ["nPageSize" => 1],
    ["ID","IBLOCK_ID","IBLOCK_TYPE_ID","NAME","CODE"]
)->fetch();

$elemLinkedIB = CIBlock::GetList(
    Array(),
    Array(
        'CODE' => $arResult["VARIABLES"]["IB_TYPE"]
    ), true
)->fetch();

$linkedSection = getRootSection(["IBLOCK_ID" => $elemLinkedIB["ID"],"UF_PO" => $SECTION["ID"],]);

$selectedTypeID = $linkedSection["UF_PRODUCT_FAQ"];
include "section.php";
$sectionUrl = $arParams["SEF_FOLDER"].$SECTION["CODE"].'/';

if($arResult["VARIABLES"]["SECTION_CODE"]){
    $APPLICATION->AddChainItem($SECTION["NAME"],$SECTION["SECTION_PAGE_URL"]);
    if($arResult["VARIABLES"]["IB_TYPE"] == "questions") {
        $APPLICATION->AddChainItem("Техническая поддержка и инструкции", $SECTION["SECTION_PAGE_URL"] . "support/");
        $APPLICATION->AddChainItem("Вопросы и ответы", $SECTION["SECTION_PAGE_URL"] . "support/faq/");
    }else if($arResult["VARIABLES"]["IB_TYPE"] == "files"){
        $APPLICATION->AddChainItem("Скачать дистрибутив и дополнителные файлы", $SECTION["SECTION_PAGE_URL"] . "download/");
        $APPLICATION->AddChainItem("Файлы", $SECTION["SECTION_PAGE_URL"] . "download/files/");
    }else if($arResult["VARIABLES"]["IB_TYPE"] == "videos"){
        $APPLICATION->AddChainItem("Техническая поддержка и инструкции", $SECTION["SECTION_PAGE_URL"] . "support/");
        $APPLICATION->AddChainItem("Видео", $SECTION["SECTION_PAGE_URL"] . "support/video/");
    }else{
        $APPLICATION->AddChainItem("Техническая поддержка и инструкции", $SECTION["SECTION_PAGE_URL"] . "support/");
        $APPLICATION->AddChainItem("Статьи", $SECTION["SECTION_PAGE_URL"] . "support/articles/");
    }
}

if($elementDetail["ID"] > 0)
{?>

    <div class="maxwidth-theme">
        <div class="row">
            <div class="col-md-3 col-sm-3 hidden-xs hidden-sm left-menu-md">
                <? include "iblock_menu.php"; ?>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12 content-md">
                <div class="<?= ($isSidebar ? "col-md-9 col-sm-8" : "col-xs-12") ?>">
                    <div class="row">
                        <div class="col-xs-12">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:news.detail",
                                "help",
                                Array(
                                    "HIDE_TOP_BANNER" => true,
                                    "S_ASK_QUESTION" => $arParams["S_ASK_QUESTION"],
                                    "S_ORDER_SERVISE" => $arParams["S_ORDER_SERVISE"],
                                    "T_GALLERY" => $arParams["T_GALLERY"],
                                    "T_DOCS" => $arParams["T_DOCS"],
                                    "S_SUBSCRIBE" => $arParams["S_SUBSCRIBE"],
                                    "FORM_ID_ORDER_SERVISE" => ($arParams["FORM_ID_ORDER_SERVISE"] ? $arParams["FORM_ID_ORDER_SERVISE"] : CPriority::getFormID("aspro_priority_order_services")),
                                    "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                                    "DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
                                    "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                                    "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                                    "IBLOCK_TYPE" => $arResult["IBLOCK_TYPE_ID"],
                                    "IBLOCK_ID" => $elementDetail["IBLOCK_ID"],
                                    "FIELD_CODE" => [
                                        0 => "PREVIEW_TEXT",
                                        1 => "DETAIL_TEXT",
                                        2 => "DETAIL_PICTURE",
                                        3 => "DATE_ACTIVE_FROM",
                                        4 => "",
                                    ],
                                    "SERVICES_LINK_ELEMENTS_TEMPLATE" => $arParams["SERVICES_LINK_ELEMENTS_TEMPLATE"],
                                    "PROPERTY_CODE" => [
                                        0 => "PHOTOPOS",
                                        1 => "FORM_ORDER",
                                        2 => "FORM_QUESTION",
                                        3 => "LINK_GOODS",
                                        4 => "LINK_SERVICES",
                                        5 => "LINK_SALE",
                                        6 => "LINK_PARTNERS",
                                        7 => "LINK_SERTIFICATES",
                                        8 => "LINK_FAQ",
                                        9 => "LINK_VACANCYS",
                                        10 => "LINK_STAFF",
                                        11 => "LINK_REVIEWS",
                                        12 => "LINK_PROJECTS",
                                        13 => "VIDEO",
                                        14 => "PHOTOS",
                                        15 => "DOCUMENTS",
                                        16 => "",
                                    ],
                                    "DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
                                    "SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                                    "META_KEYWORDS" => $arParams["META_KEYWORDS"],
                                    "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
                                    "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
                                    "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                                    "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
                                    "SET_TITLE" => $arParams["SET_TITLE"],
                                    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "ADD_ELEMENT_CHAIN" => "N",
                                    "ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
                                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                                    "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                                    "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                                    "DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
                                    "DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
                                    "PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
                                    "PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
                                    "CHECK_DATES" => $arParams["CHECK_DATES"],
                                    "ELEMENT_ID" => $elementDetail["ID"],
                                    "ELEMENT_CODE" => "",
                                    "IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
                                    "USE_SHARE" 			=> $arParams["USE_SHARE"],
                                    "SHARE_HIDE" 			=> $arParams["SHARE_HIDE"],
                                    "SHARE_TEMPLATE" 		=> $arParams["SHARE_TEMPLATE"],
                                    "SHARE_HANDLERS" 		=> $arParams["SHARE_HANDLERS"],
                                    "SHARE_SHORTEN_URL_LOGIN"	=> $arParams["SHARE_SHORTEN_URL_LOGIN"],
                                    "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                                    "GALLERY_TYPE" => $arParams["GALLERY_TYPE"],
                                    "T_PROJECTS" => $arParams["T_PROJECTS"],
                                    "T_FAQ" => $arParams["T_FAQ"],
                                    "T_SERVICES" => $arParams["T_SERVICES"],
                                    "T_ITEMS" => $arParams["T_ITEMS"],
                                    "T_PARTNERS" => $arParams["T_PARTNERS"],
                                    "T_REVIEWS" => $arParams["T_REVIEWS"],
                                    "T_STAFF" => $arParams["T_STAFF"],
                                    "T_VACANCYS" => $arParams["T_VACANCYS"],
                                    "T_SERTIFICATES" => $arParams["T_SERTIFICATES"],
                                    "REVIEWS_IBLOCK_ID" => $arParams["REVIEWS_IBLOCK_ID"],
                                    "PROJECTS_IBLOCK_ID" => $arParams["PROJECTS_IBLOCK_ID"],
                                    "SERVICES_IBLOCK_ID" => $arParams["SERVICES_IBLOCK_ID"],
                                    "CATALOG_IBLOCK_ID" => $arParams["CATALOG_IBLOCK_ID"],
                                    "STAFF_IBLOCK_ID" => $arParams["STAFF_IBLOCK_ID"],
                                    "PARTNERS_IBLOCK_ID" => $arParams["PARTNERS_IBLOCK_ID"],
                                    "NEWS_IBLOCK_ID" => $arParams["NEWS_IBLOCK_ID"],
                                    "FAQ_IBLOCK_ID" => $arParams["FAQ_IBLOCK_ID"],
                                    "VACANCYS_IBLOCK_ID" => $arParams["VACANCYS_IBLOCK_ID"],
                                    "SERTIFICATES_IBLOCK_ID" => $arParams["SERTIFICATES_IBLOCK_ID"],
                                    "COMMENTS_COUNT" => $arParams['COMMENTS_COUNT'],
                                    "DETAIL_USE_COMMENTS" => $arParams['DETAIL_USE_COMMENTS'],
                                    "FB_USE" => $arParams["DETAIL_FB_USE"],
                                    "VK_USE" => $arParams["DETAIL_VK_USE"],
                                    "BLOG_USE" => $arParams["DETAIL_BLOG_USE"],
                                    "BLOG_TITLE" => $arParams["BLOG_TITLE"],
                                    "BLOG_URL" => $arParams["DETAIL_BLOG_URL"],
                                    "EMAIL_NOTIFY" => $arParams["DETAIL_BLOG_EMAIL_NOTIFY"],
                                    "FB_TITLE" => $arParams["FB_TITLE"],
                                    "FB_APP_ID" => $arParams["DETAIL_FB_APP_ID"],
                                    "VK_TITLE" => $arParams["VK_TITLE"],
                                    "VK_API_ID" => $arParams["DETAIL_VK_API_ID"],
                                    "STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],
                                    'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
                                    'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
                                ),
                                $component
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}?>