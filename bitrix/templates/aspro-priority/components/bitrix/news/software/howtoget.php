<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
$section = "Как получить ПО";
$title = "Как получить ПО ";
$type = "howtoget";

include('section.php');
$APPLICATION->AddChainItem($SECTION["NAME"], $SECTION["SECTION_PAGE_URL"]);
$APPLICATION->AddChainItem($title);

//$APPLICATION->AddChainItem($title);
$linkedSection = getRootSection(["IBLOCK_ID" => 48,"UF_PO" => $SECTION["ID"]]);

if($linkedSection):?>
<div class="maxwidth-theme">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 content-md">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "howtoget_themes",
                array(
                    "IBLOCK_ID" => 48,
                    "SECTION_ID" => $linkedSection["ID"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                    "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
                    "SECTION_URL" => $SECTION["SECTION_PAGE_URL"]."howtoget/",
                    "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                    "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                    "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
                    "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
                ),
                $component,
                array("HIDE_ICONS" => "Y")
            );
            ?>
        </div>
    </div>
</div>
<script>
    $(function () {
		//Changing size of product's boxes.
		/*var price_h = $(".software .content").find('.prices').length?$(".software .content").find('.prices').height():0;
        let height = $(".software .content").height() - price_h;
		$(".pp-detail-img").css({"height": height + "px", "width":'auto'});*/
		//a new one
		$(".pp-detail-img").css({"height": "180px", "width":'auto'});

    })
</script>




<?endif?>