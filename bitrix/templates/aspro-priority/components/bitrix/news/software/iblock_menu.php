<?global $APPLICATION;?>
<aside class="sidebar">
    <ul class="nav nav-list side-menu">
        <?$subtitle = '';?>
        <?foreach ($iblockMenu as $ibMenuItem):?>
        <?if($subtitle != $ibMenuItem['SUBTITLE']): $subtitle = $ibMenuItem['SUBTITLE'];?>
            <li class="subtitle"><?=$subtitle?></li>
        <? endif;?>
        <li class="item <?=($SELECTED_IBLOCK["CODE"] == $ibMenuItem["CODE"] || $ibMenuItem["SELECTED"])?"active opened":""?>">
            <a href="<?=$ibMenuItem["URL"]?>"><?=$ibMenuItem["NAME"]?></a>
        </li>
        <?endforeach;?>
    </ul>
</aside>
