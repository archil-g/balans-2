<?
$MESS["ELEMENT_NOTFOUND"] = "Элемент не найден";
$MESS["SHARE_TEXT"] = 'Поделиться';
$MESS["BACK_LINK"] = "Назад к списку";
$MESS["T_DOCS"] = 'Документы';
$MESS["T_GALLERY"] = 'Фотогалерея';
$MESS["T_PROJECTS"] = 'Проекты';
$MESS["T_REVIEWS"] = 'Отзывы';
$MESS["T_STAFF1"] = 'Специалист';
$MESS["T_STAFF2"] = 'Специалисты';
$MESS["T_GOODS"] = 'Товары';
$MESS["T_SERVICES"] = 'Услуги';
$MESS["T_CHARACTERISTICS"] = "Характеристики";
$MESS["sort_title"] = "По ";
$MESS["sort_title_property"] = "По свойству \"#CODE#\" (#ORDER#)";
$MESS["sort_name_asc"] = "наименованию (А-Я)";
$MESS["sort_name_desc"] = "наименованию (Я-А)";
$MESS["sort_prop_asc"] = "возрастание";
$MESS["sort_prop_desc"] = "убывание";
$MESS["sort_sort"] = "популярности (#ORDER#)";
$MESS["sort_PRICE_asc"] = "цене (сначала дешёвые)";
$MESS["sort_PRICE_desc"] = "цене (сначала дорогие)";
$MESS["prices_pokupka"] = "Покупка";
$MESS["prices_prodlenie"] = "Продление";
?>