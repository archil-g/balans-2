<?
$MESS["SECTION_EMPTY"] = "Нет услуг";
$MESS["SECTION_NOTFOUND"] = "Раздел не найден";
$MESS['S_ASK_QUESTION'] = 'Задать вопрос';
$MESS["sort_title"] = "По ";
$MESS["sort_title_property"] = "По свойству \"#CODE#\" (#ORDER#)";
$MESS["sort_name_asc"] = "наименованию (А-Я)";
$MESS["sort_name_desc"] = "наименованию (Я-А)";
$MESS["sort_prop_asc"] = "возрастание";
$MESS["sort_prop_desc"] = "убывание";
$MESS["sort_sort"] = "популярности (#ORDER#)";
$MESS["sort_PRICE_asc"] = "цене (сначала дешёвые)";
$MESS["sort_PRICE_desc"] = "цене (сначала дорогие)";
$MESS["prices_pokupka"] = "Покупка";
$MESS["prices_prodlenie"] = "Продление";
?>