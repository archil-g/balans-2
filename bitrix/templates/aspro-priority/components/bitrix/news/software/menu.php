<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
global $SHOW_PRICES;
$arTypes = HELP_IBLOCK_TYPES;
$iblockMenu = [];
$iblocks = [];
$linkedProps = [];
$menuCodes = [];
if(!$SECTION || stristr($SECTION['LIST_PAGE_URL'], '#SITE_DIR#')){
    $SECTION = \CIBlockSection::GetList(
        [],
        [
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "CODE" => $arResult["VARIABLES"]["SECTION_CODE"]
        ],false,["UF_MIN_PRICE","UF_SHOW_PROMO","UF_SHOW_USF_LINKS"])->GetNext();
}
foreach ($arTypes as $k => $typeIb) {
    $res = CIBlock::GetList(
        ["SORT"=>"ASC"],
        Array(
            'TYPE' => $typeIb,
            'SITE_ID' => SITE_ID,
            'ACTIVE' => 'Y',
			/*"CNT_ACTIVE"=>"Y"*/
        ), true
    );
    while ($ar_res = $res->Fetch()) {
        $iblocks[$ar_res["CODE"]] = $ar_res;

        $ibSectionLinked = getRootSection(["IBLOCK_ID" => $ar_res["ID"],"UF_PO" => $SECTION["ID"]]);

        if($ibSectionLinked['ELEMENT_CNT'] == 0 && $ar_res['CODE'] != 'download_link') continue;

        if($ibSectionLinked["UF_PRODUCT_FAQ"]){
            if(!in_array($ibSectionLinked["UF_PRODUCT_FAQ"],$linkedProps)){
                $userField = CUserFieldEnum::GetList(array(), array(
                    "ID" => $ibSectionLinked["UF_PRODUCT_FAQ"],
                ));
                while($field = $userField->GetNext()){
                    $linkedProps[$field["ID"]] = $field["VALUE"];
                }
            }

            if($selectedTypeID )
            {
                $type = $linkedProps[$selectedTypeID];
            }

            $menuCodes[] = $linkedProps[$ibSectionLinked["UF_PRODUCT_FAQ"]];

            $iblockMenu[$ar_res["ID"]] = [
                "SELECTED" => ($elementDetail["IBLOCK_ID"] ==  $ar_res["ID"]),
                "TYPE" => $linkedProps[$ibSectionLinked["UF_PRODUCT_FAQ"]],
                "NAME" => $ar_res["NAME"],
                "CODE" => $ar_res["CODE"],
                "SUBTITLE" => $linkedProps[$ibSectionLinked["UF_PRODUCT_FAQ"]] == 'download' ? 'Скачать' : 'Поддержка',
                "SORT" => $ar_res["SORT"],
                "URL" => $arParams["SEF_FOLDER"].$SECTION["CODE"].'/'.$linkedProps[$ibSectionLinked["UF_PRODUCT_FAQ"]]."/".$ar_res["CODE"]."/"
            ];
        }
    }

}
uasort($iblockMenu, 'sortByField');
function sortByField($a, $b){
    return ($a['SUBTITLE'] > $b['SUBTITLE']);
}

?>
<?if($SECTION):

if($SHOW_PRICES || $SHOW_OPPORTUNITIES || $SHOW_HOW_GET || in_array("support",$menuCodes) || in_array("download",$menuCodes)){?>
<div class="product_menu_wrapper static" style="margin-top:30px;"><!--Меню по середине раздела Продукты software/manu.ph  -->
        <ul class="maxwidth-theme">
            <?$APPLICATION->ShowViewContent('show_about');
            if($SHOW_OPPORTUNITIES){?>
                <li onclick="location.href = '<?=$SECTION["SECTION_PAGE_URL"]?>opportunities/';" <?=($type == "opportunities")?'class="active"':""?> >
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/oportunities<?=($type == "opportunities")?'_active':""?>.png"/>
                    <a href="<?=$SECTION["SECTION_PAGE_URL"]?>opportunities/" >Возможности</a>
                </li>
            <?}
            if($SHOW_PRICES){?>
                <li onclick="location.href = '<?=$SECTION["SECTION_PAGE_URL"]?>prices/';" <?=($type == "prices")?'class="active"':""?> >
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/price<?=($type == "prices")?'_active':""?>.png"/>
                    <a href="<?=$SECTION["SECTION_PAGE_URL"]?>prices/" >Тарифы&nbsp;и&nbsp;цены</a>
                </li>
            <?}
            if($SHOW_HOW_GET){?>
                <li onclick="location.href = '<?=$SECTION["SECTION_PAGE_URL"]?>howtoget/';" <?=($type == "howtoget")?'class="active"':""?> >
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/get<?=($type == "howtoget")?'_active':""?>.png"/>
                    <a href="<?=$SECTION["SECTION_PAGE_URL"]?>howtoget/" > Как&nbsp;получить&nbsp;ПО</a>
                </li>
            <?}
            if(in_array("support",$menuCodes)):?>
                <li onclick="location.href = '<?=$SECTION["SECTION_PAGE_URL"]?>support/';" <?=($type == "support")?'class="active"':""?> >
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/support<?=($type == "support")?'_active':""?>.png"/>
                    <a href="<?=$SECTION["SECTION_PAGE_URL"]?>support/" >Техническая поддержка<br>и инструкции</a>
                </li>
            <?endif;?>
            <?if(in_array("download",$menuCodes)):?>
                <li onclick="location.href = '<?=$SECTION["SECTION_PAGE_URL"]?>download/';" <?=($type == "download")?'class="active"':""?> >
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/download<?=($type == "download")?'_active':""?>.png"/>
					<a href="<?=$SECTION["SECTION_PAGE_URL"]?>download/download_link/">Скачать дистрибутив<br>и дополнительный файлы</a>
                </li>
            <?endif;?>
        </ul>
    </div>

    <? $this->SetViewTarget('product_menu'); ?>
        <div class="product_menu_wrapper hide fixed_menu">
            <ul class="maxwidth-theme">

            </ul>
        </div>
    <? $this->EndViewTarget(); ?>
    <?}?>
<?endif;?>
