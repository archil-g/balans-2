<?
$section = "Возможности";
$title = "Возможности ";
$type = "opportunities";
include('section.php');

$APPLICATION->AddChainItem($SECTION["NAME"],$SECTION["SECTION_PAGE_URL"]);
$APPLICATION->AddChainItem($title);
$opSection = getRootSection(["IBLOCK_ID" => 38,"UF_PO" => $SECTION["ID"],"ACTIVE" => "Y"]);
?>
<?
$APPLICATION->IncludeComponent("bitrix:news.list","software_oportunities",Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "Y",
        "IBLOCK_TYPE" => "media",
        "IBLOCK_ID" => "38",
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "opportunitiesFilter",
        "FIELD_CODE" => Array("ID","DETAIL_TEXT"),
        "PROPERTY_CODE" => Array("DESCRIPTION","DETAIL_TEXT"),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_META_DESCRIPTION" => "Y",
        "SET_LAST_MODIFIED" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "PARENT_SECTION" => $opSection["ID"],
        "SEF_FOLDER" => $arParams["SEF_FOLDER"].$arResult["VARIABLES"]["SECTION_CODE"]."/opportunities/",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "Y",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "SET_STATUS_404" => "Y",
        "SHOW_404" => "Y",
        "MESSAGE_404" => "",
        "PAGER_BASE_LINK" => "",
        "PAGER_PARAMS_NAME" => "arrPager",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        'UF_SHOW_PROMO' => $SECTION["UF_SHOW_PROMO"],
        'SHOW_USEFUL_LINKS' => $SECTION["UF_SHOW_USF_LINKS"],
        'USER_LINKS_TYPE' => $SECTION['ID'],
        'USER_LINKS_DIR_TYPE' => 433,
        'USER_PROMO_TYPE' => $SECTION['ID'],
        'USER_PROMO_DIR_TYPE' => 436

    )
);
?>
<script>
    $(function () {
		//Changing size of product's boxes.
		/*var price_h = $(".software .content").find('.prices').length?$(".software .content").find('.prices').height():0;
        let height = $(".software .content").height() - price_h;

$(".pp-detail-img").css({"height": height + "px", "width":'auto'});*/
		//a new one
		$(".pp-detail-img").css({"height": "180px", "width":'auto'});

    })
</script>
