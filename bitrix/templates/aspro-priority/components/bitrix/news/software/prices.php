<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$section = "Цены на";
$title = "Цены на ";
$type = "prices";
global $arTheme, $bShowToogle, $menuItemAfter;
$menuItemAfter = 'prices/';
\Bitrix\Main\Loader::includeModule("rdn");
include('section.php');

$APPLICATION->AddChainItem($SECTION["NAME"], $SECTION["SECTION_PAGE_URL"]);
$APPLICATION->AddChainItem($section . ' ' . $SECTION["NAME"]);
if (!empty($SECTION['UF_OFFERS']))
    $GLOBALS[$arParams["FILTER_NAME"]]['ID'] = $SECTION['UF_OFFERS'];
$tables_prices = \rdn\Helper::getTablePrices($SECTION['UF_CRIT'], $SECTION['UF_OFFERS'], $SECTION['UF_PRICE_TPL'], $SECTION["CODE"]);
?>
<? $bShowToogle = true; ?>

<div class="maxwidth-theme mb50">
    <? if (!empty($tables_prices)): ?>
        <div class="row mb50 hidden-xs" id="prices_table">
            <div class="col-md-12 col-sm-12 col-xs-12 content-md ">
                <div class="tabs" style="margin-top: 0">
					<?	if(count($tables_prices)>1):?>
                    <ul class="nav nav-tabs font_upper_md">
                        <? $i = 0;
                        foreach ($tables_prices as $k => $table_prices): $i++; ?>
                            <li class="shadow border <?= $i == 1 ? 'active' : '' ?>"><a href="#<?= $k ?>"
                                                                                        data-toggle="tab"><?= GetMessage('prices_' . $k) ?></a>
                            </li>
                        <? endforeach; ?>
                    </ul>
					<?php endif;?>
                    <div class="tab-content" style="padding-top: 20px;">
                        <? $i = 0;
                        foreach ($tables_prices as $k => $table_prices): $i++; ?>
                            <div class="tab-pane <?= $i == 1 ? 'active' : '' ?>" id="<?= $k ?>">
                                <div class="table-responsive-custom">
                                    <?= $table_prices ?>
                                </div>

                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    <? endif ?>
    <div class="row relative">
        <? if ($arTheme["SIDE_MENU"]["VALUE"] == "LEFT" || $arTheme["SIDE_MENU"]["VALUE"] == "RIGHT"): ?>
        <div class="col-md-3 col-sm-3 hidden-xs hidden-sm<?= ($arTheme["SIDE_MENU"]["VALUE"] == "RIGHT" ? ' right-menu-md pull-right' : ' left-menu-md') ?>">
            <? if (!empty($SECTION['UF_OFFERS'])) include_once(__DIR__ . "/include_filter.php"); ?>
            <? CPriority::ShowPageType('left_block') ?>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 content-md">
            <? include_once(__DIR__ . "/include_sort.php"); ?>
            <? CPriority::get_banners_position('CONTENT_TOP'); ?>

            <? endif ?>
            <?if(!empty($SECTION['UF_OFFERS'])):?>
            <div id="products_list" class="main-section-wrapper<?= ($arSection['UF_TOP_SEO'] && strpos($_SERVER['REQUEST_URI'], 'PAGEN') === false ? ' wdesc' : ''); ?><?= ($horizontalFilter ? ' whorizontal_filter' : '') ?><?= ($arSeoItem ? ' wlanding' : '') ?>"
                 itemscope="" itemtype="http://schema.org/Product">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    $display,
                    Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "COUNT_IN_LINE" => "3",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array("NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_TEXT", "DETAIL_PICTURE", ""),
                        "FILTER_NAME" => $arParams["FILTER_NAME"],
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => $arParams['PRICES_IBLOCK_ID'],
                        "IBLOCK_TYPE" => "aspro_priority_catalog",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "20",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array("PRICE", "PRICEOLD", "ARTICLE", "FORM_ORDER", "DELIVERY", "STATUS", "SHIRINAZAXVATA", "SHIRINAZAXVAT", "RANGE_MEASURE", "WORK_TEMP", "DIMENSIONS", "MASS", "MAX_SPEED", "MODEL_ENGINE", "MAX_POWER_VT", "VOLUME_ENGINE", "SEATS", "COUNTRY", "WORK_PRESSURE", "BENDING_ANGLE", "ENGINE_POWER", "WORK_SPEED", "GUARANTEE", "COMMUNIC_PORT", "INNER_MEMORY", "PRESS_POWER", "MAXIMUM_PRESSURE", "MAX_SIZE_ZAG", "BENDING_SIZE", "MAX_MASS_ZAG", "POWER_LS", "V_DVIGATELJA", "RAZGON", "PROIZVODITEKNOST", "BRAND", "SUPPLIED", ""),
                        "SET_BROWSER_TITLE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SHOW_DETAIL_LINK" => "Y",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N",
                        "S_ASK_QUESTION" => "",
                        "S_ORDER_PRODUCT" => "",
                        "T_CHARACTERISTICS" => "",
                        "T_DOCS" => "",
                        "T_GALLERY" => "",
                        "SHOW_DETAYL_BUTTON" => "Y",
                        "ALL_CRIT" => $SECTION['UF_CRIT'],
                        "DETAIL_URL" => $SECTION["SECTION_PAGE_URL"]."prices/#ELEMENT_CODE#/",
                        "SECTION_ID" => $SECTION['ID'],
                        "SECTION_NAME" => $SECTION['NAME'],
                        "SECTION_PICTURE" => $SECTION["PICTURE"],
                        "T_PROJECTS" => ""
                    )
                ); ?>
            </div>
            <?else:?>
            <p class="alert alert-warning">Нет предложений для этого продукта</p>
            <? endif ?>
            <? if ($arTheme["SIDE_MENU"]["VALUE"] == "LEFT"): ?>
            <? CPriority::get_banners_position('CONTENT_BOTTOM'); ?>
        </div><? // class=col-md-9 col-sm-9 col-xs-8 content-md?>
        <? elseif ($arTheme["SIDE_MENU"]["VALUE"] == "RIGHT"): ?>
        <? CPriority::get_banners_position('CONTENT_BOTTOM'); ?>
    </div><? // class=col-md-9 col-sm-9 col-xs-8 content-md?>
<? endif; ?>
</div>
</div>
<script>
    $(function () {

		/*var price_h = $(".software .content").find('.prices').length?$(".software .content").find('.prices').height():0;
        let height = $(".software .content").height() - price_h;
		$(".pp-detail-img").css({"height": height + "px", "width":'auto'});*/
		// a new one
		$(".pp-detail-img").css({"height": "180px", "width":'auto'});


        <?if(!empty($_GET['sort']) || !empty($_GET['display'])):?>
        scrollToBlock($('#products_list'),-200);
        <?endif;?>
    })
</script>