<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?
global $APPLICATION;
if (!$SECTION && $arResult["VARIABLES"]["SECTION_CODE"]) {
    $SECTION = getRootSection(["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arResult["VARIABLES"]["SECTION_CODE"]]);
}

global $arSliderFilter;
$arSliderFilter = array('PROPERTY_PROGRAM_PRODUCTS' => $SECTION['ID']);
if(!$title) $title = "О продукте ";
$APPLICATION->SetTitle($title . $SECTION["NAME"]);
global $SHOW_OPPORTUNITIES,$SHOW_PRICES;
global $SHOW_HOW_GET;
$SHOW_OPPORTUNITIES = false;
$SHOW_HOW_GET = false;
$SHOW_PRICES = false;

?>
<?
$sectionCode = $arResult["VARIABLES"]["SECTION_CODE"];
$element = \CIBlockElement::GetList(
    [],
    [
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "SECTION_CODE" => $sectionCode
    ],
    false,
    ["nPageSize" => 1],
    ["ID","PROPERTY_OFFERS"]
)->fetch();

if (!empty($SECTION['UF_OFFERS'])) $SHOW_PRICES = true;
?>

<?if($SECTION){
    if (hadSectionArea(38, $SECTION["ID"])) $SHOW_OPPORTUNITIES = true;
    if (hadSectionArea(48, $SECTION["ID"])) $SHOW_HOW_GET = true;
}?>

<? if ($SECTION):?>
    <div class="maxwidth-theme">
        <div class="row software">
			<div class="col-md-3 image-block pp-detail-img" style="width:250px!important; height: auto;"><!-- Каробка в разделе возможности -->
                <img src="<?= \CFile::GetPath($SECTION["PICTURE"]) ?>"/>
            </div>
            <div class="col-md-7"><!-- Возможности и кнопки купить заказать и т д -->
                <div class="content">
                    <p><?= $SECTION["DESCRIPTION"] ?></p>
                    <div class="row">
                        <div class="buttons-block">
                            <div class="wrap">
                                <div class="button">
                                <span class="btn btn-default btn-lg btn-transparent animate-load" data-event="jqm"
                                      data-param-id="17"
                                      data-name="order_services" data-autoload-product="<?= $SECTION["NAME"] ?>">
                                    <span>Заказать продукт через менеджера</span>
                                </span>
                                </div>
                                <div class="button">
                                <span class="btn btn-default btn-lg btn-transparent animate-load" data-event="jqm"
                                      data-param-id="20" data-autoload-need_product="<?= $SECTION["NAME"] ?>"
                                      data-name="question"><span>Задать вопрос</span></span>
                                </div>
                                <div class="clearfix"></div>
                                <?if($SHOW_PRICES):?>
                                <div class="button hidden-xs">
                                    <a class="btn btn-default btn-lg animate-load" <?=$type != 'price' ? 'href="'.$SECTION["SECTION_PAGE_URL"].'prices/#prices_table"' : ''?>  <?=$type == 'prices' ? 'onclick="scrollToBlock($(\'#prices_table\'), -150);"' : ''?>><span>Купить</span></a>
                                </div>
                                <?endif;?>
                            </div>
                        </div>

                        </div>
                    </div>
                    <? if ($SECTION["UF_MIN_PRICE"]): ?>
                        <div class="prices">
                            <div class="price">
                                <span class="price-value" itemprop="price"
                                      content="<?= $SECTION["UF_MIN_PRICE"] ?>"><?= $SECTION["UF_MIN_PRICE"] ?></span>
                            </div>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
   
<? endif; ?>

<? include("menu.php"); ?>
<? if ($SECTION): ?>
    <div class="maxwidth-theme" style="margin-top: 40px;">
        <?


        if ($element["ID"] > 0 || !empty($SECTION['UF_SERVICES'])){
            $this->SetViewTarget('show_about');?>
            <li onclick="location.href = '<?=$SECTION["SECTION_PAGE_URL"]?>';" <?=(!$type)?'class="active"':""?>>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/product<?=(!$type)?'_active':""?>.png"/>
                <a href="<?=$SECTION["SECTION_PAGE_URL"]?>" >О&nbsp;продукте</a>
            </li>
        <?$this->EndViewTarget();
        }
        ?>
        <? if (!$type): ?>
            <? if ($element["ID"] > 0) {
                $lEementID = $APPLICATION->IncludeComponent(
                    "bitrix:news.detail",
                    "software",
                    Array(
                        "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                        "DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
                        "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                        "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
                        "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
                        "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
                        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                        "META_KEYWORDS" => $arParams["META_KEYWORDS"],
                        "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
                        "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
                        "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
                        "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                        "SET_TITLE" => $arParams["SET_TITLE"],
                        "MESSAGE_404" => $arParams["MESSAGE_404"],
                        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                        "SHOW_404" => $arParams["SHOW_404"],
                        "FILE_404" => $arParams["FILE_404"],
                        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                        "ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
                        "ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                        "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                        "DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
                        "DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
                        "PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
                        "PAGER_SHOW_ALL" => "N",
                        "CHECK_DATES" => $arParams["CHECK_DATES"],
                        "ELEMENT_ID" => $element["ID"],
                        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                        "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
                        "USE_SHARE" => $arParams["USE_SHARE"],
                        "SHARE_HIDE" => $arParams["SHARE_HIDE"],
                        "SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
                        "SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
                        "SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                        "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                        "ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
                        'STRICT_SECTION_CHECK' => (isset($arParams['STRICT_SECTION_CHECK']) ? $arParams['STRICT_SECTION_CHECK'] : ''),
                        'UF_SHOW_PROMO' => $SECTION["UF_SHOW_PROMO"],
                        'SHOW_USEFUL_LINKS' => $SECTION["UF_SHOW_USF_LINKS"],
                        'USER_LINKS_TYPE' => $SECTION['ID'],
                        'USER_LINKS_DIR_TYPE' => 434,
                        'USER_PROMO_TYPE' => $SECTION['ID'],
                        'USER_PROMO_DIR_TYPE' => 437
                    ),
                    $component
                );
            }
            if($SHOW_OPPORTUNITIES){
                $opSection["ID"] = hadSectionArea(38, $SECTION["ID"])[0];
                $APPLICATION->IncludeComponent("bitrix:news.list", "software_oportunities_on_main", Array(
                        "SECTION_CODE" => $SECTION["CODE"],
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "Y",
                        "IBLOCK_TYPE" => "media",
                        "IBLOCK_ID" => "38",
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "opportunitiesFilter",
                        "FIELD_CODE" => Array("ID", "DETAIL_TEXT"),
                        "PROPERTY_CODE" => Array("DESCRIPTION", "DETAIL_TEXT"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_LAST_MODIFIED" => "Y",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                        "PARENT_SECTION" => $opSection["ID"],
                        "SEF_FOLDER" => $arParams["SEF_FOLDER"] . $arResult["VARIABLES"]["SECTION_CODE"] . "/opportunities/",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "Y",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "Y",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "Y",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "PAGER_BASE_LINK_ENABLE" => "Y",
                        "SET_STATUS_404" => "Y",
                        "SHOW_404" => "Y",
                        "MESSAGE_404" => "",
                        "PAGER_BASE_LINK" => "",
                        "PAGER_PARAMS_NAME" => "arrPager",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        'UF_SHOW_PROMO' => $SECTION["UF_SHOW_PROMO"],
                        'SHOW_USEFUL_LINKS' => $SECTION["UF_SHOW_USF_LINKS"],
                        'USER_LINKS_TYPE' => 433,
                        'USER_PROMO_TYPE' => 436
                    )
                );
            }

            if (!empty($SECTION["UF_SERVICES"])) { ?>
                <div class="col-md-9"><!-- Перед Дополтинетльными услуги -->
                    <? global $servicesActions;
                    $servicesActions = ["ID" => $SECTION["UF_SERVICES"]];
                    $APPLICATION->IncludeComponent("bitrix:news.list", "services_linked_pp", Array(
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "Y",
                            "IBLOCK_TYPE" => "aspro_priority_catalog",
                            "IBLOCK_ID" => SERVICES_IBLOCK_ID,
                            "NEWS_COUNT" => "2",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "servicesActions",
                            "FIELD_CODE" => Array("ID", "DETAIL_PICTURE"),
                            "PROPERTY_CODE" => Array("DESCRIPTION"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_BROWSER_TITLE" => "N",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_LAST_MODIFIED" => "Y",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "Y",
                            "PAGER_TEMPLATE" => "main2",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_BASE_LINK_ENABLE" => "Y",
                            "SET_STATUS_404" => "Y",
                            "SHOW_404" => "Y",
                            "MESSAGE_404" => "",
                            "PAGER_BASE_LINK" => "",
                            "PAGER_PARAMS_NAME" => "arrPager",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "TITLE" => "Дополнительные услуги"
                        )
                    ); ?>
                </div>
                <div class="col-md-9 upper-margin"><!-- Полезные новости -->
                    <?
                    $APPLICATION->IncludeComponent("bitrix:news.list", "services_linked_pp", Array(
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "Y",
                            "IBLOCK_TYPE" => "aspro_priority_catalog",
                            "IBLOCK_ID" => ACTIONS_IBLOCK_ID,
                            "NEWS_COUNT" => "2",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "",//"actionsActions",
                            "FIELD_CODE" => Array("ID", "DETAIL_PICTURE"),
                            "PROPERTY_CODE" => Array("DESCRIPTION"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_BROWSER_TITLE" => "N",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_LAST_MODIFIED" => "Y",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "Y",
                            "PAGER_TEMPLATE" => "main2",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_BASE_LINK_ENABLE" => "Y",
                            "SET_STATUS_404" => "Y",
                            "SHOW_404" => "Y",
                            "MESSAGE_404" => "",
                            "PAGER_BASE_LINK" => "",
                            "PAGER_PARAMS_NAME" => "arrPager",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "TITLE" => "Полезные новости"
                        )
                    ); ?>
                </div>
            <? } ?>

            <div class="col-md-9 drag-block <? if (!empty($SECTION["UF_SERVICES"]) || $element["ID"] > 0) {
                echo "upper-margin";
            } ?>"  data-class="PARTNERS_INDEX_drag" data-order="13"><!-- Нас рекомендуют -->
                <? $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"recommended", 
	array(
		"IBLOCK_TYPE" => "aspro_priority_content",
		"IBLOCK_ID" => "31",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ID",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arRegionLink",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "100000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"ITEM_IN_BLOCK" => "6",
		"SHOW_DETAIL_LINK" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"COMPONENT_TEMPLATE" => "recommended",
		"SET_LAST_MODIFIED" => "N",
		"TITLE" => "Нас рекомендуют",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"STRICT_SECTION_CHECK" => "N",
		"SHOW_ALL_TITLE" => ""
	),
	false
); ?>
            </div>
            <div class="col-md-9"><!-- Перед блоком - блоком Остались вопросы -->
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "projects-slider",
                    array(
                        "IBLOCK_TYPE" => "aspro_priority_content",
                        "IBLOCK_ID" => "13",
                        "NEWS_COUNT" => "20",
                        "SORT_BY1" => "PROPERTY_PROGRAM_PRODUCTS",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "ID",
                        "SORT_ORDER2" => "ASC",
                        "USE_FILTER" => "Y",
                        "FILTER_NAME" => "arSliderFilter",
                        "FIELD_CODE" => array(),
                        "PROPERTY_CODE" => array(
                            0 => "PROGRAM_PRODUCTS",
                            1 => "",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "100000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "PAGER_TEMPLATE" => ".default",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "ITEM_IN_BLOCK" => "6",
                        "SHOW_DETAIL_LINK" => "Y",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "COMPONENT_TEMPLATE" => "",
                        "SET_LAST_MODIFIED" => "N",
                        "TITLE" => "",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => "",
                        "STRICT_SECTION_CHECK" => "N",
                        "SHOW_ALL_TITLE" => ""
                    ),
                    false
                ); ?>
            </div>
        <? endif; ?>
    </div>
    <? if (!$type) { ?>

        <div class="col-md-9 upper-margin question-form-on-service question-form-max-width finish_to_scroll"><!-- Перед блоком Остались вопросы -->
            <?php $APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"info_company_2", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "9953",
		"FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "",
		),
		"IBLOCK_ID" => "63",
		"IBLOCK_TYPE" => "aspro_priority_content",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Информационный блок",
		"PROPERTY_CODE" => array(
			0 => "INFO_BLOCK_NAME",
			1 => "CALLBACK_FORM",
			2 => "ORDER_FORM",
			3 => "MAIN_ACTION_PICTURE1",
			4 => "INFO_BLOCK_TEXT",
			5 => "MAIN_ACTION_PICTURE2",
			6 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "info_company_2",
		"ADDITIONAL_SECTION_NAME" => $SECTION["NAME"],
		"ORDER_BUTTON_TITLE" => "",
		"CALLBACK_BUTTON_TITLE" => ""
	),
	false
); ?>
        </div>
    <? } ?>
<? endif; ?>
