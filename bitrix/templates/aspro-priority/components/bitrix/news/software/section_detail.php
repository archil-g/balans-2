<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<style>
    .detail .tabs{margin: 0}
</style>
<?
global $APPLICATION;
global $SHOW_OPPORTUNITIES;
global $SHOW_HOW_GET;
$SHOW_OPPORTUNITIES = false;
$SHOW_HOW_GET = false;
\Bitrix\Main\Loader::includeModule("rdn");
if (!$SECTION && $arResult["VARIABLES"]["SECTION_CODE"]) {
    $SECTION = getRootSection(["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arResult["VARIABLES"]["SECTION_CODE"]]);
}
if(!empty($arResult["VARIABLES"]["ELEMENT_CODE"])){
    $rsEl = \CIBlockElement::GetList(['ID' => 'asc'],['IBLOCK_ID' => '58','CODE' => $arResult["VARIABLES"]["ELEMENT_CODE"]],false,false,['NAME','CODE','ID','IBLOCK_ID','PREVIEW_PICTURE','DETAIL_PICTURE','PROPERTY_ARTIC','PROPERTY_IS_CODE','PROPERTY_PRINT_NAME','PROPERTY_LINK','PROPERTY_HIT']);
    $issetOpt = [];
    if ($element = $rsEl->Fetch())
    {
        $prices = \rdn\Helper::getOfferPrices($element['ID']);
        $element['PRICES'] = $prices;
        if(!empty($SECTION['UF_CRIT'])){
            $crit = \rdn\Helper::getCritOpt($SECTION['UF_CRIT']);
        }
        $element['OPTIONS'] = \rdn\Helper::parseOptFromArticle($element['PROPERTY_ARTIC_VALUE']);
    }
}
/*echo "<pre>";
print_r($crit);
print_r($articles_crit);die();*/

$APPLICATION->SetTitle($title . $SECTION["NAME"] . ' '.$element['NAME']);

$APPLICATION->AddChainItem('Цены на '.$SECTION["NAME"], $SECTION["SECTION_PAGE_URL"].'prices/');
$APPLICATION->AddChainItem($element["NAME"]);

if($SECTION){
    if (hadSectionArea(38, $SECTION["ID"])) $SHOW_OPPORTUNITIES = true;
    if (hadSectionArea(48, $SECTION["ID"])) $SHOW_HOW_GET = true;
}
$filter = [];
$critNabor = [];
?>
<? if ($SECTION):?>
    <div class="maxwidth-theme detail_price">
        <div class="row software">
            <div class="col-md-3 image-block pp-detail-img">
                <img src="<?= \CFile::GetPath($SECTION["PICTURE"]) ?>"/>
            </div>
            <div class="col-md-9">
                <div class="content">
                    <?//dump($crit['tab'])?>
                    <?if(!empty($crit['tab'])):?> <div class="tabs">
                        <ul class="nav nav-tabs font_upper_md">
                            <?foreach ($crit['tab'] as $k => $tab):?>
                            <?if(!in_array($tab['ID'], $SECTION['UF_OPTIONS'])) continue?>
                            <?if(in_array($k,$element['OPTIONS']))$critNabor['Тип покупки'] = $tab['NAME']?>
                            <li class="shadow border <?=in_array($k,$element['OPTIONS'])?'active':''?> js-option-tab" data-art="<?=$k?>"><a href="#" data-toggle="tab"><?=$tab['NAME']?></a></li>
                                <?
                                if(in_array($k,$element['OPTIONS'])){
                                    $filter['PROPERTY_CRIT_'.$tab['CRIT_ID']] = $tab['NAME'];
                                }
                                ?>
                            <?endforeach?>
                            <?unset($crit['tab'])?>
                        </ul>
                    </div>
                    <?endif;?>
                <div class="row mt20">
                    <div class="col-md-6">
                        <?if(!empty($crit)):?>
                            <div class="crit">
                                <?foreach ($crit as $k => $cr):?>
                                    <div class="row mb5">
                                        <div class="col-md-6"><?=$k?></div>
                                        <div class="col-md-6">
                                            <select class="js-option-select">
                                            <?foreach ($cr as $kc => $c):
                                                if(!in_array($c['ID'], $SECTION['UF_OPTIONS'])) continue;
                                                if(($k == 'Вид лицензии' || $k == 'Тип покупки')  && in_array($kc,$element['OPTIONS'])){
                                                    $filter['PROPERTY_CRIT_'.$c['CRIT_ID']] = $c['NAME'];
                                                }
                                                ?>
                                                <?if(in_array($kc,$element['OPTIONS']))$critNabor[$k] = $c['NAME']?>
                                                <option value="<?=$kc?>"  <?=in_array($kc,$element['OPTIONS'])?'selected data-cup-option':''?>><?=$c['NAME']?></option>
                                            <?endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                <?endforeach;?>
                            </div>
                        <?endif?>
<!--                        <p>Артикул: --><?//= $element["PROPERTY_ARTIC_VALUE"] ?><!--</p>-->
                        <p><?= $element["PROPERTY_PRINT_NAME_VALUE"] ?></p>
                        <p><?= $SECTION["DESCRIPTION"] ?></p>

                    </div>
                    <div class="col-md-6">
                        <? if (!empty($element['PRICES'])): ?>
                            <div class="prices">
                                <div class="price">
                                <span class="price-value" itemprop="price"
                                      content="<?= intval($element['PRICES']['PRICE']) ?>"><?= $element['PRICES']['PRICE'] ?></span>
                                </div>
                                <?if(!empty($element['PRICES']['OLD_PRICE'])):?>
                                    <div class="price_old">
                                        <?=$element['PRICES']['OLD_PRICE']?> &#8381;
                                    </div>
                                <?endif?>
                            </div>
                        <? endif; ?>
                        <?$dataItem = array(
                            "IBLOCK_ID" => $element['IBLOCK_ID'],
                            "ID" => $element['ID'],
                            "NAME" => $SECTION["NAME"].': '.$element['NAME'],
                            "DETAIL_PAGE_URL" => $SECTION["SECTION_PAGE_URL"].'prices/'.$element['CODE'].'/',
                            "PREVIEW_PICTURE" => $element['PREVIEW_PICTURE']['ID']?$element['PREVIEW_PICTURE']['ID']:$SECTION["PICTURE"],
                            "DETAIL_PICTURE" => $element['DETAIL_PICTURE']['ID']?$element['DETAIL_PICTURE']['ID']:$SECTION["PICTURE"],
                            "PROPERTY_FILTER_PRICE_VALUE" => $element['PRICES']['PRICE']?$element['PRICES']['PRICE']:'',
                            "PROPERTY_PRICE_VALUE" => $element['PRICES']['PRICE']?$element['PRICES']['PRICE']:'',
                            "PROPERTY_PRICEOLD_VALUE" => $element['PRICES']['OLD_PRICE']?$element['PRICES']['OLD_PRICE']:'',
                            "PROPERTY_ARTICLE_VALUE" => $element['PROPERTY_ARTIC_VALUE'],
                            "PROPERTY_STATUS_VALUE" => '',
                            "SECTION_ID" => $SECTION['ID'],
                            "CRIT" => $critNabor,
                            "IS_CODE" => $element['PROPERTY_IS_CODE_VALUE'],
                        );?>
                        <div class="buttons-block" data-id="<?=$element['ID']?>" data-item="<?=htmlspecialchars(json_encode($dataItem))?>">
                            <div class="buy_block lg clearfix">
                                <div class="counter pull-left">
                                    <div class="wrap">
												<span class="minus ctrl">
													<svg width="11" height="1" viewBox="0 0 11 1">
	<rect width="11" height="1" rx="0.5" ry="0.5"></rect>
</svg>
												</span>
                                        <div class="input"><input type="text" value="1" class="count" maxlength="2"></div>
                                        <span class="plus ctrl">
													<svg width="11" height="11" viewBox="0 0 11 11">
	<path d="M1034.5,193H1030v4.5a0.5,0.5,0,0,1-1,0V193h-4.5a0.5,0.5,0,0,1,0-1h4.5v-4.5a0.5,0.5,0,0,1,1,0V192h4.5A0.5,0.5,0,0,1,1034.5,193Z" transform="translate(-1024 -187)"></path>
</svg>
												</span>
                                    </div>
                                </div>
                                <div class="buttons pull-right">
                                    <span class="btn btn-default pull-right btn-lg to_cart animate-load" data-quantity="1"><span>В корзину</span></span>
                                    <a href="/cart/" class="btn btn-default btn-lg pull-right in_cart"><span>В корзине</span></a>
                                </div>
                            </div>
                            <div class="wrap">
                                <div class="button">
                                <span class="btn btn-default btn-lg btn-transparent animate-load" data-event="jqm"
                                      data-param-id="17"
                                      data-name="order_services" data-autoload-product="<?=$SECTION["NAME"]?>: <?=$element["NAME"]?>"
                                      ><span>Купить в 1 клик</span></span>
                                </div>
                                <div class="button ">
                                <span class="btn btn-default btn-lg btn-transparent animate-load" data-event="jqm"
                                      data-param-id="20" data-autoload-need_product="<?=$SECTION["NAME"]?>: <?=$element["NAME"]?>"
                                      data-name="question"><span>Задать вопрос</span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>