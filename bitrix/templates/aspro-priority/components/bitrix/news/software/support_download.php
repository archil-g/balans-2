<?

include('section.php');
$APPLICATION->AddChainItem($SECTION["NAME"],$SECTION["SECTION_PAGE_URL"]);
$APPLICATION->AddChainItem($title, $SECTION["SECTION_PAGE_URL"]."support/");
$issetIblocks = [];
foreach ($iblockMenu as $item){
    $issetIblocks[] = $item['CODE'];
}
if(!empty($arResult["VARIABLES"]["IB_CODE"]) && !in_array($arResult["VARIABLES"]["IB_CODE"],$issetIblocks)) {
    Bitrix\Iblock\Component\Tools::process404(
        'Не найден', //Сообщение
        true, // Нужно ли определять 404-ю константу
        true, // Устанавливать ли статус
        true, // Показывать ли 404-ю страницу
        false // Ссылка на отличную от стандартной 404-ю
    );
}

if($arResult["VARIABLES"]["IB_CODE"]){
    $SELECTED_IBLOCK = $iblocks[$arResult["VARIABLES"]["IB_CODE"]];
}else{
    foreach($iblockMenu as $kk ){
        if($type == $kk["TYPE"]){
            LocalRedirect($SECTION["SECTION_PAGE_URL"]."support/".$iblocks[$kk["CODE"]]["CODE"]."/");
            $SELECTED_IBLOCK = $iblocks[$kk["CODE"]];
            break;
        }
    }
}

if($arResult["VARIABLES"]["IB_THEME"]){
    $APPLICATION->AddChainItem($SELECTED_IBLOCK["NAME"],$SECTION["SECTION_PAGE_URL"]."support/".$SELECTED_IBLOCK["CODE"]."/");
}else{
    $APPLICATION->AddChainItem($SELECTED_IBLOCK["NAME"]);
}
$selectedSection = "all";
$parentElementSection = $linkedSection = getRootSection(["IBLOCK_ID" => $SELECTED_IBLOCK["ID"],"UF_PO" => $SECTION["ID"]]);

if($arResult["VARIABLES"]["IB_THEME"]){
    $parentElementSection = CIBlockSection::GetList(
        ["SORT" => "ASC"],
        [
            "IBLOCK_ID" => $SELECTED_IBLOCK["ID"],
            "CODE" => $arResult["VARIABLES"]["IB_THEME"],
        ],
        false,
        ["ID", "NAME"])->fetch();

    $selectedSection = $parentElementSection["ID"];
    $APPLICATION->AddChainItem($parentElementSection["NAME"]);
}

$userField = CUserFieldEnum::GetList(array(), array(
    "ID" => $userFieldIDs,
));
while($field = $userField->GetNext()){
    $arResult["UF_PRODUCT_FAQ"][$field["ID"]] = $field["VALUE"];
}
$ibUrl = $iblockMenu[$SELECTED_IBLOCK["ID"]]["URL"] . "/" . $type . "/";

switch ($linkedSection["IBLOCK_TYPE_ID"]) {
    case "files":
        $catalogListTemplate = $template = "files";
        break;
    case "videos":
        $template = "video";
        $catalogListTemplate = "videos";
        break;
    case "articles":
        $catalogListTemplate = "videos";
        $template = "video";
}

?>

<?if($linkedSection):?>
<div class="maxwidth-theme">
    <div class="row padding-bottom">
        <div class="col-md-3 col-sm-4 mb20 left-menu-md">
            <? include "iblock_menu.php"; ?>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12 content-md ">
            <?if(($arResult['VARIABLES']['IB_CODE'] == 'download_link' || (empty($arResult['VARIABLES']['IB_CODE']) && $type == 'download')) && !empty($SECTION['XML_ID'])):?>
                <?
                \Bitrix\Main\Loader::includeModule("rdn");
                $download_links = \rdn\Api::downloadLink($SECTION['XML_ID']);

                if(!empty($download_links)){
                    $title = '';
                    ?>
                    <div class="accordion-type-1"><?
                    foreach ($download_links as $link_item){

                        if($link_item['group'] != $title){
                            $title = $link_item['group'];
                            ?><h3><?=$title?></h3><?
                        }


                        ?>
                        <div class="item border shadow wti">
                            <div class="accordion-head $arItem-close" data-toggle="collapse"
                                 href="#accordion_<?=$link_item['id']?>">
                                <span class="arrow_open pull-right"></span>
                                <?=$link_item['name']?>
                            </div>
                            <div id="accordion_<?=$link_item['id']?>" class="panel-collapse collapse">
                                <div class="accordion-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text">

                                                <div class="previewtext">
                                                    <div>
                                                        <?if(!empty($link_item['version'])):?><p><b>Версия:</b> <?=$link_item['version']?></p><?endif?>
                                                        <?if(!empty($link_item['version_date'])):?><p><b>Дата обновления:</b> <?=$link_item['version_date']?></p><?endif?>
                                                        <?if(!empty($link_item['changes'])):?><p><?=$link_item['changes']?></p><?endif?>
                                                        <?if(!empty($link_item['links'])):?>
                                                            <p>
                                                                <?$i = 0; foreach ($link_item['links'] as $link): $i++?>

                                                                    <a download href="<?=$link['url']?>"  >
                                                                        <span class="btn btn-default btn-transparent mr10"><?=$i == 1 ? 'Скачать' : 'Зеркало '.($i-1)?></span>
                                                                    </a>
                                                                <?endforeach?>
                                                            </p>
                                                        <?endif?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?
                    }
                    ?></div><?
                }
                ?>


            <?endif?>
            <?

            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "",
                array(
                    "SHOW_ELEMENTS" => "N",
                    "PAGE_TYPE" => "product",
                    "IBLOCK_ID" => $linkedSection["IBLOCK_ID"],
                    "SECTION_ID" => $linkedSection["ID"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                    "TOP_DEPTH" => 1,
                    "SELECTED_SECTION" => $selectedSection,
                    "SECTION_URL" => $iblockMenu[$SELECTED_IBLOCK["ID"]]["URL"],
                    "ELEMENT_URL" => $arParams["SEF_FOLDER"].$SELECTED_IBLOCK["IBLOCK_TYPE_ID"]."/",
                    "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                    "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                    "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
                    "ADD_SECTIONS_CHAIN" => "N",
                    "SHOW_SUB_ELEMENTS" => "N"
                ),
                $component,
                array("HIDE_ICONS" => "Y")
            );

            if($template == ".default" || !$template){$newsTemplate = "faqtype";}
            else {$newsTemplate = $template;}
            ?>

            <div class="item-views documents_list type_2">

                <?// dump($newsTemplate);
                $GLOBALS['dataFilter'] = ['PROPERTY_ARCHIVE' => false];
                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    $newsTemplate,
                    [
                        "HIDE_SECTION_INFO" => "Y",
                        "SECTION_URL" => $iblockMenu[$SELECTED_IBLOCK["ID"]]["URL"],
                        "ELEMENT_URL" => $arParams["SEF_FOLDER"].$SELECTED_IBLOCK["IBLOCK_TYPE_ID"]."/",
                        "SECTION_NAME" => $linkedSection["NAME"],
                        "SECTION_DESCRIPTION" => $linkedSection["~DESCRIPTION"],
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => ["NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT"],
                        "FILTER_NAME" => "dataFilter",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => $linkedSection["IBLOCK_ID"],
                        "IBLOCK_TYPE" => $linkedSection["IBLOCK_TYPE_ID"],
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "10",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => $parentElementSection["ID"],
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => ["DOCUMENTS", ""],
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N"
                    ]
                );
                ?>
            </div>
        </div>
    </div>
</div>
<?endif;?>

