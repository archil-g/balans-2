<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
/**
 * Created by PhpStorm.
 * User: Archil
 * Date: 06.02.2020
 * Time: 21:33
 */
$APPLICATION->IncludeComponent(
    "bitrix:system.auth.form",
    "main",
    Array(
        "AUTH_URL" => "/cabinet/",
        "REGISTER_URL" =>  "/cabinet/registration/",
        "FORGOT_PASSWORD_URL" =>  "/cabinet/forgot-password/",
        "PROFILE_URL" =>  "/cabinet/",
        "SHOW_ERRORS" => "Y",
    )
);?>