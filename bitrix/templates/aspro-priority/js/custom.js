/* Add here all your JS customizations */
$(document).ready(function () {

    $('.js-show-row').on('click',function () {
        var count = $(this).data('count')?$(this).data('count'):3;
        $el = $(this).closest('.row').find('.hidden-count:lt('+count+')');
        if($el.length < count){$(this).parent().remove()}
        $el.removeClass('hidden-count');
    });

    setFixedMenu();

    window.addEventListener('scroll', function () {
        if ($(".product_menu_wrapper.static").length > 0) {
            let wrapperOffset = $(".product_menu_wrapper.static").offset().top - $("#headerfixed .menu-only").height();
            let menuBlock = $(".fixed_menu");
            let windowScrollTop = $(window).scrollTop();
            //console.log(windowScrollTop, wrapperOffset);
            if (windowScrollTop > wrapperOffset) {
                menuBlock.removeClass("hide");
                menuBlock.addClass("fixed");
            } else {
                menuBlock.addClass("hide fixed");
                menuBlock.removeClass("fixed");
            }
        }
        if ($(".scrollable-block").length > 0) {
            let wrapperOffset = $(".scrollable-block").offset().top - $("#headerfixed .menu-only").height();
        }
    });

    $(window).resize(function () {
        setOwnHeight();
        setProgramItemWrapperHeight();
        if ($(window).width() > 800) {
            verticalAlign();
            hemi();
        }
    });

    setProgramItemWrapperHeight();

    $(".ancor-link-smooth").click(function (e) {
        e.preventDefault();
        let id = $(this).attr('href');
        let top = $(id).offset().top;
        let padding = parseInt($(id).css("padding-top"));
        $('body,html').animate({scrollTop: top - padding}, 750);
    });
});

function setProgramItemWrapperHeight()
{
    $(".program_item_wrapper").css({'height':'auto'});
    equalheight('.items .program_item_wrapper');
   /* $(".program_item_wrapper").css({'height':'auto'});
    let height = $(".program_item_wrapper:first").height();
    $(".program_item_wrapper").each(function () {
        if (height < $(this).height())
            height = $(this).height();
    });
    $(".program_item_wrapper").css({"height": height + "px"});*/
}

function setOwnHeight() {
    var maxHeight = 0;
    $(".media_blocks_main_page .item.col-xs-6").each(function () {
        maxHeight = this.offsetHeight > maxHeight ? this.offsetHeight : maxHeight;
    });
    setTimeout(function () {
        $(".media_blocks_main_page .item.col-xs-6").each(function () {
            $(this).css({'height': maxHeight + 'px'});
        });
    }, 700);
}

function verticalAlign() {
    if ($(window).width() > 800) {
        setTimeout(function () {
            $(".items-for-list .itemAS").each(function () {
                let imgHolder = $(this).children(".image");
                let image = imgHolder.children(".img-responsive");
               // imgHolder.height($(this).children(".text").height());
                let margin = (imgHolder.height() - image.height()) / 2;
                image.css({"margin-top": margin + "px"});
            });
        }, 700);
    } else {
        $(".items-for-list .itemAS").each(function () {
            let imgHolder = $(this).children(".image");
            let image = imgHolder.children(".img-responsive");
            imgHolder.css({"height": "inherit"});
            image.css({"margin-top": 0});
        });
    }
}

function hemi(){
    if($(".hemistyle").length > 0) {
        setTimeout(function () {
            let parent = $(".hemistyle").parent();
            $(".hemistyle").css({"height": parent.height() + "px"});
        }, 750);
    }
}

function setFixedMenu(){
    if($(".product_menu_wrapper.static").length > 0 && $(".product_menu_wrapper.fixed_menu").length > 0){
        let child = $(".product_menu_wrapper.static").children($(".maxwidth-theme"));
        $(".product_menu_wrapper.fixed_menu").children($(".maxwidth-theme")).html(child.html());
    }
}

function isValidForm(container) {
    //console.log(isValidForm.caller);
    container.find("input,textarea,select").parent().removeClass("error");
    var isValid = true;

    container.find("input,textarea,select").each(function () {

        if ($(this).data("required") == "1" && $(this).attr("type") == "radio" && !$(this).parent().find('input:checked').length && !$(this).parent().hasClass('error')) {
            $(this).parent().addClass("error").after("<label class='error'>Заполните это поле</label>");
            if($(this).closest('section.toggle').length)$(this).closest('section.toggle').addClass('error');
            isValid = false;
        }
        else if ($(this).data("required") == "1" && $(this).attr("type") == "checkbox" && !$(this).is(':checked') && !$(this).parent().hasClass('error')) {
            $(this).parent().addClass("error").after("<label class='error'>Заполните это поле</label>");
            isValid = false;
            if($(this).closest('section.toggle').length)$(this).closest('section.toggle').addClass('error');
        }
        else if ($(this).data("required") == "1" && ($(this).val() == "" || $(this).val() == null)) {
            $(this).parent().addClass("error").after("<label class='error'>Заполните это поле</label>");
            isValid = false;
            if($(this).closest('section.toggle').length)$(this).closest('section.toggle').addClass('error');
        }

    });

    $('.error input,.error select').on('change', function () {
        if ($(this).val().length){
            $(this).parent().removeClass('error').next('.error').remove();
            if($(this).closest('section.toggle').length)$(this).closest('section.toggle').removeClass('error');
        }

    });

    if(!isValid){
        scrollToBlock(container.find('.error:first'), -100);
    }

    return isValid;
}

(function($){
    $.fn.serializeAssoc = function() {
        var data = {};
        $.each( this.serializeArray(), function( key, obj ) {
            var a = obj.name.match(/(.*?)\[(.*?)\]/);
            if(a !== null)
            {
                var subName = a[1];
                var subKey = a[2];

                if( !data[subName] ) {
                    data[subName] = [ ];
                }

                if (!subKey.length) {
                    subKey = data[subName].length;
                }

                if( data[subName][subKey] ) {
                    if( $.isArray( data[subName][subKey] ) ) {
                        data[subName][subKey].push( obj.value );
                    } else {
                        data[subName][subKey] = [ ];
                        data[subName][subKey].push( obj.value );
                    }
                } else {
                    data[subName][subKey] = obj.value;
                }
            } else {
                if( data[obj.name] ) {
                    if( $.isArray( data[obj.name] ) ) {
                        data[obj.name].push( obj.value );
                    } else {
                        data[obj.name] = [ ];
                        data[obj.name].push( obj.value );
                    }
                } else {
                    data[obj.name] = obj.value;
                }
            }
        });
        return data;
    };
    $.fn.serializeObject = function() {
        var data = {};
        $.each( this.serializeArray(), function( key, obj ) {
            var a = obj.name.match(/(.*?)\[(.*?)\]/);
            if(a !== null)
            {
                var subName = new String(a[1]);
                var subKey = new String(a[2]);
                if( !data[subName] ) {
                    data[subName] = { };
                    data[subName].length = 0;
                };
                if (!subKey.length) {
                    subKey = data[subName].length;
                }
                if( data[subName][subKey] ) {
                    if( $.isArray( data[subName][subKey] ) ) {
                        data[subName][subKey].push( obj.value );
                    } else {
                        data[subName][subKey] = { };
                        data[subName][subKey].push( obj.value );
                    };
                } else {
                    data[subName][subKey] = obj.value;
                };
                data[subName].length++;
            } else {
                var keyName = new String(obj.name);
                if( data[keyName] ) {
                    if( $.isArray( data[keyName] ) ) {
                        data[keyName].push( obj.value );
                    } else {
                        data[keyName] = { };
                        data[keyName].push( obj.value );
                    };
                } else {
                    data[keyName] = obj.value;
                };
            };
        });
        return data;
    };
})(jQuery);

var equalheight = function(container){

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

$(function () {

    var myHash = location.hash; //получаем значение хеша
    location.hash = ''; //очищаем хеш
    if(myHash){ //проверяем, есть ли в хеше какое-то значение
        scrollToBlock($(myHash), -150);
    };

    $('.fancybox-video').fancybox({
        padding	: 0,
        opacity: true,
        width:650,
        height:400,
        maxWidth    : 800,
        maxHeight   : 600,
        type: "iframe",
        fitToView	: false,
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none',

    })
})

function pushNotifications(classDiv, text) {
    var pushNoti = {
        classDiv: '',
        text: '',
    };
    pushNoti.classDiv = classDiv;
    pushNoti.text = text;
    var $OuterDiv = $('<div id="successJsButton"></div>')
        .appendTo($('header'))
        .addClass(pushNoti.classDiv)
        .html(pushNoti.text)
        .fadeIn(1500).delay(3000).slideUp(300);
};

$(function(){

    $('.js-inn-mask').inputmask("mask", { "mask": "(9999999999)|(999999999999)", 'showMaskOnHover': false, 'greedy': false});
    $('#INN').inputmask("mask", { "mask": "(9999999999)|(999999999999)", 'showMaskOnHover': false,'greedy': false });

    $('.js-kpp-mask').inputmask("mask", { "mask": "999999999", 'showMaskOnHover': false, 'greedy': false});
    $('#KPP').inputmask("mask", { "mask": "999999999", 'showMaskOnHover': false,'greedy': false });
    $('.js-email-mask').inputmask("email", {'showMaskOnHover': false, "clearIncomplete": true });

    $('a[href*=#]:not([data-toggle])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
            && location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
            if ($target.length) {
                $of = $(".product_menu_wrapper.fixed_menu.fixed").length ? $(".product_menu_wrapper.fixed_menu.fixed").outerHeight(true) : 0;
                $of += $("#headerfixed").length ? $("#headerfixed").outerHeight(true) : 0;

                if($of == 0) $of = 200;
                var targetOffset = $target.offset().top;
                $('html,body').animate({scrollTop: targetOffset - $of}, 1000);
                return false;
            }
        }
    });
});