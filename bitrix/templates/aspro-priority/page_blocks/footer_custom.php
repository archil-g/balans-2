<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
global $arTheme;
$bPrintButton = ( $arTheme[ 'PRINT_BUTTON' ][ 'VALUE' ] == 'Y' ? true : false );
?>
<footer id="footer" class="footer-v1">

	<div class="footer_top">
		<div class="maxwidth-theme">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="fourth_bottom_menu">

						<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"bottom_3_col_footer", 
	array(
		"ROOT_MENU_TYPE" => "bottom5",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_USE_GROUPS" => "N",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "bottom5",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"COMPONENT_TEMPLATE" => "bottom_3_col_footer"
	),
	false
);?>

					</div>
				</div>
			</div>
<br>

			<div class="row">
				<div class="col-md-12 col-sm-12 contact-block">
					<div class="info">
						
						<div class="col-md-3 col-sm-12 center">
								<?=CPriority::showAddress('address blocks')?>
				
							</div>
							<div class="col-md-3 col-sm-12 center">
								<div class="phone blocks">
									<div class="inline-block">
										<?CPriority::ShowHeaderPhones('white sm');?>
									</div>
									<?if($arTheme["CALLBACK_BUTTON_FOOTER"]["VALUE"] == "Y"):?>
									<div class="inline-block callback_wrap">
										<span class="callback-block animate-load twosmallfont colored" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_callback");?>" data-name="callback">
											<?=GetMessage("S_CALLBACK")?>
										</span>
									</div>
									<?endif;?>
								</div>
							</div>				
							
							
							<div class="col-md-3 col-sm-12 center">
								<?=CPriority::showEmail('email blocks')?>
							</div>
						
						<?if(\Bitrix\Main\Loader::includeModule('subscribe')):?>
							<div class="col-md-3 col-sm-12 center">
								<div class="subscribe_button">
									<span class="btn" data-event="jqm" data-param-id="subscribe" data-param-type="subscribe" data-name="subscribe">
										<?=GetMessage('SUBSCRIBE_TITLE')?>
										<?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/subscribe.svg')?>
									</span>
								</div>
							</div>
							<?endif;?>
							
				</div>
			</div>
		</div>
	</div>







	<div class="footer_bottom">
		<div class="maxwidth-theme">
			<div class="row">
				<div class="copy-block col-md-6 col-sm-6 pull-left">
					<div class="copy font_xs pull-left">
						<?$APPLICATION->IncludeFile(SITE_DIR."include/footer/copy.php", Array(), Array(
								"MODE" => "php",
								"NAME" => "Copyright",
								"TEMPLATE" => "include_area.php",
							)
						);?>
					</div>
					<div id="bx-composite-banner" class="pull-left"></div>
				</div>
				<div class="link_block col-md-6 col-sm-6">
					<?if($bPrintButton):?>
					<div class="pull-right">
						<?=CPriority::ShowPrintLink();?>
					</div>
					<?endif?>
					<div class="pull-right">
						<div class="confidentiality">
							<?$APPLICATION->IncludeFile(SITE_DIR."include/footer/confidentiality.php", Array(), Array(
									"MODE" => "php",
									"NAME" => "�onfidentiality",
									"TEMPLATE" => "include_area.php",
								)
							);?>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</footer>