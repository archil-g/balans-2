<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$title = $_REQUEST['company_id']?"Редактирование компании":"Новая компания";
$APPLICATION->SetTitle($title);
?>
<?$APPLICATION->AddChainItem($title);?>
<?$APPLICATION->IncludeComponent(
	"rdn:form.rdn",
	"company",
	array(
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID" => '57',
		"USE_CAPTCHA" => "Y",
		"IS_PLACEHOLDER" => "N",
		"SUCCESS_MESSAGE" => "<p class=\"introtext\">Спасибо!</p><p>Данные отправлены!</p>",
		"SEND_BUTTON_NAME" => !empty($_REQUEST['company_id'])?"Отправить":'Сохранить',
		"SEND_BUTTON_CLASS" => "btn btn-default",
		"DISPLAY_CLOSE_BUTTON" => "N",
		"CLOSE_BUTTON_NAME" => "Обновить страницу",
		"CLOSE_BUTTON_CLASS" => "btn btn-default refresh-page",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "100000",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "company",
		"CACHE_GROUPS" => "Y",
		"REDIRECT_AFTER_ADD" => !empty($_REQUEST['redirect_url'])?$_REQUEST['redirect_url']:"/cabinet/company/",
		"NAME_CAPTION" => "Название компании"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>