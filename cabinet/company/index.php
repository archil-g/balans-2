<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Компании");

?>
<?$APPLICATION->AddChainItem("Компании");?>
<a href="/cabinet/company/form/" class="btn btn-lg pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Добавить новую компанию</a><br>
<div class="clearfix" style="margin-bottom: 20px;"></div>
<div class="companies">
<?
global $USER,$userCompanyFilter;
$userCompanyFilter[] = array(
    "LOGIC" => "OR",
    array("CREATED_BY" => $USER->GetId()),
    array("PROPERTY_USERS" => $USER->GetId()),
);

$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "company-list-accordion",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COUNT_LG" => "4",
        "COUNT_MD" => "3",
        "COUNT_SM" => "2",
        "COUNT_XS" => "1",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("NAME","CREATED_BY"),
        "FILTER_NAME" => "userCompanyFilter",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "57",
        "IBLOCK_TYPE" => "crm",
        "IMAGE_POSITION" => "left",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "MAX_COUNT_ELEMENTS_ON_PAGE" => "9",
        "MEDIA_PROPERTY" => "",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "20",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "main2",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array("CP_EMAIL","UR_ADR","FIZ_GOROD","UR_GOROD","RUK_DOC","RUK_DOLG","FIZ_INDEX","UR_INDEX","INN","KPP","CP_PHONE","CP_FAX","FIZ_ADR","ADDR_IS_EQ","CP_NAME","RUK_NAME","IS_IP","USERS_EDITORS"),
        "SEARCH_PAGE" => "/search/",
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SHOW_ALL_TITLE" => "",
        "SHOW_DETAIL_LINK" => "Y",
        "SHOW_GOODS" => "Y",
        "SHOW_PROPS_NAME" => "Y",
        "SHOW_SECTIONS" => "Y",
        "SLIDER_PROPERTY" => "",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "S_ASK_QUESTION" => "",
        "S_ORDER_PRODUCT" => "",
        "S_ORDER_SERVICE" => "",
        "TEMPLATE_THEME" => "blue",
        "TITLE" => "",
        "T_DOCS" => "",
        "T_GALLERY" => "",
        "T_GOODS" => "",
        "T_PROJECTS" => "",
        "T_REVIEWS" => "",
        "T_SERVICES" => "",
        "T_STAFF" => "",
        "USE_RATING" => "N",
        "USE_SHARE" => "N",
        "CUR_USER" => $USER->getId(),
    )
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>