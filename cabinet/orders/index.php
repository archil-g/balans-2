<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Мои заказы");
\Bitrix\Main\Loader::includeModule("rdn");
global $USER;

if(!$USER->IsAuthorized()) {
    LocalRedirect("/cabinet/");
}
$user = CUser::GetById($USER->getId())->Fetch();

?>
<?
$orders = [];
if(!empty($user['XML_ID']))$orders = \rdn\Api::getOrdersFull(10,0,$user['XML_ID']);
$ordersInternal = \rdn\Helper::getUserOrders();
if(!empty($orders) || !empty($ordersInternal)){// l($ordersInternal);return;
    ?>
        <div class="orders_list">
            <?if(!empty($ordersInternal)):?>
                <h2>Заказы на сайте</h2>
                    <?foreach ($ordersInternal as $order):?>
                    <?

                    $summ = 0;
                    foreach ($order['offers'] as $contract){
                        $summ = $summ + ($contract['price'] * count($order['data']['art'][$contract['article']]));
                    }
                    ?>
                    <div class="accordion-type-1">
                        <div class="item border shadow">
                            <div class="accordion-head accordion-close" data-toggle="collapse" href="#accordion<?=$order['id']?>">
                    <span><span class="title">Заказ №<?=$order['contract_number']?></span> <span class="small">от <?=substr($order['date'],0,10)?></span>
                        <dl class="dl-horizontal">
                            <dt>Статус:</dt>
                            <dd>
                                <?php //l($order);
                                if (empty($order['can_be_done']) && empty($order['is_done'])) {
                                    ?>
                                    <span class="label label-warning">Ожидание оплаты...</span>
                                    <?php
                                } elseif (!empty($order['can_be_done']) && empty($order['is_done'])){
                                    ?>
                                    <span class="label label-secondary">Оказание услуги</span>
                                    <?php
                                } elseif ($order['is_done']===true) {
                                ?>
                                <span class="label label-success">Выполнено<span/>
                                    <?php
                                    }
                                    ?>
                            </dd>
                                    <dt>Плтельщик:</dt>
                                    <dd><?=$order['data']['org_attr']['name']?></dd>
                                    <dt>ИНН:</dt>
                                    <dd><?=$order['data']['org_attr']['inn']?></dd>
                                    <dt>КПП:</dt>
                                    <dd><?=$order['data']['org_attr']['kpp']?></dd>
                                </dl>
                        <p>Сумма: <b><?=!empty($order['answer']['final_price']) ? round($order['answer']['final_price']) : $summ ?>  &#8381;</b></p>
                        <?//=date('d.m.Y',strtotime($order['updated_at']))?>
                        <i class="fa fa-angle-down" style="top: 26px"></i></span>

                            </div>
                            <div id="accordion<?=$order['id']?>" class="panel-collapse collapse">
                                <div class="accordion-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text">
												<h4>Документы</h4>
												<ul class="nolist">
													<?php /*<li><a href="/cabinet/license/"
																onclick="return license_order('#')"
																title="Лицензионное соглашение" target="_blank">
															<i class="icon-16-licdog"></i> Лицензионное соглашение</a>
													</li>*/?>
													<?php if (empty($order['can_be_done']) && empty($order['is_done'])) { ?>
														<li>
															<a target="_blank" href="/ajax/store/contract/<?php echo $order['id']; ?>/"
																	title="Договор счет">
																<i class="icon-16-licdog"></i> Лицензионный договор № <?php echo $order['contract_number']; ?>
																(счет на оплату)</a>
														</li>
														<?php
													} elseif (!empty($order['can_be_done']) && empty($order['is_done'])) {
														?>
														<li>
															<a href="" data-id="<?php echo $order['id']; ?>" class="btn left set_done"> <i
																		class="icon-add"></i>Получить услугу</a></li>
														<?php
													} elseif (!empty($order['is_done'])) { ?>
														<li>
															<a target="_blank" href="/ajax/store/contract/<?php echo $order['id']; ?>/"
																	title="Документы">
																<i class="icon-16-licdog"></i> Документы</a>
														</li>
														<?php
														$is_license = false;
														foreach ($order['contract_lines'] as $cl) {
															if (count($cl['licenses'])) {
																$is_license = true;
																break;
															}
														}
														if ($is_license) {
															?>
															<li>
																<a target="_blank"
																		href="/ajax/store/license/<?php echo $order['id']; ?>/"
																		title="Лицензии">
																	<i class="icon-16-licdog"></i> Лицензии</a>
															</li>
															<?php
														}

													}
													?>
												</ul>
                                                <h4>Продукты</h4>
                                                <?foreach ($order['offers'] as $contract): $i++;?>
                                                    <dl class="dl-horizontal">
                                                        <dt>Название:</dt>
                                                        <dd><?=$contract['print_name']?></dd>
                                                        <dt>Количество:</dt>
                                                        <dd><?=count($order['data']['art'][$contract['article']])?></dd>
                                                        <dt>Цена:</dt>
                                                        <dd><?=$contract['price']?> &#8381;</dd>
                                                        <dt>Итого:</dt>
                                                        <dd><?=$contract['price'] * count($order['data']['art'][$contract['article']])?>  &#8381;</dd>
                                                        <dt>Лицензиат<?=count($order['data']['art'][$contract['article']]) > 1 ? 'ы' : ''?>:</dt>
                                                        <dd>
                                                            <?foreach ($order['data']['art'][$contract['article']] as $company):?>
                                                            <div class="licensiat">
                                                                <b><?=$company['name']?></b><br>
                                                                ИНН: <?=$company['inn']?>, КПП: <?=$company['kpp']?>
                                                                <?=$contract['code']?'<br>Код компьютера '.$contract['code']:''?>
                                                            </div>
                                                            <?endforeach;?>

                                                        </dd>
                                                    </dl>
                                                <?endforeach?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?endforeach?>
            <?endif;?>
            <?if(!empty($orders)):?>
                <h2>Старые заказы</h2>
                <?
                foreach ($orders as $order){
        ?>

                    <div class="accordion-type-1">
                        <div class="item border shadow">
                            <div class="accordion-head accordion-close" data-toggle="collapse" href="#accordion<?=$order['id']?>">
                            <span><span class="title">Заказ №<?=$order['number']?></span> <span class="small">от <?=date('d.m.Y',strtotime($order['created_at']))?></span>
                                <dl class="dl-horizontal">
                                    <dt>Статус:</dt>
                                    <dd>
                                        <?php
                                        if (empty($order['can_be_done']) && empty($order['is_done'])) {
                                            ?>
                                            <span class="label label-warning">Ожидание оплаты...</span>
                                            <?php
                                        } elseif (!empty($order['is_done']) && empty($orde['is_done'])) {
                                            ?>
                                            <span class="label label-secondary">Оказание услуги</span>
                                            <?php
                                        } elseif (!empty($order['is_done'])) {
                                        ?>
                                        <span class="label label-success">Выполнено<span/>
                                            <?php
                                            }
                                            ?>
                                    </dd>
                                            <dt>Плтельщик:</dt>
                                            <dd><?=$order['org_attributes']['name']?></dd>
                                            <dt>ИНН:</dt>
                                            <dd><?=$order['org_attributes']['inn']?></dd>
                                            <dt>КПП:</dt>
                                            <dd><?=$order['org_attributes']['kpp']?></dd>
                                        </dl>
                                <p>Цена: <b><?=round($order['final_price'])?> &#8381;</b></p>
                                <?//=date('d.m.Y',strtotime($order['updated_at']))?>
                                <i class="fa fa-angle-down" style="top: 26px"></i></span>

                            </div>
                            <div id="accordion<?=$order['id']?>" class="panel-collapse collapse">
                                <div class="accordion-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text">
                                                <h4>Документы</h4>
                                                <ul class="nolist">
                                                    <li><a href="/cabinet/license/"
                                                           title="Лицензионное соглашение" target="_blank">
                                                            <i class="icon-16-licdog"></i> Лицензионное соглашение</a>
                                                    </li>
                                                    <?php if (empty($order['can_be_done']) && empty($order['is_done'])) { ?>
                                                        <li>
                                                            <a target="_blank" href="/ajax/store/doc/<?php echo $order['id']; ?>/"
                                                               title="Договор счет">
                                                                <i class="icon-16-licdog"></i> Лицензионный договор № <?php echo $order->number; ?>
                                                                (счет на оплату)</a>
                                                        </li>
                                                        <?php
                                                    } elseif (!empty($order['can_be_done']) && empty($order['is_done'])) {
                                                        ?>
                                                        <li>
                                                            <a href="" data-id="<?php echo $order['store_id']; ?>" class="btn left set_done"> <i
                                                                        class="icon-add"></i>Получить услугу</a></li>
                                                        <?php
                                                    } elseif (!empty($order['is_done'])) { ?>
                                                        <li>
                                                            <a target="_blank" href="/ajax/store/doc/<?php echo $order['id']; ?>/"
                                                               title="Документы">
                                                                <i class="icon-16-licdog"></i> Документы</a>
                                                        </li>
                                                        <?php
                                                        $is_license = false;
                                                        foreach ($order['contract_lines'] as $cl) {
                                                            if (count($cl['licenses'])) {
                                                                $is_license = true;
                                                                break;
                                                            }
                                                        }
                                                        if ($is_license) {
                                                            ?>
                                                            <li>
                                                                <a target="_blank"
                                                                   href="/ajax/store/license/#/"
                                                                   title="Лицензии">
                                                                    <i class="icon-16-licdog"></i> Лицензии</a>
                                                            </li>
                                                            <?php
                                                        }

                                                    }
                                                    ?>
                                                </ul>
                                                <h4>Продукты</h4>
                                                <?foreach ($order['contract_lines'] as $contract):?>

                                                <dl class="dl-horizontal">
                                                    <dt>Название:</dt>
                                                    <dd><?=$contract['print_name']?></dd>
                                                    <dt>Количество:</dt>
                                                    <dd><?=$contract['volume']?></dd>
                                                    <dt>Цена:</dt>
                                                    <dd><?=$contract['item_final_price']?> &#8381;</dd>
                                                    <dt>НДС:</dt>
                                                    <dd><?=$contract['item_nds']?$contract['item_nds']:'-'?></dd>
                                                    <dt>Скидка:</dt>
                                                    <dd><?=(int)$contract['discount_value']?>%</dd>
                                                    <dt>Итого:</dt>
                                                    <dd><?=$contract['final_price']?> &#8381;</dd>
                                                    <dt>Лицензиат:</dt>
                                                    <dd>
                                                        <?$company = $contract['licenses'][0]['company']?>
                                                        ИНН: <?=$company['0']?>, КПП: <?=$company['1']?><br>
                                                        <?=$company['2']?><br>
                                                        <?=$contract['licenses'][0]['computer_code']?'<br>Код компьютера '.$contract['licenses'][0]['computer_code']:''?>
                                                    </dd>
                                                </dl>

                                                <?endforeach?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <?
                }
                ?>
            <?endif?>
        </div>
    <?
}
?>
<script>
	jQuery(function () {
		jQuery('.set_done').click(function (e) {
			e.preventDefault();
			$('body').append('<div class="overlay_form"><div class="loader"><div class="duo duo1"><div class="dot dot-a"></div><div class="dot dot-b"></div></div><div class="duo duo2"><div class="dot dot-a"></div><div class="dot dot-b"></div></div></div></div>');
			jQuery.ajax('/ajax/api.php', {
				type: 'GET',
				dataType: 'json',
				data: {action:'set_done',id:jQuery(this).data('id')},
				error: function (xhr) {
					alert(xhr.responseText ? xhr.responseText : 'Ошибка');
					$('.overlay_form').remove();
				},
				success: function (ret) {
					$('.overlay_form').remove();
					if(ret.is_done){
						document.location.reload();
					}else if(ret.error){
						alert(ret.error);
					} else{
						alert('Ошибка, попробуйте позже.');
					}
				}
			})
		})
	})

</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>