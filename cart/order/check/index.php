<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

global $APPLICATION, $USER;
\Bitrix\Main\Loader::includeModule("rdn");
$userId = $USER->GetID();
$orderId = (int)$_REQUEST['ORDER_ID'];
$orders = \rdn\Helper::getUserOrders($orderId);

if (!empty($orders)) {
    $order =reset($orders);

}
if (!empty($order)):
	$APPLICATION->SetTitle("Договор №" . $order['contract_number'] . " от " . substr($order['date'], 0, 10));
	?>
<p class="alert alert-success">
    <?if(!empty($_REQUEST['new_user'])):?>
    Вы автоматически зарегистрированы. Информация выслана на email <?=$_REQUEST['new_user']?>.<br>
    <?endif?>
    Информация о заказе выслана на Ваш email.
</p>

    <? $i = 1; ?>
    <? foreach ($order['offers'] as $offer): ?>


        <dl class="dl-horizontal order-data">
            <dt>Покупка <?= count($order['offers']) > 1 ? $i++ : '' ?></dt>
            <dd><?= $offer['print_name'] ?></dd>
            <dt>Количество</dt>
            <dd><?= count($order['data']['art'][$offer['article']]) ?></dd>
            <dt>Цена</dt>
            <dd><?= $offer['price'] ?> &#8381;</dd>
            <dt>Сумма</dt>
            <dd><?= $offer['price'] * count($order['data']['art'][$offer['article']]) ?> &#8381;</dd>
            <dt>Критерии</dt>
            <dd>
                <ul>
                    <? foreach ($offer['crit'] as $crit): ?>
                        <li><?= $crit ?></li>
                    <? endforeach ?>
                </ul>
            </dd>
            <? if (!empty($order['data']['art'][$offer['article']][0]['start_date'])): ?>
                <dt>Период</dt>
                <dd><?= $order['periods'][$i - 2] ?></dd>
            <? endif ?>
        </dl>
    <? endforeach ?>
    <dl class="dl-horizontal final_price">
        <dt>Итоговая сумма<br>
            <small>(с учётом скидок)</small>
        </dt>
        <dd><b><?= $order['answer']['final_price'] ?> &#8381;</b></dd>
    </dl>
    <div class="check_order-block">
        <h2><span>1</span>Оплата</h2>
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <td colspan="3">Договор № <b><?= $order['contract_number'] ?></b>
                        от <?= substr($order['date'], 0, 10) ?> на сумму <b><?= $order['answer']['final_price'] ?></b>
                        &#8381;
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Банк получателя</b><br>
                        Филиал "Корпоративный" ПАО "Совкомбанк"
                    </td>
                    <td>
                        <b>БИК<br>К/с</b>
                    </td>
                    <td>
                        <b>044525360<br>301 01 810 4 4525 0000 360</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Получатель</b><br>
                        АО "ОВИОНТ ИНФОРМ"
                    </td>
                    <td>
                        <b>
                            Р/с<br>
                            ИНН<br>
                            КПП
                        </b>
                    </td>
                    <td>
                        <b>
                            407 02 810 4 0200 0360 682<br>
                            7725088527<br>
                            772501001
                        </b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Назначение платежа</b><br>
                        Оплата по лицензионному договору № <?= $order['contract_number'] ?>
                        от <?= substr($order['date'], 0, 10) ?> за<br>
                        <? foreach ($order['offers'] as $offer): $ii++?>
                            <?=$ii?>. <?= $offer['print_name'] ?>.<br>
                        <? endforeach; ?>
                        НДС не облагается.
                    </td>
                </tr>
            </table>
            <p>Оплата производится по безналичному расчёту. После поступления денежных средств на наш счёт, статус
                заказа в личном кабинете изменится на "Оплата поступила".<br>
                Если в течении 5 рабочих дней с момента оплаты статус заказа не поменялся, свяжитесь с нами через <a
                         title="Написать сообщение" data-event="jqm" data-param-id="20" data-name="question">Форму обратной связи</a> или по телефону +7 (495) 411-79-69</p>
        </div>
    </div>

    <div class="check_order-block">
        <h2><span>2</span>Получение лицензионного ключа</h2>
        <p>Лицензионный ключ будет доступен на странице <a href="/cabinet/orders/" target="_blank">Мои заказы</a> в Личном кабинете <b>после оплаты</b> и <b>начала периода действия лицензии</b>.<br>
           Счёт-фактура и акт будут сформированы после просмотра лицензионного ключа. Статус заказа изменится на "Услуга оказана"</p>
    </div>

    <div class="check_order-block">
        <h2><span>3</span>Получение документов</h2>
        <p>Оригиналы документов будут направлены Вам по почте после перевода заказа в статус "Услуга оказана", что требует просмотра лицензионного ключа.</p>
    </div>
    <div class="check_order-block">
        <h2><span>4</span>Получение программы</h2>
        <p>Программу обновления "Баланс-2W" можно скачать на сайтах <a href="http://www.balans2.ru">www.balans2.ru</a> и <a href="http://www.oviont.ru">www.oviont.ru</a>. До ввода ключа программа будет в демо-режиме</p>

    </div>


<? else: ?>
<?$url = $APPLICATION->GetCurUri();

    ?>
    <p class="alert alert-warning">Нет такого заказа, либо заказ принадлежит не Вам.<?if (!CUser::IsAuthorized()):?> Для просмотра заказа <a class="personal-link dark-color animate-load" data-event="jqm" data-param-type="auth" data-param-backurl="<?=$url?>" data-name="auth" href="/cabinet/">авторизуйтесь</a>.<?endif?></p>
<? endif ?>
<?if (!CUser::IsAuthorized()):?>
<div class="row">
    <div class="col-sm-9">
    <h2>Авторизация</h2>
    <?$APPLICATION->IncludeComponent(
        "bitrix:system.auth.form",
        "main",
        Array(
            "AUTH_URL" => '/cabinet/'.$arResult["URL_TEMPLATES"]["auth"],
            "REGISTER_URL" => '/cabinet/registration/'.$arResult["URL_TEMPLATES"]["registration"],
            "FORGOT_PASSWORD_URL" => '/cabinet/forgot-password/'.$arResult["URL_TEMPLATES"]["forgot_password"],
            "PROFILE_URL" => '/cabinet/',
            "SHOW_ERRORS" => "Y",
        )
    );?>
<?endif?>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>