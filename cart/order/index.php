<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//bitrix/templates/aspro-priority/vendor/slick/slick.js
global $APPLICATION, $USER;
$userID = $USER->GetID();
$userID = ($userID > 0 ? $userID : 0);
if(empty($_SESSION[SITE_ID][$userID]['BASKET_ITEMS'])) LocalREdirect('/cart/');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/vendor/slick/slick.js" );
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/vendor/slick/slick.css", true);
$APPLICATION->SetTitle("Оформление заказа");?>

<?$APPLICATION->IncludeComponent(
    "rdn:form.rdn",
    "order",
    array(
        "IBLOCK_TYPE" => "",
        "IBLOCK_ID" => 56,
        "USE_CAPTCHA" => "N",
        "IS_PLACEHOLDER" => "N",
        "SEND_BUTTON_NAME" => 'Оформить',
        "SEND_BUTTON_CLASS" => "btn btn-default",
        "DISPLAY_CLOSE_BUTTON" => "N",
        "SHOW_LICENCE" => "Y",
        "LICENCE_TEXT" => $arTheme["SHOW_LICENCE"]["DEPENDENT_PARAMS"]["LICENCE_TEXT"]["VALUE"],
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600000",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "order",
        "REDIRECT_AFTER_ADD" => "/cabinet/orders/",
        "IS_AUTH" => CUser::IsAuthorized(),
    ),
    false,
    array('HIDE_ICONS' => 'Y')
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>