<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Реквизиты");
?><img width="174" src="/upload/products/logo/main_logo.gif" height="136"><br>
Ниже приведены реквизиты компании, в случае необходимости получения дополнительных документов: свидетельства о государственной регистрации, идентификационного номера налогоплательщика вы можете обратиться в бухгалтерию предприятия.<br>
 <br>
<table class="table table-striped">
<tbody>
<tr>
	<td>
		 Полное наименование:
	</td>
	<td>
		 Акционерное Общество «ОВИОНТ ИНФОРМ»<br>
	</td>
</tr>
<tr>
	<td>
		 Сокращенное наименование:
	</td>
	<td>
		 АО «ОВИОНТ ИНФОРМ»<br>
	</td>
</tr>
<tr>
	<td>
		 ИНН/КПП:
	</td>
	<td>
		 7725088527/772501001
	</td>
</tr>
<tr>
	<td>
		 ОРГН:
	</td>
	<td>
		 1027700076051<br>
	</td>
</tr>
<tr>
	<td>
		 Юридический адрес:
	</td>
	<td>
		 115054, Москва, Жуков проезд, д.4, эт.2, пом.I, ком.3<br>
	</td>
</tr>
<tr>
	<td>
		 Фактический адрес:
	</td>
	<td>
		 127422, г.&nbsp;Москва, ул.&nbsp;Тимирязевская, 4/12 (м.&nbsp;Дмитровская)<br>
	</td>
</tr>
<tr>
	<td>
		 Телефон, факс:
	</td>
	<td>
		 +7 (495) 411-79-69
	</td>
</tr>
<tr>
	<td>
		 Электронная почта:
	</td>
	<td>
 <a href="mailto:info@oviont.ru">info@oviont.ru</a>
	</td>
</tr>
<tr>
	<td>
		 Сайт:
	</td>
	<td>
 <a href="http://www.oviont.ru/">http://www.oviont.ru/</a>
	</td>
</tr>
<tr>
	<td>
		 Банковские реквизиты:
	</td>
	<td>
		 БИК 044525360<br>
		 Р/с №40702810402000360682&nbsp;в&nbsp;Филиал «Корпоративный» ПАО «Совкомбанк», <br>
		 Кор/счет 30101810445250000360<br>
	</td>
</tr>
</tbody>
</table><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>