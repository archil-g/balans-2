
<?
//dump($arParams);
$res = \CIBlockElement::GetList(
    [],
    ['IBLOCK_ID' => 39,"PROPERTY_DIR_LIST" => $arParams["DIR_TYPE"],
        [
            "LOGIC" => "OR",
            ["PROPERTY_DIR_LINKS" => $arParams["TYPE"]],
            ["PROPERTY_DIR_LINKS" => false],
        ]
    ]
    , false, false);
while ($item = $res->fetch()) {
    $items[] = $item;
}
?>
<? if (!empty($items)): ?>
<div class="text useful_links media-links" style="display: block">
    <h4>Полезные ссылки</h4>
    <? foreach ($items as $uLink): ?>
        <a target="_blank" href="<?= $uLink["CODE"] ?>"><?= $uLink["NAME"] ?></a>
    <? endforeach; ?>
</div>
<? endif; ?>

