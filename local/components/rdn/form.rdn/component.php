<?if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true ) die();
if(\Bitrix\Main\Loader::includeModule('aspro.priority'))
{
    \Bitrix\Main\Loader::includeModule("rdn");
	global $arTheme;

	if(!$arTheme)
	{
		$arTheme = CPriority::GetFrontParametrsValues(SITE_ID);
		$arParams["SHOW_LICENCE"] = $arTheme["SHOW_LICENCE"];
	}
	else
	{
		if(isset($arTheme["SHOW_LICENCE"]["VALUE"]))
			$arParams["SHOW_LICENCE"] = $arTheme["SHOW_LICENCE"]["VALUE"];
		else
			$arParams["SHOW_LICENCE"] = $arTheme["SHOW_LICENCE"];
	}
	$useBitrixForm = (is_array($arTheme['USE_BITRIX_FORM']) ? $arTheme['USE_BITRIX_FORM']['VALUE'] : $arTheme['USE_BITRIX_FORM']);

	if($useBitrixForm == 'Y' && \Bitrix\Main\Loader::includeModule('form')){?>

		<?
		$APPLICATION->IncludeComponent(
			"bitrix:form",
			$this->getTemplateName(),
			Array(
				"AJAX_MODE" => $arParams["AJAX_MODE"],
				"SEF_MODE" => "N",
				"WEB_FORM_ID" => $arParams["IBLOCK_ID"],
				"START_PAGE" => "new",
				"SHOW_LIST_PAGE" => "N",
				"SHOW_EDIT_PAGE" => "N",
				"SHOW_VIEW_PAGE" => "N",
				"SUCCESS_URL" => "",
				"SHOW_ANSWER_VALUE" => "N",
				"SHOW_ADDITIONAL" => "N",
				"SHOW_STATUS" => "N",
				"EDIT_ADDITIONAL" => "N",
				"EDIT_STATUS" => "Y",
				"NOT_SHOW_FILTER" => "",
				"NOT_SHOW_TABLE" => "",
				"CHAIN_ITEM_TEXT" => "",
				"CHAIN_ITEM_LINK" => "",
				"IGNORE_CUSTOM_TEMPLATE" => "N",
				"USE_EXTENDED_ERRORS" => "Y",
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"AJAX_OPTION_JUMP" => $arParams["AJAX_OPTION_JUMP"],
				"AJAX_OPTION_STYLE" => $arParams["AJAX_OPTION_STYLE"],
				"AJAX_OPTION_HISTORY" => $arParams["AJAX_OPTION_HISTORY"],
				"SHOW_LICENCE" => $arParams["SHOW_LICENCE"],
				"LICENCE_TEXT" => $arParams["LICENCE_TEXT"],
				"DISPLAY_CLOSE_BUTTON" => $arParams["DISPLAY_CLOSE_BUTTON"],
				"SUCCESS_MESSAGE" => $arParams["SUCCESS_MESSAGE"],
				"CLOSE_BUTTON_NAME" => $arParams["CLOSE_BUTTON_NAME"],
				"CLOSE_BUTTON_CLASS" => $arParams["CLOSE_BUTTON_CLASS"],
				"VARIABLE_ALIASES" => Array(
					"action" => "action"
				)
			)
		);?>
	<?}
	else
	{
		if( CModule::IncludeModule("iblock") ){
			global $USER;
			if(!empty($_REQUEST['update_company'] && $_REQUEST['update_company'] == 'N')){
                if(!\rdn\Helper::checkInn($_REQUEST['INN'])){
                    $arResult["FORM_ERRORS"][] = 'Не верный ИНН';
                }
                if(strlen($_REQUEST['INN']) != 12) {
                    if(!\rdn\Helper::checkKpp($_REQUEST['KPP'])){
                        $arResult["FORM_ERRORS"][] = 'Не верный КПП';
                    }
                }
            }
			else if(!empty($_REQUEST['update_company'] && $_REQUEST['update_company'] == 'Y' && !empty($_REQUEST['isset_company']))){
                $element = \rdn\Helper::getCompanyById($_REQUEST['isset_company']);

                if(empty($element)) return false;
                $arPropFields = $_REQUEST;

                $res = \CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $_REQUEST['isset_company'], Array("sort"=>"asc"), array("CODE" => "USERS"));
                while ($ob = $res->GetNext())
                {
                    $users[] = $ob['VALUE'];
                }
                if(!empty($users)){
                    $arPropFields['USERS'] = $users;
				}

//                if($element['CREATED_BY'] == $USER->getId()) {
//                    if(!empty($arPropFields['USERS_EDITORS'])) {
//                        $arPropFields['USERS_EDITORS'] = explode(',',$arPropFields['USERS_EDITORS']);
//                    }
//                    else {
//                        $arPropFields['USERS_EDITORS'] = false;
//                    }
//                } else {
//                    if(!empty($element['USERS_EDITORS'])) {
//                        $arPropFields['USERS_EDITORS'] = $element['USERS_EDITORS'];
//                    }
//                }

                if(!empty($element['USERS_EDITORS'])) {
                    $arPropFields['USERS_EDITORS'] = $element['USERS_EDITORS'];
                }
                $arPropFields['TW_ID_COMPANY'] = $element['TW_ID_COMPANY'];

                $el = new \CIBlockElement;
                $arLoadArray = Array(
                    "PROPERTY_VALUES"=> $arPropFields,
                    "NAME"           => $arPropFields['NAME'],
                );

                $res = $el->Update($_REQUEST['isset_company'], $arLoadArray);

                LocalRedirect($arParams['REDIRECT_AFTER_ADD']?$arParams['REDIRECT_AFTER_ADD']:'/');
			}
			else{
                $GLOBALS['strError'] = '';
                $antiSpamHiddenFieldCode = 'not_send_confirm_text';
                $processingApprovalFieldCode = 'processing_approval';

                if( !is_set( $arParams["CACHE_TIME"] ) ){
                    $arParams["CACHE_TIME"] = "3600";
                }

                $bCache = !( $_SERVER["REQUEST_METHOD"] == "POST" && !empty( $_REQUEST["form_submit"] ) || $_REQUEST['formresult'] == 'ADDOK' ) && !( $arParams["CACHE_TYPE"] == "N" || ( $arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "N" ) || ( $arParams["CACHE_TYPE"] == "Y" && intval($arParams["CACHE_TIME"]) <= 0 ) );

                if( $bCache ){
                    $arCacheParams = array();
                    foreach( $arParams as $key => $value )
                        if( substr($key, 0, 1) != "~" )
                            $arCacheParams[$key] = $value;
                    if($arParams["CACHE_GROUPS"] == "Y")
                        $arCacheParams["USER_GROUPS"] = $GLOBALS["USER"]->GetGroups();
                    $obFormCache = new CPHPCache;
                    $CACHE_ID = SITE_ID."|".$componentName."|".md5(serialize($arCacheParams));
                    if( ( $tzOffset = CTimeZone::GetOffset() ) <> 0 )
                        $CACHE_ID .= "|".$tzOffset;
                    $CACHE_PATH = "/".SITE_ID.CComponentEngine::MakeComponentPath( $componentName );
                }

                if( $bCache && $obFormCache->InitCache( $arParams["CACHE_TIME"], $CACHE_ID, $CACHE_PATH ) ){
                    $arCacheVars = $obFormCache->GetVars();
                    $bVarsFromCache = true;

                    $arResult = $arCacheVars["arResult"];
                    $arResult["FORM_NOTE"] = "";
                    $arResult["isFormNote"] = "N";
                }else{
                    $bVarsFromCache = false;

                    if( $arParams["IBLOCK_ID"] > 0 ){
                        $arResult["F_RIGHT"] = CIBlock::GetPermission( $arParams["IBLOCK_ID"] );

                        if( $arResult["F_RIGHT"] == "D" ){
                            $arResult["ERROR"] = "FORM_ACCESS_DENIED";
                        }
                    }else{
                        $arResult["ERROR"] = "FORM_NOT_FOUND";
                    }

                    $arResult["EVENT_TYPE"] = "ASPRO_SEND_FORM";

                    $arIBlock = CIBlock::GetList( false, array( "ID" => $arParams["IBLOCK_ID"] ) )->Fetch();
                    $arResult["IBLOCK_CODE"] = $arIBlock["CODE"];
                    $arResult["IBLOCK_TITLE"] = $arIBlock["NAME"];
                    $arResult["IBLOCK_DESCRIPTION"] = $arIBlock["DESCRIPTION"];
                    $arResult["IBLOCK_DESCRIPTION_TYPE"] = $arIBlock["DESCRIPTION_TYPE"];

                    if($arIBlock['IBLOCK_TYPE_ID']){
                        $arResult["IBLOCK_TYPE_STRING"] = CIBlockType::GetByID($arIBlock['IBLOCK_TYPE_ID'])->Fetch();
                    }

                    $rsProp = CIBlock::GetProperties( $arParams["IBLOCK_ID"], array( "SORT"=> "ASC" ) , array("ACTIVE" => "Y"));
                    while( $arProp = $rsProp->Fetch() ){
                        $arResult["arQuestions"][] = $arProp;
                    }

                    if(is_array($arResult["arQuestions"])){
                        foreach( $arResult["arQuestions"] as $key => $arQuestion ){
                            if(in_array($arQuestion['CODE'],['TW_ID_COMPANY'])) {
                                continue;
                            }
                            $tmp = array(
                                "NAME" => $arQuestion["NAME"],
                                "CODE" => $arQuestion["CODE"],
                                "IS_REQUIRED" => $arQuestion["IS_REQUIRED"],
                                "HINT" => $arQuestion["HINT"],
                                "DEFAULT" => $arQuestion["DEFAULT_VALUE"],
                                "ICON" => $arQuestion["XML_ID"]
                            );
                            if( $arQuestion["PROPERTY_TYPE"] == "S" && empty( $arQuestion["USER_TYPE"] ) ){
                                $tmp["FIELD_TYPE"] = "text";
                            }elseif( $arQuestion["PROPERTY_TYPE"] == "S" && !empty( $arQuestion["USER_TYPE"] ) && $arQuestion["USER_TYPE"] == "Date" ){
                                $tmp["FIELD_TYPE"] = "date";
                            }elseif( $arQuestion["PROPERTY_TYPE"] == "S" && !empty( $arQuestion["USER_TYPE"] ) && $arQuestion["USER_TYPE"] == "DateTime" ){
                                $tmp["FIELD_TYPE"] = "datetime";
                            }elseif( $arQuestion["PROPERTY_TYPE"] == "S" && !empty( $arQuestion["USER_TYPE"] ) && $arQuestion["USER_TYPE"] == "HTML" ){
                                $tmp["FIELD_TYPE"] = "html";
                            }elseif( $arQuestion["PROPERTY_TYPE"] == "N" ){
                                $tmp["FIELD_TYPE"] = "integer";
                            }elseif( $arQuestion["PROPERTY_TYPE"] == "L" && $arQuestion["LIST_TYPE"] == "L" ){
                                $tmp["FIELD_TYPE"] = "list";
                            }elseif( $arQuestion["PROPERTY_TYPE"] == "L" && $arQuestion["LIST_TYPE"] == "C" ){
                                $tmp["FIELD_TYPE"] = "checkbox";
                            }elseif( $arQuestion["PROPERTY_TYPE"] == "F" ){
                                $tmp["FIELD_TYPE"] = "file";
                            }else{
                                continue;
                            }

                            $tmp["MULTIPLE"] = $arQuestion["MULTIPLE"];

                            if( $tmp["FIELD_TYPE"] != 'checkbox' && $tmp["FIELD_TYPE"] != "file"){
                                $tmp["CAPTION"] = '<label for="'.$arQuestion["CODE"].'">'.$arQuestion["NAME"].($arQuestion["IS_REQUIRED"] == "Y" ? '<span class="required-star">*</span>' : '').'</label>';
                            }

                            $arResult["QUESTIONS"][$arQuestion["CODE"]] = $tmp;
                        }

                    }
//l($arResult);
                    $arResult["SITE"] = CSite::GetByID(SITE_ID)->Fetch();
                    $arModuleOptions = CPriority::GetFrontParametrsValues(SITE_ID);
                    $arResult['MODULE_OPTIONS'] = $arModuleOptions;
                    $arResult['CAPTCHA_TYPE'] = ($arModuleOptions['CAPTCHA_FORM_TYPE'] == "Y" ? "IMG" : "HIDE");

                    if( $bCache ){
                        $obFormCache->StartDataCache();
                        $GLOBALS['CACHE_MANAGER']->StartTagCache($CACHE_PATH);
                        $GLOBALS['CACHE_MANAGER']->RegisterTag("forms");
                        $GLOBALS['CACHE_MANAGER']->RegisterTag("form_".$arParams["IBLOCK_ID"]);
                        $GLOBALS['CACHE_MANAGER']->EndTagCache();
                        $obFormCache->EndDataCache(
                            array(
                                "arResult" => $arResult,
                            )
                        );
                    }
                }

                $eventDesc = $eventDescAdmin = $messBody = $messBodyAdmin = '';
                if(is_array($arResult["QUESTIONS"])){
                    foreach($arResult["QUESTIONS"] as $FIELD_CODE => $arQuestion){
                        $eventDesc .= $arQuestion["NAME"].": "."#".$FIELD_CODE."#\n";
                        if($arQuestion["FIELD_TYPE"] == "html"){
                            $messBody .= $arQuestion["NAME"].":\n"."#".$FIELD_CODE."#\n";
                        }
                        else{
                            $messBody .= $arQuestion["NAME"].": "."#".$FIELD_CODE."#\n";
                        }
                    }
                }
                $eventDesc .= GetMessage("FORM_ET_DESCRIPTION");
                $eventDescAdmin = $eventDesc.GetMessage("FORM_ET_DESCRIPTION_ADMIN");
                $messBodyAdmin = $messBody;
                $messBodyAdmin .= GetMessage("FORM_EM_ADMIN_LINK");

                $event_name = $arResult["EVENT_TYPE"]."_".$arParams["IBLOCK_ID"];
                $arEvent = CEventType::GetByID( $event_name, $arResult["SITE"]["LANGUAGE_ID"] )->Fetch();
                if( !is_array( $arEvent ) ){
                    $et = new CEventType;
                    $arEventFields = array(
                        "LID" => $arResult["SITE"]["LANGUAGE_ID"],
                        "EVENT_NAME" => $event_name,
                        "NAME" => GetMessage("FORM_ET_NAME")." \"".$arResult["IBLOCK_TITLE"]."\"",
                        "DESCRIPTION" => $eventDesc,
                    );
                    $et->Add($arEventFields);
                    $arEventFields["LID"] = ($arResult["SITE"]["LANGUAGE_ID"] == 'ru' ? 'en' : 'ru');
                    $et->Add($arEventFields);
                }

                $arMess = CEventMessage::GetList( $arResult["SITE"]["ID"], $order="desc", array( "TYPE_ID" => $event_name ) )->Fetch();
                if( !is_array( $arMess ) ){
                    $em = new CEventMessage;
                    $arMess = array();
                    $arMess["ID"] = $em->Add( array(
                        "ACTIVE" => "Y",
                        "EVENT_NAME" => $event_name,
                        "LID" => array( $arResult["SITE"]["LID"] ),
                        "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
                        "EMAIL_TO" => "#EMAIL#",
                        "BCC" => "",
                        "SUBJECT" => GetMessage("FORM_EM_NAME"),
                        "BODY_TYPE" => "text",
                        "MESSAGE" => GetMessage("FORM_EM_START").$messBody.GetMessage("FORM_EM_END"),
                    ) );
                    $arMess["EVENT_NAME"] = $event_name;
                }

                $event_name_admin = $arResult["EVENT_TYPE"]."_ADMIN_".$arParams["IBLOCK_ID"];
                $arEvent = CEventType::GetByID( $event_name_admin, $arResult["SITE"]["LANGUAGE_ID"] )->Fetch();
                if( !is_array( $arEvent ) ){
                    $et = new CEventType;
                    $arEventFields = array(
                        "LID" => $arResult["SITE"]["LANGUAGE_ID"],
                        "EVENT_NAME" => $event_name_admin,
                        "NAME" => GetMessage("FORM_ET_NAME")." \"".$arResult["IBLOCK_TITLE"]."\"",
                        "DESCRIPTION" => $eventDescAdmin,
                    );
                    $et->Add($arEventFields);
                    $arEventFields["LID"] = ($arResult["SITE"]["LANGUAGE_ID"] == 'ru' ? 'en' : 'ru');
                    $et->Add($arEventFields);
                }

                $arMessAdmin = CEventMessage::GetList( $arResult["SITE"]["ID"], $order="desc", array( "TYPE_ID" => $event_name_admin ) )->Fetch();
                if( !is_array( $arMessAdmin ) ){
                    $em = new CEventMessage;
                    $arMessAdmin = array();
                    $arMessAdmin["ID"] = $em->Add( array(
                        "ACTIVE" => "Y",
                        "EVENT_NAME" => $event_name_admin,
                        "LID" => array( $arResult["SITE"]["LID"] ),
                        "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
                        "EMAIL_TO" => "#DEFAULT_EMAIL_FROM#",
                        "BCC" => "",
                        "SUBJECT" => GetMessage("FORM_EM_NAME"),
                        "BODY_TYPE" => "text",
                        "MESSAGE" => GetMessage("FORM_EM_START_ADMIN").$messBodyAdmin.GetMessage("FORM_EM_END"),
                    ) );
                    $arMessAdmin["EVENT_NAME"] = $event_name_admin;
                }

                if(is_array($arResult["QUESTIONS"])){

                    foreach( $arResult["QUESTIONS"] as $FIELD_CODE => $arQuestion ){
                        if(isset($_REQUEST[$FIELD_CODE])){
                            $val = !empty( $_REQUEST[$FIELD_CODE] ) ? $_REQUEST[$FIELD_CODE] : $arQuestion["DEFAULT"];
                            if(!is_array($val)){
                                $val = htmlspecialchars($val, (ENT_COMPAT | ENT_HTML401), LANG_CHARSET);
                            }
                        }
						elseif($arQuestion["FIELD_TYPE"] === "file"){
                            $val = $valFile = $arQuestion["DEFAULT"];
                            if($arQuestion["MULTIPLE"] == "Y"){
                                $valFile = array('n0' => $arQuestion["DEFAULT"], 'n1' => $arQuestion["DEFAULT"]);
                            }
                        }

                        $required = $arQuestion["IS_REQUIRED"] == "Y" ? 'required' : '';
                        $phone = strpos( $arQuestion["CODE"], "PHONE" ) !== false ? 'phone' : '';
                        $placeholder = $arParams["IS_PLACEHOLDER"] == "Y" ? 'placeholder="'.$arQuestion["NAME"].'"' : '';
                        $html = '';

                        switch( $arQuestion["FIELD_TYPE"] ){
                            case "hidden":
                                $html = '<input type="hidden" id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].'" class="form-control ignore '.$required.' '.$phone.'" '.$placeholder.' value="'.$val.'" />';
                                break;
                            case "text":
                                /*if( $arQuestion["MULTIPLE"] == "Y" ){
                                    $html = '';
                                    for( $i = 0; $i < 1; ++$i ){
                                        $html .= '<input type="'.( $arQuestion["CODE"] == "EMAIL" ? "email" : "text" ).'" id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].'['.$i.']" '.$required.' '.$placeholder.' class="form-control '.$required.' '.$phone.'" value="'.$val['n'.$i].'" />';
                                    }
                                }
                                else{*/
                                $html = '<input type="'.( strpos( $arQuestion["CODE"], "EMAIL" ) ? "email" : (strpos( $arQuestion["CODE"], "PHONE" ) ? "tel" : "text") ).'" id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].'" class="form-control '.$required.' '.$phone.'" '.$placeholder.' value="'.$val.'" />'.$icon;
                                //}
                                break;
                            case "integer":
                                $html = '<input type="number" id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].'" class="form-control '.$required.'" '.$placeholder.' value="'.$val.'" />';
                                break;
                            case "date":
                                $html = '<input type="text" id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].'" class="form-control date '.$required.'" '.$placeholder.' value="'.$val.'" />';
                                break;
                            case "datetime":
                                $html = '<input type="text" id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].'" class="form-control datetime '.$required.'" '.$placeholder.' value="'.$val.'" />';
                                break;
                            case "html":
                                $cur_val = (isset($val['TEXT']) ? $val['TEXT'] : $val);
                                $html = '<textarea id="'.$arQuestion["CODE"].'" rows="3" name="'.$arQuestion["CODE"].'" class="form-control '.$required.'" '.$placeholder.'>'.$cur_val.'</textarea>';
                                break;
                            case "list":
                                $rsValue = CIBlockProperty::GetPropertyEnum( $arQuestion["CODE"], array( "SORT" => "ASC", "ID" => "ASC" ), array("IBLOCK_ID" => $arParams["IBLOCK_ID"]));
                                $multiple = $arQuestion["MULTIPLE"] == "Y" ? ' multiple ' : '';
                                $html = '<select id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].($arQuestion["MULTIPLE"] == "Y" ? '[]' : '').'" class="form-control '.$required.'" '.$multiple.$placeholder.'>';
                                while( $arValue = $rsValue->Fetch() ){
                                    $selected = '';
                                    if( !empty( $val ) && (!is_array($val) ? ($arValue["ID"] == $val) : (in_array($arValue["ID"], $val))) ){
                                        $selected = 'selected="selected"';
                                    }
                                    if( empty( $val ) && $arValue["DEF"] == "Y" ){
                                        $selected = 'selected="selected"';
                                    }
                                    $html .= '<option value="'.$arValue["ID"].'" '.$selected.' >'.$arValue["VALUE"].'</option>';
                                    $arResult["QUESTIONS"][$FIELD_CODE]["ENUMS"][$arValue["ID"]] = $arValue["VALUE"];
                                }
                                $html .= '</select>';
                                break;
                            case "checkbox":
                                $html = '';
                                $rsValue = CIBlockProperty::GetPropertyEnum( $arQuestion["CODE"], array( "SORT" => "ASC", "ID" => "ASC" ), array("IBLOCK_ID" => $arParams["IBLOCK_ID"]));
                                $count = 0;
                                while( $arValue = $rsValue->Fetch() ){
                                    $count++;
                                }
                                $rsValue = CIBlockProperty::GetPropertyEnum( $arQuestion["CODE"], array( "SORT" => "ASC", "ID" => "ASC" ), array("IBLOCK_ID" => $arParams["IBLOCK_ID"]) );
                                while( $arValue = $rsValue->Fetch() ){
                                    $artmpValue = $arValue;
                                    $checked = '';
                                    if( !empty( $val ) && (!is_array($val) ? ($arValue["ID"] == $val) : (in_array($arValue["ID"], $val))) ){
                                        $checked = 'checked="checked"';
                                    }
                                    if( empty( $val ) && $arValue["DEF"] == "Y" ){
                                        $checked = 'checked="checked"';
                                    }
                                    ob_start();
                                    ?>
                                    <div class="licence_block bx_filter mb20">
                                        <input type="checkbox" class="js-onoff"  id="<?=$arQuestion["CODE"]?>" name="<?=$arQuestion["CODE"].($arQuestion["MULTIPLE"] == "Y" ? '[]' : '')?>" value="<?=$arValue["ID"]?>" <?=$checked?> >
                                        <?
                                        if( $count == 1 ){
                                            ?>
                                            <label for="<?=$arValue["ID"]?>">
                                                <?=$arQuestion["NAME"]?>
                                            </label>
                                            <?
                                        }else{
                                            ?>
                                            <label for="<?=$arValue["ID"]?>">
                                                <?=$arValue["VALUE"]?>
                                            </label>
                                            <?
                                        }
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?
                                    $html = ob_get_clean();
                                    $arResult["QUESTIONS"][$FIELD_CODE]["ENUMS"][$arValue["ID"]] = $arValue["VALUE"];
                                }
                                break;
                            case "file":
                                if( $arQuestion["MULTIPLE"] == "Y" ){
                                    $html = '';
                                    for( $i = 0; $i < 1; ++$i ){
                                        $html .= '<input type="file" id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].'_n'.$i.'" '.$required.' '.$placeholder.' class="inputfile" value="'.$valFile['n'.$i].'" />';
                                    }
                                }else{
                                    $html = '<input type="file" id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].'" '.$required.' '.$placeholder.' class="inputfile" value="'.$valFile.'" />';
                                }
                                break;
                        }

                        $arResult["QUESTIONS"][$FIELD_CODE]["VALUE"] = $val;
                        $arResult["QUESTIONS"][$FIELD_CODE]["HTML_CODE"] = $html;
                    }
                }

                if( strlen( $arResult["ERROR"] ) <= 0 ){ # проверка данных заказа и сохранение
                    if( strlen( $_REQUEST["form_order_submit"] ) > 0 ){
                        if($arResult["CAPTCHA_TYPE"] != "NONE")
                        {
                            if($arResult["CAPTCHA_TYPE"] == "IMG" && ( empty( $_REQUEST["captcha_word"] ) || !$APPLICATION->CaptchaCheckCode( $_REQUEST["captcha_word"], $_REQUEST["captcha_sid"] ) ) )
                            {
                                $arResult["FORM_ERRORS"][] = GetMessage("FORM_CAPTCHA");
                                $captcha_error = true;
                            }
                        }
						$data = [];
                        $org_pay = [];

                        $org_pay['inn'] = $_REQUEST['COMPANY_PAY_INN'];
                        $org_pay['kpp'] = $_REQUEST['COMPANY_PAY_KPP'];
                        $org_pay['name'] = $_REQUEST['COMPANY_PAY_NAME'];
                        $org_pay['address'] = $_REQUEST['COMPANY_PAY_ADDRESS'];

                        if(!\rdn\Helper::checkInn($org_pay['inn'])){
                            $arResult["FORM_ERRORS"][] = 'Не верный ИНН плательщика';
						}
						if(strlen($org_pay['inn']) != 12) {
							if(!\rdn\Helper::checkKpp($org_pay['kpp'])){
								$arResult["FORM_ERRORS"][] = 'Не верный КПП плательщика'.$org_pay['kpp'];
							}
						}


                        if(!empty($_REQUEST['OFFER'])) {
                        	$orgs = [];
                        	$orgs_eq = [];
							foreach ($_REQUEST['OFFER'] as $o_k => $offer) {
								$org = [];
                                $dates = [];
                                $periods = [];
                                if(!empty($_REQUEST['PERIOD'][$o_k])){
                                    $current_q = intval((date('n')+2)/3);
                                    $next_q = $current_q < 4 ? $current_q + 1 : 1;
                                   // $length_month = intval($_REQUEST['PERIOD'][$o_k]);
									$length_month = intval(empty($arResult['PRODUCTS'][$o_k]['CRIT']['Срок лицензирования'])?4:$arResult['PRODUCTS'][$o_k]['CRIT']['Срок лицензирования']);

                                    $quarter = $_REQUEST['REP_PERIOD'][$o_k] == 'current' ? $current_q : $next_q;
                                    $dates = \rdn\Helper::get_start_and_end_date($quarter,$length_month);

									$rimArr = [1 => 'I',2 => 'II',3 => 'III',4 => 'IV',];

									$dates_cur_q = \rdn\Helper::get_start_and_end_date($current_q,$length_month,'d.m.Y');
									$dates_next_q = \rdn\Helper::get_start_and_end_date($next_q,$length_month,'d.m.Y');

									$cut_q_text = $rimArr[$current_q] . ' квартал ' . date('Y') . ' года ('.$dates_cur_q['start'].'-'.$dates_cur_q['end'].')';
									$cut_q_text4 = 'С ' . $rimArr[$current_q] . ' квартала ' . date('Y') . ' года ('.$dates_cur_q['start'].'-'.$dates_cur_q['end'].')';
									$next_q_text = $current_q < 4 ? $rimArr[$next_q] . ' квартал ' . date('Y') . ' года ('.$dates_next_q['start'].'-'.$dates_next_q['end'].')' :  $rimArr[$next_q] . ' квартал ' . date('Y', strtotime('+1 year')) . ' года ('.$dates_next_q['start'].'-'.$dates_next_q['end'].')';
									$next_q_text4 = $current_q < 4 ? 'С ' . $rimArr[$next_q] . ' квартала ' . date('Y') . ' года ('.$dates_next_q['start'].'-'.$dates_next_q['end'].')' : 'С ' . $rimArr[$next_q] . ' квартала ' . date('Y', strtotime('+1 year')) . ' года ('.$dates_next_q['start'].'-'.$dates_next_q['end'].')';

									if($_REQUEST['REP_PERIOD'][$o_k] == 'current'){
										if($length_month < 4){
											$p = $cut_q_text;
										}
										else{
											$p = $cut_q_text4;
										}
									}
									else{
										if($length_month < 4){
											$p = $next_q_text;
										}
										else{
											$p = $next_q_text4;
										}
									}

                                    $periods[] = $p;
                                }
								if(!empty($_REQUEST['company_diff'][$o_k])){
									if(is_array($_REQUEST['company_diff'][$o_k])){
										foreach ($_REQUEST['company_diff'][$o_k] as $k => $val){
                                            $orgs[$o_k][$k] = $org_pay;
                                            $orgs[$o_k][$k]['code'] = $_REQUEST['DOP_CODE'][$o_k][$k];
                                            if(!empty($dates['start'])) {
                                                $orgs[$o_k][$k]['start_date'] = !empty($dates)?$dates['start']:'';
                                                $orgs[$o_k][$k]['end_date'] = !empty($dates)?$dates['end']:'';
											}

                                            unset($orgs[$o_k][$k]['address']);
                                            $orgs_eq[$o_k][$k] = 'Y';
										}
									}else{
										$org = $org_pay;
                                        $org['code'] = $_REQUEST['DOP_CODE'][$o_k][$k];
                                        if(!empty($dates['start'])) {
                                            $org['start_date'] = !empty($dates)?$dates['start']:'';
                                            $org['end_date'] = !empty($dates)?$dates['end']:'';
                                        }
                                        unset($org['address']);
                                        $orgs[$o_k][] = $org;
                                        $orgs_eq[$o_k] = 'Y';
									}
								}


								if(is_array($_REQUEST['COMPANY_INN'][$o_k])){
									foreach ($_REQUEST['COMPANY_INN'][$o_k] as $k => $val){ //COMPANY_INN[1034][1]: 7731415966
										if(empty($orgs_eq[$o_k][$k])){

											if(!\rdn\Helper::checkInn($val)){
												$arResult["FORM_ERRORS"][] = 'Не верный ИНН компании '.$_REQUEST['COMPANY_NAME'][$o_k][$k];
											}
											if(strlen($_REQUEST['COMPANY_INN'][$o_k][$k]) != 12) {
												if(!\rdn\Helper::checkKpp($_REQUEST['COMPANY_KPP'][$o_k][$k])){
													$arResult["FORM_ERRORS"][] = 'Не верный КПП компании '.$_REQUEST['COMPANY_NAME'][$o_k][$k];
												}
											}

											$orgs[$o_k][$k]['inn'] = $val;
											$orgs[$o_k][$k]['kpp'] = $_REQUEST['COMPANY_KPP'][$o_k][$k];
											$orgs[$o_k][$k]['name'] = $_REQUEST['COMPANY_NAME'][$o_k][$k];
											$orgs[$o_k][$k]['code'] = $_REQUEST['DOP_CODE'][$o_k][$k];
                                            if(!empty($dates['start'])) {
                                                $orgs[$o_k][$k]['start_date'] = !empty($dates)?$dates['start']:'';
                                                $orgs[$o_k][$k]['end_date'] = !empty($dates)?$dates['end']:'';
                                            }

										//	$orgs[$o_k][$k]['address'] = $_REQUEST['COMPANY_ADDRESS'][$o_k][$k];
										}
									}
								}else{
									if(empty($orgs_eq[$o_k])){

										$org['inn'] = $_REQUEST['COMPANY_INN'][$o_k];
										$org['kpp'] = $_REQUEST['COMPANY_KPP'][$o_k];
										$org['name'] = $_REQUEST['COMPANY_NAME'][$o_k];
										$org['code'] = $_REQUEST['DOP_CODE'][$o_k][0];

										if(!\rdn\Helper::checkInn($_REQUEST['COMPANY_INN'][$o_k])){
											$arResult["FORM_ERRORS"][] = 'Не верный ИНН компании '.$_REQUEST['COMPANY_NAME'][$o_k];
										}
										if(strlen($_REQUEST['COMPANY_INN'][$o_k]) != 12) {
											if(!\rdn\Helper::checkKpp($_REQUEST['COMPANY_KPP'][$o_k])){
												$arResult["FORM_ERRORS"][] = 'Не верный КПП компании '.$_REQUEST['COMPANY_NAME'][$o_k];
											}
										}


                                        if(!empty($dates['start'])) {
                                            $org['start_date'] = !empty($dates)?$dates['start']:'';
                                            $org['end_date'] = !empty($dates)?$dates['end']:'';
                                        }

									//	$org['address'] = $_REQUEST['COMPANY_ADDRESS'][$o_k];
										$orgs[$o_k][] = $org;
									}
								}


							}
                        }

                        foreach ($orgs as $k => $val){
                            $data['art'][$_REQUEST['art'][$k]] = array_values($val);
						}
                        $data['roistat_id'] = $_COOKIE['roistat_id'];
                        if(!empty($_REQUEST['COMPANY_PAY_EXT_ID'])) {
                            $data['k_org_id'] = $_REQUEST['tw_id_company'];
                            $data['org_attr'] = [
								'addr_is_eq' => !empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ']),//(empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS'])?$_REQUEST['addr_is_eq']:$_REQUEST['COMPANY_PAY_FIZ_ADDRESS']),
								'fiz_adr' => (empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS'])?$_REQUEST['fiz_adr']:$_REQUEST['COMPANY_PAY_FIZ_ADDRESS']),
								'fiz_gorod' => (empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS_CITY'])?$_REQUEST['fiz_gorod']:$_REQUEST['COMPANY_PAY_FIZ_ADDRESS_CITY']),
								'fiz_index' => (empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS_IND'])?$_REQUEST['fiz_index']:$_REQUEST['COMPANY_PAY_FIZ_ADDRESS_IND']),
								'ruk_doc' => (empty($_REQUEST['RUK_DOC'])?$_REQUEST['ruk_doc']:$_REQUEST['RUK_DOC']),
                                'ruk_dolg' =>  (empty($_REQUEST['RUK_DOLG'])?$_REQUEST['ruk_dolg']:$_REQUEST['RUK_DOLG']),
                                'ruk_name' => (empty($_REQUEST['RUK_NAME'])?$_REQUEST['ruk_name']:$_REQUEST['RUK_NAME']),
                                'cp_name' => $_REQUEST['cp_name'],
                                'cp_phone' => $_REQUEST['cp_phone'],
                                'cp_email' => $_REQUEST['cp_email'],
                                'name' => $_REQUEST['COMPANY_PAY_NAME'],
                                'inn' => $_REQUEST['COMPANY_PAY_INN'],
                                'kpp' => $_REQUEST['COMPANY_PAY_KPP'],
								'ur_gorod' => $_REQUEST['COMPANY_PAY_ADDRESS_CITY'],
								'ur_index' => $_REQUEST['COMPANY_PAY_ADDRESS_IND'],
								'ur_adr' => $_REQUEST['COMPANY_PAY_ADDRESS_CITY'],
                            ];
                        }
                        else {
                            $data['k_org_id'] = 0;
                            $data['org_attr'] = array_merge([
                                    'addr_is_eq' => !empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ']),//(empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS'])?$_REQUEST['addr_is_eq']:$_REQUEST['COMPANY_PAY_FIZ_ADDRESS']),
                                    'fiz_adr' => (empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS'])?$_REQUEST['fiz_adr']:$_REQUEST['COMPANY_PAY_FIZ_ADDRESS']),
                                    'fiz_gorod' => (empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS_CITY'])?$_REQUEST['fiz_gorod']:$_REQUEST['COMPANY_PAY_FIZ_ADDRESS_CITY']),
                                    'fiz_index' => (empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS_IND'])?$_REQUEST['fiz_index']:$_REQUEST['COMPANY_PAY_FIZ_ADDRESS_IND']),
                                    'ruk_doc' => (empty($_REQUEST['RUK_DOC'])?$_REQUEST['ruk_doc']:$_REQUEST['RUK_DOC']),
                                    'ruk_dolg' => (empty($_REQUEST['RUK_DOLG'])?$_REQUEST['ruk_dolg']:$_REQUEST['RUK_DOLG']),
                                    'ruk_name' => (empty($_REQUEST['RUK_NAME'])?$_REQUEST['ruk_name']:$_REQUEST['RUK_NAME']),
                                    'cp_name' => $_REQUEST['cp_name'],
                                    'cp_phone' => $_REQUEST['cp_phone'],
                                    'cp_email' => $_REQUEST['cp_email'],
									'inn' => $_REQUEST['COMPANY_PAY_INN'],
									'kpp' => $_REQUEST['COMPANY_PAY_KPP'],
									'name' => $_REQUEST['COMPANY_PAY_NAME'],
									'ur_gorod' => $_REQUEST['COMPANY_PAY_ADDRESS_CITY'],
									'ur_index' => $_REQUEST['COMPANY_PAY_ADDRESS_IND'],
									'ur_adr' => $_REQUEST['COMPANY_PAY_ADDRESS_CITY'],
							],$org_pay);
                        }

                        global $USER;
                        $userID = $USER->GetID();
                        $userID = ($userID > 0 ? $userID : 0);
						$newUser = 0;

                        if(empty($userID)) {
                            $filter = Array
                            (
                                "ACTIVE"              => "Y",
                                "EMAIL"               => $_REQUEST['EMAIL']
                            );
                            $rsUsers = $USER->GetList(($by="timestamp_x"), ($order="desc"), $filter);
                            if($user = $rsUsers->Fetch()) {
                                $userID = $user['ID'];
                                $data['org_attr']['store_id '] = $user['XML_ID'];
                            } else {
                            	if($_REQUEST['EMAIL']){

                                    $new_password = randString(7);
                                    $arFields = Array(
                                        "NAME"              => $_REQUEST['FIO'],
                                        "EMAIL"             => $_REQUEST['EMAIL'],
                                        "LOGIN"             => $_REQUEST['EMAIL'],
                                        "ACTIVE"            => "Y",
                                        "GROUP_ID"          => array(3,4),
                                        "PASSWORD"          => $new_password,
                                        "CONFIRM_PASSWORD"  => $new_password,
                                    );
                                    //$userID = CUser::Add($arFields);
                                    $userID = $USER->Add($arFields);
                                    $arEventFields = [
                                        "NAME" => $_REQUEST['FIO'],
                                        "EMAIL" => $_REQUEST['EMAIL'],
                                        "PASSWORD" => $new_password,
                                        "ID" => $userID,
                                    ];
                                    $newUser = $_REQUEST['EMAIL'];
                                    CEvent::Send("RDN_NEW_USER", ['s1'], $arEventFields);
                                    $USER->Authorize($userID);
									$data['org_attr']['store_id '] = $userID;

								}else {
									$data['org_attr']['store_id '] = 0;
								}
                            }
                        } else {
							$user = $USER->GetByID($userID)->Fetch();
                            $data['org_attr']['store_id '] = $user['XML_ID'];
						}

                        AddMessage2Log($arPropFields);

                        $arPropFields['COMPANY_PAY'] = [
                        	'VALUE' => $_REQUEST['COMPANY_PAY'],
							'DESCRIPTION' => serialize([
								'inn' => $_REQUEST['COMPANY_PAY_INN'],
								'kpp' => $_REQUEST['COMPANY_PAY_KPP'],
								'name' => $_REQUEST['COMPANY_PAY_NAME'],
								'address' => $_REQUEST['COMPANY_PAY_ADDRESS'],// TODO Понять нужно ли это поле, и если нет удалить
								'ur_gorod' => $_REQUEST['COMPANY_PAY_ADDRESS_CITY'],
								'ur_index' => $_REQUEST['COMPANY_PAY_ADDRESS_IND'],
								'ur_adr' => $_REQUEST['COMPANY_PAY_ADDRESS'],
							])
						];

                        if(is_array($arResult["QUESTIONS"])){
                            foreach( $arResult["QUESTIONS"] as $FIELD_CODE => $arQuestion ){
                                if(empty($arPropFields[$FIELD_CODE]) && $arQuestion["IS_REQUIRED"] == "Y" ){
                                    $arResult["FORM_ERRORS"][] = GetMessage("FORM_REQUIRED_INPUT").$arQuestion["NAME"];
                                }
                            }
                        }

                        if (count($arResult["FORM_ERRORS"]) <= 0) {
                        	# отправка заказа в CRM
                            $result = \rdn\Api::newOrder(['orders' => [$data]]);

                            $data_json = json_encode(['orders' => [$data]],JSON_UNESCAPED_UNICODE);
                            $result_json = json_encode($result,JSON_UNESCAPED_UNICODE);

                            $arPropFields['PRODUCT'] = array_unique(array_values($_REQUEST['PRODUCT']));
                            $arPropFields['OFFER'] = array_unique(array_values($_REQUEST['OFFER']));
                            $arPropFields['REP_PERIOD'] = array_values($periods);
                            $arPropFields['CONTRACT_NUMBER'] = $result[0]["contract_number"];
                            if(!empty($result[0]['error'])){
                                $arResult["FORM_ERRORS"][] = $result[0]['error'];//.' '.json_encode(['orders' => [$data]],JSON_UNESCAPED_UNICODE);
                            }
                            Bitrix\Main\Diag\Debug::writeToFile(['datetime' => date('d.m.Y H:i:s')/*,'request' => $_REQUEST*/, 'data' => $data, 'answer' => $result],"","order-log.txt");
						}

                        if (count($arResult["FORM_ERRORS"]) <= 0) {
                            unset($_SESSION[SITE_ID][$userID]['BASKET_ITEMS']);
                            $el = new CIBlockElement;
                            $arFields = array(
                                "XML_ID" => $result[0]["contract_id"],
                                'CREATED_BY' => $userID,
                                "CODE" => $_REQUEST["XML_ID"],
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "ACTIVE" => 'Y',
                                "NAME" => "Новый заказ " . ConvertTimeStamp(),
                                "DETAIL_TEXT" => $data_json,
                                "PREVIEW_TEXT" => $result_json,
                                "PROPERTY_VALUES" => $arPropFields,
                            );
                            $arFields['PROPERTY_VALUES']['STATUS'] = 448;
                            if ($RESULT_ID = $el->Add($arFields)) {

                                $ordersInternal = \rdn\Helper::getUserOrders($RESULT_ID);
                                if(!empty($ordersInternal)) {
                                    $order = current($ordersInternal);
                                    $arEventFields = [
                                        "NAME" => $_REQUEST['FIO'],
                                        "EMAIL" => $_REQUEST['EMAIL'],
                                        "ORDER_ID" => $RESULT_ID,
                                        "CONTRACT_NUMBER" => $order['contract_number'],
                                        "DATE" => substr($order['date'], 0, 10),

                                    ];
                                    ob_start();
                                    $summ = 0;
                                    foreach ($order['offers'] as $contract){
                                        $summ = $summ + ($contract['price'] * count($order['data']['art'][$contract['article']]));
                                    }

                                    ?>
                                    <table cellspacing="0" cellpadding="3" border="1" width="100%" >
                                        <tr>
                                            <th>Статус:</th>
                                            <td>
                                                <?php //l($order);
                                                if (empty($order['can_be_done']) && empty($order['is_done'])) {
                                                    ?>
                                                    <span class="label label-warning">Ожидание оплаты...</span>
                                                    <?php
                                                } elseif (!empty($order['can_be_done']) && empty($order['is_done'])) {
                                                    ?>
                                                    <span class="label label-secondary">Оказание услуги</span>
                                                    <?php
                                                }
                                                elseif ($order['is_done'] === true) {
                                                ?>
                                                <span class="label label-success">Выполнено<span/>
                                                    <?php
                                                    }
                                                    ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Плтельщик:</th>
                                            <td>
                                                <?= $order['data']['org_attr']['name'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>ИНН:</th>
                                            <td>
                                                <?= $order['data']['org_attr']['inn'] ?></td>
                                        </tr>
                                        <tr>
                                            <th>КПП:</th>
                                            <td>
                                                <?= $order['data']['org_attr']['kpp'] ?></td>
                                        </tr>
                                    </table>
                                    <p>Сумма: <b><?=!empty($order['answer']['final_price']) ? round($order['answer']['final_price']) : $summ ?>  &#8381;</b></p>

                                    <h4>Продукты</h4>
                                    <?foreach ($order['offers'] as $contract): $i++;?>
                                        <table cellspacing="0" cellpadding="3" border="1" width="100%">
                                            <tr>
                                                <td>Название:</td>
                                                <th>
                                                    <?= $contract['print_name'] ?></th>
                                            </tr>
                                            <tr>
                                                <th>Количество:</th>
                                                <td>
                                                    <?= count($order['data']['art'][$contract['article']]) ?></td>
                                            </tr>
                                            <tr>
                                                <th>Цена:</th>
                                                <td>
                                                    <?= $contract['price'] ?> &#8381;
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Итого:</th>
                                                <td>
                                                    <?= $contract['price'] * count($order['data']['art'][$contract['article']]) ?>
                                                    &#8381;
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Лицензиат<?= count($order['data']['art'][$contract['article']]) > 1 ? 'ы' : '' ?>
                                                    :
                                                </th>
                                                <td>

                                                    <? foreach ($order['data']['art'][$contract['article']] as $company): ?>
                                                        <div class="licensiat">
                                                            <b><?= $company['name'] ?></b><br>
                                                            ИНН: <?= $company['inn'] ?>, КПП: <?= $company['kpp'] ?>
                                                            <?= $contract['code'] ? '<br>Код компьютера ' . $contract['code'] : '' ?>
                                                        </div>
                                                    <? endforeach; ?>

                                                </td>
                                            </tr>
                                        </table>
                                    <?endforeach?>
                                    <?

                                    $html = ob_get_clean();
                                    $arEventFields['HTML'] = $html;
                                    CEvent::Send("RDN_NEW_ORDER", ['s1'], $arEventFields);
                                }

                                $arResult["FORM_RESULT"] = "ADDOK";
								if(!empty($result['update_company'])){
									# обновляем данные компании
									# TODO
									foreach($result['update_company'] as $company){
										if(!empty($company['error']))continue;
										$props = [
											'IS_IP' => $company['is_ip'],
											'ADDR_IS_EQ' => $company['addr_is_eq'],
											'FIZ_ADR' => $company['fiz_adr'],
											'FIZ_GOROD' => $company['fiz_gorod'],
											'FIZ_INDEX' => $company['fiz_index'],
											'RUK_DOC' => $company['ruk_doc'],
											'RUK_DOLG' => $company['ruk_dolg'],
											'RUK_NAME' => $company['ruk_name'],
											'UR_ADR' => $company['ur_adr'],
											'UR_GOROD' => $company['ur_gorod'],
											'UR_INDEX' => $company['ur_index'],
											'TW_ID_COMPANY' => $company['id'],
										];
										$arCompanyFields = array(
											"XML_ID" => $company['id'],
											'CREATED_BY' => $userID,
											"CODE" => $company['inn'].$company['kpp'],
											"IBLOCK_ID" => 57,
											"ACTIVE" => 'Y',
											"NAME" => $company["name"],
											"PROPERTY_VALUES" => $props,
										);
										//'CODE'=>$arCompanyFields['CODE']
										define('LOG_FILENAME','/home/bitrix/www/log.log');
										$res=$el->getList(null,['IBLOCK_ID'=>$arCompanyFields['IBLOCK_ID'],"XML_ID"=>$arCompanyFields['XML_ID']]);
										if($ob = $res->GetNextElement()){
											$companyInStore = $ob->GetFields();
											$id=$companyInStore['ID'];
											unset($arCompanyFields['XML_ID'],$arCompanyFields['CREATED_BY'],$arCompanyFields['IBLOCK_ID'],$arCompanyFields['NAME']);
											AddMessage2Log(json_encode(['insert',$arCompanyFields],256));
											$el->update($id,$arCompanyFields);
										}else{
											# Новая
											AddMessage2Log(json_encode(['insert',$arCompanyFields],256));
											$el->Add($arCompanyFields);
										}
										//
									}
								}
                                if(!empty($result['new_company'])){
                                	$company = $result['new_company'][0];
                                	$props = [
                                		'INN' => $company['inn'],
                                		'KPP' => $company['kpp'],
                                		'IS_IP' => $company['is_ip'],
                                		'ADDR_IS_EQ' => $company['addr_is_eq'],
                                		'FIZ_ADR' => $company['fiz_adr'],
                                		'FIZ_GOROD' => $company['fiz_gorod'],
                                		'FIZ_INDEX' => $company['fiz_index'],
                                		'RUK_DOC' => $company['ruk_doc'],
                                		'RUK_DOLG' => $company['ruk_dolg'],
                                		'RUK_NAME' => $company['ruk_name'],
                                		'UR_ADR' => $company['ur_adr'],
                                		'UR_GOROD' => $company['ur_gorod'],
                                		'UR_INDEX' => $company['ur_index'],
                                		'CP_NAME' => $company['cp_name'],
                                		'CP_PHONE' => $company['cp_phone'],
                                		'CP_EMAIL' => $company['cp_email'],
                                		'CP_FAX' => $company['cp_fax'],
                                		'TW_ID_COMPANY' => $company['id'],
									];

                                    $arCompanyFields = array(
                                        "XML_ID" => $company['id'],
                                        'CREATED_BY' => $userID,
                                        "CODE" => $company['inn'].$company['kpp'],
                                        "IBLOCK_ID" => 57,
                                        "ACTIVE" => 'Y',
                                        "NAME" => $company["name"],
                                        "PROPERTY_VALUES" => $props,
                                    );
                                    $el->Add($arCompanyFields);
								} elseif(!empty($result[0]['companies'][0])) {
                                    $issComp = \rdn\Helper::getCompanyByInnKpp($result[0]['companies'][0]['inn'],$result[0]['companies'][0]['kpp']);
                                    Bitrix\Main\Diag\Debug::writeToFile(['datetime' => date('d.m.Y H:i:s'), 'company' => $result[0]['companies'][0], 'issComp' => $issComp, 'user_id' => $userID],"","bind-log.txt");
                                    if($issComp && !in_array($userID,$issComp['USERS'])) {
                                        \rdn\Helper::addUserToCompany($issComp['id']);
									}
								}

                                LocalRedirect("/cart/order/check/?ORDER_ID=" . $RESULT_ID . '&new_user='.$newUser);
                                die();
                            } else {

                                die();
                            }
                        }
                    }
                	elseif( strlen( $_REQUEST["form_submit"] ) > 0 ){
                        if($arResult["CAPTCHA_TYPE"] != "NONE")
                        {
                            if($arResult["CAPTCHA_TYPE"] == "IMG" && ( empty( $_REQUEST["captcha_word"] ) || !$APPLICATION->CaptchaCheckCode( $_REQUEST["captcha_word"], $_REQUEST["captcha_sid"] ) ) )
                            {
                                $arResult["FORM_ERRORS"][] = GetMessage("FORM_CAPTCHA");
                                $captcha_error = true;
                            }
                        }

                        if(is_array($_REQUEST)){
                            foreach($_REQUEST as $code => $value){
                                if($arResult["QUESTIONS"][$code]["FIELD_TYPE"] == "html"){
                                    $_REQUEST[$code] = array( "VALUE" => array ("TEXT" => $value, "TYPE" => $arResult["QUESTIONS"][$code]["FIELD_TYPE"]) );
                                }
								elseif($arResult["QUESTIONS"][$code]["FIELD_TYPE"] == "date"){
                                    if(strlen($value)){
                                        $objDate = new \Bitrix\Main\Type\Date(str_replace(array('-', '/', ' ', ':'), array('.', '.', '.', '.'), $value));
                                        $_REQUEST[$code] = array("VALUE" => $objDate->toString());
                                    }
                                    else{
                                        $_REQUEST[$code] = array("VALUE" => $value);
                                    }
                                }
								elseif($arResult["QUESTIONS"][$code]["FIELD_TYPE"] == "datetime"){
                                    if(strlen($value)){
                                        $arDateTime = explode(' ', $value);
                                        $objDateTime = new \Bitrix\Main\Type\DateTime(str_replace(array('-', '/', ' ', ':'), array('.', '.', '.', '.'), $arDateTime[0]).' '.str_replace(array('-', '/', ' ', ':'), array(':', ':', ':', ':'), $arDateTime[1]));
                                        $_REQUEST[$code] = array("VALUE" => $objDateTime->toString());
                                    }
                                    else{
                                        $_REQUEST[$code] = array("VALUE" => $value);
                                    }
                                }
                            }
                        }

                        $arPropFields = $_REQUEST;
                        AddMessage2Log($arPropFields);

                        if(is_array($_FILES)){
                            foreach($arResult["QUESTIONS"] as $FIELD_CODE => $arQuestion){
                                if($arQuestion["FIELD_TYPE"] === 'file'){
                                    $bMultiple = $arQuestion["MULTIPLE"] === "Y";
                                    $arFiles = array();
                                    if(isset($_FILES[$FIELD_CODE]) && !$bMultiple){
                                        $arFiles[$FIELD_CODE] = $_FILES[$FIELD_CODE];
                                    }
									elseif($bMultiple){
                                        foreach($_FILES as $key => $arFile){
                                            // if(isset($_FILES[$FIELD_CODE.'_n0'])){
                                            if(strpos($key, '_n') !== false){
                                                if(is_numeric(str_replace(array($FIELD_CODE, '_', 'n'), '', $key))){
                                                    $arFiles[$key] = $_FILES[$key];
                                                }
                                            }
                                        }
                                    }

                                    if($arFiles){
                                        foreach($arFiles as $key => $arFile){
                                            if($arFile['name']){
                                                if($arFile['error']){
                                                    $arResult["FORM_ERRORS"][] = GetMessage('FORM_FILE_UPLOAD_ERROR').$arFile['name'];
                                                }
                                                else{
                                                    $code = explode('_', $key);
                                                    $tmp = $code[$cntCode - 1];
                                                    $arPropFields[$FIELD_CODE][($tmp ? $tmp : count($arPropFields[$FIELD_CODE]))] = $arFile;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        if(is_array($arResult["QUESTIONS"])){
                            foreach( $arResult["QUESTIONS"] as $FIELD_CODE => $arQuestion ){
                                if(empty($arPropFields[$FIELD_CODE]) && $arQuestion["IS_REQUIRED"] == "Y" ){
                                    $arResult["FORM_ERRORS"][] = GetMessage("FORM_REQUIRED_INPUT").$arQuestion["NAME"];
                                }
                            }
                        }

                        if( count( $arResult["FORM_ERRORS"] ) <= 0 ){
                            //if( check_bitrix_sessid() ){
                            $el = new CIBlockElement;

                            $arFields = array(
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "ACTIVE" => (isset($arTheme['MODERATION_REVIEWS']) && $arTheme['MODERATION_REVIEWS'] == 'Y' ? 'N' : 'Y'),
                                "NAME" => $arPropFields['NAME'] ? $arPropFields['NAME'] : GetMessage("DEFAULT_NAME").ConvertTimeStamp(),
                                "PROPERTY_VALUES" => $arPropFields,
                            );

                            foreach( GetModuleEvents("aspro.form", "OnBeforeFormSend", true) as $arEvent )
                                ExecuteModuleEventEx($arEvent, array(&$arFields));


                            if( $RESULT_ID = $el->Add( $arFields ) ){
                                $arResult["FORM_RESULT"] = "ADDOK";
                                foreach( GetModuleEvents("aspro.form", "OnAfterFormSend", true) as $arEvent )
                                    ExecuteModuleEventEx($arEvent, array(&$arFields));

                                $arEventFields = array(
                                    "SITE_NAME" => $arResult["SITE"]["NAME"],
                                    "FORM_NAME" => $arResult["IBLOCK_TITLE"],
                                    "ADMIN_RESULT_URL" => (isset($_SERVER['HTTPS']) ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].'/bitrix/admin/iblock_element_edit.php?IBLOCK_ID='.$arParams['IBLOCK_ID'].'&type='.$arResult["IBLOCK_TYPE_STRING"]['ID'].'&ID='.$RESULT_ID.'&lang='.$arResult["SITE"]["LANGUAGE_ID"].'&find_section_section=0&WF=Y',
                                    "PAGE_LINK" => ((CSite::inDir(SITE_DIR.'ajax/') || CSite::inDir(SITE_DIR.'form/')) ? (isset($_SERVER["HTTP_REFERER"]) && $_SERVER["HTTP_REFERER"] ? $_SERVER["HTTP_REFERER"] : $APPLICATION->GetCurPage()) : $APPLICATION->GetCurPage()),
                                );

                                if(is_array($arResult["QUESTIONS"])){
                                    foreach( $arResult["QUESTIONS"] as $FIELD_CODE => $arQuestion ){
                                        if($FIELD_CODE !== $antiSpamHiddenFieldCode){
                                            if($arQuestion["FIELD_TYPE"] == "list" || $arQuestion["FIELD_TYPE"] == "checkbox"){
                                                if($arQuestion['MULTIPLE'] == 'Y' && is_array($arQuestion["VALUE"])){
                                                    foreach($arQuestion["VALUE"] as $value){
                                                        $arEventFields[$FIELD_CODE][] = $arQuestion["ENUMS"][$value];
                                                    }
                                                    $arEventFields[$FIELD_CODE] = (count($arEventFields[$FIELD_CODE]) > 1 ? "\n" : '').implode("\n", $arEventFields[$FIELD_CODE]);
                                                }
                                                else{
                                                    $arEventFields[$FIELD_CODE] = $arQuestion["ENUMS"][$arQuestion["VALUE"]];
                                                }
                                            }
											elseif($arQuestion["FIELD_TYPE"] == "file"){
                                                $dbRes = CIBlockElement::GetList(array(), array('ID' => $RESULT_ID, 'IBLOCK_ID' => $arParams['IBLOCK_ID']), false, false, array('ID', 'PROPERTY_'.$FIELD_CODE));
                                                while($arItem = $dbRes->Fetch()){
                                                    if($arItem['PROPERTY_'.strtoupper($FIELD_CODE).'_VALUE']){
                                                        $filePath = CFile::GetPath($arItem['PROPERTY_'.strtoupper($FIELD_CODE).'_VALUE']);
                                                        $fileSize = filesize($_SERVER['DOCUMENT_ROOT'].$filePath);
                                                        $fileLink = (CMain::IsHTTPS() ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$filePath;
                                                        $format = 0;
                                                        $formats = array(GetMessage('FORM_CT_NAME_b'), GetMessage('FORM_CT_NAME_KB'), GetMessage('FORM_CT_NAME_MB'), GetMessage('FORM_CT_NAME_GB'), GetMessage('FORM_CT_NAME_TB'));
                                                        while($fileSize > 1024 && count($formats) != ($format + 1)){
                                                            ++$format;
                                                            $fileSize = round($fileSize / 1024, 1);
                                                        }
                                                        $arEventFields[$FIELD_CODE][] = GetMessage('FORM_CT_NAME_SIZE').$fileSize.$formats[$format].'. '.GetMessage('FORM_CT_NAME_LINK').$fileLink;
                                                    }
                                                }

                                                $arEventFields[$FIELD_CODE] = (count($arEventFields[$FIELD_CODE]) > 1 ? "\n" : '').implode("\n", $arEventFields[$FIELD_CODE]);
                                            }
                                            else{
                                                $arEventFields[$FIELD_CODE] = $arQuestion["VALUE"];
                                            }
                                        }
                                    }
                                }

                                if(strlen($arEventFields["EMAIL"])){
                                    if(isset($arEventFields['ORDER_LIST']) && strpos('aspro_priority_order_page', $arResult['IBLOCK_CODE']) !== false){
                                        $arEventFields['ORDER_LIST'] = implode(($arMess['BODY_TYPE'] === 'text' ? "\n" : '<br />'), $arResult["QUESTIONS"]['ORDER_LIST']["VALUE"]);
                                        if(isset($arEventFields['SESSION_ID'])){
                                            unset($arEventFields['SESSION_ID']);
                                        }
                                    }
                                    CEvent::SendImmediate( $arMess["EVENT_NAME"], SITE_ID, $arEventFields, "Y", $arMess["ID"] );
                                }

                                if(isset($arEventFields['ORDER_LIST']) && strpos('aspro_priority_order_page', $arResult['IBLOCK_CODE']) !== false){
                                    $arEventFields['ORDER_LIST'] = implode(($arMessAdmin['BODY_TYPE'] === 'text' ? "\n" : '<br />'), $arResult["QUESTIONS"]['ORDER_LIST']["VALUE"]);
                                }
                                CEvent::SendImmediate( $arMessAdmin["EVENT_NAME"], SITE_ID, $arEventFields, "Y", $arMessAdmin["ID"] );

                                if($arParams["REDIRECT_AFTER_ADD"]){
                                    LocalRedirect($arParams["REDIRECT_AFTER_ADD"]);
                                }
								elseif( $arParams["SEF_MODE"] == "Y" ){
                                    LocalRedirect(
                                        $APPLICATION->GetCurPageParam(
                                            "formresult=".urlencode($arResult["FORM_RESULT"]),
                                            array('formresult', 'strFormNote', 'SEF_APPLICATION_CUR_PAGE_URL')
                                        )
                                    );

                                    die();
                                }
                                else{
                                    LocalRedirect(
                                        $APPLICATION->GetCurPageParam(
                                            "IBLOCK_ID=".$arParams["IBLOCK_ID"]
                                            ."&RESULT_ID=".$RESULT_ID
                                            ."&formresult=".urlencode($arResult["FORM_RESULT"]),
                                            array('formresult', 'strFormNote', 'IBLOCK_ID', 'RESULT_ID')
                                        )
                                    );

                                    die();
                                }
                            }else{
                                $arResult["FORM_ERRORS"][] = $el->LAST_ERROR;
                            }
                            //}
                        }
                    }

                    if(!empty( $_REQUEST["formresult"] ) && strtoupper($_REQUEST["formresult"]) == "ADDOK"){
                        $successNoteFile = SITE_DIR."include/form/success_{$arResult["IBLOCK_CODE"]}.php";
                        if(\Bitrix\Main\IO\File::isFileExists(\Bitrix\Main\Application::getDocumentRoot().$successNoteFile)):
                            $arResult['FORM_NOTE'] = \Bitrix\Main\IO\File::getFileContents(\Bitrix\Main\Application::getDocumentRoot().$successNoteFile);
                        else:
                            $arResult['FORM_NOTE'] = !empty( $arParams["SUCCESS_MESSAGE"] ) ? $arParams["~SUCCESS_MESSAGE"] : GetMessage('FORM_NOTE_ADDOK');
                        endif;
                    }

                    $arResult["isFormErrors"] = (isset($arResult["FORM_ERRORS"]) && count($arResult["FORM_ERRORS"]) > 0 ? "Y" : "N");

                    if($arResult["CAPTCHA_TYPE"] == "IMG")
                        $arResult["CAPTCHACode"] = $APPLICATION->CaptchaGetCode();

                    $arResult = array_merge(
                        $arResult,
                        array(
                            "isFormNote" => strlen( $arResult["FORM_NOTE"] ) ? "Y" : "N",

                            "FORM_HEADER" => sprintf(
                                    "<form name=\"%s\" action=\"%s\" method=\"%s\" enctype=\"multipart/form-data\"  class='".($arResult['MODULE_OPTIONS']['RECAPTCHA_LOGO'] == 'N' ? 'hidde_gr_block' : '')."'>",
                                    $arResult["IBLOCK_CODE"], POST_FORM_ACTION_URI, "POST"
                                ).$res .= bitrix_sessid_post(),
                            "isIblockTitle" => strlen( $arResult["IBLOCK_TITLE"] ) > 0 ? "Y" : "N",
                            "isIblockDescription" => strlen( $arResult["IBLOCK_DESCRIPTION"] ) > 0 ? "Y" : "N",
                            "DATE_FORMAT" => CLang::GetDateFormat("SHORT"),
                            "FORM_FOOTER" => "</form>",
                            //"SUBMIT_BUTTON" => "<button class=\"".$arParams["SEND_BUTTON_CLASS"]."\" type=\"submit\">".$arParams["SEND_BUTTON_NAME"]."</button><br/><input type=\"hidden\" name=\"form_submit\" value=\"".GetMessage("FORM_ADD")."\">",
                            "CLOSE_BUTTON" => "<button class=\"".$arParams["CLOSE_BUTTON_CLASS"]."\" data-url=\"".$APPLICATION->GetCurPageParam('',	array('formresult', 'strFormNote', 'IBLOCK_ID', 'RESULT_ID', 'bxajaxid', 'AJAX_CALL', 'SEF_APPLICATION_CUR_PAGE_URL'))."\">".$arParams["CLOSE_BUTTON_NAME"]."</button>",

                        )
                    );

                    if(($arResult["isUseCaptcha"] = ($arResult["CAPTCHA_TYPE"] === 'IMG') ? 'Y' : 'N') === 'Y')
                    {
                        $arResult["CAPTCHA_CAPTION"] = "<label for=\"captcha_word\">".GetMessage("FORM_CAPTCHA_FIELD_TITLE")."<span class=\"required-star\">*</span></label>";
                        $arResult["CAPTCHA_IMAGE"] = "<input type=\"hidden\" name=\"captcha_sid\" class=\"captcha_sid\" value=\"".htmlspecialcharsbx($arResult["CAPTCHACode"])."\" /><img src=\"/bitrix/tools/captcha.php?captcha_sid=".htmlspecialcharsbx($arResult["CAPTCHACode"])."\" class=\"captcha_img\" width=\"180\" height=\"40\" />";
                        $captcha_val = !empty( $_REQUEST["captcha_word"] ) ? $_REQUEST["captcha_word"] : "";
                        $captcha_val = htmlspecialchars($captcha_val, (ENT_COMPAT | ENT_HTML401), LANG_CHARSET);
                        $arResult["CAPTCHA_FIELD"] = "<input id=\"captcha_word\" type=\"text\" name=\"captcha_word\" value=\"".$captcha_val."\" class=\"form-control captcha required\" autocomplete=\"off\" />".($captcha_error ? "<label for=\"captcha_word\" class=\"error\">".GetMessage("CAPTCHA_ERROR")."</label>" : "");


                        $arResult["CAPTCHA_ERROR"] = $captcha_error ? "Y" : "N";
                    }

                    if($arResult["isFormErrors"] == "Y"){
                        ob_start();
                        ShowError( implode( '<br />', $arResult["FORM_ERRORS"] ) );
                        $arResult["FORM_ERRORS_TEXT"] = ob_get_contents();
                        ob_end_clean();
                    }

                    if($arResult["CAPTCHA_TYPE"] == "HIDE")
                    {
                        $val = "";
                        if(isset($_REQUEST["nspm"])){
                            $val = !empty( $_REQUEST["nspm"] ) ? $_REQUEST["nspm"]  : "";
                            if(!is_array($val)){
                                $val = htmlspecialchars($val, (ENT_COMPAT | ENT_HTML401), LANG_CHARSET);
                            }
                        }
                        $arResult["QUESTIONS"]["CAPTCHA"] = array(
                            "HTML_CODE" => "<textarea name='nspm' style='display:none;'>".$val."</textarea>",
                            "CAPTION" => "",
                            "FIELD_TYPE" => "hidden",
                            "STRUCTURE" => array(
                                array(
                                    "FIELD_TYPE" => "hidden"
                                )
                            )
                        );
                    }

                    $this->initComponentTemplate();

                    $this->IncludeComponentTemplate();
                }else{
                    ShowError(GetMessage($arResult["ERROR"]));
                }
			}
		}else{
			ShowError(GetMessage("FORM_MODULE_NOT_INSTALLED"));
		}
	}
}
?>