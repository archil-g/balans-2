$(function () {

    $('#INN,#KPP').on('change',function () {
        if($('#INN').val() && ($('#INN').val().length == 12 || ($('#INN').val().length == 10 && $('#KPP').val()))){
            var form = $('#INN').closest('form');
            $.ajax({
                url: '/ajax/api.php',
                dataType : 'json',
                data: {"action": "company_info",'inn':$('#INN').val(),'kpp':$('#KPP').val()},
                beforeSend: function(){
                    form.addClass('loadings');
                },
                success: function(data){
                    if(data.success && data.isset_company){
                        $('.js-bind-company').closest('.row').removeClass('hidden');
                        $('#isset_company').val(data.isset_company.id);
                        $('.hidden_bifore').removeClass('showed');
                    }
                    else if(data.success && !data.isset_company){
                        $.each(data.data,function (key,value) {
                            if(value){
                                var ukey = key.toUpperCase();
                                $('#'+ukey).val(value).closest('.form-group').addClass('input-filed');
                            }
                        });
                        $('.js-bind-company').closest('.row').addClass('hidden');
                        $('.hidden_bifore').addClass('showed');
                        $('#INN,#KPP').attr('readonly','readonly');
                        $('.isset_company').removeClass('hidden');
                        $('#isset_company').val('Y');
                    }
                    else{
                        $('.js-bind-company').closest('.row').addClass('hidden');
                        $('.hidden_bifore').addClass('showed');
                    }
                },
                complete: function () {
                    form.removeClass('loadings');
                }
            });
        }

    });

    $('#ADDR_IS_EQ').on('change', function () {
        if($(this).is(':checked')) {
            $('[data-sid="FIZ_ADR"],[data-sid="FIZ_GOROD"],[data-sid="FIZ_INDEX"]').removeClass('showed');
        } else {
            $('[data-sid="FIZ_ADR"],[data-sid="FIZ_GOROD"],[data-sid="FIZ_INDEX"]').addClass('showed');
        }
    });

    $('.js-bind-company').on('click',function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $.ajax({
            url: '/ajax/api.php',
            dataType : 'json',
            data: {"action": "bind_company",'id':$('#isset_company').val()},
            beforeSend: function(){
                form.addClass('loadings');
            },
            success: function(data){
                if(data.success){
                  document.location.href = '/cabinet/company/';
                }
                else{
                    alert(data.message);
                    document.location.href = '/cabinet/company/';
                }
            },
            complete: function () {
                form.removeClass('loadings');
            }
        });
    })
});

function fill_company(id) {
    var form = $('#INN').closest('form');
    $.ajax({
        url: '/ajax/api.php',
        dataType : 'json',
        data: {"action": "site_company_info",'id':id},
        beforeSend: function(){
            form.addClass('loadings');
        },
        success: function(data){
            if(data.success){
                $('.js-bind-company').closest('.row').addClass('hidden');
                $('.hidden_bifore').addClass('showed');
                $.each(data.data,function (key,value) {
                    if(value){
                        if(key == 'ADDR_IS_EQ' || key == 'IS_IP') {
                            $('#'+key).prop('checked',true);
                            if(key == 'ADDR_IS_EQ') {
                                $('[data-sid="FIZ_ADR"],[data-sid="FIZ_GOROD"],[data-sid="FIZ_INDEX"]').removeClass('showed');
                            }
                        } else {
                            $('#'+key).val(value).closest('.form-group').addClass('input-filed');
                        }
                    }
                });

                // if(data.data.is_patron == 'Y') {
                //     $('[data-sid="USERS_EDITORS"]').removeClass('hidden');
                // }

                $('#INN,#KPP').attr('readonly','readonly');
                $('#isset_company').val(id);
                $('#update_company').val('Y');
            }
            else{
               document.location.href = '/cabinet/company/form/';
            }
        },
        complete: function () {
            form.removeClass('loadings');
        }
    });
}