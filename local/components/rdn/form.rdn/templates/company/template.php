<?if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true ) die();?>
<div class="form order border<?=($arResult['isFormNote'] == 'Y' ? ' success' : '')?><?=($arResult['isFormErrors'] == 'Y' ? ' error' : '')?>" id="company_form">
	<?=$arResult["FORM_HEADER"]?>
    <input type="hidden" name="XML_ID" value="0">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?if( $arResult["isIblockDescription"] ){?>
					<div class="description">
						<?if( $arResult["IBLOCK_DESCRIPTION_TYPE"] == "text" ){?>
							<p><?=$arResult["IBLOCK_DESCRIPTION"]?></p>
						<?}else{?>
							<?=$arResult["IBLOCK_DESCRIPTION"]?>
						<?}?>
					</div>
				<?}?>
			</div>
			<div class="col-md-12 col-sm-12">
				<?if($arResult["isUseCaptcha"] === "Y" && $arResult["isUseReCaptcha2"] === "Y"):?>
					<div class="input <?=($arResult['CAPTCHA_ERROR'] == 'Y' ? 'error' : '')?>">
						<div class="g-recaptcha" data-sitekey="<?=RECAPTCHA_SITE_KEY?>" data-callback="reCaptchaVerifyHidden" data-size="invisible"></div>
					</div>
				<?endif;?>
				<div class="row">
					<?if($arResult['isFormErrors'] == 'Y'):?>
						<div class="col-md-12">
							<div class="form-error alert alert-danger">
								<?=$arResult['FORM_ERRORS_TEXT']?>
							</div>
						</div>
					<?endif;?>
					<div class="col-md-12 col-sm-12">
                        <?
                       /* $arResult["QUESTIONS"]['NAME'] = [
                            'CAPTION' => $arParams['NAME_CAPTION'] ? $arParams['NAME_CAPTION'] : GetMessage("FORM_NAME_CAPTION"),
                            'HTML_CODE' => '<input type="text" id="NAME" name="NAME" class="form-control required" value="" />'
                        ];*/
                        ?>
                        <p>Введите ИНН и КПП, чтобы проверить наличие компании</p>
                        <p class="isset_company hidden">Найдена компания с такими данными. Часть данных заполнились автоматически. Заполните остальные поля.</p>
                        <input type="hidden" name="isset_company" id="isset_company" value="N">
                        <input type="hidden" name="update_company" id="update_company" value="N">
						<?if(is_array($arResult["QUESTIONS"])):?>
							<?$i = 0; foreach( $arResult["QUESTIONS"] as $FIELD_SID => $arQuestion ){
							    $i++;
								if( $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden' ){
									echo $arQuestion["HTML_CODE"];
								}else{?>
									<?$hidden = ($FIELD_SID == 'ORDER_LIST' || $FIELD_SID == 'SESSION_ID');?>
                                    <?$hidden_before = $i > 2;?>
                                    <?if($i == 3):?>
                                        <div class="row hidden_bifore" data-sid="NAME">
                                            <div class="col-md-12">
                                                <div class="form-group animated-labels">
                                                    <label for="NAME"><?=$arParams['NAME_CAPTION'] ? $arParams['NAME_CAPTION'] : GetMessage("FORM_NAME_CAPTION")?><span class="required-star">*</span></label>												<div class="input">
                                                        <input type="text" id="NAME" name="NAME" class="form-control required " value="" aria-required="true">																									</div>
                                                </div>
                                            </div>
                                        </div>
                                    <?endif?>
                                    <?if($FIELD_SID == 'UR_INDEX'):?>
                                        <div class="row<?=($hidden ? ' hidden' : '');?><?=($hidden_before ? ' hidden_bifore' : '');?>" data-SID="<?=$FIELD_SID?>">
                                            <div class="col-xs-12 mb5"><label><b>Юридический адрес</b></label></div>
                                            <div class="col-md-3">
                                                <div class="animated-labels form-group"  <?=( $arQuestion['VALUE'] ? "input-filed" : "");?>">
                                                    <?=$arQuestion["CAPTION"]?>
                                                    <div class="input">
                                                        <?=$arQuestion["HTML_CODE"]?>
                                                        <?if($arQuestion['FIELD_TYPE'] == "file" && $arQuestion['MULTIPLE'] == 'Y'):?>
                                                            <div class="add_file"><span><?=GetMessage('JS_FILE_ADD');?></span></div>
                                                        <?endif;?>
                                                    </div>
                                                    <?if( !empty( $arQuestion["HINT"] ) ){?>
                                                        <div class="hint"><?=$arQuestion["HINT"]?></div>
                                                    <?}?>
                                                </div>
                                            </div>
                                    <?elseif($FIELD_SID == 'UR_GOROD'):?>
                                        <div class="col-md-3">
                                            <div class="animated-labels form-group"  <?=( $arQuestion['VALUE'] ? "input-filed" : "");?>">
                                                <?=$arQuestion["CAPTION"]?>
                                                <div class="input">
                                                    <?=$arQuestion["HTML_CODE"]?>
                                                    <?if($arQuestion['FIELD_TYPE'] == "file" && $arQuestion['MULTIPLE'] == 'Y'):?>
                                                        <div class="add_file"><span><?=GetMessage('JS_FILE_ADD');?></span></div>
                                                    <?endif;?>
                                                </div>
                                                <?if( !empty( $arQuestion["HINT"] ) ){?>
                                                    <div class="hint"><?=$arQuestion["HINT"]?></div>
                                                <?}?>
                                            </div>
                                        </div>
                                    <?elseif($FIELD_SID == 'UR_ADR'):?>
                                        <div class="col-md-6">
                                            <div class="animated-labels form-group"  <?=( $arQuestion['VALUE'] ? "input-filed" : "");?>">
                                                <?=$arQuestion["CAPTION"]?>
                                                <div class="input">
                                                    <?=$arQuestion["HTML_CODE"]?>
                                                    <?if($arQuestion['FIELD_TYPE'] == "file" && $arQuestion['MULTIPLE'] == 'Y'):?>
                                                        <div class="add_file"><span><?=GetMessage('JS_FILE_ADD');?></span></div>
                                                    <?endif;?>
                                                </div>
                                                <?if( !empty( $arQuestion["HINT"] ) ){?>
                                                    <div class="hint"><?=$arQuestion["HINT"]?></div>
                                                <?}?>
                                            </div>
                                        </div>
                                        </div>
    <?elseif($FIELD_SID == 'FIZ_INDEX'):?>
        <div class="row<?=($hidden ? ' hidden' : '');?><?=($hidden_before ? ' hidden_bifore' : '');?>" data-SID="<?=$FIELD_SID?>">
            <div class="col-xs-12 mb5"><label><b>Физический адрес</b></label></div>
            <div class="col-md-3">
                <div class="animated-labels form-group"  <?=( $arQuestion['VALUE'] ? "input-filed" : "");?>">
                <?=$arQuestion["CAPTION"]?>
                <div class="input">
                    <?=$arQuestion["HTML_CODE"]?>
                    <?if($arQuestion['FIELD_TYPE'] == "file" && $arQuestion['MULTIPLE'] == 'Y'):?>
                        <div class="add_file"><span><?=GetMessage('JS_FILE_ADD');?></span></div>
                    <?endif;?>
                </div>
                <?if( !empty( $arQuestion["HINT"] ) ){?>
                    <div class="hint"><?=$arQuestion["HINT"]?></div>
                <?}?>
            </div>
        </div>
    <?elseif($FIELD_SID == 'FIZ_GOROD'):?>
    <div class="col-md-3">
        <div class="animated-labels form-group"  <?=( $arQuestion['VALUE'] ? "input-filed" : "");?>">
        <?=$arQuestion["CAPTION"]?>
        <div class="input">
            <?=$arQuestion["HTML_CODE"]?>
            <?if($arQuestion['FIELD_TYPE'] == "file" && $arQuestion['MULTIPLE'] == 'Y'):?>
                <div class="add_file"><span><?=GetMessage('JS_FILE_ADD');?></span></div>
            <?endif;?>
        </div>
        <?if( !empty( $arQuestion["HINT"] ) ){?>
            <div class="hint"><?=$arQuestion["HINT"]?></div>
        <?}?>
    </div>
</div>
<?elseif($FIELD_SID == 'FIZ_ADR'):?>
    <div class="col-md-6">
        <div class="animated-labels form-group"  <?=( $arQuestion['VALUE'] ? "input-filed" : "");?>">
        <?=$arQuestion["CAPTION"]?>
        <div class="input">
            <?=$arQuestion["HTML_CODE"]?>
            <?if($arQuestion['FIELD_TYPE'] == "file" && $arQuestion['MULTIPLE'] == 'Y'):?>
                <div class="add_file"><span><?=GetMessage('JS_FILE_ADD');?></span></div>
            <?endif;?>
        </div>
        <?if( !empty( $arQuestion["HINT"] ) ){?>
            <div class="hint"><?=$arQuestion["HINT"]?></div>
        <?}?>
    </div>
    </div>
    </div>
                                    <?else:?>
                                        <div class="row<?=($hidden ? ' hidden' : '');?><?=($hidden_before ? ' hidden_bifore' : '');?>" data-SID="<?=$FIELD_SID?>">
                            <div class="col-md-12">
                                <div class="<?=$arQuestion['FIELD_TYPE'] == "checkbox" ? '' : 'animated-labels form-group'?>  <?=( $arQuestion['VALUE'] ? "input-filed" : "");?>">
                                    <?=$arQuestion["CAPTION"]?>
                                    <div class="input">
                                        <?=$arQuestion["HTML_CODE"]?>
                                        <?if($arQuestion['FIELD_TYPE'] == "file" && $arQuestion['MULTIPLE'] == 'Y'):?>
                                            <div class="add_file"><span><?=GetMessage('JS_FILE_ADD');?></span></div>
                                        <?endif;?>
                                    </div>
                                    <?if( !empty( $arQuestion["HINT"] ) ){?>
                                        <div class="hint"><?=$arQuestion["HINT"]?></div>
                                    <?}?>
                                </div>
                            </div>
                        </div>
                                    <?endif?>

								<?}
							}?>
						<?endif;?>
					</div>
					<?if($arResult["QUESTIONS"]["MESSAGE"]):?>
						<div class="col-md-12 col-sm-12">
							<div class="row" data-SID="MESSAGE">
								<div class="col-md-12">
									<div class="form-group animated-labels <?=($arResult["QUESTIONS"]["MESSAGE"]["VALUE"]["TEXT"] ? 'input-filed' : '');?>">
										<?=$arResult["QUESTIONS"]["MESSAGE"]["CAPTION"]?>
										<div class="input">
											<?=$arResult["QUESTIONS"]["MESSAGE"]["HTML_CODE"]?>
										</div>
										<?if( !empty( $arResult["QUESTIONS"]["MESSAGE"]["HINT"] ) ){?>
											<div class="hint"><?=$arResult["QUESTIONS"]["MESSAGE"]["HINT"]?></div>
										<?}?>
									</div>
								</div>
							</div>
						</div>
					<?endif;?>
					<?if($arResult["isUseCaptcha"] === "Y"):?>
						<div class="captcha-row">
							<div class="col-md-12">
								<?=$arResult["CAPTCHA_CAPTION"];?>
								<div class="captcha_image">
									<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" class="captcha_img" border="0" />
									<input type="hidden" name="captcha_sid" class="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" />
									<div class="captcha_reload"></div>
									<span class="refresh"><a href="javascript:;" rel="nofollow"><?=GetMessage("REFRESH")?></a></span>
								</div>
								<div class="captcha_input">
									<input type="text" class="inputtext form-control captcha" name="captcha_word" size="30" maxlength="50" value="" required />
								</div>
							</div>
						</div>
					<?endif;?>
				</div>
<?/*?>
                <div class="row hidden" data-sid="USERS_EDITORS">
                    <div class="col-md-12">
                        <div class="animated-labels form-group">
                            <label for="USERS_EDITORS">Право на редактирование (ид пользователей через запятую)</label>
                            <div class="input">
                                <input type="text" id="USERS_EDITORS" name="USERS_EDITORS" class="form-control" value="">
                            </div>
                        </div>
                    </div>
                </div>
<?*/?>
				<div class="row">
					<div class="bottom_block col-md-12 col-sm-12">
						<div class="pull-left">
							<?=str_replace('class="', 'class="btn-lg ', $arResult["SUBMIT_BUTTON"])?>

                            <button class="hidden_bifore <?=$arParams["SEND_BUTTON_CLASS"]?> btn-lg" type="submit"><?=$arParams["SEND_BUTTON_NAME"]?></button>
                            <input type="hidden" name="form_submit" value="<?=GetMessage("FORM_ADD")?>">
						</div>
						<div class="clearfix"></div>
					</div>
				</div>

                <div class="row hidden">
                    <div class="bottom_block col-md-12 col-sm-12">
                        <p>Найдена компания с такими данными. Можно привязать её к Вашему аккаунту без возможности редактирования.</p>
                        <div class="pull-left">
                            <button class="<?=$arParams["SEND_BUTTON_CLASS"]?> btn-lg js-bind-company" type="button">Привзять компанию</button>
                            <input type="hidden" name="form_submit" value="<?=GetMessage("FORM_ADD")?>">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

			</div>
		</div>
	<?=$arResult["FORM_FOOTER"]?>
</div>

<script>
	var bFormNote = <?=CUtil::PhpToJSObject($arResult['FORM_NOTE']);?>;

	$(document).ready(function(){
		if(arPriorityOptions['THEME']['CAPTCHA_FORM_TYPE'] == 'RECAPTCHA' || arPriorityOptions['THEME']['CAPTCHA_FORM_TYPE'] == 'RECAPTCHA2'){
			reCaptchaRender();
		}
		if(arPriorityOptions['THEME']['USE_SALE_GOALS'] !== 'N'){
			var eventdata = {goal: 'goal_order_begin'};
			BX.onCustomEvent('onCounterGoals', [eventdata]);
		}
		$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"]').validate({
			ignore: ".ignore",
			highlight: function( element ){
				$(element).parent().addClass('error');
			},
			unhighlight: function( element ){
				$(element).parent().removeClass('error');
			},
            submitHandler: function( form ){
			    var _this = this;
				if( $('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"]').valid() ){
					$(form).find('button[type="submit"]').attr("disabled", "disabled");
					var eventdata = {type: 'form_submit', form: form, form_name: '<?=$arResult["IBLOCK_CODE"]?>'};
					if(!bFormNote){
						$(form).prepend('<div class="overlay_form"><div class="loader"><div class="duo duo1"><div class="dot dot-a"></div><div class="dot dot-b"></div></div><div class="duo duo2"><div class="dot dot-a"></div><div class="dot dot-b"></div></div></div></div>');
					}

					if($('#update_company').val() == 'Y'){
                        BX.onCustomEvent('onSubmitForm', [eventdata]);
                        return;
                    }

                    var data = {
                        "inn": $('#INN').val()?$('#INN').val():null,
                        "kpp": $('#KPP').val()?$('#KPP').val():null,
                        "name": $('#NAME').val()?$('#NAME').val():null,
                        "is_ip": $('#IS_IP').val() == '555',
                        "addr_is_eq": $('#ADDR_IS_EQ').val() == '556',
                        "fiz_adr": $('#FIZ_ADR').val()?$('#FIZ_ADR').val():null,
                        "fiz_gorod": $('#FIZ_GOROD').val()?$('#FIZ_GOROD').val():null,
                        "fiz_index": $('#FIZ_INDEX').val()?$('#FIZ_INDEX').val():null,
                        "org_id": $('#ORG_ID').val()?$('#ORG_ID').val():null,
                        "ruk_doc": $('#RUK_DOC').val()?$('#RUK_DOC').val():null,
                        "ruk_dolg": $('#RUK_DOLG').val()?$('#RUK_DOLG').val():null,
                        "ruk_name": $('#RUK_NAME').val()?$('#RUK_NAME').val():null,
                        "ur_adr": $('#UR_ADR').val()?$('#UR_ADR').val():null,
                        "ur_gorod": $('#UR_GOROD').val()?$('#UR_GOROD').val():null,
                        "ur_index": $('#UR_INDEX').val()?$('#UR_INDEX').val():null,
                        "cp_name": $('#CP_NAME').val()?$('#CP_NAME').val():null,
                        "cp_phone": $('#CP_PHONE').val()?$('#CP_PHONE').val():null,
                        "cp_email": $('#CP_EMAIL').val()?$('#CP_EMAIL').val():null,
                        "cp_fax": $('#CP_FAX').val()?$('#CP_FAX').val():null
                    };

                    $.ajax({
                        url: '/ajax/api.php',
                        dataType : 'json',
                        data: {"action": "add_company",'data':data,'isset_company': $('#isset_company').val()},
                        beforeSend: function(){

                        },
                        success: function(data){
                            if(data.success){
                                BX.onCustomEvent('onSubmitForm', [eventdata]);
                            }
                            else if(data.error){
                                for (var key in data.error){
                                    var ukey = key.toUpperCase();
                                    if($('#'+ukey).length){
                                        var str = data.error[key][0];
                                        _this.showErrors({
                                            [ukey] : str
                                        });
                                    }
                                }
                            }
                        },
                        complete: function () {
                            $('.overlay_form').remove();
                        }
                    });
				}
			},
			errorPlacement: function( error, element ){
				if($(element).hasClass('captcha')){
					$(element).closest('.captcha-row').append(error);
				}
				else if($(element).closest('.licence_block').length){
					$(element).closest('.licence_block').append(error);
				}
				else if($(element).closest('[data-sid=FILE]')){
					$(element).closest('.form-group').append(error);
				}
				else{
					if($(element).closest('.licence_block').length){
						$(element).closest('.licence_block').append(error);
					}
					else if($(element).closest('[data-sid=FILE]')){
						$(element).closest('.form-group').append(error);
					}
					else{
						error.insertAfter(element);
					}
				}
			},
			messages:{
				licenses_popup: {
					required : BX.message('JS_REQUIRED_LICENSES')
				}
			}
		});

		if(arPriorityOptions['THEME']['PHONE_MASK'].length){
			var base_mask = arPriorityOptions['THEME']['PHONE_MASK'].replace( /(\d)/g, '_' );
			$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"] input.phone').inputmask("mask", { "mask": arPriorityOptions['THEME']['PHONE_MASK'], 'showMaskOnHover': false });
			$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"] input.phone').blur(function(){
				if( $(this).val() == base_mask || $(this).val() == '' ){
					if( $(this).hasClass('required') ){
						$(this).parent().find('div.error').html(BX.message("JS_REQUIRED"));
					}
				}
			});
		}

		var sessionID = '<?=bitrix_sessid()?>';
		$('input#SESSION_ID').val(sessionID);

		if(arPriorityOptions['THEME']['DATE_MASK'].length){
			$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"] input.date').inputmask('datetime', {
				inputFormat: arPriorityOptions['THEME']['DATE_MASK'],
				placeholder: arPriorityOptions['THEME']['DATE_PLACEHOLDER'],
				showMaskOnHover: false
			});
		}

		if(arPriorityOptions['THEME']['DATETIME_MASK'].length){
			$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"] input.datetime').inputmask('datetime', {
				inputFormat: arPriorityOptions['THEME']['DATETIME_MASK'],
				placeholder: arPriorityOptions['THEME']['DATETIME_PLACEHOLDER'],
				showMaskOnHover: false
			});
		}

		$("input[type=file]").uniform({ fileButtonHtml: BX.message("JS_FILE_BUTTON_NAME"), fileDefaultHtml: BX.message("JS_FILE_DEFAULT") });
		$(document).on('change', 'input[type=file]', function(){
			if($(this).val())
			{
				$(this).closest('.uploader').addClass('files_add');
			}
			else
			{
				$(this).closest('.uploader').removeClass('files_add');
			}
		})
		$('.form .add_file').on('click', function(){
			var index = $(this).closest('.input').find('input[type=file]').length+1;
			$('<input type="file" id="POPUP_FILE" name="FILE_n'+index+'"   class="inputfile" value="" />').insertBefore($(this));
			$('input[type=file]').uniform({fileButtonHtml: BX.message('JS_FILE_BUTTON_NAME'), fileDefaultHtml: BX.message('JS_FILE_DEFAULT')});
		})
	});

	<?if(!empty($_REQUEST['company_id'])):?>
        fill_company(<?=(int)$_REQUEST['company_id']?>);
    <?endif?>
</script>