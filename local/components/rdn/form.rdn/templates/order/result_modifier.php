<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Loader::includeModule("rdn");
global $APPLICATION, $USER;
$userID = $USER->GetID();


$userID = ($userID > 0 ? $userID : 0);

if($userID > 0)
    $arResult['USER'] = $USER->GetByID($USER->GetID())->fetch();

$arResult['COMPANIES'] = \rdn\Helper::getUserCompanies($USER->GetId());
$arResult['PRODUCTS'] = $_SESSION[SITE_ID][$userID]['BASKET_ITEMS'];
$arResult['QUANTYTY_MANY'] = false;
$allSumm = 0;
$cnt_summ = 0;
$allSummOld = 0;
foreach ($arResult['PRODUCTS'] as $k => $arItem){
    $cnt_summ = $cnt_summ + $arItem['QUANTITY'];
    $pic = (strlen($arItem['PREVIEW_PICTURE']) ? $arItem['PREVIEW_PICTURE'] : (strlen($arItem['DETAIL_PICTURE']) ? $arItem['DETAIL_PICTURE'] : ''));
    if($pic){
        $picArr = CFile::ResizeImageGet($pic, array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true );
        $arResult['PRODUCTS'][$k]['PICTURE'] = $picArr['src'];
    }
    else{
        $arResult['PRODUCTS'][$k]['PICTURE'] = SITE_TEMPLATE_PATH.'/images/svg/noimage_product.svg';
    }
    if((int)$arItem['QUANTITY'] > 10){
        $arResult['PRODUCTS'][$k]['QUANTYTY_MANY'] = true;
        $arResult['PRODUCTS'][$k]['QUANTITY'] = 1;
    }
    $allSumm += floatval(str_replace(' ', '', $arItem['PROPERTY_PRICE_VALUE'])) * $arItem['QUANTITY'];
    $allSummOld += $arItem['PROPERTY_PRICEOLD_VALUE']?floatval(str_replace(' ', '', $arItem['PROPERTY_PRICEOLD_VALUE'])) * $arItem['QUANTITY']:floatval(str_replace(' ', '', $arItem['PROPERTY_PRICE_VALUE'])) * $arItem['QUANTITY'];
}
if($cnt_summ > 9) $arResult['QUANTYTY_MANY'] = true;
$arResult['SUMM'] = $allSumm;
$arResult['SUMM_OLD'] = $allSummOld != $allSumm ? $allSummOld : 0 ;

?>