$(function () {
    $('.js-inn-mask','#INN').inputmask("(9999999999)|(999999999999)",{greedy: false});

    /*$('.slick-order').slick({
        dots: false,
        draggable: false,
        infinite: false,
        speed: 100,
        slidesToShow: 1,
        adaptiveHeight: false,
        prevArrow: null,
        nextArrow: null,
    });*/

   /* $('.js-toggle-company_pay-fields').on('click', function (e) {
        e.preventDefault();
        $('.company_pay-fields-wrap,.company_pay-select-wrap').toggleClass('hidden');
        if (!$(this).hasClass('toggled')) {
            $(this).text('К выбору компании из списка').addClass('toggled');
            $('.company_pay-select-wrap select').prop('selectedIndex', 0).removeClass('required').data('required',0).removeAttr('required').parent().removeClass('error');
            $('.company_pay-fields-wrap input').addClass('required').attr('required',true).data('required',1);
            $('.js-company_info.pay').addClass('hidden');
        }
        else{
            $(this).text('Ввести ИНН/КПП').removeClass('toggled');
            $('.company_pay-fields-wrap input').val('').removeClass('required').data('required',0).removeAttr('required').parent().removeClass('error');
            $('.company_pay-select-wrap select').addClass('required').attr('required',true).data('required',1);
            $('.js-company_info.pay').addClass('hidden');
        }
    });

    $('.js-toggle-company_patron-fields').on('click', function (e) {
        e.preventDefault();
        var parent = $(this).parent().parent();
        parent.find('.company_patron-fields-wrap,.company_patron-select-wrap').toggleClass('hidden');
        if (!$(this).hasClass('toggled')) {
            $(this).text('К выбору компании из списка').addClass('toggled');
            parent.find('.company_patron-select-wrap select').prop('selectedIndex', 0).removeClass('required').data('required',0).removeAttr('required').parent().removeClass('error');
            parent.find('.company_patron-fields-wrap input').addClass('required').attr('required',true).data('required',1);
            parent.find('.js-company_info.patron').addClass('hidden');
        }
        else{
            $(this).text('Ввести ИНН/КПП').removeClass('toggled');
            parent.find('.company_patron-fields-wrap input').val('').removeClass('required').data('required',0).removeAttr('required').parent().removeClass('error');
            parent.find('.company_patron-select-wrap select').addClass('required').attr('required',true).data('required',1);
            parent.find('.js-company_info.patron').addClass('hidden');
        }
    });*/
    /*
    $('.next-step').on('click',function (e) {
        if(isValidForm($(this).closest('.slide'))){
            $('.slick-order').slick('slickNext');

            if($(this).hasClass('finish')){
                var formData = $(this).closest('form').serializeObject();
                console.log(formData);
                var dataHtmlWrap = '';
                var dataHtml = '';

                if(formData['OFFER'].length > 1){
                    $.each(formData['OFFER'],function (i,v) {
                        dataHtml = '<dl class="dl-horizontal">';
                        if(i > 50){
                            var prName = $('#product_'+i).text();
                            dataHtmlWrap += '<h5>'+prName+'</h5>';
                            $.each(formData,function (index,value) {
                                switch (index) {
                                    case 'COMPANY_PAY':
                                        if(value){
                                            dataHtml += '<dt>Компания плательщик:</dt>';
                                            var valObj = $('dl.pay.js-company_info');
                                            dataHtml += '<dd><b>Наименование:</b> ' + valObj.find('.name').text() + '<br>' +
                                                '<b>ИНН:</b> ' + valObj.find('.inn').text() + '<br>' +
                                                '<b>КПП:</b> ' + valObj.find('.kpp').text() + '</dd>';
                                        }

                                        break;
                                    case 'company_diff':
                                        if(formData['COMPANY_PAY']){
                                            dataHtml += '<dt>Компания получатель лицензии:</dt>';
                                            if(value[i] == 'Y'){
                                                dataHtml += '<dd>совпадает с плательщиком</dd>';
                                            }
                                            else {
                                                var valObj = $('dl.patron.js-company_info[data-offer='+v+']');
                                                dataHtml += '<dd><b>Наименование:</b> ' + valObj.find('.name').text() + '<br>' +
                                                    '<b>ИНН:</b> ' + valObj.find('.inn').text() + '<br>' +
                                                    '<b>КПП:</b> ' + valObj.find('.kpp').text() + '</dd>';;
                                            }
                                        }

                                        break;
                                    case 'REP_PERIOD':
                                        if(value[i] !== undefined){
                                            dataHtml += '<dt>Период:</dt>';
                                            dataHtml += '<dd>'+value[i]+'</dd>';
                                        }

                                        break;
                                }
                            });



                            dataHtmlWrap += dataHtml;
                        }

                    });
                    setTimeout(function () {
                        var dop_data = '';
                        if(formData['DOP_INN']){
                            dop_data += '<dt>Доп. ИНН</dt><dd>'+formData['DOP_INN']+'</dd>';
                        }
                        if(formData['DOP_KPP']){
                            dop_data += '<dt>Доп. КПП</dt><dd>'+formData['DOP_KPP']+'</dd>';
                        }
                        if(formData['DOP_CODE']){
                            dop_data += '<dt>Доп. КОД</dt><dd>'+formData['DOP_CODE']+'</dd>';
                        }

                        if(dop_data){
                            dop_data = '<h6><b>Дополнительные данные</b></h6>' + dop_data;
                        }
                        dataHtmlWrap += dop_data + '</dl>';
                        $('#order_data').html(dataHtmlWrap);
                    }, 300);

                }else{

                    var keys = Object.keys(formData['OFFER']);
                    var offer_id = formData['OFFER'][keys[0]];

                    var prName = $('#product_'+offer_id).text();
                    dataHtmlWrap += '<h5>'+prName+'</h5>';
                    $.each(formData,function (index,value) {
                        switch (index) {
                            case 'COMPANY_PAY':
                                if(value[i] !== undefined){

                                }
                                dataHtml += '<dt>Компания плательщик:</dt>';
                                var valObj = $('dl.pay.js-company_info');
                                dataHtml += '<dd><b>Наименование:</b> ' + valObj.find('.name').text() + '<br>' +
                                    '<b>ИНН:</b> ' + valObj.find('.inn').text() + '<br>' +
                                    '<b>КПП:</b> ' + valObj.find('.kpp').text() + '</dd>';
                                break;
                            case 'company_diff':
                                dataHtml += '<dt>Компания получатель лицензии:</dt>';
                                if(value[offer_id] == 'Y'){
                                    dataHtml += '<dd>совпадает с плательщиком</dd>';
                                }
                                else {
                                    var valObj = $('dl.patron.js-company_info[data-offer='+v+']');
                                    dataHtml += '<dd><b>Наименование:</b> ' + valObj.find('.name').text() + '<br>' +
                                        '<b>ИНН:</b> ' + valObj.find('.inn').text() + '<br>' +
                                        '<b>КПП:</b> ' + valObj.find('.kpp').text() + '</dd>';
                                }
                                break;
                            case 'REP_PERIOD':
                                dataHtml += '<dt>Период:</dt>';
                                dataHtml += '<dd>'+value[offer_id]+'</dd>';
                                break;
                        }
                    });

                    var dop_data = '';
                    if(formData['DOP_INN']){
                        dop_data += '<dt>Доп. ИНН</dt><dd>'+formData['DOP_INN']+'</dd>';
                    }
                    if(formData['DOP_KPP']){
                        dop_data += '<dt>Доп. КПП</dt><dd>'+formData['DOP_KPP']+'</dd>';
                    }
                    if(formData['DOP_CODE']){
                        dop_data += '<dt>Доп. КОД</dt><dd>'+formData['DOP_CODE']+'</dd>';
                    }

                    if(dop_data){
                        dop_data = '<h6><b>Дополнительные данные</b></h6>' + dop_data;
                    }


                    dataHtmlWrap += dataHtml + dop_data + '</dl>';

                    $('#order_data').html(dataHtmlWrap);
                }




            }
            scrollToBlock($('.form.order'), -100);
        }

    });
    $('.prev-step').on('click',function (e) {
        $('.slick-order').slick('slickPrev');
        scrollToBlock($('.form.order'), -100);
    });
    $('.js-company_select').on('change',function () {
        var option = $(this).find('option:selected');
        var company_info = $(this).closest('.row').find('.js-company_info');
        if($(this).val()){
            company_info.find('.name').text(option.text());
            company_info.find('.inn').text(option.attr('inn'));
            company_info.find('.kpp').text(option.attr('kpp'));
            company_info.removeClass('hidden');
        }
        else{
            company_info.addClass('hidden');
        }
    })
*/
    $('.js-company_diff').on('change',function (e) {
        e.stopPropagation();
        e.stopImmediatePropagation();
        if($(this).is(':checked')){
            $(this).closest('.row').find('.company_patron').addClass('hidden');
            //$(this).closest('.row').find('select').removeClass('required').data('required',0).removeAttr('required').parent().removeClass('error');
            $(this).closest('.row').find('input.js-required').removeClass('required').removeAttr('required').parent().removeClass('error');
            $(this).closest('.row').find('input').removeAttr('required').removeClass('required');
        }else{
            $(this).closest('.row').find('.company_patron').removeClass('hidden');
            $(this).closest('.row').find('input.js-required').addClass('required').attr('required',1);
        }
    });
	$('#js-address_eq').on('change',function (e) {
	    if(this.checked){
			//$('.fiz_address').hide();
			$('.fiz_address').addClass('hidden').find('input').removeAttr('required').removeClass('required');
        }else{
			//$('.fiz_address').show();
			$('.fiz_address').removeClass('hidden').find('input').attr('required',true).addClass('required');
        }
    })

    $('.js-company-input').on('change', function() {
        var parent = $(this).closest('.js-company-wrap');
        var _this = $(this);
        var kpp = '';
        kpp = parent.find('.js-kpp').val();

        if(_this.val()){
            if(kpp){
                var option = $('#'+_this.attr('list')).find('[value="'+_this.val()+'"][data-kpp='+kpp+']');
            } else {
                var option = $('#'+_this.attr('list')).find('[value="'+_this.val()+'"]');
            }

            if(option.length){
                parent.find('.js-id').val(option.data('id'));
                parent.find('.js-ext-id').val(option.data('ext_id'));
                parent.find('.js-kpp').val(option.data('kpp')).closest('.animated-labels').addClass('input-filed');
                parent.find('.js-name').val(option.attr('label')).closest('.animated-labels').addClass('input-filed');
                //parent.find('.js-address').val(option.data('address')).closest('.animated-labels').addClass('input-filed');
                parent.find('.js-address').addClass('hidden').find('input').removeAttr('required').removeClass('required');

                parent.find('.js-addr_is_eq').val(option.data('addr_is_eq'));
                parent.find('.js-fiz_adr').val(option.data('fiz_adr'));
                parent.find('.js-fiz_gorod').val(option.data('fiz_gorod'));
                parent.find('.js-fiz_index').val(option.data('fiz_index'));
                parent.find('.js-ruk_doc').val(option.data('ruk_doc'));
                parent.find('.js-ruk_dolg').val(option.data('ruk_dolg'));
                parent.find('.js-ruk_name').val(option.data('ruk_name'));
                parent.find('.js-cp_name').val(option.data('cp_name'));
                parent.find('.js-cp_phone').val(option.data('cp_phone'));
                parent.find('.js-tw_id_company').val(option.data('tw_id_company'));

            }else{

                $.ajax({
                    url: '/ajax/api.php',
                    dataType : 'json',
                    data: {"action": "company_info",'inn':_this.val(),'kpp':kpp},
                    beforeSend: function(){
                        parent.addClass('loadings');
                    },
                    success: function(data){
                        if(data.success && data.isset_company){
                            parent.find('.js-id').val(data.isset_company.id);
                            parent.find('.js-ext-id').val(data.isset_company.ext_id);
                            parent.find('.js-kpp').val(data.isset_company.KPP).closest('.animated-labels').addClass('input-filed');
                            parent.find('.js-name').val(data.isset_company.NAME).closest('.animated-labels').addClass('input-filed');
                           // parent.find('.js-address').val(data.isset_company.UR_GOROD + ', ' + data.isset_company.UR_ADR).closest('.animated-labels').addClass('input-filed');
                           // parent.find('.js-address').addClass('hidden').find('input').removeAttr('required').removeClass('required');
                            parent.find('.js-addr_is_eq').val(data.isset_company.ADDR_IS_EQ);
                            parent.find('.js-fiz_adr').val(data.isset_company.FIZ_ADR);
                            parent.find('.js-fiz_gorod').val(data.isset_company.FIZ_GOROD);
                            parent.find('.js-fiz_index').val(data.isset_company.FIZ_INDEX);
                            parent.find('.js-ruk_doc').val(data.isset_company.RUK_DOC);
                            parent.find('.js-ruk_dolg').val(data.isset_company.RUK_DOLG);
                            parent.find('.js-ruk_name').val(data.isset_company.RUK_NAME);
                            parent.find('.js-cp_name').val(data.isset_company.CP_NAME);
                            parent.find('.js-cp_phone').val(data.isset_company.CP_PHONE);
                            parent.find('.js-cp_email').val(data.isset_company.CP_EMAIL);
                            parent.find('.js-tw_id_company').val(data.isset_company.TW_ID_COMPANY);
							if(data.isset_company.need_address==false){
								parent.find('.js-address').addClass('hidden').find('input').removeAttr('required').removeClass('required');
							}else{
								parent.find('.js-address').removeClass('hidden').find('input').attr('required',true).addClass('required');
							}
                        }
                        else if(data.success && !data.isset_company){
                            parent.find('.js-id').val(0);
                            parent.find('.js-ext-id').val(data.data.id);
                            parent.find('.js-kpp').val(data.data.kpp).closest('.animated-labels').addClass('input-filed');
                            parent.find('.js-name').val(data.data.name).closest('.animated-labels').addClass('input-filed');
                         //   parent.find('.js-address').val(data.data.ur_gorod + ', ' + data.data.ur_adr).closest('.animated-labels').addClass('input-filed');
                           // parent.find('.js-address').addClass('hidden').find('input').removeAttr('required').removeClass('required');
                            parent.find('.js-addr_is_eq').val(data.data.addr_is_eq);
                            parent.find('.js-fiz_adr').val(data.data.fiz_adr);
                            parent.find('.js-fiz_gorod').val(data.data.fiz_gorod);
                            parent.find('.js-fiz_index').val(data.data.fiz_index);
                            parent.find('.js-ruk_doc').val(data.data.ruk_doc);
                            parent.find('.js-ruk_dolg').val(data.data.ruk_dolg);
                            parent.find('.js-ruk_name').val(data.data.ruk_name);
                            parent.find('.js-cp_name').val(data.data.cp_name);
                            parent.find('.js-cp_phone').val(data.data.cp_phone);
                            parent.find('.js-cp_email').val(data.data.cp_email);
                            parent.find('.js-tw_id_company').val(data.data.tw_id_company);
                            if(data.data.need_address==false){
								parent.find('.js-address').addClass('hidden').find('input').removeAttr('required').removeClass('required');
                            }else{
								parent.find('.js-address').removeClass('hidden').find('input').attr('required',true).addClass('required');
                            }
                        }
                        else{
                            parent.find('.js-id').val(0);
                            parent.find('.js-ext-id').val(0);
                          //  parent.find('.js-kpp').val('').closest('.animated-labels').removeClass('input-filed');
                           // parent.find('.js-name').val('').closest('.animated-labels').removeClass('input-filed');
                            // parent.find('.js-address').val('').closest('.animated-labels').removeClass('input-filed');
                            parent.find('.js-address').removeClass('hidden').find('input').attr('required',true).addClass('required');
                            parent.find('.js-addr_is_eq').val('');
                            parent.find('.js-fiz_adr').val('');
                            parent.find('.js-fiz_gorod').val('');
                            parent.find('.js-fiz_index').val('');
                            parent.find('.js-ruk_doc').val('');
                            parent.find('.js-ruk_dolg').val('');
                            parent.find('.js-ruk_name').val('');
                            parent.find('.js-cp_name').val('');
                            parent.find('.js-cp_phone').val('');
                            parent.find('.js-cp_email').val('');
                            parent.find('.js-tw_id_company').val('');
                        }
                    },
                    complete: function () {
                        parent.removeClass('loadings');
                    }
                });
            }

        } else{
            parent.find('.js-id').val(0);
            parent.find('.js-ext-id').val(0);
           // parent.find('.js-kpp').val('').closest('.animated-labels').removeClass('input-filed');
           // parent.find('.js-name').val('').closest('.animated-labels').removeClass('input-filed');
           // parent.find('.js-address').val('').closest('.animated-labels').removeClass('input-filed');
            parent.find('.js-address').removeClass('hidden').find('input').attr('required',true).addClass('required');
            parent.find('.js-addr_is_eq').val('');
            parent.find('.js-fiz_adr').val('');
            parent.find('.js-fiz_gorod').val('');
            parent.find('.js-fiz_index').val('');
            parent.find('.js-ruk_doc').val('');
            parent.find('.js-ruk_dolg').val('');
            parent.find('.js-ruk_name').val('');
            parent.find('.js-cp_name').val('');
            parent.find('.js-cp_phone').val('');
            parent.find('.js-cp_email').val('');
            parent.find('.js-tw_id_company').val('');
        }
		var inn = parent.find('.js-inn-mask').val();
		var kpp=parent.find('.js-kpp')
        if(inn.length==12){
			kpp.val('').attr('required',false).removeClass('required').closest('.form-group').hide();
        }else{
			kpp.attr('required',true).addClass('required').closest('.form-group').show();
        }

    })

    $('.js-kpp').on('change', function () {
        var parent = $(this).closest('.js-company-wrap');
        parent.find('.js-company-input').trigger('change');
    })
});