<?if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true ) die();?>
<?\Bitrix\Main\Loader::includeModule("rdn");?>
<?//dump($arResult['PRODUCTS']);?>
<?if(!empty($arResult['FORM_ERRORS'])){
    foreach ($arResult['FORM_ERRORS'] as $error){
        ?><p class="alert alert-warning"><?=$error?></p><?
    }
}

?>
<div class="basket_order">
    <div class="row">
        <div class="form_wrap col-md-8 col-xs-12">
            <div class="form order slider border<?=($arResult['isFormNote'] == 'Y' ? ' success' : '')?><?=($arResult['isFormErrors'] == 'Y' ? ' error' : '')?>">
                <?=$arResult["FORM_HEADER"]?>
                <input type="hidden" name="form_order_submit" value="Y">
                <input type="hidden" name="store_id" value="Y">
                <input type="hidden" name="SUMM" value="<?=$arResult['SUMM']?>">
                    <div class="slick-order">
                        <h4>1. Укажите даные для оплаты</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="company_pay-wrap <?=($arParams['IS_AUTH'])?'auth_user':''?> js-company-wrap" >
                                    <input type="hidden" name="COMPANY_PAY" value="<?=!empty($_REQUEST['COMPANY_PAY'])?$_REQUEST['COMPANY_PAY']:''?>" class="js-id">
                                    <input type="hidden" name="COMPANY_PAY_EXT_ID" value="<?=!empty($_REQUEST['COMPANY_PAY_EXT_ID'])?$_REQUEST['COMPANY_PAY_EXT_ID']:''?>" class="js-ext-id">

                                    <input type="hidden" name="addr_is_eq" value="<?=!empty($_REQUEST['addr_is_eq'])?$_REQUEST['addr_is_eq']:''?>" class="js-addr_is_eq">
                                    <input type="hidden" name="fiz_adr" value="<?=!empty($_REQUEST['fiz_adr'])?$_REQUEST['fiz_adr']:''?>" class="js-fiz_adr">
                                    <input type="hidden" name="fiz_gorod" value="<?=!empty($_REQUEST['fiz_gorod'])?$_REQUEST['fiz_gorod']:''?>" class="js-fiz_gorod">
                                    <input type="hidden" name="fiz_index" value="<?=!empty($_REQUEST['fiz_index'])?$_REQUEST['fiz_index']:''?>" class="js-fiz_index">
                                    <input type="hidden" name="ruk_doc" value="<?=!empty($_REQUEST['ruk_doc'])?$_REQUEST['ruk_doc']:''?>" class="js-ruk_doc">
                                    <input type="hidden" name="ruk_dolg" value="<?=!empty($_REQUEST['ruk_dolg'])?$_REQUEST['ruk_dolg']:''?>" class="js-ruk_dolg">
                                    <input type="hidden" name="ruk_name" value="<?=!empty($_REQUEST['ruk_name'])?$_REQUEST['ruk_name']:''?>" class="js-ruk_name">
                                    <input type="hidden" name="cp_name" value="<?=!empty($_REQUEST['cp_name']?$_REQUEST['cp_name']:'')?>" class="js-cp_name">
                                    <input type="hidden" name="cp_phone" value="<?=!empty($_REQUEST['cp_phone'])?$_REQUEST['cp_phone']:''?>" class="js-cp_phone">
                                    <input type="hidden" name="cp_email" value="<?=!empty($_REQUEST['cp_email'])?$_REQUEST['cp_email']:''?>" class="js-cp_email">
                                    <input type="hidden" name="tw_id_company" value="<?=!empty($_REQUEST['tw_id_company'])?$_REQUEST['tw_id_company']:''?>" class="js-tw_id_company">
                                    <h6>Введите данные компании</h6>
                                    <div class="form-group animated-labels">
                                        <label>ИНН <span class="required-star">*</span></label>
                                        <div class="input">
                                            <input type="text" list="comp-pay-list" class="form-control required js-company-input js-inn-mask" required name="COMPANY_PAY_INN" value="<?=!empty($_REQUEST['COMPANY_PAY_INN'])?$_REQUEST['COMPANY_PAY_INN']:''?>">
                                            <datalist id="comp-pay-list">
                                                <?foreach ($arResult['COMPANIES'] as $company):
												$validComp=\rdn\Helper::CheckCompanyAttr($company);
												if(!$validComp['all_ok'])continue;
												?>
                                                <option value="<?=$company['INN']?>" label="<?=$company['NAME']?>"
                                                        data-id="<?=$company['ID']?>"
                                                        data-ext_id="<?=$company['TW_ID_COMPANY']?>"
                                                        data-kpp="<?=$company['KPP']?>"
                                                        data-addr_is_eq="<?=$company['ADDR_IS_EQ']?>"
                                                        data-fiz_adr="<?=$company['FIZ_ADR']?>"
                                                        data-fiz_gorod="<?=$company['FIZ_GOROD']?>"
                                                        data-fiz_index="<?=$company['FIZ_INDEX']?>"
                                                        data-ruk_doc="<?=$company['RUK_DOC']?>"
                                                        data-ruk_dolg="<?=$company['RUK_DOLG']?>"
                                                        data-ruk_name="<?=$company['RUK_NAME']?>"
                                                        data-cp_name="<?=$company['CP_NAME']?>"
                                                        data-cp_phone="<?=$company['CP_PHONE']?>"
                                                        data-cp_email="<?=$company['CP_EMAIL']?>"
                                                        data-tw_id_company="<?=$company['TW_ID_COMPANY']?>"
                                                >
                                                <?endforeach?>
                                            </datalist>
                                        </div>
                                        <div class="hint"></div>
                                    </div>
                                    <div class="form-group animated-labels" <?=strlen($_REQUEST['COMPANY_PAY_INN'])==12?'style="display:none"':''?>>
                                        <label>КПП <span class="required-star">*</span></label>
                                        <div class="input">
                                            <input type="text" class="form-control <?=strlen($_REQUEST['COMPANY_PAY_INN'])==12?'':'required'?> js-kpp js-kpp-mask" value="<?=!empty($_REQUEST['COMPANY_PAY_KPP'])?$_REQUEST['COMPANY_PAY_KPP']:''?>" <?=strlen($_REQUEST['COMPANY_PAY_INN'])==12?'':'required'?> name="COMPANY_PAY_KPP">
                                        </div>
                                        <div class="hint"></div>
                                    </div>
                                    <div class="form-group animated-labels">
                                        <label>Наименование компании*<span class="required-star">*</span></label>
                                        <div class="input">
                                            <input type="text" class="form-control required js-name" value="<?=!empty($_REQUEST['COMPANY_PAY_NAME'])?$_REQUEST['COMPANY_PAY_NAME']:''?>" required name="COMPANY_PAY_NAME">
                                        </div>
                                        <div class="hint"></div>
                                    </div>
                                    <div class="row js-address">
                                        <div class="col-xs-12"><label>Юридичекий адрес</label></div>
                                        <div class="col-xs-2">
                                            <div class="form-group animated-labels">
                                                <label>Индекс <span class="required-star">*</span></label>
                                                <div class="input">
                                                    <input type="text" class="form-control" value="<?=htmlspecialchars(!empty($_REQUEST['COMPANY_PAY_ADDRESS_IND'])?$_REQUEST['COMPANY_PAY_ADDRESS_IND']:'')?>" name="COMPANY_PAY_ADDRESS_IND">
                                                </div>
                                                <div class="hint"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group animated-labels">
                                                <label>Город <span class="required-star">*</span></label>
                                                <div class="input">
                                                    <input type="text" class="form-control" value="<?=htmlspecialchars(!empty($_REQUEST['COMPANY_PAY_ADDRESS_CITY'])?$_REQUEST['COMPANY_PAY_ADDRESS_CITY']:'')?>" name="COMPANY_PAY_ADDRESS_CITY">
                                                </div>
                                                <div class="hint"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-7">
                                            <div class="form-group animated-labels">
                                                <label>Адрес <span class="required-star">*</span></label>
                                                <div class="input">
                                                    <input type="text" class="form-control _addr" value="<?=!empty($_REQUEST['COMPANY_PAY_ADDRESS'])?$_REQUEST['COMPANY_PAY_ADDRESS']:''?>" name="COMPANY_PAY_ADDRESS">
                                                </div>
                                                <div class="hint"></div>
                                            </div>
                                        </div>
                                    </div>
									<div class="row js-address">
										<div class="col-xs-12"><label>Фактический адрес</label></div>
										<div class="col-xs-12">
											<div class="licence_block bx_filter">
												<input type="checkbox" class="js-onoff js-address_eq ignore"  id="js-address_eq" name="COMPANY_PAY_ADDR_IS_EQ" value="Y" <?=!empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ'])?'checked':''?> >
												<label for="js-address_eq">
													Фактический адрес совпадает с юридическим
												</label>
											</div>
										</div>
										<div class="fiz_address <?=!empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ'])?'hidden':''?>">
											<div class="col-xs-2">
												<div class="form-group animated-labels">
													<label>Индекс (факт) <span class="required-star">*</span></label>
													<div class="input">
														<input type="text" class="form-control<?=!empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ'])?'':' required'?>" <?=!empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ'])?'':'required="required" '?>value="<?=htmlspecialchars(!empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS_IND'])?$_REQUEST['COMPANY_PAY_FIZ_ADDRESS_IND']:'')?>" name="COMPANY_PAY_FIZ_ADDRESS_IND">
													</div>
													<div class="hint"></div>
												</div>
											</div>
											<div class="col-xs-3">
												<div class="form-group animated-labels">
													<label>Город (факт) <span class="required-star">*</span></label>
													<div class="input">
														<input type="text" class="form-control" <?=!empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ'])?'':' required'?>" <?=!empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ'])?'':'required="required" '?>value="<?=htmlspecialchars(!empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS_CITY'])?$_REQUEST['COMPANY_PAY_FIZ_ADDRESS_CITY']:'')?>" name="COMPANY_PAY_FIZ_ADDRESS_CITY">
													</div>
													<div class="hint"></div>
												</div>
											</div>
											<div class="col-xs-7">
												<div class="form-group animated-labels">
													<label>Адрес (факт) (улица, дом , строение офис)<span class="required-star">*</span></label>
													<div class="input">
														<input type="text" class="form-control _addr" <?=!empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ'])?'':' required'?>" <?=!empty($_REQUEST['COMPANY_PAY_ADDR_IS_EQ'])?'':'required="required" '?>value="<?=!empty($_REQUEST['COMPANY_PAY_FIZ_ADDRESS'])?$_REQUEST['COMPANY_PAY_FIZ_ADDRESS']:''?>" name="COMPANY_PAY_FIZ_ADDRESS">
													</div>
													<div class="hint"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="row js-address">
										<div class="col-xs-12"><label>Уполномоченное лицо</label></div>
										<div class="col-xs-12">
											<div class="form-group animated-labels">
												<label>Фамилия Имя Отчество <span class="required-star">*</span></label>
												<div class="input">
													<input type="text" class="form-control" value="<?=htmlspecialchars(!empty($_REQUEST['RUK_NAME'])?$_REQUEST['RUK_NAME']:'')?>" name="RUK_NAME">
												</div>
												<div class="hint">В родительном падеже, например: «Сидорова Петра Анатольевича»</div>
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group animated-labels">
												<label>Должность <span class="required-star">*</span></label>
												<div class="input">
													<input type="text" class="form-control" value="<?=htmlspecialchars(!empty($_REQUEST['RUK_DOLG'])?$_REQUEST['RUK_DOLG']:'')?>" name="RUK_DOLG">
												</div>
												<div class="hint">В родительном падеже, например: «генерального директора»</div>
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group animated-labels">
												<label>Основание действия <span class="required-star">*</span></label>
												<div class="input">
													<input type="text" class="form-control" value="<?=htmlspecialchars(!empty($_REQUEST['RUK_DOC'])?$_REQUEST['RUK_DOC']:'')?>" name="RUK_DOC">
												</div>
												<div class="hint">Документ, на основании которого действует уполномоченное лицо, в родительном падеже.<br>
													Например: «Устава», «Доверенности № 01 от 01.01.2003» и т.п.</div>
											</div>
										</div>
									</div>
                                    <div class="hint"></div>
                                </div>

                            </div>
                        </div>
                        <h4>2. Укажите даные для лицензирования</h4>
                        <?$cnt_summ = 0;?>
                        <?foreach ($arResult['PRODUCTS'] as $product): $p++;?>
                            <input type="hidden" name="OFFER[<?=$product['ID']?>]" value="<?=$product['ID']?>">
                            <input type="hidden" name="PRODUCT[<?=$product['ID']?>]" value="<?=$product['SECTION_ID']?>">
                            <input type="hidden" name="art[<?=$product['ID']?>]" value="<?=$product['PROPERTY_ARTICLE_VALUE']?>">
                            <div class="product-item-wrap pt20">
                                <div class="product-item">
                                    <div class="image"><a href="<?=$product['DETAIL_PAGE_URL']?>"><img class="img-responsive" src="<?=$product['PICTURE'];?>" alt="<?=$product['NAME'];?>" title="<?=$product['NAME'];?>" /></a></div>

                                    <div class="title">
                                        <a class="dark-color" href="<?=$product['DETAIL_PAGE_URL']?>"><?=$p?>. <?=$product['NAME'];?></a>
                                        <div class="meta">Артикул: <?=$product['PROPERTY_ARTICLE_VALUE'];?></div>
                                    </div>
                                    <?if(strlen($product['PROPERTY_PRICE_VALUE']) || strlen($product['PROPERTY_PRICEOLD_VALUE'])):?>
                                        <div class="prices">
                                            <?if(strlen($product['PROPERTY_PRICEOLD_VALUE'])):?>
                                                <div class="price_old"><div><?=$product['PROPERTY_PRICEOLD_VALUE'];?></div></div>
                                            <?endif;?>
                                            <?if(strlen($product['PROPERTY_PRICE_VALUE'])):?>
                                                <div class="price_new"><div><?=$product['QUANTITY']?> x <?=$product['PROPERTY_PRICE_VALUE']?></div></div>
                                            <?endif;?>

                                        </div>
                                    <?endif;?>

                                </div>
                                <?if(!empty($product['QUANTYTY_MANY'])){
                                    ?><p class='alert alert-warning'>У вас превышен лимит количества лицензий (больше 10). Обратитесь к менеджеру.</p><?
                               }?>
                                <?if($product['QUANTITY'] > 1):?>
                                    <?for($i=1; $i < $product['QUANTITY'] + 1; $i++): $cnt_summ++;?>
                                        <fieldset>
                                            <legend>Копия <?=$i?></legend>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="licence_block bx_filter">
                                                            <input type="checkbox" class="js-onoff js-company_diff"  id="company_diff<?=$product['ID']?>_<?=$i?>" name="company_diff[<?=$product['ID']?>][<?=$i?>]" value="Y" <?=(!empty($_REQUEST['company_diff'][$i][$product['ID']]) || empty($_REQUEST["form_order_submit"]) && $i == 1)?'checked':''?> >
                                                            <label for="company_diff<?=$product['ID']?>_<?=$i?>">
                                                                Лицензиат совпадает с плательщиком
                                                            </label>
                                                        </div>
                                                        <div class="<?=(!empty($_REQUEST['company_diff'][$i][$product['ID']]) || empty($_REQUEST["form_order_submit"]) && $i == 1)?'hidden':''?> company_patron">
                                                            <div class="company_patron-wrap <?=($arParams['IS_AUTH'])?'auth_user':''?> js-company-wrap">
                                                                <h6>Введите данные получателя лицензии</h6>
                                                                <input type="hidden" name="COMPANY_EXT_ID[<?=$product['ID']?>][<?=$i?>]" value="<?=!empty($_REQUEST['COMPANY_EXT_ID'][$product['ID']][$i])?$_REQUEST['COMPANY_EXT_ID'][$product['ID']][$i]:''?>" class="js-ext-id">
                                                                <input type="hidden" name="COMPANY[<?=$product['ID']?>][<?=$i?>]" value="<?=!empty($_REQUEST['COMPANY'][$product['ID']][$i])?$_REQUEST['COMPANY'][$product['ID']][$i]:''?>" class="js-id">
                                                                <div class="form-group animated-labels">
                                                                    <label>ИНН <span class="required-star">*</span></label>
                                                                    <div class="input">
                                                                        <input type="text" list="comp-list<?=$product['ID']?>" class="form-control js-required js-company-input js-inn js-inn-mask" name="COMPANY_INN[<?=$product['ID']?>][<?=$i?>]" value="<?=!empty($_REQUEST['COMPANY_INN'][$product['ID']][$i])?$_REQUEST['COMPANY_INN'][$product['ID']][$i]:''?>">
                                                                        <datalist id="comp-list<?=$product['ID']?>">
                                                                            <?foreach ($arResult['COMPANIES'] as $company):?>
                                                                            <option value="<?=$company['INN']?>" label="<?=$company['NAME']?>"  data-id="<?=$company['ID']?>"  data-ext_id="<?=$company['TW_ID_COMPANY']?>"  data-kpp="<?=$company['KPP']?>" data-address="<?=$company['UR_ADR']?>">
                                                                                <?endforeach?>
                                                                        </datalist>
                                                                    </div>
                                                                    <div class="hint"></div>
                                                                </div>
                                                                <div class="form-group animated-labels" <?=strlen($_REQUEST['COMPANY_INN'][$product['ID']][$i])==12?'style="display:none"':''?>>
                                                                    <label>КПП <span class="required-star">*</span></label>
                                                                    <div class="input">
                                                                        <input type="text" class="form-control <?=strlen($_REQUEST['COMPANY_INN'][$product['ID']][$i])==12?'':'js-required'?> js-kpp js-kpp-mask" name="COMPANY_KPP[<?=$product['ID']?>][<?=$i?>]" value="<?=!empty($_REQUEST['COMPANY_KPP'][$product['ID']][$i])?$_REQUEST['COMPANY_KPP'][$product['ID']][$i]:''?>">
                                                                    </div>
                                                                    <div class="hint"></div>
                                                                </div>
                                                                <div class="form-group animated-labels">
                                                                    <label>Наименование компании <span class="required-star">*</span></label>
                                                                    <div class="input">
                                                                        <input type="text" class="form-control js-required js-name" name="COMPANY_NAME[<?=$product['ID']?>][<?=$i?>]" value="<?=!empty($_REQUEST['COMPANY_NAME'][$product['ID']][$i])?$_REQUEST['COMPANY_NAME'][$product['ID']][$i]:''?>">
                                                                    </div>
                                                                    <div class="hint"></div>
                                                                </div>

                                                                <?php /*<div class="row js-address hidden">
                                                                    <div class="col-xs-12"><label>Юридичекий адрес</label></div>
                                                                    <div class="col-xs-2">
                                                                        <div class="form-group animated-labels">
                                                                            <label>Индекс <span class="required-star">*</span></label>
                                                                            <div class="input">
                                                                                <input type="text" class="form-control" value="<?=!empty($_REQUEST['COMPANY_ADDRESS_IND'][$product['ID']][$i])?$_REQUEST['COMPANY_ADDRESS_IND'][$product['ID']][$i]:''?>" name="COMPANY_ADDRESS_IND[<?=$product['ID']?>][<?=$i?>]">
                                                                            </div>
                                                                            <div class="hint"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-3">
                                                                        <div class="form-group animated-labels">
                                                                            <label>Город <span class="required-star">*</span></label>
                                                                            <div class="input">
                                                                                <input type="text" class="form-control" value="<?=!empty($_REQUEST['COMPANY_ADDRESS_CITY'][$product['ID']][$i])?$_REQUEST['COMPANY_ADDRESS_CITY'][$product['ID']][$i]:''?>" name="COMPANY_ADDRESS_CITY[<?=$product['ID']?>][<?=$i?>]">
                                                                            </div>
                                                                            <div class="hint"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-7">
                                                                        <div class="form-group animated-labels">
                                                                            <label>Адрес <span class="required-star">*</span></label>
                                                                            <div class="input">
                                                                                <input type="text" class="form-control _addr" value="<?=!empty($_REQUEST['COMPANY_ADDRESS'][$product['ID']][$i])?$_REQUEST['COMPANY_ADDRESS'][$product['ID']][$i]:''?>" name="COMPANY_ADDRESS[<?=$product['ID']?>][<?=$i?>]">
                                                                            </div>
                                                                            <div class="hint"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>*/?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?if($product['IS_CODE'] == 'Y'):?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group animated-labels">
                                                            <label>Код компьютера</label>
                                                            <div class="input">
                                                                <input type="text" class="form-control"
                                                                       name="DOP_CODE[<?= $product['ID'] ?>][<?= $i ?>]" value="<?=!empty($_REQUEST['DOP_CODE'][$product['ID']][$i])?$_REQUEST['DOP_CODE'][$product['ID']][$i]:''?>">
                                                            </div>
                                                            <div class="hint">Код рабочего места</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif?>
                                        </fieldset>
                                    <?endfor;?>
                                <?else: $cnt_summ++;?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="licence_block bx_filter">
                                                <input type="checkbox" class="js-onoff js-company_diff"  id="company_diff<?=$product['ID']?>" name="company_diff[<?=$product['ID']?>]" value="Y"  <?=((!empty($_REQUEST['company_diff'][$product['ID']]) && $_REQUEST['company_diff'][$product['ID']] == 'Y') || empty($_REQUEST["form_order_submit"]))?'checked':''?> >
                                                <label for="company_diff<?=$product['ID']?>">
                                                    Лицензиат совпадает с плательщиком
                                                </label>
                                            </div>
                                            <div class="<?=((!empty($_REQUEST['company_diff'][$product['ID']]) && $_REQUEST['company_diff'][$product['ID']] == 'Y') || empty($_REQUEST["form_order_submit"]))?'hidden':''?> company_patron">
                                                <div class="company_patron-wrap <?=($arParams['IS_AUTH'])?'auth_user':''?> js-company-wrap">
                                                    <h6>Введите данные получателя лицензии</h6>
                                                    <input type="hidden" name="COMPANY_EXT_ID[<?=$product['ID']?>]" value="<?=!empty($_REQUEST['COMPANY_EXT_ID'][$product['ID']]?$_REQUEST['COMPANY_EXT_ID'][$product['ID']]:'')?>" class="js-ext-id">
                                                    <input type="hidden" name="COMPANY[<?=$product['ID']?>]" value="<?=!empty($_REQUEST['COMPANY'][$product['ID']]?$_REQUEST['COMPANY'][$product['ID']]:'')?>" class="js-id">
                                                    <div class="form-group animated-labels">
                                                        <label>ИНН <span class="required-star">*</span></label>
                                                        <div class="input">
                                                            <input type="text" list="comp-list<?=$product['ID']?>" class="form-control js-required js-company-input js-inn-mask" name="COMPANY_INN[<?=$product['ID']?>]" value="<?=!empty($_REQUEST['COMPANY_INN'][$product['ID']])?$_REQUEST['COMPANY_INN'][$product['ID']]:''?>">
                                                            <datalist id="comp-list<?=$product['ID']?>">
                                                                <?foreach ($arResult['COMPANIES'] as $company):?>
                                                                <option value="<?=$company['INN']?>" label="<?=$company['NAME']?>"  data-id="<?=$company['ID']?>"  data-ext_id="<?=$company['TW_ID_COMPANY']?>"  data-kpp="<?=$company['KPP']?>" data-address="<?=$company['UR_ADR']?>">
                                                                    <?endforeach?>
                                                            </datalist>
                                                        </div>
                                                        <div class="hint"></div>
                                                    </div>
                                                    <div class="form-group animated-labels" <?=strlen($_REQUEST['COMPANY_INN'][$product['ID']])==12?'style="display:none"':''?>>
                                                        <label>КПП <span class="required-star">*</span></label>
                                                        <div class="input">
                                                            <input type="text" class="form-control <?=strlen($_REQUEST['COMPANY_INN'][$product['ID']])==12?'':'js-required'?> js-kpp js-kpp-mask" name="COMPANY_KPP[<?=$product['ID']?>]" value="<?=!empty($_REQUEST['COMPANY_KPP'][$product['ID']])?$_REQUEST['COMPANY_KPP'][$product['ID']]:''?>">
                                                        </div>
                                                        <div class="hint"></div>
                                                    </div>
                                                    <div class="form-group animated-labels">
                                                        <label>Наименование компании*<span class="required-star">*</span></label>
                                                        <div class="input">
                                                            <input type="text" class="form-control js-required js-name" name="COMPANY_NAME[<?=$product['ID']?>]" value="<?=!empty($_REQUEST['COMPANY_NAME'][$product['ID']])?$_REQUEST['COMPANY_NAME'][$product['ID']]:''?>">
                                                        </div>
                                                        <div class="hint"></div>
                                                    </div>
                                                    <?php /*<div class="row js-address hidden">
                                                        <div class="col-xs-12"><label>Юридичекий адрес</label></div>
                                                            <div class="col-xs-2">
                                                                <div class="form-group animated-labels">
                                                                    <label>Индекс <span class="required-star">*</span></label>
                                                                    <div class="input">
                                                                        <input type="text" class="form-control" value="<?=!empty($_REQUEST['COMPANY_ADDRESS_IND'][$product['ID']])?$_REQUEST['COMPANY_ADDRESS_IND'][$product['ID']]:''?>" name="COMPANY_ADDRESS_IND[<?=$product['ID']?>]>
                                                                    </div>
                                                                    <div class="hint"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <div class="form-group animated-labels">
                                                                    <label>Город <span class="required-star">*</span></label>
                                                                    <div class="input">
                                                                        <input type="text" class="form-control" value="<?=!empty($_REQUEST['COMPANY_ADDRESS_CITY'][$product['ID']])?$_REQUEST['COMPANY_ADDRESS_CITY'][$product['ID']]:''?>" name="COMPANY_ADDRESS_CITY[<?=$product['ID']?>]">
                                                                    </div>
                                                                    <div class="hint"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-7">
                                                                <div class="form-group animated-labels">
                                                                    <label>Адрес <span class="required-star">*</span></label>
                                                                    <div class="input">
                                                                        <input type="text" class="form-control _addr" value="<?=!empty($_REQUEST['COMPANY_ADDRESS'][$product['ID']])?$_REQUEST['COMPANY_ADDRESS'][$product['ID']]:''?>" name="COMPANY_ADDRESS[<?=$product['ID']?>]">
                                                                    </div>
                                                                    <div class="hint"></div>
                                                                </div>
                                                            </div>
                                                        </div>*/?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?if($product['IS_CODE'] == 'Y'):?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group animated-labels">
                                                    <label>Код компьютера</label>
                                                    <div class="input">
                                                        <input type="text" class="form-control"
                                                               name="DOP_CODE[<?= $product['ID'] ?>]" value="<?=!empty($_REQUEST['DOP_CODE'][$product['ID']])?$_REQUEST['DOP_CODE'][$product['ID']]:''?>">
                                                    </div>
                                                    <div class="hint">Код рабочего места</div>
                                                </div>
                                            </div>
                                        </div>
                                    <?endif?>
                                <?endif?>
								<? #l($product);?>
                                <?#if(!empty($product['CRIT']['Срок лицензирования'])):?>
                                <?

                                    $rimArr = [1 => 'I',2 => 'II',3 => 'III',4 => 'IV',];
                                    $current_q = intval((date('n')+2)/3);
                                    $next_q = $current_q < 4 ? $current_q + 1 : 1;
                                    $length_month = 4;//intval($product['CRIT']['Срок лицензирования']);

                                    $dates_cur_q = \rdn\Helper::get_start_and_end_date($current_q,$length_month,'d.m.Y');
                                    $dates_next_q = \rdn\Helper::get_start_and_end_date($next_q,$length_month,'d.m.Y');

                                    $cut_q_text = '<span data-toggle="tooltip" title="'.$dates_cur_q['start'].'-'.$dates_cur_q['end'].'">'.$rimArr[$current_q] . ' квартал ' . date('Y') . ' года</span>';
                                    $cut_q_text4 = '<span data-toggle="tooltip" title="'.$dates_cur_q['start'].'-'.$dates_cur_q['end'].'">С ' . $rimArr[$current_q] . ' квартала ' . date('Y') . ' года</span>';
                                    $next_q_text = $current_q < 4 ? '<span data-toggle="tooltip" title="'.$dates_next_q['start'].'-'.$dates_next_q['end'].'">'.$rimArr[$next_q] . ' квартал ' . date('Y') . ' года</span>' : '<span data-toggle="tooltip" title="'.$dates_next_q['start'].'-'.$dates_next_q['end'].'">' . $rimArr[$next_q] . ' квартал ' . date('Y', strtotime('+1 year')) . ' года</span>';
                                    $next_q_text4 = $current_q < 4 ? '<span data-toggle="tooltip" title="'.$dates_next_q['start'].'-'.$dates_next_q['end'].'">'.'С ' . $rimArr[$next_q] . ' квартала ' . date('Y') . ' года</span>' : '<span data-toggle="tooltip" title="'.$dates_next_q['start'].'-'.$dates_next_q['end'].'">'.'С ' . $rimArr[$next_q] . ' квартала ' . date('Y', strtotime('+1 year')) . ' года</span>';




                                ?>
                                    <div class="row">
                                        <div class="col-md-12 bx_filter">
                                            <div class="form-group">
                                                <h5>Выбор отчётного квартала <span class="required-star">*</span></h5>
                                                <input type="hidden" name="PERIOD[<?=$product['ID']?>]" value="<?=empty($product['CRIT']['Срок лицензирования'])?4:$product['CRIT']['Срок лицензирования']?>">
                                                <div class=" filter radio">
                                                    <input type="radio" id="period1<?=$product['ID']?>" name="REP_PERIOD[<?=$product['ID']?>]" class="required" data-required="1" value="current" <?=(!empty($_REQUEST['REP_PERIOD'][$product['ID']]) && $_REQUEST['REP_PERIOD'][$product['ID']] == 'current')?'checked':''?> />
                                                    <label for="period1<?=$product['ID']?>">
                                                        <?=$product['CRIT']['Срок лицензирования'] == '1 квартал'?$cut_q_text:$cut_q_text4?>
                                                    </label>
                                                    &nbsp;&nbsp;
                                                    <input type="radio" name="REP_PERIOD[<?=$product['ID']?>]" id="period2<?=$product['ID']?>" class="required" data-required="1" value="next" <?=(!empty($_REQUEST['REP_PERIOD'][$product['ID']]) && $_REQUEST['REP_PERIOD'][$product['ID']] == 'next')?'checked':''?>/>
                                                    <label for="period2<?=$product['ID']?>"><?=$product['CRIT']['Срок лицензирования'] == '1 квартал'?$next_q_text:$next_q_text4?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? #endif?>
                            </div>
                        <?endforeach?>

                        <div class="row">
                            <div class="col-md-12">
                                <h4>3. Дополнительные данные</h4>
                                <div class="form-group animated-labels">
                                    <label>Контатное лицо <span class="required-star">*</span></label>
                                    <div class="input">
                                        <input type="text" class="form-control required" name="FIO" data-required="1" value="<?=!empty($_REQUEST['FIO'])?$_REQUEST['FIO']:(!empty($arResult['USER'])?trim($arResult['USER']['LAST_NAME'] . ' ' . $arResult['USER']['NAME']):'')?>">
                                    </div>
                                    <div class="hint"></div>
                                </div>
                                <div class="form-group animated-labels">
                                    <label>E-Mail <span class="required-star">*</span></label>
                                    <div class="input">
                                        <input type="text" class="form-control required js-email-mask" name="EMAIL" data-required="1" value="<?=!empty($_REQUEST['EMAIL'])?$_REQUEST['EMAIL']:(!empty($arResult['USER'])?$arResult['USER']['EMAIL']:'')?>">
                                    </div>
                                    <div class="hint">Для сохранения истории заказов в личном кабинете указывайте тот, что используете для авторизации</div>
                                </div>
                                <div class="form-group animated-labels">
                                    <label>Телефон</label>
                                    <div class="input">
                                        <input type="text" class="form-control" name="PHONE" value="<?=!empty($_REQUEST['PHONE'])?$_REQUEST['PHONE']:(!empty($arResult['USER'])?$arResult['USER']['PERSONAL_PHONE']:'')?>">
                                    </div>
                                    <div class="hint"></div>
                                </div>
                                <?/*?>
                                <div class="form-group animated-labels">
                                    <label>ИНН</label>
                                    <div class="input">
                                        <input type="text" class="form-control required" name="DOP_INN" data-required="1">
                                    </div>
                                    <div class="hint"></div>
                                </div>
                                <div class="form-group animated-labels">
                                    <label>КПП</label>
                                    <div class="input">
                                        <input type="text" class="form-control required" name="DOP_KPP" data-required="1">
                                    </div>
                                    <div class="hint"></div>
                                </div>
<?*/?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="bottom_block col-md-12 col-sm-12">
                                <div class="licence_block license-required bx_filter">
                                    <input type="checkbox" data-required="1" id="licenses" <?=(COption::GetOptionString("aspro.priority", "LICENCE_CHECKED", "N") == "Y" ? "checked" : "");?> name="licenses" required value="Y">
                                    <label for="licenses">
                                        <?$APPLICATION->IncludeFile(SITE_DIR."include/licenses_text.php", Array(), Array("MODE" => "html", "NAME" => "LICENSES")); ?>
                                    </label>
                                </div>
                                <div class="pull-right">
                                    <div class="summ">Сумма: <b><?=$arResult['SUMM']?> &#8381;</b><?=!empty($arResult['SUMM_OLD']) ? '<sup class="old_summ">'.$arResult['SUMM_OLD'].'</sup>' : ''?></div>
                                </div>
                                <div class="pull-left">
                                    <button class="btn btn-lg btn-default" type="submit"><?=$arParams["SEND_BUTTON_NAME"]?></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                <?=$arResult["FORM_FOOTER"]?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12 scrollable-block">
            <div class="fixed_block_fix"></div>
            <div class="fixed_block_wrapper">
                <div class="fixed_block">
                    <?
                    if(!CUser::isAuthorized()){
                        ?><h4>Авторизация</h4><?
                        $APPLICATION->IncludeComponent(
                            "bitrix:system.auth.form",
                            "main",
                            Array(
                                "REGISTER_URL" => SITE_DIR."cabinet/registration/?register=yes",
                                "PROFILE_URL" => SITE_DIR."cabinet/",
                                "FORGOT_PASSWORD_URL" => SITE_DIR."cabinet/forgot-password/?forgot-password=yes",
                                "AUTH_URL" => SITE_DIR."cabinet/",
                                "SHOW_ERRORS" => "Y",
                                "POPUP_AUTH" => "Y",
                                "AJAX_MODE" => "Y",
                                "BACKURL" => "/cart/order/"
                            )
                        );
                    }
                    ?>
                    <?/*?><div class="summ hidden-xs">Сумма:<br><b><?=$arResult['SUMM']?> руб.</b><sup class="old_summ"><?=$arResult['SUMM_OLD']?></sup></div><?*/?>
                </div>
            </div>


        </div>
    </div>
</div>
<script>
	var bFormNote = <?=CUtil::PhpToJSObject($arResult['FORM_NOTE']);?>;

	$(document).ready(function(){
		if(arPriorityOptions['THEME']['CAPTCHA_FORM_TYPE'] == 'RECAPTCHA' || arPriorityOptions['THEME']['CAPTCHA_FORM_TYPE'] == 'RECAPTCHA2'){
			reCaptchaRender();
		}
		if(arPriorityOptions['THEME']['USE_SALE_GOALS'] !== 'N'){
			var eventdata = {goal: 'goal_order_begin'};
			BX.onCustomEvent('onCounterGoals', [eventdata]);
		}
		$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"]').validate({
			ignore: ".ignore",
			highlight: function( element ){
				$(element).parent().addClass('error');
			},
			unhighlight: function( element ){
				$(element).parent().removeClass('error');
			},
			submitHandler: function( form ){
				if( $('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"]').valid() ){
					$(form).find('button[type="submit"]').attr("disabled", "disabled");
					var eventdata = {type: 'form_submit', form: form, form_name: '<?=$arResult["IBLOCK_CODE"]?>'};
					BX.onCustomEvent('onSubmitForm', [eventdata]);
					if(!bFormNote){
						$(form).prepend('<div class="overlay_form"><div class="loader"><div class="duo duo1"><div class="dot dot-a"></div><div class="dot dot-b"></div></div><div class="duo duo2"><div class="dot dot-a"></div><div class="dot dot-b"></div></div></div></div>');
					}
				}
			},
			errorPlacement: function( error, element ){
				if($(element).hasClass('captcha')){
					$(element).closest('.captcha-row').append(error);
				}
				else if($(element).closest('.license-required').length){
					$(element).closest('.license-required').append(error);
				}
				else if($(element).closest('[data-sid=FILE]')){
					$(element).closest('.form-group').append(error);
				}
				else{
					if($(element).closest('.license-required').length){
						$(element).closest('.license-required').append(error);
					}
					else if($(element).closest('[data-sid=FILE]')){
						$(element).closest('.form-group').append(error);
					}
					else{
						error.insertAfter(element);
					}
				}
			},
			messages:{
				licenses_popup: {
					required : BX.message('JS_REQUIRED_LICENSES')
				}
			}
		});

		if(arPriorityOptions['THEME']['PHONE_MASK'].length){
			var base_mask = arPriorityOptions['THEME']['PHONE_MASK'].replace( /(\d)/g, '_' );
			$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"] input.phone').inputmask("mask", { "mask": arPriorityOptions['THEME']['PHONE_MASK'], 'showMaskOnHover': false });
			$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"] input.phone').blur(function(){
				if( $(this).val() == base_mask || $(this).val() == '' ){
					if( $(this).hasClass('required') ){
						$(this).parent().find('div.error').html(BX.message("JS_REQUIRED"));
					}
				}
			});
		}

		var sessionID = '<?=bitrix_sessid()?>';
		$('input#SESSION_ID').val(sessionID);

		if(arPriorityOptions['THEME']['DATE_MASK'].length){
			$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"] input.date').inputmask('datetime', {
				inputFormat: arPriorityOptions['THEME']['DATE_MASK'],
				placeholder: arPriorityOptions['THEME']['DATE_PLACEHOLDER'],
				showMaskOnHover: false
			});
		}

		if(arPriorityOptions['THEME']['DATETIME_MASK'].length){
			$('.order.form form[name="<?=$arResult["IBLOCK_CODE"]?>"] input.datetime').inputmask('datetime', {
				inputFormat: arPriorityOptions['THEME']['DATETIME_MASK'],
				placeholder: arPriorityOptions['THEME']['DATETIME_PLACEHOLDER'],
				showMaskOnHover: false
			});
		}

		$("input[type=file]").uniform({ fileButtonHtml: BX.message("JS_FILE_BUTTON_NAME"), fileDefaultHtml: BX.message("JS_FILE_DEFAULT") });
		$(document).on('change', 'input[type=file]', function(){
			if($(this).val())
			{
				$(this).closest('.uploader').addClass('files_add');
			}
			else
			{
				$(this).closest('.uploader').removeClass('files_add');
			}
		})
		$('.form .add_file').on('click', function(){
			var index = $(this).closest('.input').find('input[type=file]').length+1;
			$('<input type="file" id="POPUP_FILE" name="FILE_n'+index+'"   class="inputfile" value="" />').insertBefore($(this));
			$('input[type=file]').uniform({fileButtonHtml: BX.message('JS_FILE_BUTTON_NAME'), fileDefaultHtml: BX.message('JS_FILE_DEFAULT')});
		})
	});
</script>