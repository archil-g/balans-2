<?php
\Bitrix\Main\Loader::registerAutoLoadClasses('rdn', [
        '\rdn\Helper' => 'lib/helper.php',
        '\rdn\Import' => 'lib/import.php',
        '\rdn\Api' => 'lib/api.php',
    ]
);
function backtrace_line(&$d, $i, $maxDept = 10)
{
	return (isset($d[$i]) ?
		str_repeat(' ', $maxDept - $i) . (isset($d[$i]['file']) ? $d[$i]['file'] : '') . ' ' . (isset($d[$i]['line'])
			? $d[$i]['line'] : '') . ' ' . (isset($d[$i]['class']) ? $d[$i]['class'] : '') . ' '
		. (isset($d[$i]['function']) ? $d[$i]['function'] : '') . "\n"
		: '');
}

function l(){
	$d = debug_backtrace();
	$file = backtrace_line($d, 2, 3) . backtrace_line($d, 1, 3) . backtrace_line($d, 0, 3);
	echo '<pre>'.$file."\r\n";
	foreach(func_get_args() as $k=>$i) {
		print_r($i);
		echo "\r\n";
	}
	echo '</pre>';
}
function t(){
	static $t;
	$time=number_format((microtime(1)-$t),5,"."," ");
	$t=microtime(1);
	l($time);
}



?>