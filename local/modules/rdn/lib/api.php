<?php
namespace rdn;
use \Bitrix\Main\Data\Cache,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Diag\Debug;

/**
 * Class Api
 * @package
 */
class Api extends Helper
{
    private static $rest_url = "https://api.balans2.ru/php/bitrix";

    public static function company_info($inn,$kpp){
        if(empty($inn)) return false;

        if(strlen($inn) == 10 && empty($kpp)){
            return false;
        }

        return self::getRestData('company_info',['inn' => $inn, 'kpp' => $kpp]);
    }
    public static function add_company($data){
        if(empty($data) || empty($data['inn']) || empty($data['kpp']) || empty($data['name'])) return false;
        return self::sendRequest('add_company',$data);
    }

    /**
     * запрос
     * @param $service
     * @param $action
     * @param $container
     */
    private function getRestData($action,$params = [])
    {
        $result = null;
        $query = '';
        if(!empty($params)) {
            $query = '?'.http_build_query($params);
        }

        if($action)
            $response = file_get_contents(self::$rest_url.'/'.$action.$query);

        if($response){
            $result = self::objectToArray(json_decode($response));
        }

        return $result;
    }

    /**
     * запрос POST/GET
     * @param $service
     * @param $action
     * @param $container
     */
    private function sendRequest($action,$params = [], $method = 'POST')
    {
        $result = null;
        $postdata = '';
        if(!empty($params)) {
            $postdata = json_encode($params, JSON_UNESCAPED_UNICODE);
        }

        if($action){
            $opts = array('http' =>
                array(
                    'method'  => $method,
                    'header'  => 'Content-type: application/json',
                    'content' => $postdata/*,
					'proxy'           => 'tcp://127.0.0.1:8889',
					'request_fulluri' => true,*/
                )
            );
            $context  = stream_context_create($opts);


            $response = file_get_contents(self::$rest_url.'/'.$action, false, $context);
        }

        if($response){
            $result = self::objectToArray(json_decode($response));
        }
        return $result;
    }

    /** Запрос заказов пользователей
     * @param int $limit
     * @param int $offset
     * @param null $userId
     * @return array|null
     */
    public static function getOrders($limit = 10, $offset = 0, $userId = null){
        self::$rest_url = "https://api.balans2.ru/bitrix";
        $filter = ['limit' => $limit,'offset'=>$offset];

        if($userId){
            $filter['id'] = (int) $userId;
        }

        return self::getRestData('store/orders',$filter);
    }

    /** Запрос заказов пользователей
     * @param int $limit
     * @param int $offset
     * @param null $userId
     * @return array|null
     */
    public static function getOrdersFull($limit = 10, $offset = 0, $userId = null){
        self::$rest_url = "https://api.balans2.ru/bitrix";
        $filter = ['limit' => $limit,'offset'=>$offset];
        if($userId){
            $filter['id'] = (int) $userId;
        }
        $orders = self::getRestData('store/orders',$filter);
        $orderList = [];
        if(!empty($orders['data'])) {
			$ids = [];
			foreach ($orders['data'] as $order) {
				# собираем id
				$ids[] = $order['tw_contract_id'];
				/*$data = self::getOrderInfo($order['tw_contract_id']);
				if($data){
					$orderList[] = $data[0];
				}*/
			}
			if ($ids) {
				$data = self::getOrderInfo($ids);
				if (count($data)) {
					$orderList = $data;
				}
			}
		}
        return $orderList;
    }

    /** Запрос информации заказа
     * @param int $id
     * @return array|null
     */
    public static function getOrderInfo($id){
        self::$rest_url = "https://api.balans2.ru/php/bitrix";
        if(empty($id)) return false;
        $filter = ['id' => $id];
		return self::getRestData('orders', $filter);
    }
 /** Запрос информации заказа статусы
     * @param int $id
     * @return array|null
     */
    public static function getOrderInfoMin($id){
        self::$rest_url = "https://api.balans2.ru/php/bitrix";
        if(empty($id)) return false;
        $filter = ['id' => $id];
        return self::getRestData('orders_min',$filter);
    }

    /** Создание договоров
     * @param $data array
     * @return array|bool|null
     */
    public static function newOrder($data){
        if(empty($data) || !is_array($data)) return false;
        return self::sendRequest('new_order',$data);
    }

    /** Список дистрибутивов
     * @param $id
     * @return array|bool|null
     */
    public static function downloadLink($id){
        if(empty($id)) return false;

        return self::getRestData('download_link',['id' => $id], 'GET');
    }

    /** Информация по заказу
     * @param $id
     * @return array|bool|null
     */
    public static function orders($id){
        if(empty($id)) return false;

        return self::getRestData('orders',$id, 'GET');
    }


    /** Запрос информация присутствии лицензий
     * @param $data
     * @return array|bool|null
     */
    public static function checkLicense($data){
        if(empty($data)) return false;

        return self::getRestData('check_license',$data);
    }

	/** Фиксация получении услуги(отгрузка)
	 * @param $id договора в CRM
	 * @return array|bool|null
	 */
	public static function setDone($id){
		if(empty($id)) return ['is_done'=>false];
		return self::getRestData('set_done',['id' => $id], 'GET');
	}

}