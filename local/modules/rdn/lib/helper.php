<?php
namespace rdn;

use \Bitrix\Main\Data\Cache,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Entity;

\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("sale");
\Bitrix\Main\Loader::includeModule("catalog");

/**
 * Class Helper
 * Вспомогательный класс
 * @package rdn
 */
class Helper {

    /**
     * Очистка телефона
     *@param $phone
     */
    public static function clearPhone($phone) {
        $phone = preg_replace("#[^\d]#", "", $phone);
        if (substr($phone, 0, 1) == '7'
            || substr($phone, 0, 1) == '8'
        ) {
            $phone = substr($phone, 1);
        }
        return $phone;
    }

    public static function objectToArray($object) {
        if( !is_object($object) && !is_array($object)) {
            return $object;
        }
        if( is_object($object )) {
            $object = get_object_vars($object);
        }
        return array_map('self::objectToArray', $object);
    }

    public static function getOfferPrices($item_id){
        global $USER;
        $result = [];
        $currency_code = 'RUB';
        // Простой товар, без торговых предложений (для количества равному 1)
        $price = \CCatalogProduct::GetOptimalPrice($item_id, 1, $USER->GetUserGroupArray(), 'N');
        // Получили цену?
        if(!$price || !isset($price['PRICE'])) {
            return false;
        }

        // Меняем код валюты, если нашли
        if(isset($price['CURRENCY'])) {
            $currency_code = $price['CURRENCY'];
        }
        if(isset($price['PRICE']['CURRENCY'])) {
            $currency_code = $price['PRICE']['CURRENCY'];
        }

        // Получаем итоговую цену
        $final_price = $price['PRICE']['PRICE'];

        // Ищем скидки и пересчитываем цену товара с их учетом
        $arDiscounts = \CCatalogDiscount::GetDiscountByProduct($item_id, $USER->GetUserGroupArray(), "N", 1);
        if(is_array($arDiscounts) && sizeof($arDiscounts) > 0) {
            foreach ($arDiscounts as $item){
                if($item['ACTIVE'] == 'Y'){
                    $discount = $item;
                    break;
                }
            }
            $discount_price = \CCatalogProduct::CountPriceWithDiscount($final_price, $currency_code, $arDiscounts);
            $result = ['PRICE' => round($discount_price).' &#8381;', 'OLD_PRICE' => round($final_price), 'DISCOUNT_SIZE' => $discount['VALUE'], 'DISCOUNT_NAME' => $discount['NAME']];
        }
        else{
            $result = ['PRICE' => round($final_price).' &#8381;'];
        }
        return $result;
    }

    public static function getTablePrices($crit_ids, $price_ids, $price_tpl, $section_code = ''){
        $crit = [];
        $ind = 0;
        $rsCrit = \CIBlockElement::GetList(['SORT' => 'asc'],['IBLOCK_ID' => '54','ACTIVE' => 'Y','ID' => $crit_ids,'!PROPERTY_OPT' => false],false,false,['NAME','ID','XML_ID','PROPERTY_OPT','PROPERTY_ARTIK']);
        $price_data = self::getPricesPriceToArticles($price_ids);

        while ($arCrit = $rsCrit->Fetch())
        {
            $crit[$arCrit['PROPERTY_ARTIK_VALUE']]['NAME'] = $arCrit['NAME'];
            $rsOpt = \CIBlockElement::GetList(['SORT' => 'asc'],['IBLOCK_ID' => '55','ACTIVE' => 'Y','ID' => $arCrit['PROPERTY_OPT_VALUE']],false,false,['NAME','PROPERTY_ARTIK','PROPERTY_OPT_VAL']);
            while ($arOpt = $rsOpt->Fetch())
            {
                $crit[$arCrit['PROPERTY_ARTIK_VALUE']]['VAL'][$arOpt['PROPERTY_ARTIK_VALUE']] = $arOpt['NAME'];
            }
        }

        $tplInfoArr = self::parsePriceTpl($price_tpl);
        $tables = [];
        foreach ($tplInfoArr as $tplKey => $tplInfo){
            $lineCount = array();
            foreach ($tplInfo as $n => &$vv) {
                foreach ($vv as $k => &$vvv) {
                    if ($vvv == '*') {
                        $vvv = array_keys($crit[$k]['VAL']);
                    }
                    else{
                        $vvv = explode(',', $vvv);
                    }
                    $lineCount[$n][] = count($vvv);
                }
            }
            # собираем критерии которые одинаковы для всего прайса
            $static_krit=array();
            foreach($tplInfo['s'] as $s){
                $static_krit=array_merge($static_krit,$s);
            }
            # шапка
            $table = '<table class="table gray nomargin">';
            $i = 0;

            foreach ($tplInfo['x'] as $cid => $x) {
                $colspan = 1;
                for ($c = $i + 1; $c < count($lineCount['x']); $c++) {
                    $colspan = $lineCount['x'][$c] * $colspan;
                }
                $repeatCount = 1;
                for ($c = $i - 1; $c >= 0; $c--) {
                    $repeatCount = $lineCount['x'][$c] * $repeatCount;
                }

                if($i == 0) {
                    $table .= '<tr><th  class="">' . $crit[$cid]['NAME'] . '</th>';
                    for ($c = 1; $c <= $repeatCount; $c++) {
                        foreach ($x as $vid) {
                            $table .='<th' . ($colspan > 1 ? ' colspan="' . $colspan . '"' : '') . '>' . $crit[$cid]['VAL'][$vid]
                                .(isset($_GET['h'])?'(<b>'.$vid.'</b>)':''). '</th>';
                        }
                    }
                }
                else {

                    $table .= '<tr class="font-weight-bold"><td  class="">' . $crit[$cid]['NAME'] . '</td>';

                    for ($c = 1; $c <= $repeatCount; $c++) {
                        foreach ($x as $vid) {
                            $table .='<td' . ($colspan > 1 ? ' colspan="' . $colspan . '"' : '') . '>' . $crit[$cid]['VAL'][$vid]
                                .(isset($_GET['h'])?'(<b>'.$vid.'</b>)':''). '</td>';
                        }
                    }
                }

                $table .= '</tr>';
                $i++;
            }
# тело
            $table .= '<tbody>';
            if(!isset($tplInfo['y'])){
                # только одна строка по X
                $table .= '<tr class="' . ($ind++ % 2 == 0 ? 'odd' : 'even') . '"><td> </td>';
                $line = array();
                $i = 0;
                foreach ($tplInfo['x'] as $cidX => $x) {
                    $colspan = 1;
                    for ($c = $i + 1; $c < count($lineCount['x']); $c++) {
                        $colspan = $lineCount['x'][$c] * $colspan;
                    }
                    $repeatCount = 1;
                    for ($c = $i - 1; $c >= 0; $c--) {
                        $repeatCount = $lineCount['x'][$c] * $repeatCount;
                    }
                    $posX = 0;
                    for ($c = 0; $c <= $repeatCount - 1; $c++) {
                        foreach ($x as $vid) {
                            // $table .='<td>' . $crit[$cid]['val'][$vid] . '</td>';
                            for ($cc = 0; $cc <= $colspan - 1; $cc++) {
                                $line[$posX * $colspan + $cc][] = $vid;//$crit[$cid]['val'][$vid];
                            }
                            $posX++;
                        }
                    }
                    $i++;
                }

                foreach ($line as $v) {
                    $krAr = array_merge($v, array($cr), $static_krit);
                    sort($krAr);
                    $krit = str_replace(' ','', implode(' ',$krAr));
                    if (isset($price_data[$krit])) {
                        $cena = $price_data[$krit]['PRICE'];
                        $url = '/software/'.$section_code.'/prices/'.$price_data[$krit]['CODE'].'/';
                    } else {
                        $url = '#';
                        $cena = '-';
                    }


                    if ($cena == '-') {
                        $table .= '<td><span><a class="ico" data-param-id="20" data-event="jqm" data-form_description="По продукту ' . $price_data[$krit]['NAME'] . '" data-form_title="Уточнить информацию" data-autoload-need_product="' . $price_data[$krit]['NAME'] . '" rel="tooltip" title="' . $price_data[$krit]['NAME'] . '" data-name="question"><img src="/images/quest.svg"></a></span>' . (isset($_GET['h']) ? '(<b>' . $krit . '</b>)' : '') . '</td>';
                    } else {
                        $table .= '<td><a rel="tooltip" title="' . $price_data[$krit]['NAME'] . '" href="' . $url . '">' . $cena . (isset($_GET['h']) ? '(<b>' . $krit . '</b>)' : '') . '</a></td>';
                    }
                }
                $table .= '</tr>';
            }else {

                foreach ($tplInfo['y'] as $cid => $y) {
                    $ind = 0;

                    foreach ((array)$y as $cr) {//($ind % 2>0?'odd':'even').(++$c%2)
                        $table .= '<tr class="' . ($ind++ % 2 == 0 ? 'odd' : 'even') . '"><td>' . $crit[$cid]['VAL'][$cr] . (isset($_GET['h']) ? '(<b>' . $cr . '</b>)' : '') . '</td>';
                        $line = array();
                        $i = 0;
                        foreach ($tplInfo['x'] as $cidX => $x) {
                            $colspan = 1;
                            for ($c = $i + 1; $c < count($lineCount['x']); $c++) {
                                $colspan = $lineCount['x'][$c] * $colspan;
                            }
                            $repeatCount = 1;
                            for ($c = $i - 1; $c >= 0; $c--) {
                                $repeatCount = $lineCount['x'][$c] * $repeatCount;
                            }
                            $posX = 0;
                            for ($c = 0; $c <= $repeatCount - 1; $c++) {
                                foreach ($x as $vid) {
                                    // $table .='<td>' . $crit[$cid]['val'][$vid] . '</td>';
                                    for ($cc = 0; $cc <= $colspan - 1; $cc++) {
                                        $line[$posX * $colspan + $cc][] = $vid;//$crit[$cid]['val'][$vid];
                                    }
                                    $posX++;
                                }
                            }
                            $i++;
                        }
                        // A::d('~#0f0', $line);

                        foreach ($line as $v) {
                            $krAr = array_merge($v, array($cr), $static_krit);
                            sort($krAr);
                            $krit = str_replace(' ','', implode(' ',$krAr));

                            if (isset($price_data[$krit])) {
                                $cena = $price_data[$krit]['PRICE'];
                                $url = '/software/'.$section_code.'/prices/'.$price_data[$krit]['CODE'].'/';
                            } else {
                                $cena = '-';
                                $url = '#';
                            }

                            if(!empty($price_data[$krit])){
                                if ($cena == '-') {
                                    $table .= '<td><span><a class="ico" data-param-id="20" data-event="jqm" data-form_description="По продукту ' . $price_data[$krit]['NAME'] . '" data-form_title="Уточнить информацию" data-autoload-need_product="' . $price_data[$krit]['NAME'] . '" rel="tooltip" title="' . $price_data[$krit]['NAME'] . '" data-name="question"><img src="/images/quest.svg"></a></span>' . (isset($_GET['h']) ? '(<b>' . $krit . '</b>)' : '') . '</td>';
                                } else {
                                    $table .= '<td><a rel="tooltip" title="' . $price_data[$krit]['NAME'] . '" href="' . $url . '">' . $cena . (isset($_GET['h']) ? '(<b>' . $krit . '</b>)' : '') . '</a></td>';
                                }
                            }

                        }
                        $table .= '</tr>';
                    }


                    $i++;
                    break; # по y может быть только 1 запись
                }
            }
            $table .= '</tbody></table>';
            $tables[$tplKey] = $table;
        }



       /* $cache = \Cache::createInstance();
        if ($cache->initCache(360000, "getTablePrices")) {
            $data = $cache->getVars();
        }
        elseif ($cache->startDataCache()) {




            $cache->endDataCache($data);
        }*/


       return $tables;
    }

    public static  function getCritOpt($crit_ids){
        $data = [];
        $rsCrit = \CIBlockElement::GetList(['SORT' => 'asc'],['IBLOCK_ID' => '54','ACTIVE' => 'Y','ID' => $crit_ids,'!PROPERTY_OPT' => false],false,false,['NAME','ID','XML_ID','PROPERTY_OPT']);
        while ($arCrit = $rsCrit->Fetch()) {
            $key = '';
            if (strstr($arCrit['NAME'], 'тип покупки') || strstr($arCrit['NAME'], 'Тип покупки')) {
                $key = 'tab';
            }
            $rsOpt = \CIBlockElement::GetList(['SORT' => 'asc'], ['IBLOCK_ID' => '55', 'ACTIVE' => 'Y', 'ID' => $arCrit['PROPERTY_OPT_VALUE']], false, false, ['ID','NAME','XML_ID', 'PROPERTY_ARTIK', 'PROPERTY_OPT_VAL']);
            while ($arOpt = $rsOpt->Fetch()) {

                $data[$key?$key:$arCrit['NAME']][$arOpt['PROPERTY_ARTIK_VALUE']] = ['CRIT_ID' => $arCrit['ID'], 'NAME' => $arOpt['NAME'], 'ID' => $arOpt['ID']];
            }
        }
        return $data;
    }

    public static  function getCritByOpt($crit_ids){
        $data = [];
        $rsCrit = \CIBlockElement::GetList(['SORT' => 'asc'],['IBLOCK_ID' => '54','ACTIVE' => 'Y', 'ID' => $crit_ids,'!PROPERTY_OPT' => false],false,false,['NAME','ID','XML_ID','PROPERTY_OPT']);
        while ($arCrit = $rsCrit->Fetch()) {
            $rsOpt = \CIBlockElement::GetList(['SORT' => 'asc'], ['IBLOCK_ID' => '55', 'ACTIVE' => 'Y', 'ID' => $arCrit['PROPERTY_OPT_VALUE']], false, false, ['ID','NAME','XML_ID', 'PROPERTY_ARTIK', 'PROPERTY_OPT_VAL']);
            while ($arOpt = $rsOpt->Fetch()) {

                $data[$arOpt['PROPERTY_ARTIK_VALUE']] = ['CRIT' => $arCrit['NAME'], 'VAL' => $arOpt['NAME'], 'ID' => $arOpt['ID']];
            }
        }
        return $data;
    }

    public static  function getOptCrits($opt_articles){
        $data = [];
        $rsOpt = \CIBlockElement::GetList(['SORT' => 'asc'], ['IBLOCK_ID' => '55', 'ACTIVE' => 'Y', 'PROPERTY_ARTIK' => $opt_articles], false, false, ['ID','NAME','XML_ID', 'PROPERTY_ARTIK', 'PROPERTY_OPT_VAL']);

        while ($arOpt = $rsOpt->Fetch()) {
            $rsCrit = \CIBlockElement::GetList(['SORT' => 'asc'],['IBLOCK_ID' => '54','ACTIVE' => 'Y', 'PROPERTY_OPT' => $arOpt['ID']],false,false,['NAME','ID','XML_ID','PROPERTY_OPT']);
            if ($arCrit = $rsCrit->Fetch()) {
                $data[$arOpt['ID']] = ['CRIT_NAME' => $arCrit['NAME'],'CRIT_XML_ID' => $arCrit['XML_ID'],'CRIT_ID' => $arCrit['ID'], 'NAME' => $arOpt['NAME']];
            }
        }
        return $data;
    }

    public static function parseOptFromArticle($article){
        $res = [];
        if(!empty($article)) {
            preg_match('/\[(.*)\]/', $article, $output_array);
            if(!empty($output_array)) {
                $res = explode(',',$output_array[1]);
            }
        }
        return $res;
    }

    public static function getPricesCodesToArticles($ids) {
        $rs = \CIBlockElement::GetList(['SORT' => 'asc'], ['IBLOCK_ID' => '58', 'ID' => $ids], false, false, ['CODE','XML_ID', 'PROPERTY_ARTIC']);
        $result = [];

        while ($arPr = $rs->Fetch()) {
            $articArr = self::parseOptFromArticle($arPr['PROPERTY_ARTIC_VALUE']);

            $articStr = str_replace(' ','',implode(' ',$articArr));

            $result[$articStr] = $arPr['CODE'];
        }
        return $result;
    }

    public static function getPricesPriceToArticles($ids) {
        $rs = \CIBlockElement::GetList(['SORT' => 'asc'], ['IBLOCK_ID' => '58', 'ID' => $ids, 'ACTIVE' => 'Y'], false, false, ['NAME','CODE','XML_ID', 'PROPERTY_ARTIC','CATALOG_PRICE_1']);
        $result = [];

        while ($arPr = $rs->Fetch()) {

            $articArr = self::parseOptFromArticle($arPr['PROPERTY_ARTIC_VALUE']);

            $articStr = str_replace(' ','',implode(' ',$articArr));

            $result[$articStr] = ['PRICE' => round($arPr['CATALOG_PRICE_1']), 'CODE' => $arPr['CODE'], 'NAME' => $arPr['NAME']];
        }
        return $result;
    }

    public static function parsePriceTpl($tpl){
		$ret = array();
		$r = '';
		preg_match_all('/(\w+)\(([^)]+)\)/', $tpl, $m, PREG_SET_ORDER);
		foreach ($m as $v) {
			preg_match_all('/(([xysn]+):(\d+):([0-9,*]+));?/', $v[2], $mm, PREG_SET_ORDER);
			foreach ($mm as $vv) {
				$ret[$v[1]][$vv[2]][$vv[3]] = $vv[4];
			}
		}
		return $ret;
    }
    public static function getCompanyByInnKpp($inn,$kpp){
        $arfilter = ['IBLOCK_ID' => '57','PROPERTY_INN' => str_replace('_','',$inn)];

        if(strlen($inn) == 10) {
            $arfilter['PROPERTY_KPP'] = str_replace('_','',$kpp);
        }

        $rs = \CIBlockElement::GetList(['SORT' => 'asc'], $arfilter, false, false, ['ID','XML_ID', "IBLOCK_ID",'NAME','PROPERTY_*']);
        if($ob = $rs->GetNextElement()){
            $exclude = ['~IBLOCK_ID','~ID','~NAME','IBLOCK_ID','ID','NAME'];
            $field = $ob->GetFields();
            $company = [];
            $prop = $ob->GetProperties();
            $company['NAME'] = $field['~NAME'];
            $company['ext_id'] = $field['XML_ID'];
            $company['id'] = $field['ID'];
            foreach ($prop as $k => $v){
                if(!in_array($k,$exclude))
                    $company[$k] = $v['VALUE'];

                $companies[$field['ID']] = $company;
            }
            return $company;
        }
        else{
            return false;
        }
    }

    public static function addUserToCompany($id){
        global $USER;
        $VALUES = [];
        $res =\CIBlockElement::GetProperty(57, $id, Array("sort"=>"asc"), array("CODE" => "USERS"));
        while ($ob = $res->GetNext())
        {
            $VALUES[] = $ob['VALUE'];
        }

        $PROPERTY_CODE = "USERS";
        $PROPERTY_VALUE = $USER->GetId();

        if(in_array($PROPERTY_VALUE,$VALUES)){
            return ['success'=>false,'message'=>'Компания уже привязана к Вам'];
        }
        else{
            $VALUES[] = $PROPERTY_VALUE;
            \CIBlockElement::SetPropertyValuesEx($id, false, array($PROPERTY_CODE => $VALUES));
            return ['success'=>true];
        }
    }

    public static function removeUserFromCompany($id){
        global $USER;
        $VALUES = [];
        $company = \CIBlockElement::getById($id)->Fetch();

        if($company['CREATED_BY'] == $USER->getId()) {
            \CIBlockElement::Update($id,['CREATED_BY' => 1]);
        }

        $res =\CIBlockElement::GetProperty(57, $id, Array("sort"=>"asc"), array("CODE" => "USERS"));
        while ($ob = $res->GetNext())
        {
            $VALUES[] = $ob['VALUE'];
        }
        $PROPERTY_CODE = "USERS";
        $PROPERTY_VALUE = $USER->GetId();

        if(in_array($PROPERTY_VALUE,$VALUES)){
           unset($VALUES[array_search($PROPERTY_VALUE,$VALUES)]);
           if(empty($VALUES))$VALUES = false;

            \CIBlockElement::SetPropertyValuesEx($id, false, array($PROPERTY_CODE => $VALUES));
        }

        return ['success'=>true];

    }

    public static function editorUserRequest($id, $user_id){
        global $USER;
        $arEventFields = ['ID' => $id, 'USER_ID' => $user_id];
        \CEvent::Send("RDN_EDITOR_REQUEST", ['s1'], $arEventFields);
        return ['success'=>true];

    }

    public static function getCompanyById($id,$access = false){
        global $USER;
        $arFilter = ['IBLOCK_ID' => '57','ID' => $id,['LOGIC' => 'OR',['CREATED_BY' => $USER->getId()],['PROPERTY_USERS_EDITORS' => $USER->getId()]]];
        $rs = \CIBlockElement::GetList(['SORT' => 'asc'], $arFilter, false, false, ["ID","CREATED_BY","XML_ID", "IBLOCK_ID",'NAME','PROPERTY_*']);
        $fields = [];
        $exclude = ['USERS','~IBLOCK_ID','~ID','~NAME','IBLOCK_ID','ID','NAME'];
        if($ob = $rs->GetNextElement()){
            $field = $ob->GetFields();
            $prop = $ob->GetProperties();

            if($field['CREATED_BY'] == $USER->getId()) $fields['is_patron'] = 'Y';
            else $fields['is_patron'] = 'N';
            $fields['NAME'] = $field['~NAME'];
            $fields['XML_ID'] = $field['XML_ID'];
            foreach ($prop as $k => $v){
                if(!in_array($k,$exclude))
                    $fields[$k] = $v['VALUE'];
            }
        }
        return $fields;
    }

    public static function getUserCompanies($user_id){
        $companies = [];

        if(!empty($user_id)) {
            $arFilter = ['IBLOCK_ID' => '57',
                ["LOGIC" => "OR",
                    ['CREATED_BY' => $user_id],
                    ['PROPERTY_USERS' => $user_id]
                ]
            ];
            $rs = \CIBlockElement::GetList(['SORT' => 'asc'], $arFilter, false, false, ["ID", "XML_ID", "IBLOCK_ID",'NAME','PROPERTY_*']);
            $exclude = ['USERS','~IBLOCK_ID','~ID','~NAME','IBLOCK_ID','NAME'];

            while($ob = $rs->GetNextElement()){
                $field = $ob->GetFields();
                $company = [];
                $prop = $ob->GetProperties();
                $company['NAME'] = $field['NAME'];
                $company['XML_ID'] = $field['XML_ID'];
                $company['ID'] = $field['ID'];
                foreach ($prop as $k => $v){
                    if(!in_array($k,$exclude))
                        $company[$k] = $v['VALUE'];

                    $companies[$field['ID']] = $company;
                }
            }
        }
        return $companies;
    }

    public static function getUserOrders( $orderId = 0, $userId = 0 ){
        $orders = [];
        global $USER;
        if(empty($userId)){
            $userId = $USER->GetID();
        }

        if(!empty($userId)) {

            $arFilter = [
                'IBLOCK_ID' => '56',
                'CREATED_BY' => (int) $userId
            ];
            if(!empty($orderId)) {
                $arFilter['ID'] = (int) $orderId;
            }

            $rs = \CIBlockElement::GetList(['ID' => 'DESC'], $arFilter, false, false, ["ID", "PREVIEW_TEXT", "DETAIL_TEXT",'DATE_CREATE','PROPERTY_CONTRACT_NUMBER','PROPERTY_REP_PERIOD','PROPERTY_PRODUCT','PROPERTY_OFFER','PROPERTY_PRODUCT.NAME','PROPERTY_OFFER.NAME']);
			$api= new \rdn\Api();
            while($order = $rs->Fetch()){
                $product = [];
                $offers = [];
                foreach ($order['PROPERTY_PRODUCT_VALUE'] as $pId) {
                    $pr = \GetIBlockSection($pId);

                    $product[$pr['ID']] = $pr['NAME'];
                }
                $rsOf = \CIBlockElement::GetList(['SORT' => 'asc'], ['IBLOCK_ID' => '58', 'ID' => $order['PROPERTY_OFFER_VALUE']], false, false, ["ID",'NAME','catalog_PRICE_1','PROPERTY_*']);

                while($ob = $rsOf->GetNextElement()){
                    $arFields = $ob->GetFields();
                    $arProps = $ob->GetProperties();
                    $crit = [];
                    foreach ($arProps as $k => $prop) {
                        if(strstr($k, 'CRIT_') && !empty($prop['VALUE'])) {
                            $crit[] = "{$prop['NAME']}: {$prop['VALUE']}";
                        }

                    }

                    $offers[] = [
                        'name' => $arFields['NAME'],
                        'print_name' => $arFields['PROPERTY_638'],
                        'article' => $arFields['PROPERTY_637'],
                        'price' => round($arFields['CATALOG_PRICE_1']),
                        'crit' => $crit ,

                    ];
                }

                //
                $orderArr = self::objectToArray(json_decode($order['DETAIL_TEXT']));
                $answerrArr = self::objectToArray(json_decode($order['PREVIEW_TEXT']));
				/*$order_data =$api->getOrderInfoMin($answerrArr[0]['contract_id']);
				if(empty($order_data))continue;*/

	            $orders[$answerrArr[0]['contract_id']] = [
                    'id' => $order['ID'],
                    'contract_number' => $order['PROPERTY_CONTRACT_NUMBER_VALUE'],
                    'date' => $order['DATE_CREATE'],
                    'data' => $orderArr['orders'][0],
                    'answer' => $answerrArr[0],
                    'offers' => $offers,
                    'product' => $product,
                    'periods' => $order['PROPERTY_REP_PERIOD_VALUE'],
					/*'is_done'=>($order_data[$answerrArr[0]['contract_id']]['is_done']??false),
					'is_new'=>($order_data[$answerrArr[0]['contract_id']]['is_new']??false),
					'date_done'=>($order_data[$answerrArr[0]['contract_id']]['date_done']??null),
					'can_be_done'=>($order_data[$answerrArr[0]['contract_id']]['can_be_done']??false),
					'contract_lines'=>($order_data[$answerrArr[0]['contract_id']]['contract_lines']??[]),
					'tw_id'=>$answerrArr[0]['contract_id']*/
                ];
               // dump($orders);
            }
            if(count($orders)){
				$order_data =$api->getOrderInfoMin(array_filter(array_keys($orders)));//l($order_data);
				if($order_data)
				foreach($orders as $k=>&$v){
					$v['is_done'] =($order_data[$k]['is_done']??false);
					$v['is_new'] = ($order_data[$k]['is_new']??false);
					$v['date_done'] = ($order_data[$k]['date_done']??null);
					$v['can_be_done'] = ($order_data[$k]['can_be_done']??false);
					$v['contract_lines'] = ($order_data[$k]['contract_lines']??[]);
					$v['tw_id'] =$k;
				}
			}
        }
        return $orders;
    }

    public static function get_start_and_end_date($quarter,$length, $format = '') {
        $start = 'first day of ';
        $end = 'last day of ';

        switch ('quarter_'.$quarter) {
            case 'quarter_1'     : $start .= 'January';        $end .= 'March';          break;
            case 'quarter_2'     : $start .= 'April';          $end .= 'June';           break;
            case 'quarter_3'     : $start .= 'July';           $end .= 'September';      break;
            case 'quarter_4'     : $start .= 'October';        $end .= 'December';       break;
        }
        $length = ($length * 3) - 1;

        $date = new \DateTime($start, new \DateTimeZone('Europe/Moscow'));
        $start_date = $format?$date->format($format):$date->getTimestamp() + $date->getOffset();
        $date->modify("+{$length} month");
        $date->modify('last day of this month');
        $end_date = $format?$date->format($format):$date->getTimestamp() + $date->getOffset();

        return [
            'start' => $start_date,
            'end' => $end_date,
        ];
    }

	public static function checkInn($n)
	{
		if (!is_scalar($n)) {
			//triggertrigger_error('Scalar type expected, ' . gettype($n) . ' given ', E_USER_WARNING);
			return false;
		}
		$n = strval($n);
		if (!in_array(strlen($n), array(10, 12)) || !ctype_digit($n)) {
			return false;
		}

		if (strlen($n) == 10) {
			$sum = 0;
			foreach (array(2, 4, 10, 3, 5, 9, 4, 6, 8) as $i => $weight) {
				$sum += $weight * substr($n, $i, 1);
			}
			return $sum % 11 % 10 == substr($n, 9, 1);
		}
		#для 12 знаков:
		$sum1 = $sum2 = 0;
		foreach (array(7, 2, 4, 10, 3, 5, 9, 4, 6, 8) as $i => $weight) {
			$sum1 += $weight * substr($n, $i, 1);
		}
		foreach (array(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8) as $i => $weight) {
			$sum2 += $weight * substr($n, $i, 1);
		}
		return ($sum1 % 11 % 10) . ($sum2 % 11 % 10) == substr($n, 10, 2);
	}

	public static function checkKpp($n){
		return  preg_match('/^[0-9]{4}[0-9A-Z]{2}[0-9]{3}$/',$n);
	}
	public static function setDone($orderId){
		$order = reset(self::getUserOrders( $orderId));
		if($order['is_done']===false && $order['can_be_done']===true){
			$api= new \rdn\Api();
			return $api->setDone($order['tw_id']);
		}else{
			return ['is_done'=>false];
		}
	}

	/**
	 * Проверяем заполненость адреса и руководителя
	 * @param $company
	 */
	public static function CheckCompanyAttr($company){
		$ret=[
			'is_address'=>false,# заполнен адрес
			'is_ruk'=>false,	# заполнен руководитель,
			'is_ok' =>false		# Все заполнено, можно формировать заказ на эту компанию
		];
    	# определяем массив CRM или сайта
		if(isset($company['inn'])){
			# сомпания из crm
			//"addr_is_eq":false,"fiz_adr":"","fiz_gorod":"","fiz_index":"","ruk_doc":"","ruk_dolg":"","ruk_name":"","ur_adr":null,"ur_gorod":null,"ur_index":null,
			if(!empty($company['ur_adr']) && !empty($company['ur_gorod']) && !empty($company['ur_index']) && (
				$company['addr_is_eq']==true ||(  !empty($company['fiz_index']) && !empty($company['fiz_gorod']) && !empty($company['fiz_adr'])  ))
			){
				$ret['is_address']=true;
			}
			if(!empty($company['ruk_name']) && !empty($company['ruk_dolg']) && !empty($company['ruk_doc']) ){
				$ret['is_ruk']=true;
			}

		}else{
			# компания с сайта
			//"UR_INDEX":"","UR_GOROD":"","UR_ADR": ,"RUK_DOC":,"RUK_DOLG":"RUK_NAME" "ADDR_IS_EQ":["\u0414\u0430"], "FIZ_INDEX":"","FIZ_GOROD":"","FIZ_ADR":"
			if(!empty($company['UR_ADR']) && !empty($company['UR_GOROD']) && !empty($company['UR_INDEX']) && (
					in_array('Да',$company['ADDR_IS_EQ']) ||(  !empty($company['FIZ_INDEX']) && !empty($company['FIZ_GOROD']) && !empty($company['FIZ_ADR'])  ))
			){
				$ret['is_address']=true;
			}
			if(!empty($company['RUK_NAME']) && !empty($company['RUK_DOLG']) && !empty($company['RUK_DOC']) ){
				$ret['is_ruk']=true;
			}
		}

		$ret['is_ok']=$ret['is_address'] && $ret['is_ruk'] ;
		return $ret;

	}

}