<?php
namespace rdn;
use \Bitrix\Main\Data\Cache,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Diag\Debug;
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("sale");
\Bitrix\Main\Loader::includeModule("catalog");
set_time_limit(3600);
/**
 * Class Import
 * Импорт
 * @package мп
 */
class Import extends Helper
{

    private static $rest_url = "https://api.balans2.ru/php/bitrix";
    public static $report = [];


    /**
     * Импорт товаров
     */
    public static function importTovars(){
        self::$report = ['IMPORT_TOVARS' => 'COMPLETE'];

        $el = new \CIBlockElement;
        $bs = new \CIBlockSection;
        $tovars = self::getTovars();

        $rsSect = $bs->GetList(['ID' => 'asc'],['IBLOCK_ID' => '37','!XML_ID' => false],false,['ID','XML_ID']);
        $issetSect = [];
        while ($arSect = $rsSect->Fetch())
        {
            $issetSect[$arSect['XML_ID']] = $arSect['ID'];
        }

        $rsCrit = $el->GetList(['ID' => 'asc'],['IBLOCK_ID' => '54','!XML_ID' => false],false,false,['ID','XML_ID']);
        $issetCrit = [];
        while ($arCrit = $rsCrit->Fetch())
        {
            $issetCrit[$arCrit['XML_ID']] = $arCrit['ID'];
        }

        $rsOpt = $el->GetList(['ID' => 'asc'],['IBLOCK_ID' => '55','!XML_ID' => false],false,false,['ID','XML_ID']);
        $issetOpt = [];
        while ($arOpt = $rsOpt->Fetch())
        {
            $issetOpt[$arOpt['XML_ID']] = $arOpt['ID'];
        }

        foreach ($tovars as $tovar){
            $crit_ids = [];
            $price_ids = [];
            $avalOpts = [];

            $arFields = [
                "ACTIVE"         => $tovar['show']?"Y":"N",
                "IBLOCK_ID" => 37,
                "XML_ID" => $tovar['id'],
                "NAME" => $tovar['name'],
                "UF_ARTIK" => $tovar['artic'],
                "UF_PRICE_TPL" => $tovar['price_tpl'],
            ];

            if(!empty($tovar['crit'])){
                foreach ($tovar['crit'] as $crit){
                    $opt_ids = [];
                    $PROP_CRIT['ARTIK'] = $crit['artik'];
                    $PROP_CRIT['SALE_INSTRUCT_NAME'] = $crit['sale_instruct_name'];
                    $arLoadArrayCrit = [
                        "IBLOCK_ID"      => 54,
                        "XML_ID" => $crit['id'],
                        "CODE" => $crit['artik'],
                        "NAME"           => $crit['name'],
                        "ACTIVE"         => 'Y',
                    ];

                    if(!empty($crit['opt'])){
                        foreach ($crit['opt'] as $opt){
                            $PROP_OPT['ARTIK'] = $opt['artik'];
                            $PROP_OPT['OPT_VAL'] = $opt['opt_val'];
                            $PROP_OPT['PREORDER'] = $opt['preorder'];
                            $arLoadArrayOpt = [
                                "IBLOCK_ID"      => 55,
                                "XML_ID" => $opt['id'],
                                "CODE" => $opt['artik'],
                                "PROPERTY_VALUES"=> $PROP_OPT,
                                "NAME"           => $opt['name'],
                                "SORT"           => $opt['pos'],
                            ];
                            $activeOpt = ($opt['show'] == 1);

                            if(empty($issetOpt[$opt['id']])){
                                $id = $el->Add($arLoadArrayOpt);
                                if($activeOpt)
                                    $avalOpts[] = $id;

                                    $opt_ids[] = $id;
                                $issetOpt[$opt['id']] = $id;
                                self::$report['ADD_OPT']++;
                            }
                            else{
                                $el->Update($issetOpt[$opt['id']],$arLoadArrayOpt);
                                if($activeOpt)
                                    $avalOpts[] = $issetOpt[$opt['id']];

                                    $opt_ids[] = $issetOpt[$opt['id']];
                                self::$report['UPD_OPT']++;
                            }
                        }
                    }

                    $PROP_CRIT['OPT'] = $opt_ids;
                    $arLoadArrayCrit['PROPERTY_VALUES'] = $PROP_CRIT;
                  //  print_r($issetCrit[$crit['id']]);  print_r($arLoadArrayCrit); die();
                    if(empty($issetCrit[$crit['id']])){
                        $id = $el->Add($arLoadArrayCrit);
                        $crit_ids[] = $id;
                        $issetCrit[$crit['id']] = $id;
                        self::$report['ADD_CRIT']++;
                    }
                    else{
						unset($arFields['NAME']);
                        $el->Update($issetCrit[$crit['id']],$arLoadArrayCrit);
                        $crit_ids[] = $issetCrit[$crit['id']];
                        self::$report['UPD_CRIT']++;
                    }
                }
            }
            $arFields['UF_CRIT'] = $crit_ids;
            $arFields['UF_OPTIONS'] = $avalOpts;

            if($price_ids = self::importPrice($tovar['id'])){
                $arFields['UF_OFFERS'] = $price_ids;
            }


            if(!empty($issetSect[$tovar['id']])){
                $bs->Update($issetSect[$tovar['id']],$arFields);
                self::$report['UPDATE_PROD']++;
            }
            else{
                $bs->Add($arFields);
                self::$report['ADD_PROD']++;
            }
        }
        self::fillOfferCriteries();
        self::printReport(self::$report);
    }

    /**
     * Импорт ценника
     */

    public  static function importPrice($product_id){
        $el = new \CIBlockElement;

        $prise_ids = [];

        $rsPrice = $el->GetList(['ID' => 'asc'],['IBLOCK_ID' => '58','!XML_ID' => false],false,false,['ID','XML_ID']);
        $issetPrice = [];

        while ($arPrice = $rsPrice->Fetch())
        {
            $issetPrice[$arPrice['XML_ID']] = $arPrice['ID'];
        }

        $offers = self::getPrice($product_id);
        if(!empty($offers)) {
            foreach ($offers as $offer){
                $props = [];
                $props['ARTIC'] = $offer['artic'];
                $props['PRINT_NAME'] = $offer['print_name'];
                $props['LINK'] = $offer['link'];
                $props['IS_CODE'] = $offer['is_code']?"Y":"N";

                $price = floatval($offer['cena']);

                $optionArticles = \rdn\Helper::parseOptFromArticle($offer['artic']);
                $optionCriteries = \rdn\Helper::getOptCrits($optionArticles);


                $arLoadProductArray = [
                    "IBLOCK_ID"         => 58,
                    "NAME"              => $offer['param_name'],
                    "CODE"              => $offer['id'],
                    "XML_ID"            => $offer['id'],
                    "ACTIVE"            => $price > 0 ? "Y" : 'N',
                    "PROPERTY_VALUES"   => $props,
                ];

                if(empty($issetPrice[$offer['id']])){
                    if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                        self::$report['ADD_PRICE_OFFER']++;
                        // Установление цены для товара
                        \CCatalogProduct::add(array('ID' => $PRODUCT_ID, 'AVAILABLE' => 'Y', 'CAN_BUY_ZERO' => 'Y'));
                        $PRICE_TYPE_ID = 1;

                        $arFields = Array(
                            "PRODUCT_ID"       => $PRODUCT_ID,
                            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                            "PRICE"            => $price,
                            "CURRENCY"         => "RUB",
                        );
                        \CPrice::Add($arFields);

                        $prise_ids[] = $PRODUCT_ID;
                    }
                }
                else{
                    $res = $el->Update($issetPrice[$offer['id']], $arLoadProductArray,false,true,true,true);

                    $PRICE_TYPE_ID = 1;

                    $arFields = Array(
                        "PRODUCT_ID" => $issetPrice[$offer['id']],
                        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                        "PRICE" => $price,
                        "CURRENCY" => "RUB",
                        "QUANTITY_FROM" => 1
                    );

                    $res = \CPrice::GetList(
                        array(),
                        array(
                            "PRODUCT_ID" => $issetPrice[$offer['id']],
                            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
                        )
                    );

                    if ($arr = $res->Fetch())
                    {
                        \CPrice::Update($arr["ID"], $arFields);
                    }
                    else
                    {
                        \CPrice::Add($arFields);
                    }
                    self::$report['UPDATE_PRICE_OFFER']++;
                    $prise_ids[] = $issetPrice[$offer['id']];
                }
            }
        }

        return $prise_ids;
    }

    /**
     * Список товаров
     */
    public static function getTovars(){
        return self::getRestData('tovars');
    }

    /**
     * Прайс товара
     * @param id
     */
    public static function getPrice($id){
        if(empty($id)) return false;
        return self::getRestData('price',['product_id' => $id]);
    }



    /**
     * запрос
     * @param $service
     * @param $action
     * @param $container
     */
    private function getRestData($action,$params = [])
    {
        $result = null;
        $query = '';
        if(!empty($params)) {
            $query = '?'.http_build_query($params);
        }
        if($action)
            $response = file_get_contents(self::$rest_url.'/'.$action.$query);

        if($response){
            $result = self::objectToArray(json_decode($response));
        }

        return $result;
    }

    public static function printReport($report){
        foreach ($report as $k => $v){
            if(is_array($v)){
                echo "<b>{$k}</b>: ".implode('<br>',$v).'<br>';
            } else {
                echo "<b>{$k}: {$v}</b><br>";
            }

        }
    }

    public static function fillOfferCriteries(){
        self::$report['FILL_CRITERIES'] = 'COMPLETE';
        $el = new \CIBlockElement;
        $ibp = new \CIBlockProperty;
        $allProps = [];
        $properties = $ibp->GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>58));
        while ($propFields = $properties->GetNext())
        {
            $allProps[] = $propFields['CODE'];
        }

        $rsPrice = $el->GetList(['ID' => 'asc'],['IBLOCK_ID' => '58','!XML_ID' => false],false,false,['ID','PROPERTY_ARTIC']);
        $issetPrice = [];
        while ($arPrice = $rsPrice->Fetch())
        {
            $optionArticles = \rdn\Helper::parseOptFromArticle($arPrice['PROPERTY_ARTIC_VALUE']);
            $optionCriteries = \rdn\Helper::getOptCrits($optionArticles);
            $props = [];
            foreach ($optionCriteries as $item){
                if(in_array('CRIT_'.$item['CRIT_XML_ID'],$allProps)){
                    $props['CRIT_'.$item['CRIT_XML_ID']] = $item['NAME'];
                }
                else{
                    $arFields = Array(
                        "NAME" => $item['CRIT_NAME'],
                        "ACTIVE" => "Y",
                        "SMART_FILTER" => "Y",
                        "SORT" => "600",
                        "CODE" => 'CRIT_'.$item['CRIT_XML_ID'],
                        "PROPERTY_TYPE" => "S",
                        "IBLOCK_ID" => 58,
                    );

                    if($ibp->Add($arFields)){
                        self::$report['ADD_PROP']++;
                        $allProps[] = 'CRIT_'.$item['CRIT_XML_ID'];
                        $props['CRIT_'.$item['CRIT_XML_ID']] = $item['NAME'];
                    }
                }
            }
            if(!empty($props)){
                $el->SetPropertyValuesEx($arPrice['ID'], 58, $props);
                self::$report['ADD_CRIT_VALUE']++;
            }
            $issetPrice[$arPrice['ID']] = $arPrice['PROPERTY_ARTIC_VALUE'];

        }
        self::printReport(self::$report);
        return true;
    }

    public static function importUsers(){
        self::$report = ['USER_IMPORT' => 'Y'];
        $u = new \CUser;
        $limit = 500;
        $offset = 0;
        if(!empty($_REQUEST['page'])){
            $offset = $_REQUEST['page'] * $limit - $limit;
        }
        $users = self::getUsers($limit,$offset);

        if($users['count'] > 0) {
            foreach ($users['data'] as $user) {
                if(!empty($user['email'])){
                    $arFields = array(
                        "NAME" => $user['name'],
                        "XML_ID" => $user['id'],
                        "LOGIN" => $user['email'],
                        "EMAIL" => $user['email'],
                        "PERSONAL_PHONE" => $user['phone'],
                        "PERSONAL_WWW" => $user['password_hash'],
                        "ACTIVE" => "Y",
                        "PASSWORD" => '123456',
                        "CONFIRM_PASSWORD" => '123456',
                        "GROUP_ID" => array(3, 4)
                    );
                    if($new_user_ID = $u->Add($arFields)){
                        self::$report['ADD']++;
                    }else{
                        self::$report['ERROR']++;
                        self::$report['ERROR_T'][] = $u->LAST_ERROR;
                    }
                }
                else{
                    self::$report['EMPTY_EMAIL']++;
                    self::$report['EMPTY_EMAIL_LIST'][] = $user['id'].' - '.$user['name'];
                }
            }


        }



        self::printReport(self::$report);
    }

    public static function importCompanies(){
        self::$report = ['COMPANIES_IMPORT' => 'Y'];
        $el = new \CIBlockElement;
        $arUsers = [];

        $userBy = "id";
        $userOrder = "desc";

        $userFilter = array(
            'ACTIVE' => 'Y'
        );

        $userParams = ['SELECT' => ['ID', 'XML_ID']];

        $rsUsers = \CUser::GetList(
            $userBy,
            $userOrder,
            $userFilter,
            $userParams
        );

        while ($user = $rsUsers->fetch()) {
            $arUsers[$user['XML_ID']] = $user['ID'];
        }
        $limit = 500;
        $offset = 0;
        if(!empty($_REQUEST['page'])){
            $offset = $_REQUEST['page'] * $limit - $limit;
        }
        $companys = self::getCompanyList($limit,$offset);

        if(empty($companys['data'])) return;
        $el = new \CIBlockElement;
        $rsComp = $el->GetList(['ID' => 'asc'],['IBLOCK_ID' => '57'],false,false,['ID','CREATED_BY','PROPERTY_INN','PROPERTY_USERS']);
        $issetComp = [];
        while ($arComp = $rsComp->Fetch())
        {
            $issetComp[$arComp['PROPERTY_INN_VALUE']] = $arComp;
        }

        foreach ( $companys['data'] as $company){

            $props['INN'] = $company['inn'];
            $props['KPP'] = $company['kpp'];
            $props['TW_ID_COMPANY'] = $company['tw_id_company'];

            if($company['is_ip'] == 1){
                $props['IS_IP'] = 'Y';
            }
            if($company['addr_is_eq'] == 1){
                $props['ADDR_IS_EQ'] = 'Y';
            }

            $props['FIZ_ADR'] = $company['fiz_adr'];
            $props['FIZ_GOROD'] = $company['fiz_gorod'];
            $props['FIZ_INDEX'] = $company['fiz_index'];
            $props['ORG_ID'] = $company['org_id'];
            $props['RUK_DOC'] = $company['ruk_doc'];
            $props['RUK_DOLG'] = $company['ruk_dolg'];
            $props['RUK_NAME'] = $company['ruk_name'];
            $props['UR_ADR'] = $company['ur_adr'];
            $props['UR_GOROD'] = $company['ur_gorod'];
            $props['UR_INDEX'] = $company['ur_index'];

            $name = str_replace('\\','',$company['name']);
            $arFields = array(
                "NAME" => !empty($name)?$name:$company['inn'],
                "XML_ID" => $company['id'],
                "IBLOCK_ID" => 57,
                "CODE" => $company['inn'].$company['kpp'],
                "EMAIL" => $company['email'],
                "PROPERTY_VALUES" => $props,
                "ACTIVE" => "Y",
            );

            if($arUsers[$company['id_user']]){
                $arFields['CREATED_BY'] = $arUsers[$company['id_user']];
                self::$report['WITH_USER']++;
            } else {
                self::$report['WITHOUT_USER']++;
            }

            if(empty($issetComp[$company['inn']])){
                if($id = $el->Add($arFields)){
                    $issetComp[$company['inn']] = ['ID' => $id, 'CREATED_BY' => $arUsers[$company['id_user']]];
                    self::$report['ADD']++;
                }else{
                    self::$report['ERROR']++;
                    self::$report['ERROR_T'][] = $company['inn'];
                }
            } elseif($issetComp[$company['inn']]['CREATED_BY'] != $arUsers[$company['id_user']] && !in_array($arUsers[$company['id_user']],$issetComp[$company['inn']]['PROPERTY_USERS_VALUE'])){
                $issetComp[$company['inn']]['PROPERTY_USERS_VALUE'][] = $arUsers[$company['id_user']];
                $el->SetPropertyValuesEx($issetComp[$company['inn']]['ID'], 57, ['USERS' => $issetComp[$company['inn']]['PROPERTY_USERS_VALUE']]);
                self::$report['ADD_USER_TO']++;
            }

        }

        self::printReport(self::$report);
    }

    /**
     * Список пользователей
     */
    public static function getUsers($limit = 1000,$offset = 0){
        self::$rest_url = "https://api.balans2.ru/bitrix";
        return self::getRestData('store/user',['limit' => $limit,'offset'=>$offset]);
    }

    /**
     * Список компаний
     */
    public static function getCompanyList($limit = 1000,$offset = 0){
        self::$rest_url = "https://api.balans2.ru/bitrix";
        return self::getRestData('store/company',['limit' => $limit,'offset'=>$offset]);
    }

    public static function createSections(){
        $bs = new \CIBlockSection;

        $rsSect = $bs->GetList(['ID' => 'asc'],['IBLOCK_ID' => '37','ACTIVE' => 'Y'],false,['ID','CODE', 'NAME']);
        $issetSect = [];
        while ($arSect = $rsSect->Fetch())
        {
            $issetSect[$arSect['XML_ID']] = $arSect['ID'];

            /*$arFieldsFiles = [
                "ACTIVE"         => "Y",
                "IBLOCK_ID" => 51,
                "NAME" => $arSect['NAME'],
                "CODE" => $arSect['CODE'],
                "IBLOCK_SECTION_ID" => 91,
                "UF_PO" => $arSect['ID'],
                "UF_PRODUCT_FAQ" => 20,
            ];

            $arFieldsArticles = [
                "ACTIVE"         => "Y",
                "IBLOCK_ID" => 52,
                "NAME" => $arSect['NAME'],
                "CODE" => $arSect['CODE'],
                "IBLOCK_SECTION_ID" => 95,
                "UF_PO" => $arSect['ID'],
                "UF_PRODUCT_FAQ" => 23,
            ];

            $arFieldsVideo = [
                "ACTIVE"         => "Y",
                "IBLOCK_ID" => 53,
                "NAME" => $arSect['NAME'],
                "CODE" => $arSect['CODE'],
                "IBLOCK_SECTION_ID" => 93,
                "UF_PO" => $arSect['ID'],
                "UF_PRODUCT_FAQ" => 21,
            ];
            self::$report['COMPANIES_IMPORT'];
            if($bs->Add($arFieldsFiles)){
                self::$report['FILES']++;
            } else{
                self::$report['FILES_E'][] = $bs->LAST_ERROR.$arSect['CODE'];
            }

            if($bs->Add($arFieldsArticles)){
                self::$report['ARTICLES']++;
            } else{
                self::$report['ARTICLES_E'][] = $bs->LAST_ERROR.$arSect['CODE'];
            }

            if($bs->Add($arFieldsVideo)){
                self::$report['VIDEO']++;
            } else{
                self::$report['VIDEO_E'][] = $bs->LAST_ERROR.$arSect['CODE'];
            }*/


            $arField5 = [
                "ACTIVE"         => "Y",
                "IBLOCK_ID" => 62,
                "NAME" => $arSect['NAME'],
                "CODE" => $arSect['CODE'],
                "IBLOCK_SECTION_ID" => 0,
                "UF_PO" => $arSect['ID'],
                "UF_PRODUCT_FAQ" => 33,
            ];

            if($bs->Add($arField5)){
                self::$report['IB4']++;
            } else{
                self::$report['IB4_E'][] = $bs->LAST_ERROR.$arSect['CODE'];
            }

        }
        self::printReport(self::$report);
    }

    public static function setUserComp() {
        require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
        $csvFile = new \CCSVData('R', true);
        $csvFile->LoadFile($_SERVER["DOCUMENT_ROOT"]."/companies_users.csv");
        $csvFile->SetDelimiter(';');


$dataArr = [];
$companies = [];
$users = [];
$u_s = '';
        while ($arRes = $csvFile->Fetch()) {

            $dataArr[] = $arRes;
            $companies[] = $arRes[0];
            $u_s .= $arRes[1].',';

        }
        $users = array_unique(explode(',',trim($u_s,',')));



      //  dump($users);
      //  dump($arRes);
        $el = new \CIBlockElement;
        $rsComp = $el->GetList(['ID' => 'asc'],['IBLOCK_ID' => '57','PROPERTY_TW_ID_COMPANY' => $companies],false,false,['ID','CREATED_BY','PROPERTY_TW_ID_COMPANY']);
        $issetComp = [];
        while ($arComp = $rsComp->Fetch())
        {
            dump($arComp); die();
            $issetComp[$arComp['XML_ID']] = $arComp['ID'];
        }
    }
}