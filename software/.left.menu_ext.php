<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$aMenuLinksExt = array();

if($arMenuParametrs = CPriority::GetDirMenuParametrs(__DIR__)){
    if($arMenuParametrs['MENU_SHOW_SECTIONS'] == 'Y' || $arMenuParametrs['MENU_SHOW_ELEMENTS'] == 'Y'){
        global $APPLICATION, $arRegion, $arTheme;
    }

    if($arMenuParametrs['MENU_SHOW_SECTIONS'] == 'Y'){
        $arSections = CCache::CIBlockSection_GetList(array('SORT' => 'ASC', 'ID' => 'ASC', 'CACHE' => array('TAG' => 'aspro_priority_catalog27', 'MULTI' => 'Y')), array('IBLOCK_ID' => 37, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y'), false, array('IBLOCK_ID', 'ID', 'IBLOCK_SECTION_ID', 'NAME', 'SECTION_PAGE_URL', 'PICTURE', 'DEPTH_LEVEL', 'UF_ICON', 'UF_BACKGROUND'));
        $arSectionsByParentSectionID = CCache::GroupArrayBy($arSections, array('MULTI' => 'Y', 'GROUP' => array('IBLOCK_SECTION_ID')));
    }


    if($arSections){
        CPriority::getSectionChilds(false, $arSections, $arSectionsByParentSectionID, $arItemsBySectionID, $aMenuLinksExt, '27');
    }
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>