<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Test");
\Bitrix\Main\Loader::includeModule("rdn");
?>
<p>Выберите любимого персонажа:</p>
<p><input list="character" class="form-control required js-company-input">
    <datalist id="character">
        <option value="Чебурашка"></option>
        <option value="Крокодил Гена"></option>
        <option value="Шапокляк"></option>
    </datalist></p>

<p>
<div class="input">
    <input type="text" list="comp-pay-list" class="form-control required js-company-input js-inn-mask" required="" name="COMPANY_PAY_INN" value="" aria-required="true">
    <datalist id="comp-pay-list">
        <option value="7722134736" label="ООО &quot;Фирма &quot;ГАММА-ГРУПП&quot;" data-id="7751" data-ext_id="8359" data-kpp="772301001" data-addr_is_eq="" data-fiz_adr="109341,Москва,ул.Люблинская,д.157" data-fiz_gorod="" data-fiz_index="" data-ruk_doc="Устава" data-ruk_dolg="Генеарльного директора" data-ruk_name="Депутатовой Е. П." data-cp_name="" data-cp_phone="" data-cp_email="" data-tw_id_company="8359">
        </option><option value="7701133058" label="ЗАО &quot;УПТК-1&quot;" data-id="7752" data-ext_id="6731" data-kpp="770101001" data-addr_is_eq="" data-fiz_adr="101000, Москва, ул.Маросейка, д.7/8, стр.1" data-fiz_gorod="" data-fiz_index="" data-ruk_doc="Устава" data-ruk_dolg="Ген.директора" data-ruk_name="Депутатовой Е. П." data-cp_name="" data-cp_phone="" data-cp_email="" data-tw_id_company="6731">
        </option><option value="7731415966" label="Общество с ограниченной ответственностью «Хоум тойз» " data-id="7781" data-ext_id="13622" data-kpp="773101001" data-addr_is_eq="Array" data-fiz_adr="" data-fiz_gorod="" data-fiz_index="" data-ruk_doc="Устава" data-ruk_dolg="Генеральный директор" data-ruk_name="Алексей" data-cp_name="" data-cp_phone="" data-cp_email="" data-tw_id_company="13622">
        </option><option value="5017115560" label="ООО Л-Клининг" data-id="9595" data-ext_id="29457" data-kpp="501701001" data-addr_is_eq="Array" data-fiz_adr="" data-fiz_gorod="" data-fiz_index="" data-ruk_doc="Устава" data-ruk_dolg="Генерального директора" data-ruk_name="Казаковой Елены Леонидовны" data-cp_name="" data-cp_phone="" data-cp_email="" data-tw_id_company="29457">
        </option><option value="7743755484" label="ООО &amp;quot;ОЗОН-МОНТАЖ&amp;quot;" data-id="9596" data-ext_id="29640" data-kpp="774301001" data-addr_is_eq="Array" data-fiz_adr="" data-fiz_gorod="" data-fiz_index="" data-ruk_doc="" data-ruk_dolg="" data-ruk_name="Сапова Александра Анатольевича" data-cp_name="" data-cp_phone="" data-cp_email="" data-tw_id_company="29640">
        </option>
    </datalist>
</div>
</p>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
